(channel-news
  (version 0)
  (entry (commit "653d07f98d6ab6e850b592075af899cfd4e30a24")
         (title (en "New @code{helix-without-grammar} package"))
         (body  (en "This package is the same as the default @code{helix} package, but
without the @code{languages.toml} patches. This means it builds
with no grammars, and has the default grammars from upstream
@code{languages.toml}, so you can @code{hx --grammar fetch} followed
by @code{hx --grammar build} and it will update and build grammars to
the latest version, this includes the grammars not supported by the
@code{helix} package e.g. hyprlang, move, twig, qmljs, janet-simple.
@code{guix shell rust gcc-toolchain --development rust} can run
@code{hx --grammar build}.")))
  (entry (commit "6a481b4998b14ae1e3539e3a9b41254a71a02340")
         (title (en "New @code{typst} package"))
         (body  (en "Added the typst package, the new markup-based typesetting system that
is designed to be as powerful as LaTeX while being much easier to
learn and use.")))
  (entry (commit "e2b469704d90384e1aacbfbb839a47793e2049d0")
         (title (en "Removed @code{helix-without-grammar} package"))
         (body  (en "The old @code{helix-without-grammar} package is now the default
package. I'll make a home-helix-service-type in the future for
managing helix configuration, plugins and eventually all the runtime
grammars.
For now you can manage grammars with @code{hx --grammar fetch} followed by @code{guix shell gcc-toolchain -- hx --grammar build}.")))
  (entry (commit "ea3a590e0fb75f7be7223b8da88e95cd6dc20455")
         (title (en "Added NVENC packages"))
         (body  (en "Added @code{ffmpeg-nvenc} and @code{obs/nvenc} packages
to (saayix packages video).

You should now be able to use hardware encode on OBS with an
NVIDIA GPU for recording/streaming. Really excited for this
as I was desperate for the new WHIP feature that came with
@code{obs@30}.

Remember these packages only add NVENC from ffnvcodec for
@code{h264_nvenc} and @code{hevc_nvenc}, you still need to
graft @code{mesa} with @code{nvda} when needed.

Feel free to open issues/PRs asking for new flags on ffmpeg
if needed.")))
  (entry (commit "6abc3affbe5e13c11535ced7abfa3fb6e104e6bf")
         (title (en "Added zen-browser-bin"))
         (body  (en "Added @code{zen-browser-bin} package.

Renamed binary packages for consistency:

@code{croc}  -> @code{croc-bin}
@code{jdtls} -> @code{jdtls-bin}
@code{maven} -> @code{maven-bin}")))
  (entry (commit "15230ee5f35f7c4cd48f808ee81085beec8e5456")
         (title (en "Added hyprland packages"))
         (body  (en "All hyprland packages have been moved from @code{(saayix packages wm)} to
@code{(saayix packages hyprland)}.

The entire module is now from https://codeberg.org/squishypinkelephant/guix-hyprland

I'm yanked it here for personal use because their channel has no authentication.")))
  (entry (commit "8545c1f23ac4d12d0195cd723f1220005d00c575")
         (title (en "Added manim"))
         (body  (en "Added @code{manim} from manim-community (not manimgl from 3b1b).

This also came with me having to update @code{python-numpy} and @code{python-scipy} to latest, along
with some other libraries in @code{(saayix packages python-xyz)}.

Likely will get upstreamed next year.")))
  (entry (commit "e268f78597c80ea1aedd969d463d9ecc4b057217")
         (title (en "Renamed zen-browser-bin package"))
         (body  (en "The package @code{zen-browser-bin} has been
renamed to @code{zen-browser-bin/specific}.

Added a new package for @code{zen-browser-bin/generic} which is the generic and
more compatible version of @code{zen-browser}.")))

  (entry (commit "9469787df56db60b4a668646010649e9fcfec5ad")
         (title (en "Package changes"))
         (body  (en "
- Remove @code{hypr*} packages (upstreamed).

- Remove @code{zathura-*} packages (mainly for synctex support).

- Add @code{font-nerd-opendyslexic}.

- Add @code{hyprpaper}.

- Add @code{hypridle}.

- Add @code{hyprlock}.")))

  (entry (commit "025896e5e5b3a64a54cf183cdb67c6db17c5c7e6")
         (title (en "Zen Browser breaking changes"))
         (body  (en "As of @code{1.0.2-b.4}, @code{zen-browser} won't ship
optimized builds anymore, therefore they have been removed from here and there
is now only the @code{zen-browser-bin} package once again.

Please replace your installations and declarations with the new
@code{zen-browser-bin} package.

More information upstream at @url{https://github.com/zen-browser/desktop/wiki/Why-have-optimized-builds-been-removed%3F}.")))
  (entry (commit "868989df5e66197b352bb74db8aed3d3626c8188")
         (title (en "Added the ghostty package"))
         (body  (en "Added the new ghostty terminal emulator based on the new zig update on guix.")))
  (entry (commit "0f570673c40600aa68b67a4a54ab1661eb340202")
         (title (en "Removed onnxruntime"))
         (body  (en "I do not wish to maintain onnxruntime anymore because I
don't use it, feel free to yank the definition from a previous commit if you
were using it from here and use it however you want."))))
