# Saayix

My **GNU/Guix** channel for developing and sharing my **services** and **packages**.

My **dotfiles** and personal configurations can be found in [Misako](https://codeberg.org/look/misako).

## Packages
- **Browser extensions**
    - darkreader
    - keepassxc-browser
- **Text editors**
    - helix
- **File managers**
    - yazi
- **Binaries**
    - croc
    - jdtls
    - maven
    - zen-browser-bin/generic
    - zen-browser-bin/specific
- **Fonts**
    - font-awesome
    - font-meslo-lg
    - font-meslo-lg-dz
    - font-nerd-fira-code
    - font-nerd-fira-mono
    - font-nerd-noto
    - font-nerd-opendyslexic
    - font-nerd-space-mono
    - font-nerd-symbols
- **Telegram**
    - telegram-desktop/xwayland
- **PDF**
    - sioyek/xwayland
    - zaread
- **LaTeX**
    - latexindent
    - texlab
- **Typst**
    - typst
- **Minecraft**
    - prismlauncher
    - glfw-wayland-minecraft
    - In order to play minecraft with NVIDIA and native wayland:
        1. Build `glfw-wayland-minecraft` with nvda graft from [nonguix](https://gitlab.com/nonguix/nonguix):
        ```bash
        guix build glfw-wayland-minecraft --with-graft=mesa=nvda
        ```
        
        2. Get the output path, lets say `/gnu/store/...-glfw-wayland-minecraft-3.4.0`, and append `/lib/libglfw.so.3.5` to it.
        
        3. Add it to Prism Launcher in Settings > Minecraft > Tweaks > [x] Use system installation of GLFW > GLFW library path > `/gnu/store/...-glfw-wayland-minecraft-3.4.0/lib/libglfw.so.3.5`
        4. Enjoy. If you get some OpenGL context error, disable mangohud from Tweaks.
- **LSP**
    - guile-lsp-server
    - taplo-cli
- **WM**
    - eww
    - eww/x11
    - eww/wayland
- **Toys**
    - wshowkeys
- **Productivity**
    - clipboard-jh
    - dooit
- **Math**
    - kalker
    - manim
    - manimgl
    - manim-slides

## Services
### Home
- Emacs (daemon)
- Gammastep
- Mako
- Sway
- wlsunset

### System
- rfkill

## Usage

To update **Saayix**, along with **Guix**, via `guix pull`, you can add one of the following definitions to your `~/.config/guix/channels.scm`. Alternatively, for a more **declarative approach**, consider integrating it directly into your configuration using [Guix Home Services](https://guix.gnu.org/manual/en/html_node/Guix-Home-Services.html).

### Stable branch: main
The **stable branch** named `main` offers a more reliable and stable experience. It is recommended for users who prioritize a **dependable environment** over the latest experimental changes.

```scheme
(channel
  (name 'saayix)
  (branch "main")
  (url "https://codeberg.org/look/saayix")
  (introduction
    (make-channel-introduction
      "12540f593092e9a177eb8a974a57bb4892327752"
      (openpgp-fingerprint
        "3FFA 7335 973E 0A49 47FC  0A8C 38D5 96BE 07D3 34AB"))))
```

### Experimental branch: entropy
The **experimental branch** named `entropy` may contain **cutting-edge** features and updates that are still in **testing**. It provides users with the latest developments but may be **less stable**.

```scheme
(channel
  (name 'saayix)
  (branch "entropy")
  (url "https://codeberg.org/look/saayix")
  (introduction
    (make-channel-introduction
      "12540f593092e9a177eb8a974a57bb4892327752"
      (openpgp-fingerprint
        "3FFA 7335 973E 0A49 47FC  0A8C 38D5 96BE 07D3 34AB"))))
```

## Recommended channels

These **channels** were invaluable sources of **examples** and **inspiration** during my initial foray into **GNU/Guix**, aiding me not only in grasping the fundamentals but also in setting up my own channel. I **recommend** exploring them for a richer learning experience, and for discovering a diverse array of **packages** and **modules**.

- [Rosenthal](https://codeberg.org/hako/Rosenthal) (Hyprland, NVIDIA)
- [Radix](https://codeberg.org/anemofilia/radix) (utils, doas, guile)
