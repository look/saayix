(define-module (saayix services home wlsunset)
               #:use-module (gnu)
               #:use-module (gnu home services shepherd)
               #:use-module (gnu packages xdisorg))

(define (wlsunset-shepherd-service config)
  (list (shepherd-service
          (provision '(wlsunset))
          (documentation "Run wlsunset.")
          (start #~(make-forkexec-constructor
                     (list (string-append #$wlsunset "/bin/wlsunset")
                           "-t 4000"
                           "-T 6500"
                           "-S 6:30"
                           "-s 18:00")))
          (stop #~(make-kill-destructor)))))

(define-public home-wlsunset-service-type
  (service-type (name 'wlsunset)
                (default-value #f)
                (extensions (list (service-extension
                                    home-shepherd-service-type
                                    wlsunset-shepherd-service)))
                (description "wlsunset daemon.")))
