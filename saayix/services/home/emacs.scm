(define-module (saayix services home emacs)
               #:use-module (gnu)
               #:use-module (gnu home services shepherd)
               #:use-module (gnu packages emacs))

(define (emacs-shepherd-service config)
  (list (shepherd-service
          (provision '(emacs))
          (documentation "Run the emacs daemon.")
          (start #~(make-forkexec-constructor
                     (list (string-append #$emacs "/bin/emacs")
                           "--fg-daemon"
                           "--chdir" (getenv "HOME"))))
          ;; #:log-file "/home/look/.local/var/log/emacs.log"

          (stop #~(make-kill-destructor))
          (one-shot? #f))))

(define-public home-emacs-service-type
  (service-type
    (name 'emacs)
    (default-value #f)
    (extensions (list (service-extension
                        home-shepherd-service-type
                        emacs-shepherd-service)))
    (description "Emacs daemon.")))
