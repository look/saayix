  (define-module (saayix services home wm)
  #:use-module (gnu services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:export (home-wm-service-type
            home-wm-configuration))

(define-maybe/no-serialization file-like)
(define (boolean-or-string-or-file-like? x)
  (or (string? x)
      (file-like? x)
      (boolean? x)))

(define-configuration/no-serialization home-wm-configuration
  (wm
    maybe-file-like
    "The window manager binary path to be used.")
  (pid-file
    (boolean-or-string-or-file-like #f)
    "The PID file location for the window manager process.")
  (log-file
    (boolean-or-string-or-file-like #f)
    "The log file location for the window manager to write its STDOUT to.")
  (extra-options
    (list-of-strings '())
    "Extra options to be passed to the wm command."))

(define home-wm-shepherd-service
  (match-record-lambda <home-wm-configuration>
    (wm pid-file log-file extra-options)
    (list
      (shepherd-service
        (provision '(wm))
        (documentation "Run the window manager through a shepherd service.")
        (start #~(make-forkexec-constructor
                   (list #$wm
                         #$@extra-options)
                   #:log-file #$log-file
                   #:pid-file #$pid-file))
        (stop #~(make-kill-destructor))
        (one-shot? #t)
        (respawn? #t)))))

(define home-wm-service-type
  (service-type
    (name 'wm)
    (default-value (home-wm-configuration))
    (extensions
      (list
        (service-extension home-shepherd-service-type
                           home-wm-shepherd-service)))
    (description "Run a window manager with shepherd.")))
