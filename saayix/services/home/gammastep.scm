(define-module (saayix services home gammastep)
               #:use-module (gnu)
               #:use-module (gnu home services shepherd)
               #:use-module (gnu packages xdisorg))

(define (gammastep-shepherd-service config)
  (list (shepherd-service
          (provision '(gammastep))
          (requirement '(hyprland dbus))
          (documentation "Run gammastep.")
          (start #~(make-forkexec-constructor
                     (list (string-append #$gammastep "/bin/gammastep"))
                     #:log-file (string-append (getenv "XDG_LOG_HOME") "gammastep.log")
                     #:environment-variables (cons "WAYLAND_DISPLAY=wayland-1" (environ))))
          (stop #~(make-kill-destructor)))))

(define-public home-gammastep-service-type
  (service-type
    (name 'gammastep)
    (default-value #f)
    (extensions (list (service-extension
                        home-shepherd-service-type
                        gammastep-shepherd-service)))
    (description "Gammastep daemon.")))
