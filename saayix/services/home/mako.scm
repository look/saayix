(define-module (saayix services home mako)
               #:use-module (gnu)
               #:use-module (gnu home services shepherd)
               #:use-module (gnu packages wm))

(define (mako-shepherd-service config)
  (list (shepherd-service
          (provision '(mako))
          (requirement '(hyprland dbus))
          (documentation "Run Mako.")
          (start #~(make-forkexec-constructor
                     (list (string-append #$mako "/bin/mako"))
                     #:log-file (string-append (getenv "XDG_LOG_HOME") "mako.log")
                     #:environment-variables (cons "WAYLAND_DISPLAY=wayland-1" (environ))))
          (stop #~(make-kill-destructor)))))

(define-public home-mako-service-type
  (service-type
    (name 'mako)
    (default-value #f)
    (extensions (list (service-extension
                        home-shepherd-service-type
                        mako-shepherd-service)))
    (description "Mako daemon.")))
