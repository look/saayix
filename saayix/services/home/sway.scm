(define-module (saayix services home sway)
               #:use-module (gnu)
               #:use-module (gnu home services shepherd)
               #:use-module (gnu packages wm))

(define (sway-shepherd-service config)
  (list (shepherd-service
          (provision '(sway))
          (documentation "Run the Sway window manager.")
          (start #~(make-forkexec-constructor (list (string-append #$sway "/bin/sway"))))
          (stop #~(make-kill-destructor)))))

(define-public home-sway-service-type
  (service-type
    (name 'sway)
    (default-value #f)
    (extensions (list (service-extension
                        home-shepherd-service-type
                        sway-shepherd-service)))
    (description "Sway wayland compositor.")))
