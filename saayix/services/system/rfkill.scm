(define-module (saayix services system rfkill)
               #:use-module (gnu)
               #:use-module (gnu services shepherd)
               #:use-module (gnu packages linux))

(define (rfkill-shepherd-service config)
  (list (shepherd-service
          (provision '(rfkill))
          (documentation "Unblock all rfkill interfaces.")
          (one-shot? #t)
          (requirement '(networking))
          (start #~(make-forkexec-constructor
                     (list (string-append #$rfkill "/sbin/rfkill")
                           "unblock"
                           "all")))
          (stop #~(make-kill-destructor)))))


(define-public rfkill-service-type
  (service-type
    (name 'rfkill)
    (default-value #f)
    (extensions
      (list
        (service-extension shepherd-root-service-type
                           rfkill-shepherd-service)))
    (description "RFkill unblock all interfaces")))
