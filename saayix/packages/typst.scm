;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages typst)
  #:use-module (guix packages)
  #:export (typst))

(define typst
  (package
    (inherit (@@ (saayix packages rust typst) typst))))
