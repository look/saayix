(define-module (saayix packages lsp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:export (taplo-cli
            guile-lsp-server))

(define guile-json-rpc
  (let ((version "0.4.5")
        (revision "0")
        (commit "3c4f55aa55bda8616f84f647f47e79b9fbd45889"))
    (package
      (name "guile-json-rpc")
      (version (git-version version revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://codeberg.org/rgherdt/scheme-json-rpc")
                 (commit commit)))
          (sha256
            (base32 "0z5azrxm4qd8dccz7hx2gnnw4wgig1n8x1jqv3bi9zack8q1wyfk"))))
      (build-system gnu-build-system)
      (arguments
        (list
          #:phases
          #~(modify-phases %standard-phases
              (add-after 'unpack 'move-to-guile-directory
                (lambda _
                  (chdir "./guile"))))))
      (inputs
        (list guile-srfi-145
              guile-srfi-180))
      (native-inputs
        (list autoconf
              automake
              pkg-config
              texinfo
              guile-3.0))
      (synopsis "An implementation of the JSON-RPC for Guile.")
      (description "@code{scheme-json-rpc} allows calling procedures on remote servers by exchanging JSON objects.  It implements the @url{jsonrpc specifications, https://www.jsonrpc.org/specification}. The low-level API strives to be R7RS compliant, relying on some SRFI's when needed.")
      (home-page "https://codeberg.org/rgherdt/scheme-json-rpc")
      (license license:expat))))

(define guile-lsp-server
  (package
    (name "guile-lsp-server")
    (version "0")
    (source
      (origin
         (method git-fetch)
         (uri (git-reference
                (url "https://codeberg.org/rgherdt/scheme-lsp-server")
                (commit "81d2b3613ea48f345e868d191bbf1b92686cbe60")))
         (sha256
           (base32 "0z8h4fbhphaw3wr0k75pbnpvsxa2n1kw3vnrcc8qgbaf5pqcl533"))))
    (build-system gnu-build-system)
    (arguments
      (list
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'unpack 'move-to-guile-directory
              (lambda _
                (chdir "./guile"))))))
    (native-inputs
      (list autoconf
            automake
            pkg-config
            texinfo
            guile-3.0))
    (propagated-inputs
      (list guile-3.0
            guile-json-rpc
            guile-srfi-145
            guile-srfi-180
            guile-irregex))
    (synopsis "LSP (Language Server Protocol) server for Guile.")
    (description "Provides a library (lsp-server) and an executable @code{guile-lsp-server} that can be used by LSP clients in order to provide IDE functionality for Guile Scheme.")
    (home-page "https://codeberg.org/rgherdt/scheme-lsp-server")
    (license license:expat)))

(define taplo-cli
  (package
    (inherit (@@ (saayix packages rust taplo-cli) taplo-cli))))
