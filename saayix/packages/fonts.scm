(define-module (saayix packages fonts)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system font)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:export (font-nerd-symbols
            font-nerd-noto
            font-nerd-space-mono
            font-nerd-fira-mono
            font-nerd-fira-code
            font-nerd-opendyslexic
            font-awesome-p
            font-meslo-lg
            font-meslo-lg-dz))

(define %font-nerd-version "3.3.0")

(define (make-nerd-font-package font name hash)
  (package
    (name name)
    (version %font-nerd-version)
    (source
      (origin
        (method url-fetch/zipbomb)
        (uri (string-append "https://github.com/ryanoasis/nerd-fonts"
                            "/releases/download/" "v" version "/"
                            font ".zip"))
        (sha256
          (base32 hash))))
    (build-system font-build-system)
    (home-page "https://github.com/ryanoasis/nerd-fonts")
    (synopsis "Iconic font aggregator")
    (description "Nerd Fonts is a project that patches developer targeted fonts
with a high number of glyphs (icons). Specifically to add a high number of extra
glyphs from popular 'iconic fonts' such as Font Awesome, Devicons, Octicons,
and others.")
    (license license:silofl1.1)))

(define font-nerd-symbols
  (make-nerd-font-package
    "NerdFontsSymbolsOnly"
    "font-nerd-symbols"
    "0h53ldrkydxaps4kv087k71xgmb40b1s2nv2kvxc4bvs3qy60y10"))

(define font-nerd-noto
  (make-nerd-font-package
    "Noto"
    "font-nerd-noto"
    "14mna22dam0kx0axi53rjvkr97wlv161a9w2ap771890cjxnw70k"))

(define font-nerd-space-mono
  (make-nerd-font-package
    "SpaceMono"
    "font-nerd-space-mono"
    "09dw1rvjc0g7zkdmzh0lg60ivl9az7yv4b1y3bq5bad4nclnvb8v"))

(define font-nerd-fira-mono
  (make-nerd-font-package
    "FiraMono"
    "font-nerd-fira-mono"
    "0jiacwyylzwzw7gp06sqqh8dr6jy5ljmjd69ad9300kk7nh6w109"))

(define font-nerd-fira-code
  (make-nerd-font-package
    "FiraCode"
    "font-nerd-fira-code"
    "15xks0619yip4gkgwandn529fmk1v5g1s5irlf34410dhxpqx5w9"))

(define font-nerd-opendyslexic
  (make-nerd-font-package
    "OpenDyslexic"
    "font-nerd-opendyslexic"
    "06v4iizzjj6ljvj68vv3rb4dl51dm8gsccx524zz0bp73f9f57gb"))

(define font-awesome-p
  (package
    (name "font-awesome")
    (version "6.4.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/FortAwesome/Font-Awesome")
               (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0mmjq8z1sldg8s7cspskbljc88qhyv8djwbxv89lw1ajdydmfk29"))))
    (build-system font-build-system)
    (home-page "https://fontawesome.com/")
    (synopsis "Font that contains a rich iconset")
    (description "Font Awesome is a full suite of pictographic icons for easy
scalable vector graphics.")
    (license license:silofl1.1)))

(define font-meslo-lg
  (package
    (name "font-meslo-lg")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch/zipbomb)
        (uri (string-append "https://github.com/andreberg/Meslo-Font"
                            "/raw/master/dist/" "v" version "Meslo%20LG%20"
                            "v" version ".zip"))
        (file-name (string-append name "-" version))
        (sha256
          (base32 "1l08mxlzaz3i5bamnfr49s2k4k23vdm64b8nz2ha33ysimkbgg6h"))))
    (build-system font-build-system)
    (home-page "https://github.com/andreberg/Meslo-Font")
    (synopsis "Font for dyslexics and high readability")
    (description "Meslo LG is a customized version of Apple’s Menlo-Regular font
(which is a customized Bitstream Vera Sans Mono).")
    (license license:silofl1.1)))

(define font-meslo-lg-dz
  (package
    (inherit font-meslo-lg)
    (name "font-meslo-lg-dz")
    (version (package-version font-meslo-lg))
    (source
      (origin
        (method url-fetch/zipbomb)
        (uri (string-append "https://github.com/andreberg/Meslo-Font"
                            "/raw/master/dist/" "v" version "Meslo%20LG%20DZ%20"
                            "v" version ".zip"))
        (file-name (string-append name "-" version))
        (sha256
          (base32 "0lnbkrvcpgz9chnvix79j6fiz36wj6n46brb7b1746182rl1l875"))))))
