;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages tex)
  #:use-module (guix packages)
  #:export (texlab))

(define texlab
  (package
    (inherit (@@ (saayix packages rust texlab) texlab))))
