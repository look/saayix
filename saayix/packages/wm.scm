;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages wm)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages wm)
  #:use-module (guix build-system copy)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:export (eww
            eww/x11
            eww/wayland
            waybar-sans-elogind
            cursor-mcmojave))

(define cursor-mcmojave
  (package
    (name "cursor-mcmojave")
    (version "0.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/OtaK/McMojave-hyprcursor")
               (commit "7ed49d93f7c56df81d085fa8f70c4129956884b2")))
        (file-name (git-file-name name version))
        (sha256
         (base32 "0f863vzaqbaq8119zm1wr6syv283d3dv0hyiiw1pdlj28bq3q2pr"))))
    (build-system copy-build-system)
    (arguments
      (list #:install-plan
            #~'(("dist" "share/icons/McMojave"))))
    (home-page "https://github.com/OtaK/McMojave-hyprcursor")
    (synopsis "Pure SVG Hyprcursor port of the amazing McMojave XCursor theme")
    (description "Pure SVG Hyprcursor port of the amazing McMojave XCursor theme.")
    (license license:gpl3)))

(define eww
  (package
    (inherit (@@ (saayix packages rust eww) eww))))

(define eww/x11
  (package
    (inherit (@@ (saayix packages rust eww) eww/x11))))

(define eww/wayland
  (package
    (inherit (@@ (saayix packages rust eww) eww/wayland))))

(define waybar-sans-elogind
  ((package-input-rewriting
     `((,wireplumber . ,wireplumber-minimal)))
   waybar))
