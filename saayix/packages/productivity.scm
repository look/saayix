(define-module (saayix packages productivity)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages check)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages base)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (saayix packages python-xyz)
  #:export (clipboard-jh))

(define-public dooit
  (package
    (name "dooit")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "dooit" version))
        (sha256
          (base32 "0kxgbkpg792756n20k32l9id0rqa6q2g7szqr0a36dzsnjy87g9z"))))
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f))
    (native-inputs (list python-dateutil
                         python-linkify-it-py
                         python-markdown-it-py-for-dooit
                         python-mdit-py-plugins
                         python-poetry-core
                         python-pygments-for-dooit
                         python-pyperclip
                         python-pytz
                         python-pyyaml
                         python-rich-for-dooit
                         python-textual-for-dooit
                         python-typing-extensions
                         python-tzlocal-for-dooit
                         python-appdirs))
    (home-page "https://github.com/kraanzu/dooit")
    (synopsis "A TUI todo manager")
    (description "This package provides a TUI todo manager")
    (license license:expat)))

(define clipboard-jh
  (package
    (name "clipboard-jh")
    (version "0.9.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/Slackadays/clipboard")
                     (commit version)))
              (sha256
                (base32 "0sjk1kayw5jg0gsphlmvfxf2ri40lk2xq580f9ich2jq8k4yv0l8"))))
    (arguments
      (list #:tests? #f ;; There are no tests
            #:configure-flags
            #~(list "-Wno-dev"
                    (string-append "-DINSTALL_PREFIX=" #$output))
            #:phases
            #~(modify-phases %standard-phases
                (add-after 'install 'patch-elf
                  (lambda _
                    (let ((rpath (string-join
                                   (list
                                     ;; For some reason bundling both wayland and X11 libraries cause weird bugs on wayland
                                     ;; I'm not sure if it works on X11 because I don't use it, need some testing
                                     ; (string-append #$output "/lib")
                                     (string-append (ungexp (this-package-input "gcc") "lib") "/lib")
                                     (string-append #$(this-package-input "libffi") "/lib")
                                     (string-append #$(this-package-input "alsa-lib") "/lib")
                                     (string-append #$(this-package-input "glibc") "/lib"))
                                   ":")))
                             (invoke "patchelf"
                               (string-append #$output "/bin/cb")
                               "--set-rpath"
                               rpath)))))))
    (build-system cmake-build-system)
    (native-inputs (list pkg-config patchelf))
    (inputs (list libffi
                  wayland-protocols
                  wayland
                  libx11
                  alsa-lib
                  `(,gcc "lib")
                  glibc))
    (home-page "https://github.com/Slackadays/clipboard")
    (synopsis "Your new, ridonkuliciously smart clipboard manager")
    (description "The Clipboard Project is a fast and lightweight, feature packed, and user friendly tool that lets you do more on the computer in style. Seriously.")
    (license license:gpl3)))

clipboard-jh
