(define-module (saayix packages sddm)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system trivial)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:export (sugar-candy-sddm-theme))

(define sugar-candy-sddm-theme
  (package
    (name "sugar-candy-sddm-theme")
    (version "1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Syndrizzle/hotfiles")
                    (commit "xfce")))
              (sha256
               (base32 "1i88qf0z74a9jqspj6p55hnwijk0hqagb1pm8f260ikw971zl6j1"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (input (assoc-ref %build-inputs "source"))
                (sddm-themes (string-append out "/share/sddm/themes")))
           (mkdir-p sddm-themes)
           (copy-recursively (string-append input "/sddm-sugar-candy")
                             (string-append sddm-themes "/sugar-candy"))))))
    (home-page "https://github.com/Syndrizzle/hotfiles")
    (synopsis "A collection of personal configuration files for various rices I have made.")
    (description "A collection of personal configuration files for various rices I have made.")
    (license license:gpl3+)))
