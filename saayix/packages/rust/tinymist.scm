;; TODO: Manual hash fix: rust-sync-lsp
;; TODO: Manual hash fix: rust-tests
;; TODO: Manual hash fix: rust-tinymist

(define-module (saayix packages rust tinymist)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (gnu packages crates-tls)
  #:use-module (gnu packages crates-web)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-crypto)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-io))

(define (make-additional-source name git-url revision hash)
  (origin
    (method git-fetch)
    (uri (git-reference
          (url git-url)
          (commit revision)))
    (file-name (git-file-name name revision))
    (sha256
      (base32 hash))
    (snippet
      #~(begin (use-modules (guix build utils)
                    (ice-9 match)
                    (srfi srfi-1))
          (substitute* "Cargo.toml"
            (("\\[workspace\\][\\s\\S]+?^\\[")
             "[")
            (("\\[workspace.package\\]")
             "[package]")
            (("\\[workspace.dependencies\\]")
             "[dependencies]"))))
          ))

(define additional-typst
  (make-additional-source "typst"
                       "https://github.com/Myriad-Dreamin/typst.git"
                       "tinymist-v0.11.1-2"
                       "1k5mipd3ini6hhra3gvpikmfgd34q9smblc8q3ijz7lfhslzh1vm"))

(define additional-typstfmt
  (make-additional-source "typstfmt"
                       "https://github.com/astrale-sharp/typstfmt"
                       "0.2.7"
                       "04my3xl6vfny7nlygch7xaqv1i85diyxrnfyh6jlzyfr5162q5ic"))

(define rust-addr2line-0.22
  (package
    (name "rust-addr2line")
    (version "0.22.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "addr2line" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0y66f1sa27i9kvmlh76ynk60rxfrmkba9ja8x527h32wdb206ibf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gimli" ,rust-gimli-0.29))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ahash-0.7
  (package
    (name "rust-ahash")
    (version "0.7.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ahash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1y9014qsy6gs9xld4ch7a6xi9bpki8vaciawxq4p75d8qvh7f549"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-getrandom" ,rust-getrandom-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-version-check" ,rust-version-check-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ahash-0.8
  (package
    (name "rust-ahash")
    (version "0.8.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ahash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04chdfkls5xmhp1d48gnjsmglbqibizs3bpbj6rsj604m10si7g8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-version-check" ,rust-version-check-0.9)
         ("rust-zerocopy" ,rust-zerocopy-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-aho-corasick-1
  (package
    (name "rust-aho-corasick")
    (version "1.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aho-corasick" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05mrpkvdgp5d20y2p989f187ry9diliijgwrs254fs9s1m1x6q4f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-allocator-api2-0.2
  (package
    (name "rust-allocator-api2")
    (version "0.2.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "allocator-api2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kr6lfnxvnj164j1x38g97qjlhb7akppqzvgfs0697140ixbav2w"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstream-0.6
  (package
    (name "rust-anstream")
    (version "0.6.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstream" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nx1vnfs2lil1sl14l49i6jvp6zpjczn85wxx4xw1ycafvx7b321"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anstyle" ,rust-anstyle-1)
         ("rust-anstyle-parse" ,rust-anstyle-parse-0.2)
         ("rust-anstyle-query" ,rust-anstyle-query-1)
         ("rust-anstyle-wincon" ,rust-anstyle-wincon-3)
         ("rust-colorchoice" ,rust-colorchoice-1)
         ("rust-is-terminal-polyfill"
          ,rust-is-terminal-polyfill-1)
         ("rust-utf8parse" ,rust-utf8parse-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstyle-1
  (package
    (name "rust-anstyle")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06qxmrba0xbhv07jpdvrdrhw1hjlb9icj88bqvlnissz9bqgr383"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstyle-parse-0.2
  (package
    (name "rust-anstyle-parse")
    (version "0.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1m121pa4plpcb4g7xali2kv9njmgb3713q3fxf60b4jd0fli2fn0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-utf8parse" ,rust-utf8parse-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstyle-query-1
  (package
    (name "rust-anstyle-query")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-query" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14a3h3k541jmqm5s8hbdw8l0dcgkrryqwxgicm8x6623fvxnw65d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstyle-wincon-3
  (package
    (name "rust-anstyle-wincon")
    (version "3.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-wincon" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06gv2vbj4hvwb8fxqjmvabp5kx2w01cjgh86pd98y1mpzr4q98v1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anstyle" ,rust-anstyle-1)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-append-only-vec-0.1
  (package
    (name "rust-append-only-vec")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "append-only-vec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18bbvdpkq4qccd47k3wz5b62j2w70xhkxp7lqdnp70xf6i8ivkx6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-approx-0.5
  (package
    (name "rust-approx")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "approx" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ilpv3dgd58rasslss0labarq7jawxmivk17wsh8wmkdm3q15cfa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-archery-1
  (package
    (name "rust-archery")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "archery" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0qc53wk1ib528izaclkcm5ys7s9g0kbgn52fci7rbqg9r4fcsrw9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-static-assertions"
          ,rust-static-assertions-1)
         ("rust-triomphe" ,rust-triomphe-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-arraydeque-0.5
  (package
    (name "rust-arraydeque")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "arraydeque" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dn2xdfg3rkiqsh8a6achnmvf5nf11xk33xgjzpksliab4yjx43x"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-arrayref-0.3
  (package
    (name "rust-arrayref")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "arrayref" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ia5ndyxqkzdymqr4ls53jdmajf09adjimg5kvw65kkprg930jbb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-trait-0.1
  (package
    (name "rust-async-trait")
    (version "0.1.81")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-trait" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01w10ad3984czxrmc8ckdrabhmsv80aynfxibjnqwz1dr3f2h33f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-autocfg-1
  (package
    (name "rust-autocfg")
    (version "1.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "autocfg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c3njkfzpil03k92q0mij5y1pkhhfr4j3bf0h53bgl2vs85lsjqc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-az-1
  (package
    (name "rust-az")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "az" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ww9k1w3al7x5qmb7f13v3s9c2pg1pdxbs8xshqy6zyrchj4qzkv"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-backtrace-0.3
  (package
    (name "rust-backtrace")
    (version "0.3.73")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "backtrace" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02iffg2pkg5nc36pgml8il7f77s138hhjw9f9l56v5zqlilk5hjw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-addr2line" ,rust-addr2line-0.22)
         ("rust-cc" ,rust-cc-1)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.7)
         ("rust-object" ,rust-object-0.36)
         ("rust-rustc-demangle" ,rust-rustc-demangle-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-base64-serde-0.7
  (package
    (name "rust-base64-serde")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "base64-serde" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1iz3rcb544z4ndfvml94d4byzfygkjrz235gkajbx9bnvvsqsdms"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.21)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-biblatex-0.9
  (package
    (name "rust-biblatex")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "biblatex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1f5py80y8aj3yibsd9gcffvg0i1zg32f35akig6jf0hd0j2p5zi7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-numerals" ,rust-numerals-0.1)
         ("rust-paste" ,rust-paste-1)
         ("rust-strum" ,rust-strum-0.26)
         ("rust-unicode-normalization"
          ,rust-unicode-normalization-0.1)
         ("rust-unscanny" ,rust-unscanny-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bitflags-2
  (package
    (name "rust-bitflags")
    (version "2.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitflags" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pkidwzn3hnxlsl8zizh0bncgbjnw7c41cx7bby26ncbzmiznj5h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bstr-1
  (package
    (name "rust-bstr")
    (version "1.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bstr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01ipr5rncw3kf4dyc1p2g00njn1df2b0xpviwhb8830iv77wbvq5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bumpalo-3
  (package
    (name "rust-bumpalo")
    (version "3.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bumpalo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0b015qb4knwanbdlp1x48pkb4pm57b8gidbhhhxr900q2wb6fabr"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-by-address-1
  (package
    (name "rust-by-address")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "by_address" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01idmag3lcwnnqrnnyik2gmbrr34drsi97q15ihvcbbidf2kryk4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bytecheck-0.6
  (package
    (name "rust-bytecheck")
    (version "0.6.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytecheck" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hmipv4yyxgbamcbw5r65wagv9khs033v9483s9kri9sw9ycbk93"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytecheck-derive"
          ,rust-bytecheck-derive-0.6)
         ("rust-ptr-meta" ,rust-ptr-meta-0.1)
         ("rust-simdutf8" ,rust-simdutf8-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bytecheck-derive-0.6
  (package
    (name "rust-bytecheck-derive")
    (version "0.6.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytecheck_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ng6230brd0hvqpbgcx83inn74mdv3abwn95x515bndwkz90dd1x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bytemuck-1
  (package
    (name "rust-bytemuck")
    (version "1.16.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytemuck" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0pkjipj5jc2x78ahyx258yca9pawgf8z98cdndsyv5rc629gqdmj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bytes-1
  (package
    (name "rust-bytes")
    (version "1.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jf2awc1fywpk15m6pxay3wqcg65ararg9xi4b08vnszwiyy2kai"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-camino-1
  (package
    (name "rust-camino")
    (version "1.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "camino" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ff28kc3qjcrmi8k88b2j2p7mzrvbag20yqcrj9sl30n3fanpv70"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cargo-platform-0.1
  (package
    (name "rust-cargo-platform")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cargo-platform" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1z5b7ivbj508wkqdg2vb0hw4vi1k1pyhcn6h1h1b8svcb8vg1c94"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cc-1
  (package
    (name "rust-cc")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1j7zm7ycxsjaplmzfcxyl7z1y6bsziijsrw6lwzpgf86wn66zzza"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-chinese-number-0.7
  (package
    (name "rust-chinese-number")
    (version "0.7.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chinese-number" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0v5va8drix8gs2kv6pmv5yzdxhlpzrwkp3ch86kxdxj6cgpwmz29"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chinese-variant" ,rust-chinese-variant-1)
         ("rust-enum-ordinalize" ,rust-enum-ordinalize-4)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-chinese-variant-1
  (package
    (name "rust-chinese-variant")
    (version "1.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chinese-variant" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12s91vg2m9wfs9b3f0q2alj9am08y7r2prb0szg3fwjh8m8lg23m"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-chrono-0.4
  (package
    (name "rust-chrono")
    (version "0.4.38")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chrono" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "009l8vc5p8750vn02z30mblg4pv2qhkbfizhfwmzc6vpy5nr67x2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-android-tzdata" ,rust-android-tzdata-0.1)
         ("rust-iana-time-zone" ,rust-iana-time-zone-0.1)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-windows-targets"
          ,rust-windows-targets-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ciborium-0.2
  (package
    (name "rust-ciborium")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ciborium" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03hgfw4674im1pdqblcp77m7rc8x2v828si5570ga5q9dzyrzrj2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ciborium-io" ,rust-ciborium-io-0.2)
         ("rust-ciborium-ll" ,rust-ciborium-ll-0.2)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ciborium-io-0.2
  (package
    (name "rust-ciborium-io")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ciborium-io" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0my7s5g24hvp1rs1zd1cxapz94inrvqpdf1rslrvxj8618gfmbq5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ciborium-ll-0.2
  (package
    (name "rust-ciborium-ll")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ciborium-ll" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1n8g4j5rwkfs3rzfi6g1p7ngmz6m5yxsksryzf5k72ll7mjknrjp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ciborium-io" ,rust-ciborium-io-0.2)
         ("rust-half" ,rust-half-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-citationberg-0.3
  (package
    (name "rust-citationberg")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "citationberg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02rx28ar1yqzfh97n18ihd1bmgshvgyj07aq36hhbylgsygzwnfj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-quick-xml" ,rust-quick-xml-0.31)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-4
  (package
    (name "rust-clap")
    (version "4.5.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ql4kc5nclygivr0711lzid3z3g26jf1ip3qda9zxhaldn2c3b34"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap-builder" ,rust-clap-builder-4)
         ("rust-clap-derive" ,rust-clap-derive-4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-builder-4
  (package
    (name "rust-clap-builder")
    (version "4.5.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_builder" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hp9xn5dnzr0xc8mancr7b3kqm1b9s74a8waybx7nbmscwykkf3g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anstream" ,rust-anstream-0.6)
         ("rust-anstyle" ,rust-anstyle-1)
         ("rust-clap-lex" ,rust-clap-lex-0.7)
         ("rust-strsim" ,rust-strsim-0.11)
         ("rust-terminal-size" ,rust-terminal-size-0.3)
         ("rust-unicase" ,rust-unicase-2)
         ("rust-unicode-width" ,rust-unicode-width-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-complete-4
  (package
    (name "rust-clap-complete")
    (version "4.5.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_complete" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1bikzcnyl7bc79c1q1k2gvch4qba5f1f0l57v1w0pwxiqk2fjjsv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-complete-fig-4
  (package
    (name "rust-clap-complete-fig")
    (version "4.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_complete_fig" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04p6krszn3x1j4wa5snxq83ss2c3b7b5bdbgfchd676wrl1wajzv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-4)
         ("rust-clap-complete" ,rust-clap-complete-4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-derive-4
  (package
    (name "rust-clap-derive")
    (version "4.5.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11bh9ybcslr1psl06y9jlq9pr4cpmvzs9nbmsba0y1pvvb33bb1b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-heck" ,rust-heck-0.5)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-lex-0.7
  (package
    (name "rust-clap-lex")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_lex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w0fci2zp1bi2kqjnp14vdxsa0r34yjd35i845c8bmfvmc5wz0jb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-mangen-0.2
  (package
    (name "rust-clap-mangen")
    (version "0.2.22")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_mangen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pp23z585n48xm6s641qp5a4lrssdvmyamz4iljdcly8q1dxw3gm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-4)
         ("rust-roff" ,rust-roff-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-colorchoice-1
  (package
    (name "rust-colorchoice")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "colorchoice" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08h4jsrd2j5k6lp1b9v5p1f1g7cmyzm4djsvb3ydywdb4hmqashb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-comemo-0.3
  (package
    (name "rust-comemo")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "comemo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1x66czkfc1dwg2d20hryspzqnhkcdqq1i4szdbp041m8ix30amxz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-comemo-macros" ,rust-comemo-macros-0.3)
         ("rust-siphasher" ,rust-siphasher-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-comemo-0.4
  (package
    (name "rust-comemo")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "comemo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14bsiayib4lhz3jrbf1fqh2fpwsm6cii90mifym3jhvji901csfz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-comemo-macros" ,rust-comemo-macros-0.4)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-siphasher" ,rust-siphasher-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-comemo-macros-0.3
  (package
    (name "rust-comemo-macros")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "comemo-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15f0mj14prq745add56hcq4393i2fqkba6ncm4gicbfsib36mbsl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-comemo-macros-0.4
  (package
    (name "rust-comemo-macros")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "comemo-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nr8w81hkzg49s515v61shxb077iq6d6001pybxbvxdlz516x4y8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-console-0.15
  (package
    (name "rust-console")
    (version "0.15.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "console" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1sz4nl9nz8pkmapqni6py7jxzi7nzqjxzb3ya4kxvmkb0zy867qf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-encode-unicode" ,rust-encode-unicode-0.3)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-core-maths-0.1
  (package
    (name "rust-core-maths")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core_maths" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18q9fwy80lk1lccam375skmsslryik00zkhsl850pidqrh2jbc73"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libm" ,rust-libm-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cpufeatures-0.2
  (package
    (name "rust-cpufeatures")
    (version "0.2.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpufeatures" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "012m7rrak4girqlii3jnqwrr73gv1i980q4wra5yyyhvzwk5xzjk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crc32fast-1
  (package
    (name "rust-crc32fast")
    (version "1.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crc32fast" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1czp7vif73b8xslr3c9yxysmh9ws2r8824qda7j47ffs9pcnjxx9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-channel-0.5
  (package
    (name "rust-crossbeam-channel")
    (version "0.5.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-channel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wkx45r34v7g3wyi3lg2wz536lrrrab4h4hh741shfhr8rlhsj1k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-queue-0.3
  (package
    (name "rust-crossbeam-queue")
    (version "0.3.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-queue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0d8y8y3z48r9javzj67v3p2yfswd278myz1j9vzc4sp7snslc0yz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-utils-0.8
  (package
    (name "rust-crossbeam-utils")
    (version "0.8.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-utils" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "100fksq5mm1n7zj242cclkw6yf7a4a8ix3lvpfkhxvdhbda9kv12"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-darling-0.20
  (package
    (name "rust-darling")
    (version "0.20.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1299h2z88qn71mizhh05j26yr3ik0wnqmw11ijds89l8i9nbhqvg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling-core" ,rust-darling-core-0.20)
         ("rust-darling-macro" ,rust-darling-macro-0.20))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-darling-core-0.20
  (package
    (name "rust-darling-core")
    (version "0.20.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rgr9nci61ahnim93yh3xy6fkfayh7sk4447hahawah3m1hkh4wm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fnv" ,rust-fnv-1)
         ("rust-ident-case" ,rust-ident-case-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-strsim" ,rust-strsim-0.11)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-darling-macro-0.20
  (package
    (name "rust-darling-macro")
    (version "0.20.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling_macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01kq3ibbn47czijj39h3vxyw0c2ksd0jvc097smcrk7n2jjs4dnk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling-core" ,rust-darling-core-0.20)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-data-encoding-2
  (package
    (name "rust-data-encoding")
    (version "2.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "data-encoding" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qnn68n4vragxaxlkqcb1r28d3hhj43wch67lm4rpxlw89wnjmp8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-deranged-0.3
  (package
    (name "rust-deranged")
    (version "0.3.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "deranged" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1d1ibqqnr5qdrpw8rclwrf1myn3wf0dygl04idf4j2s49ah6yaxl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-powerfmt" ,rust-powerfmt-0.2)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dhat-0.3
  (package
    (name "rust-dhat")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dhat" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09xq763lpf0kdv4fzbdgxkd4sgv3p08dwrz41kg37qi88vc13kcq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-mintex" ,rust-mintex-0.1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-thousands" ,rust-thousands-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-displaydoc-0.2
  (package
    (name "rust-displaydoc")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "displaydoc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1q0alair462j21iiqwrr21iabkfnb13d6x5w95lkdg21q2xrqdlp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-downcast-rs-1
  (package
    (name "rust-downcast-rs")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "downcast-rs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1lmrq383d1yszp7mg5i7i56b17x2lnn3kb91jwsq0zykvg2jbcvm"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ecow-0.1
  (package
    (name "rust-ecow")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ecow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1s18cxys3n354w3qc9mvz80yyrgbn2ixp0h60czxyvngag89068x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ecow-0.2
  (package
    (name "rust-ecow")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ecow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jvg2jrjgczy05mbsnirqqh3rxghxbdbwkbc18cj71lq10bvpgsl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-either-1
  (package
    (name "rust-either")
    (version "1.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "either" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1w2c1mybrd7vljyxk77y9f4w9dyjrmp3yp82mk7bcm8848fazcb0"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-elsa-1
  (package
    (name "rust-elsa")
    (version "1.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "elsa" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "041yf5a6mm2kck1hbapwvp39408c4f8cprd2h90j2zgm9np733nr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-stable-deref-trait"
          ,rust-stable-deref-trait-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ena-0.14
  (package
    (name "rust-ena")
    (version "0.14.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ena" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1m9a5hqk6qn5sqnrc40b55yr97drkfdzd0jj863ksqff8gfqn91x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-encode-unicode-1
  (package
    (name "rust-encode-unicode")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "encode_unicode" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1h5j7j7byi289by63s3w4a8b3g6l5ccdrws7a67nn07vdxj77ail"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-encoding-rs-0.8
  (package
    (name "rust-encoding-rs")
    (version "0.8.34")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "encoding_rs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nagpi1rjqdpvakymwmnlxzq908ncg868lml5b70n08bm82fjpdl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-enum-ordinalize-4
  (package
    (name "rust-enum-ordinalize")
    (version "4.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "enum-ordinalize" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1max64z9giii61qcwl56rndd7pakaylkaij5zqbbbvjl9vxdr87y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-enum-ordinalize-derive"
          ,rust-enum-ordinalize-derive-4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-enum-ordinalize-derive-4
  (package
    (name "rust-enum-ordinalize-derive")
    (version "4.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "enum-ordinalize-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zy53fabazimwv5cl0366k834ybixzl84lxj9mfavbnlfn532a0d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-env-logger-0.11
  (package
    (name "rust-env-logger")
    (version "0.11.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "env_logger" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fa34dr082zfih5pw821d13kr6lcg18x6z08pa09d0aip8wmicrq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anstream" ,rust-anstream-0.6)
         ("rust-anstyle" ,rust-anstyle-1)
         ("rust-env-filter" ,rust-env-filter-0.1)
         ("rust-humantime" ,rust-humantime-2)
         ("rust-log" ,rust-log-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-errno-0.3
  (package
    (name "rust-errno")
    (version "0.3.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "errno" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fi0m0493maq1jygcf1bya9cymz2pc1mqxj26bdv7yjd37v5qk2k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fastrand-2
  (package
    (name "rust-fastrand")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fastrand" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06p5d0rxq7by260m4ym9ial0bwgi0v42lrvhl6nm2g7h0h2m3h4z"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-flate2-1
  (package
    (name "rust-flate2")
    (version "1.0.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "flate2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1bjx56n0wq5w7vsjn7b5rbmqiw0vc3mfzz1rl7i2jy0wzmy44m2z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crc32fast" ,rust-crc32fast-1)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fontconfig-parser-0.5
  (package
    (name "rust-fontconfig-parser")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fontconfig-parser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kfqacd2m7rpcnhs4wdp2610b4h4hdlzr783jrv136j3a2smqnba"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-roxmltree" ,rust-roxmltree-0.19))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fontdb-0.16
  (package
    (name "rust-fontdb")
    (version "0.16.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fontdb" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hqxv3jnh06s4bflrwnb39mi3knllfs4mxm44vsn0gzgqch90adh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fontconfig-parser"
          ,rust-fontconfig-parser-0.5)
         ("rust-log" ,rust-log-0.4)
         ("rust-memmap2" ,rust-memmap2-0.9)
         ("rust-slotmap" ,rust-slotmap-1)
         ("rust-tinyvec" ,rust-tinyvec-1)
         ("rust-ttf-parser" ,rust-ttf-parser-0.20))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-getrandom-0.2
  (package
    (name "rust-getrandom")
    (version "0.2.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "getrandom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mzlnrb3dgyd1fb84gvw10pyr8wdqdl4ry4sr64i1s8an66pqmn4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-wasi" ,rust-wasi-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-gif-0.13
  (package
    (name "rust-gif")
    (version "0.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gif" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1whrkvdg26gp1r7f95c6800y6ijqw5y0z8rgj6xihpi136dxdciz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-color-quant" ,rust-color-quant-1)
         ("rust-weezl" ,rust-weezl-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-gimli-0.29
  (package
    (name "rust-gimli")
    (version "0.29.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gimli" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zgzprnjaawmg6zyic4f2q2hc39kdhn116qnkqpgvsasgc3x9v20"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-globmatch-0.2
  (package
    (name "rust-globmatch")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "globmatch" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1809rjlpyq8g55fvz7knzcy0klzfw4cffpw5afzjnaycg5ihfm9p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-globset" ,rust-globset-0.4)
         ("rust-log" ,rust-log-0.4)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-h2-0.3
  (package
    (name "rust-h2")
    (version "0.3.26")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "h2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1s7msnfv7xprzs6xzfj5sg6p8bjcdpcqcmjjbkd345cyi1x55zl1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-half-2
  (package
    (name "rust-half")
    (version "2.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "half" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "123q4zzw1x4309961i69igzd1wb7pj04aaii3kwasrz3599qrl3d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-crunchy" ,rust-crunchy-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hashbrown-0.14
  (package
    (name "rust-hashbrown")
    (version "0.14.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashbrown" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wa1vy1xs3mp11bn3z9dv0jricgr6a2j0zkf1g19yz3vw4il89z5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ahash" ,rust-ahash-0.8)
         ("rust-allocator-api2" ,rust-allocator-api2-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hashlink-0.8
  (package
    (name "rust-hashlink")
    (version "0.8.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashlink" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xy8agkyp0llbqk9fcffc1xblayrrywlyrm2a7v93x8zygm4y2g8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hashbrown" ,rust-hashbrown-0.14))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hayagriva-0.5
  (package
    (name "rust-hayagriva")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hayagriva" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yn9j48hj491swwvpbw4xszaprp2acb2lsw76zknrf3pig4j038x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-biblatex" ,rust-biblatex-0.9)
         ("rust-ciborium" ,rust-ciborium-0.2)
         ("rust-citationberg" ,rust-citationberg-0.3)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-numerals" ,rust-numerals-0.1)
         ("rust-paste" ,rust-paste-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-yaml" ,rust-serde-yaml-0.9)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-unic-langid" ,rust-unic-langid-0.9)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1)
         ("rust-unscanny" ,rust-unscanny-0.1)
         ("rust-url" ,rust-url-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hermit-abi-0.3
  (package
    (name "rust-hermit-abi")
    (version "0.3.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hermit-abi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "092hxjbjnq5fmz66grd9plxd0sh6ssg5fhgwwwqbrzgzkjwdycfj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-http-0.2
  (package
    (name "rust-http")
    (version "0.2.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1w81s4bcbmcj9bjp7mllm8jlz6b31wzvirz8bgpzbqkpwmbvn730"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-itoa" ,rust-itoa-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-http-1
  (package
    (name "rust-http")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0n426lmcxas6h75c2cp25m933pswlrfjz10v91vc62vib2sdvf91"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-itoa" ,rust-itoa-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-httparse-1
  (package
    (name "rust-httparse")
    (version "1.9.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "httparse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nc2s1pziq5ncl39xm7ybdhpnw5xsm505smqirr0py2v2550pk0g"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hyper-0.14
  (package
    (name "rust-hyper")
    (version "0.14.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyper" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jayxag79yln1nzyzx652kcy1bikgwssn6c4zrrp5v7s3pbdslm1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-futures-channel"
          ,rust-futures-channel-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-h2" ,rust-h2-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-http-body" ,rust-http-body-0.4)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-httpdate" ,rust-httpdate-1)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-socket2" ,rust-socket2-0.5)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-want" ,rust-want-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hypher-0.1
  (package
    (name "rust-hypher")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hypher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ra2kxbpi033jy42wkr7m7rgg6yhy69xad0hmc0z43936xbas91v"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-collections-1
  (package
    (name "rust-icu-collections")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_collections" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09j5kskirl59mvqc8kabhy7005yyy7dp88jw9f6f3gkf419a8byv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-yoke" ,rust-yoke-0.7)
         ("rust-zerofrom" ,rust-zerofrom-0.1)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-locid-1
  (package
    (name "rust-icu-locid")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_locid" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dznvd1c5b02iilqm044q4hvar0sqibq1z46prqwjzwif61vpb0k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-litemap" ,rust-litemap-0.7)
         ("rust-tinystr" ,rust-tinystr-0.7)
         ("rust-writeable" ,rust-writeable-0.5)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-locid-transform-1
  (package
    (name "rust-icu-locid-transform")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_locid_transform" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kmmi1kmj9yph6mdgkc7v3wz6995v7ly3n80vbg0zr78bp1iml81"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-icu-locid" ,rust-icu-locid-1)
         ("rust-icu-locid-transform-data"
          ,rust-icu-locid-transform-data-1)
         ("rust-icu-provider" ,rust-icu-provider-1)
         ("rust-tinystr" ,rust-tinystr-0.7)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-locid-transform-data-1
  (package
    (name "rust-icu-locid-transform-data")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_locid_transform_data" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vkgjixm0wzp2n3v5mw4j89ly05bg3lx96jpdggbwlpqi0rzzj7x"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-properties-1
  (package
    (name "rust-icu-properties")
    (version "1.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_properties" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xgf584rx10xc1p7zjr78k0n4zn3g23rrg6v2ln31ingcq3h5mlk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-icu-collections" ,rust-icu-collections-1)
         ("rust-icu-locid-transform"
          ,rust-icu-locid-transform-1)
         ("rust-icu-properties-data"
          ,rust-icu-properties-data-1)
         ("rust-icu-provider" ,rust-icu-provider-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-tinystr" ,rust-tinystr-0.7)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-properties-data-1
  (package
    (name "rust-icu-properties-data")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_properties_data" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0scms7pd5a7yxx9hfl167f5qdf44as6r3bd8myhlngnxqgxyza37"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-provider-1
  (package
    (name "rust-icu-provider")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_provider" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nb8vvgw8dv2inqklvk05fs0qxzkw8xrg2n9vgid6y7gm3423m3f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-icu-locid" ,rust-icu-locid-1)
         ("rust-icu-provider-macros"
          ,rust-icu-provider-macros-1)
         ("rust-postcard" ,rust-postcard-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-stable-deref-trait"
          ,rust-stable-deref-trait-1)
         ("rust-tinystr" ,rust-tinystr-0.7)
         ("rust-writeable" ,rust-writeable-0.5)
         ("rust-yoke" ,rust-yoke-0.7)
         ("rust-zerofrom" ,rust-zerofrom-0.1)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-provider-adapters-1
  (package
    (name "rust-icu-provider-adapters")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_provider_adapters" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1g60fydak0i4rxf8vfrr31mpck846k9ynix4fh1qx2il13ylscnn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-icu-locid" ,rust-icu-locid-1)
         ("rust-icu-locid-transform"
          ,rust-icu-locid-transform-1)
         ("rust-icu-provider" ,rust-icu-provider-1)
         ("rust-tinystr" ,rust-tinystr-0.7)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-provider-blob-1
  (package
    (name "rust-icu-provider-blob")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_provider_blob" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lfgvia5v76gkpfgbga5ga6z1b5465v821f2hs0xfmaz6v8rhjy2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-icu-provider" ,rust-icu-provider-1)
         ("rust-postcard" ,rust-postcard-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-writeable" ,rust-writeable-0.5)
         ("rust-zerotrie" ,rust-zerotrie-0.1)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-provider-macros-1
  (package
    (name "rust-icu-provider-macros")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_provider_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mjs0w7fcm2lcqmbakhninzrjwqs485lkps4hz0cv3k36y9rxj0y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-segmenter-1
  (package
    (name "rust-icu-segmenter")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_segmenter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pmharib9s1hn5650d4lyd48145n1n14pja2gcnzqvrl29b745x7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-core-maths" ,rust-core-maths-0.1)
         ("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-icu-collections" ,rust-icu-collections-1)
         ("rust-icu-locid" ,rust-icu-locid-1)
         ("rust-icu-provider" ,rust-icu-provider-1)
         ("rust-icu-segmenter-data"
          ,rust-icu-segmenter-data-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-utf8-iter" ,rust-utf8-iter-1)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-icu-segmenter-data-1
  (package
    (name "rust-icu-segmenter-data")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_segmenter_data" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pvrgnxi7fq47hfpc66jgvxzfc8nmzmgv0xw63imbnb0f9rywfgp"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-if-chain-1
  (package
    (name "rust-if-chain")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "if_chain" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vdrylnqw8vd99j20jgalh1vp1vh7dwnkdzsmlx4yjsvfsmf2mnb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-image-0.24
  (package
    (name "rust-image")
    (version "0.24.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "image" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17gnr6ifnpzvhjf6dwbl9hki8x6bji5mwcqp0048x1jm5yfi742n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytemuck" ,rust-bytemuck-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-color-quant" ,rust-color-quant-1)
         ("rust-gif" ,rust-gif-0.13)
         ("rust-jpeg-decoder" ,rust-jpeg-decoder-0.3)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-png" ,rust-png-0.17))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-imagesize-0.12
  (package
    (name "rust-imagesize")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "imagesize" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "114jvqiyv13il1qghv2xm0xqrcjm68fh282hdlzdds6qfgsp7782"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-indexmap-2
  (package
    (name "rust-indexmap")
    (version "2.2.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "indexmap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09hgwi2ig0wyj5rjziia76zmhgfj95k0jb4ic3iiawm4vlavg3qn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-equivalent" ,rust-equivalent-1)
         ("rust-hashbrown" ,rust-hashbrown-0.14)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-indexmap-nostd-0.4
  (package
    (name "rust-indexmap-nostd")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "indexmap-nostd" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "145mrkrrnzzg8xbv6si8j3b8cw1pi3g13vrjgf1fm2415gyy414f"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-inotify-0.9
  (package
    (name "rust-inotify")
    (version "0.9.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inotify" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zxb04c4qccp8wnr3v04l503qpxzxzzzph61amlqbsslq4z9s1pq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-inotify-sys" ,rust-inotify-sys-0.1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-insta-1
  (package
    (name "rust-insta")
    (version "1.39.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "insta" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xd64139vi0hrxs8l8k3r1xqf2x8b0x5chsh47lwkqj85l2fc2l1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-console" ,rust-console-0.15)
         ("rust-globset" ,rust-globset-0.4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-linked-hash-map"
          ,rust-linked-hash-map-0.5)
         ("rust-similar" ,rust-similar-2)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-instant-0.1
  (package
    (name "rust-instant")
    (version "0.1.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "instant" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08h27kzvb5jw74mh0ajv0nv9ggwvgqm8ynjsn2sa9jsks4cjh970"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ipnet-2
  (package
    (name "rust-ipnet")
    (version "2.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ipnet" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hzrcysgwf0knf83ahb3535hrkw63mil88iqc6kjaryfblrqylcg"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-is-terminal-0.4
  (package
    (name "rust-is-terminal")
    (version "0.4.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "is-terminal" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12vk6g0f94zlxl6mdh5gc4jdjb469n9k9s7y3vb0iml05gpzagzj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hermit-abi" ,rust-hermit-abi-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-is-terminal-polyfill-1
  (package
    (name "rust-is-terminal-polyfill")
    (version "1.70.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "is_terminal_polyfill" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0018q5cf3rifbnzfc1w1z1xcx9c6i7xlywp2n0fw4limq1vqaizq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-itertools-0.13
  (package
    (name "rust-itertools")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itertools" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11hiy3qzl643zcigknclh446qb9zlg4dpdzfkjaa9q9fqpgyfgj1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-either" ,rust-either-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-jpeg-decoder-0.3
  (package
    (name "rust-jpeg-decoder")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jpeg-decoder" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c1k53svpdyfhibkmm0ir5w0v3qmcmca8xr8vnnmizwf6pdagm7m"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-js-sys-0.3
  (package
    (name "rust-js-sys")
    (version "0.3.69")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "js-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0v99rz97asnzapb0jsc3jjhvxpfxr7h7qd97yqyrf9i7viimbh99"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kamadak-exif-0.5
  (package
    (name "rust-kamadak-exif")
    (version "0.5.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kamadak-exif" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xw0lpmra8j1y98c0agwrmjajpkh91mnl89hzaxbdrdp186wfkzg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-mutate-once" ,rust-mutate-once-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kqueue-1
  (package
    (name "rust-kqueue")
    (version "1.0.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kqueue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "033x2knkbv8d3jy6i9r32jcgsq6zm3g97zh5la43amkv3g5g2ivl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-kqueue-sys" ,rust-kqueue-sys-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kqueue-sys-1
  (package
    (name "rust-kqueue-sys")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kqueue-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12w3wi90y4kwis4k9g6fp0kqjdmc6l00j16g8mgbhac7vbzjb5pd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kurbo-0.9
  (package
    (name "rust-kurbo")
    (version "0.9.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kurbo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16w4k313z8smic4zifpwnxk8alh17dncgj2r40p0ql6rdivsb1dx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arrayvec" ,rust-arrayvec-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lazy-static-1
  (package
    (name "rust-lazy-static")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lazy_static" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zk6dqqni0193xg6iijh7i3i44sryglwgvx20spdvwk3r6sbrlmv"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-libc-0.2
  (package
    (name "rust-libc")
    (version "0.2.155")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0z44c53z54znna8n322k5iwg80arxxpdzjj5260pxxzc9a58icwp"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-linux-raw-sys-0.4
  (package
    (name "rust-linux-raw-sys")
    (version "0.4.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "linux-raw-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12gsjgbhhjwywpqcrizv80vrp7p7grsz5laqq773i33wphjsxcvq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lipsum-0.9
  (package
    (name "rust-lipsum")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lipsum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0r40mf2cwh4fp9pdfcc1n8hjxw05w7galjvb1z23r5pq38jn0s33"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand" ,rust-rand-0.8)
         ("rust-rand-chacha" ,rust-rand-chacha-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-litemap-0.7
  (package
    (name "rust-litemap")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "litemap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0157lf44c3s2piqiwpppnynzzpv1rxyddl2z9l089hpwsjwb0g34"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lock-api-0.4
  (package
    (name "rust-lock-api")
    (version "0.4.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lock_api" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05qvxa6g27yyva25a5ghsg85apdxkvr77yhkyhapj6r8vnf8pbq7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1)
         ("rust-scopeguard" ,rust-scopeguard-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-log-0.4
  (package
    (name "rust-log")
    (version "0.4.22")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "log" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "093vs0wkm1rgyykk7fjbqp2lwizbixac1w52gv109p5r4jh0p9x7"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-memchr-2
  (package
    (name "rust-memchr")
    (version "2.7.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memchr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18z32bhxrax0fnjikv475z7ii718hq457qwmaryixfxsl2qrmjkq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-memmap2-0.9
  (package
    (name "rust-memmap2")
    (version "0.9.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memmap2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08hkmvri44j6h14lyq4yw5ipsp91a9jacgiww4bs9jm8whi18xgy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mime-guess-2
  (package
    (name "rust-mime-guess")
    (version "2.0.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mime_guess" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03jmg3yx6j39mg0kayf7w4a886dl3j15y8zs119zw01ccy74zi7p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-mime" ,rust-mime-0.3)
         ("rust-unicase" ,rust-unicase-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-miniz-oxide-0.7
  (package
    (name "rust-miniz-oxide")
    (version "0.7.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "miniz_oxide" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "024wv14aa75cvik7005s5y2nfc8zfidddbd7g55g7sjgnzfl18mq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-adler" ,rust-adler-1)
         ("rust-simd-adler32" ,rust-simd-adler32-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mintex-0.1
  (package
    (name "rust-mintex")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mintex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01ydy8pvyy96cjvjh4hgfqmjalr6hnbyc6c8a9xwq4yvznc4bv4v"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mio-0.8
  (package
    (name "rust-mio")
    (version "0.8.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "034byyl0ardml5yliy1hmvx8arkmn9rv479pid794sm07ia519m4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-wasi" ,rust-wasi-0.11)
         ("rust-windows-sys" ,rust-windows-sys-0.48))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mutate-once-0.1
  (package
    (name "rust-mutate-once")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mutate_once" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ys9mpjhwsj5md10ykmkin0wv7bz8dvc292hqczs9l5l4cd6ikqn"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-native-tls-0.2
  (package
    (name "rust-native-tls")
    (version "0.2.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "native-tls" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rkl65z70n7sy4d5w0qa99klg1hr43wx6kcprk4d2n9xr2r4wqd8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-openssl" ,rust-openssl-0.10)
         ("rust-openssl-probe" ,rust-openssl-probe-0.1)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-schannel" ,rust-schannel-0.1)
         ("rust-security-framework"
          ,rust-security-framework-2)
         ("rust-security-framework-sys"
          ,rust-security-framework-sys-2)
         ("rust-tempfile" ,rust-tempfile-3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-nohash-hasher-0.2
  (package
    (name "rust-nohash-hasher")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nohash-hasher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lf4p6k01w4wm7zn4grnihzj8s7zd5qczjmzng7wviwxawih5x9b"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-num-bigint-0.4
  (package
    (name "rust-num-bigint")
    (version "0.4.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-bigint" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1f903zd33i6hkjpsgwhqwi2wffnvkxbn6rv4mkgcjcqi7xr4zr55"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-num-threads-0.1
  (package
    (name "rust-num-threads")
    (version "0.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_threads" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ngajbmhrgyhzrlc4d5ga9ych1vrfcvfsiqz6zv0h2dpr2wrhwsw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-numerals-0.1
  (package
    (name "rust-numerals")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "numerals" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cdx6yf5zcx2nvmzavr4qk9m35ha6i2rhy5fjxgx2wm7fq9y4nz2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-object-0.36
  (package
    (name "rust-object")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "object" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kng5iqp4az48kn2vfd1irv12yp0yvj25agizlcgrpan3mnq86q8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-open-5
  (package
    (name "rust-open")
    (version "5.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "open" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cy40lf0hk8b0pwm9ix5zi53m4lqnjgviw9ylm16cwdxdazpga31"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-is-wsl" ,rust-is-wsl-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pathdiff" ,rust-pathdiff-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-openssl-macros-0.1
  (package
    (name "rust-openssl-macros")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "173xxvfc63rr5ybwqwylsir0vq6xsj4kxiv4hmg4c3vscdmncj59"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-openssl-probe-0.1
  (package
    (name "rust-openssl-probe")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl-probe" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kq18qm48rvkwgcggfkqq6pm948190czqc94d6bm2sir5hq1l0gz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-openssl-sys-0.9
  (package
    (name "rust-openssl-sys")
    (version "0.9.102")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18jzni7xzdcqwf9r8kp6j46abrxqn82dvc2ylf9kij7varyn75y5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-vcpkg" ,rust-vcpkg-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-palette-0.7
  (package
    (name "rust-palette")
    (version "0.7.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "palette" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rmn02mv6cb112504qyg7pyfa83c08hxpk5sw7jc5v659hc73gsc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-approx" ,rust-approx-0.5)
         ("rust-fast-srgb8" ,rust-fast-srgb8-1)
         ("rust-libm" ,rust-libm-0.2)
         ("rust-palette-derive" ,rust-palette-derive-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-palette-derive-0.7
  (package
    (name "rust-palette-derive")
    (version "0.7.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "palette_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0c0xhpk1nqyq4jr2m8xnka7w47vqzc7m2vq9ih8wxyjv02phs0zm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-by-address" ,rust-by-address-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parking-lot-0.12
  (package
    (name "rust-parking-lot")
    (version "0.12.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09ws9g6245iiq8z975h8ycf818a66q3c6zv4b5h8skpm7hc1igzi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lock-api" ,rust-lock-api-0.4)
         ("rust-parking-lot-core"
          ,rust-parking-lot-core-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parking-lot-core-0.9
  (package
    (name "rust-parking-lot-core")
    (version "0.9.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1y3cf9ld9ijf7i4igwzffcn0xl16dxyn4c5bwgjck1dkgabiyh0y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-redox-syscall" ,rust-redox-syscall-0.5)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-windows-targets"
          ,rust-windows-targets-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-paste-1
  (package
    (name "rust-paste")
    (version "1.0.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "paste" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02pxffpdqkapy292harq6asfjvadgp1s005fip9ljfsn9fvxgh2p"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pdf-writer-0.9
  (package
    (name "rust-pdf-writer")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pdf-writer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0arksb7ahf389q0r7gjm3y3x4fh4v7nckahwcrp82g06ams15s94"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-ryu" ,rust-ryu-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-generator-0.11
  (package
    (name "rust-phf-generator")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_generator" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c14pjyxbcpwkdgw109f7581cc5fa3fnkzdq1ikvx7mdq9jcrr28"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-shared" ,rust-phf-shared-0.11)
         ("rust-rand" ,rust-rand-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pin-project-lite-0.2
  (package
    (name "rust-pin-project-lite")
    (version "0.2.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pin-project-lite" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00nx3f04agwjlsmd3mc5rx5haibj2v8q9b52b0kwn63wcv4nz9mx"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pixglyph-0.3
  (package
    (name "rust-pixglyph")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pixglyph" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04m7l6y49nimb8pw9x6mqxjqcy248p2c705q4n0v6z8r9jnziq72"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ttf-parser" ,rust-ttf-parser-0.20))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pkg-config-0.3
  (package
    (name "rust-pkg-config")
    (version "0.3.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pkg-config" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1v07557dj1sa0aly9c90wsygc0i8xv5vnmyv0g94lpkvj8qb4cfj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-plist-1
  (package
    (name "rust-plist")
    (version "1.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "plist" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05hh3s44km2hyig48xjq580mj6s1r4yijzf6dcwmy3w0l7ligks2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.22)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-quick-xml" ,rust-quick-xml-0.32)
         ("rust-serde" ,rust-serde-1)
         ("rust-time" ,rust-time-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pretty-0.12
  (package
    (name "rust-pretty")
    (version "0.12.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pretty" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yd5kyv3l73sbmkpsds2asfj03b6bpffbxnsyki3gdllv4blsp5m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arrayvec" ,rust-arrayvec-0.5)
         ("rust-typed-arena" ,rust-typed-arena-2)
         ("rust-unicode-width" ,rust-unicode-width-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-prettytable-rs-0.10
  (package
    (name "rust-prettytable-rs")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "prettytable-rs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nnryfnahfwy0yxhv4nsp1id25k00cybx3ih8xjsp9haa43mx8pf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-csv" ,rust-csv-1)
         ("rust-encode-unicode" ,rust-encode-unicode-1)
         ("rust-is-terminal" ,rust-is-terminal-0.4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-term" ,rust-term-0.7)
         ("rust-unicode-width" ,rust-unicode-width-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-proc-macro2-1
  (package
    (name "rust-proc-macro2")
    (version "1.0.86")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xrv22p8lqlfdf1w0pj4si8n2ws4aw0kilmziwf0vpv5ys6rwway"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-ident" ,rust-unicode-ident-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-psm-0.1
  (package
    (name "rust-psm")
    (version "0.1.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "psm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0x78nj5wxkxwijd2gvv2ycq06443b2y1ih4j46kk6c2flg6zg1sp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-qcms-0.3
  (package
    (name "rust-qcms")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "qcms" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yihv9rsa0qc4mmhzp8f0xdfrnkw7q8l7kr4ivcyb9amszazrv7d"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-quick-xml-0.32
  (package
    (name "rust-quick-xml")
    (version "0.32.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quick-xml" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hk9x4fij5kq1mnn7gmxz1hpv8s9wnnj4gx4ly7hw3mn71c6wfhx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-quote-1
  (package
    (name "rust-quote")
    (version "1.0.36")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quote" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19xcmh445bg6simirnnd4fvkmp6v2qiwxh5f6rw4a70h76pnm9qg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-redox-syscall-0.5
  (package
    (name "rust-redox-syscall")
    (version "0.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zg03n8rjbqnnbzp5nvai2pjgzxf7cw42hpcamgh8mj4y77zhb68"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-reflexo-0.5
  (package
    (name "rust-reflexo")
    (version "0.5.0-rc5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "reflexo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08xw3x9psi92pna8gs00qdfn831icizza3krgrfigax0vdqszrba"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.22)
         ("rust-bitvec" ,rust-bitvec-1)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-dashmap" ,rust-dashmap-5)
         ("rust-ecow" ,rust-ecow-0.2)
         ("rust-fxhash" ,rust-fxhash-0.2)
         ("rust-instant" ,rust-instant-0.1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-path-clean" ,rust-path-clean-1)
         ("rust-rkyv" ,rust-rkyv-0.7)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-repr" ,rust-serde-repr-0.1)
         ("rust-serde-with" ,rust-serde-with-3)
         ("rust-siphasher" ,rust-siphasher-1)
         ("rust-tiny-skia-path" ,rust-tiny-skia-path-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-reflexo-vfs-0.5
  (package
    (name "rust-reflexo-vfs")
    (version "0.5.0-rc5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "reflexo-vfs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03xws69x8b7c6g2l28nj336qwz0x31c22fj1gzpwipw32izhrj5c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-append-only-vec"
          ,rust-append-only-vec-0.1)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-log" ,rust-log-0.4)
         ("rust-nohash-hasher" ,rust-nohash-hasher-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-reflexo" ,rust-reflexo-0.5)
         ("rust-rpds" ,rust-rpds-1)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-typst" ,rust-typst-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-reflexo-world-0.5
  (package
    (name "rust-reflexo-world")
    (version "0.5.0-rc5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "reflexo-world" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0pvw290v2cm5r4l9mdrrirrxibl3x0mm8hkzq5svwp03j67hzsv8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-append-only-vec"
          ,rust-append-only-vec-0.1)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-codespan-reporting"
          ,rust-codespan-reporting-0.11)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-dashmap" ,rust-dashmap-5)
         ("rust-dirs" ,rust-dirs-5)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-fontdb" ,rust-fontdb-0.16)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-log" ,rust-log-0.4)
         ("rust-nohash-hasher" ,rust-nohash-hasher-0.2)
         ("rust-notify" ,rust-notify-6)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-reflexo" ,rust-reflexo-0.5)
         ("rust-reflexo-vfs" ,rust-reflexo-vfs-0.5)
         ("rust-reqwest" ,rust-reqwest-0.11)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-sha2" ,rust-sha2-0.10)
         ("rust-strum" ,rust-strum-0.25)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-typst" ,rust-typst-0.11)
         ("rust-typst-ts-core" ,rust-typst-ts-core-0.5)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-regex-1
  (package
    (name "rust-regex")
    (version "1.10.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zsiqk2sxc1kd46qw0yp87s2a14ialwyxinpl0k266ddkm1i64mr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-regex-automata" ,rust-regex-automata-0.4)
         ("rust-regex-syntax" ,rust-regex-syntax-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-regex-automata-0.4
  (package
    (name "rust-regex-automata")
    (version "0.4.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-automata" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pwjdi4jckpbaivpl6x4v5g4crb37zr2wac93wlfsbzgqn6gbjiq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-regex-syntax" ,rust-regex-syntax-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-regex-syntax-0.8
  (package
    (name "rust-regex-syntax")
    (version "0.8.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-syntax" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16r0kjy20vx33dr4mhasj5l1f87czas714x2fz6zl0f8wwxa0rks"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rend-0.4
  (package
    (name "rust-rend")
    (version "0.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0z4rrkycva0lcw0hxq479h4amxj9syn5vq4vb2qid5v2ylj3izki"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytecheck" ,rust-bytecheck-0.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-reqwest-0.11
  (package
    (name "rust-reqwest")
    (version "0.11.27")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "reqwest" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0qjary4hpplpgdi62d2m0xvbn6lnzckwffm0rgkm2x51023m6ryx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.21)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-h2" ,rust-h2-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-http-body" ,rust-http-body-0.4)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-hyper-rustls" ,rust-hyper-rustls-0.24)
         ("rust-hyper-tls" ,rust-hyper-tls-0.5)
         ("rust-ipnet" ,rust-ipnet-2)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-rustls" ,rust-rustls-0.21)
         ("rust-rustls-pemfile" ,rust-rustls-pemfile-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-urlencoded"
          ,rust-serde-urlencoded-0.7)
         ("rust-sync-wrapper" ,rust-sync-wrapper-0.1)
         ("rust-system-configuration"
          ,rust-system-configuration-0.5)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls"
          ,rust-tokio-native-tls-0.3)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.24)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ("rust-url" ,rust-url-2)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-wasm-bindgen-futures"
          ,rust-wasm-bindgen-futures-0.4)
         ("rust-web-sys" ,rust-web-sys-0.3)
         ("rust-webpki-roots" ,rust-webpki-roots-0.25)
         ("rust-winreg" ,rust-winreg-0.50))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-resvg-0.38
  (package
    (name "rust-resvg")
    (version "0.38.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "resvg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07hcwi72y0cybyj45n2hq95gj58wyg3jsam61d3hd7lm8q850d2w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gif" ,rust-gif-0.12)
         ("rust-jpeg-decoder" ,rust-jpeg-decoder-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-pico-args" ,rust-pico-args-0.5)
         ("rust-png" ,rust-png-0.17)
         ("rust-rgb" ,rust-rgb-0.8)
         ("rust-svgtypes" ,rust-svgtypes-0.13)
         ("rust-tiny-skia" ,rust-tiny-skia-0.11)
         ("rust-usvg" ,rust-usvg-0.38))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rgb-0.8
  (package
    (name "rust-rgb")
    (version "0.8.44")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rgb" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1na1sapcj3z7xbq1czva2bm6c45q2g6rlabvsc034nhx53f87vhs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytemuck" ,rust-bytemuck-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rust-iso3166-0.1
  (package
    (name "rust-rust-iso3166")
    (version "0.1.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust_iso3166" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "140a03g5faki3lq1hzvvkjmza6sysnq6qdhsfsj8rvqpnpm2ccfx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-phf" ,rust-phf-0.11)
         ("rust-prettytable-rs" ,rust-prettytable-rs-0.10)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rust-iso639-0.0.1
  (package
    (name "rust-rust-iso639")
    (version "0.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust_iso639" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wxxyigwgli788jfbvz2dh1m4y376xkmncjg4kbl57frbyczlyd0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf" ,rust-phf-0.11)
         ("rust-prettytable-rs" ,rust-prettytable-rs-0.10)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustc-demangle-0.1
  (package
    (name "rust-rustc-demangle")
    (version "0.1.24")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc-demangle" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07zysaafgrkzy2rjgwqdj2a8qdpsm6zv6f5pgpk9x0lm40z9b6vi"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustc-hash-2
  (package
    (name "rust-rustc-hash")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc-hash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lni0lf846bzrf3jvci6jaf4142n1mdqxvcpczk5ch9pfgyk8c2q"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustix-0.38
  (package
    (name "rust-rustix")
    (version "0.38.34")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustix" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03vkqa2ism7q56rkifyy8mns0wwqrk70f4i4fd53r97p8b05xp3h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-errno" ,rust-errno-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-linux-raw-sys" ,rust-linux-raw-sys-0.4)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustls-0.21
  (package
    (name "rust-rustls")
    (version "0.21.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustls" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gjdg2a9r81sdwkyw3n5yfbkrr6p9gyk3xr2kcsr3cs83x6s2miz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4)
         ("rust-ring" ,rust-ring-0.17)
         ("rust-rustls-webpki" ,rust-rustls-webpki-0.101)
         ("rust-sct" ,rust-sct-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustversion-1
  (package
    (name "rust-rustversion")
    (version "1.0.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustversion" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mm3fckyvb0l2209in1n2k05sws5d9mpkszbnwhq3pkq8apjhpcm"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustybuzz-0.12
  (package
    (name "rust-rustybuzz")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustybuzz" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0b12arlca4lfniphg91v9s5awkl7szpdwc18walxdamyqn95dbph"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-bytemuck" ,rust-bytemuck-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-ttf-parser" ,rust-ttf-parser-0.20)
         ("rust-unicode-bidi-mirroring"
          ,rust-unicode-bidi-mirroring-0.1)
         ("rust-unicode-ccc" ,rust-unicode-ccc-0.1)
         ("rust-unicode-properties"
          ,rust-unicode-properties-0.1)
         ("rust-unicode-script" ,rust-unicode-script-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ryu-1
  (package
    (name "rust-ryu")
    (version "1.0.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ryu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17xx2s8j1lln7iackzd9p0sv546vjq71i779gphjq923vjh5pjzk"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-schannel-0.1
  (package
    (name "rust-schannel")
    (version "0.1.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "schannel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0d1m156bsjrws6xzzr1wyfyih9i22mb2csb5pc5kmkrvci2ibjgv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-security-framework-2
  (package
    (name "rust-security-framework")
    (version "2.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "security-framework" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1h0ipvv8wi9wvhad0a9w7jpmb189jng4jhfgnp6vl1lps0zp49y6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-core-foundation"
          ,rust-core-foundation-0.9)
         ("rust-core-foundation-sys"
          ,rust-core-foundation-sys-0.8)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-security-framework-sys"
          ,rust-security-framework-sys-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-security-framework-sys-2
  (package
    (name "rust-security-framework-sys")
    (version "2.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "security-framework-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mq7ykz3fi0ba55aj4afz24v9qvwdpkbjiirb197f8h5pnxkcy9i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-core-foundation-sys"
          ,rust-core-foundation-sys-0.8)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-semver-1
  (package
    (name "rust-semver")
    (version "1.0.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "semver" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12wqpxfflclbq4dv8sa6gchdh92ahhwn4ci1ls22wlby3h57wsb1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-1
  (package
    (name "rust-serde")
    (version "1.0.204")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04kwpwqz559xlhxkggmm8rjxqgywy5swam3kscwsicnbw1cgaxmw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-derive-1
  (package
    (name "rust-serde-derive")
    (version "1.0.204")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08p25262mbmhsr2cg0508d5b1wvljj956rvpg0v3qgg6gc8pxkg0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-json-1
  (package
    (name "rust-serde-json")
    (version "1.0.120")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_json" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1idpv3zxcvl76z2z47jgg1f1wjqdnhfc204asmd27qfam34j23af"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-itoa" ,rust-itoa-1)
         ("rust-ryu" ,rust-ryu-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-repr-0.1
  (package
    (name "rust-serde-repr")
    (version "0.1.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_repr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1sb4cplc33z86pzlx38234xr141wr3cmviqgssiadisgl8dlar3c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-spanned-0.6
  (package
    (name "rust-serde-spanned")
    (version "0.6.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_spanned" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1839b6m5p9ijjmcwamiya2r612ks2vg6w2pp95yg76lr3zh79rkr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-with-3
  (package
    (name "rust-serde-with")
    (version "3.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_with" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xz3fpwi4k7bylankr8q9jdc7ha9d6jfanyqbxn5xm62bsy3jcg7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.22)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-with-macros"
          ,rust-serde-with-macros-3)
         ("rust-time" ,rust-time-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-with-macros-3
  (package
    (name "rust-serde-with-macros")
    (version "3.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_with_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00v7yj9m7sfhbfhm2nsq9k0hhpiw7gigspqf3303ahxnarmks3dq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling" ,rust-darling-0.20)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-yaml-0.9
  (package
    (name "rust-serde-yaml")
    (version "0.9.34+deprecated")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_yaml" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0isba1fjyg3l6rxk156k600ilzr8fp7crv82rhal0rxz5qd1m2va"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-indexmap" ,rust-indexmap-2)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-ryu" ,rust-ryu-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-unsafe-libyaml" ,rust-unsafe-libyaml-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-signal-hook-registry-1
  (package
    (name "rust-signal-hook-registry")
    (version "1.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook-registry" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cb5akgq8ajnd5spyn587srvs4n26ryq0p78nswffwhv46sf1sd9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-similar-2
  (package
    (name "rust-similar")
    (version "2.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "similar" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0h361jw5wdfp5mic9d2mljmx8y7i3j9py9kgnalmvl7i2c9wjhps"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-simplecss-0.2
  (package
    (name "rust-simplecss")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "simplecss" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17g8q1z9xrkd27ic9nrfirj6in4rai6l9ws0kxz45n97573ff6x1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-siphasher-1
  (package
    (name "rust-siphasher")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "siphasher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17f35782ma3fn6sh21c027kjmd227xyrx06ffi8gw4xzv9yry6an"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-slab-0.4
  (package
    (name "rust-slab")
    (version "0.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slab" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rxvsgir0qw5lkycrqgb1cxsvxzjv9bmx73bk5y42svnzfba94lg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-slotmap-1
  (package
    (name "rust-slotmap")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slotmap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0amqb2fn9lcy1ri0risblkcp88dl0rnfmynw7lx0nqwza77lmzyv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-smallvec-1
  (package
    (name "rust-smallvec")
    (version "1.13.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smallvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rsw5samawl3wsw6glrsb127rx6sh89a8wyikicw6dkdcjd1lpiw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-socket2-0.5
  (package
    (name "rust-socket2")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "socket2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "070r941wbq76xpy039an4pyiy3rfj7mp7pvibf1rcri9njq5wc6f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strict-num-0.1
  (package
    (name "rust-strict-num")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strict-num" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cb7l1vhb8zj90mzm8avlk815k40sql9515s865rqdrdfavvldv6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-float-cmp" ,rust-float-cmp-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strsim-0.11
  (package
    (name "rust-strsim")
    (version "0.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strsim" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kzvqlw8hxqb7y598w1s0hxlnmi84sg5vsipp3yg5na5d1rvba3x"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strum-0.26
  (package
    (name "rust-strum")
    (version "0.26.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01lgl6jvrf4j28v5kmx9bp480ygf1nhvac8b4p7rcj9hxw50zv4g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-strum-macros" ,rust-strum-macros-0.26))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strum-macros-0.26
  (package
    (name "rust-strum-macros")
    (version "0.26.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gl1wmq24b8md527cpyd5bw9rkbqldd7k1h38kf5ajd2ln2ywssc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-heck" ,rust-heck-0.5)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-subsetter-0.1
  (package
    (name "rust-subsetter")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "subsetter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ygjaz4nd8m5k5h966s3i3wqgiy78nz5jk5x00ibm2gz7flbish9"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-svg2pdf-0.10
  (package
    (name "rust-svg2pdf")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "svg2pdf" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gk9nd92nlp40r5ickfjqj31nqkvfmx255v5v5zlks1b0qqb6dms"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-image" ,rust-image-0.24)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.7)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-pdf-writer" ,rust-pdf-writer-0.9)
         ("rust-resvg" ,rust-resvg-0.38)
         ("rust-tiny-skia" ,rust-tiny-skia-0.11)
         ("rust-usvg" ,rust-usvg-0.38))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-svgtypes-0.13
  (package
    (name "rust-svgtypes")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "svgtypes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w4xknlff1np8l9if7y8ig6bx44bjr006m5xgj8ih0wnrn4f4i3f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-kurbo" ,rust-kurbo-0.9)
         ("rust-siphasher" ,rust-siphasher-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-syn-2
  (package
    (name "rust-syn")
    (version "2.0.70")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05jc0v1jch76xayprcwc7glczvfaxia5747cw29v04rnifv0j0ig"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-unicode-ident" ,rust-unicode-ident-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

; (define rust-sync-lsp-0.11
;   (package
;     (name "rust-sync-lsp")
;     (version "0.11.18")
;     (source
;       (origin
;         (method url-fetch)
;         (uri (crate-uri "sync-lsp" version))
;         (file-name
;           (string-append name "-" version ".tar.gz"))
;         (sha256
;           (base32
;             "0000000000000000000000000000000000000000000000000000"))))
;     (build-system cargo-build-system)
;     (arguments
;       `(#:skip-build?
;         #t
;         #:cargo-inputs
;         (("rust-anyhow" ,rust-anyhow-1)
;          ("rust-clap" ,rust-clap-4)
;          ("rust-crossbeam-channel"
;           ,rust-crossbeam-channel-0.5)
;          ("rust-futures" ,rust-futures-0.3)
;          ("rust-log" ,rust-log-0.4)
;          ("rust-lsp-server" ,rust-lsp-server-0.7)
;          ("rust-lsp-types" ,rust-lsp-types-0.95)
;          ("rust-parking-lot" ,rust-parking-lot-0.12)
;          ("rust-reflexo" ,rust-reflexo-0.5)
;          ("rust-serde" ,rust-serde-1)
;          ("rust-serde-json" ,rust-serde-json-1)
;          ("rust-tinymist-query" ,rust-tinymist-query-0.11)
;          ("rust-tokio" ,rust-tokio-1)
;          ("rust-tokio-util" ,rust-tokio-util-0.7))))
;     (home-page "")
;     (synopsis "")
;     (description "")
;     (license #f)))

(define rust-synstructure-0.13
  (package
    (name "rust-synstructure")
    (version "0.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "synstructure" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wc9f002ia2zqcbj0q2id5x6n7g1zjqba7qkg2mr0qvvmdk7dby8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tar-0.4
  (package
    (name "rust-tar")
    (version "0.4.41")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tar" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02a9wksw2ci87461czsn0vpvb0wlb152yw4ya77nzdxmbynpsyfb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-filetime" ,rust-filetime-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-xattr" ,rust-xattr-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-termcolor-1
  (package
    (name "rust-termcolor")
    (version "1.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "termcolor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mappjh3fj3p2nmrg4y7qv94rchwi9mzmgmfflr8p2awdj7lyy86"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-util" ,rust-winapi-util-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tests-0.11
  (package
    (name "rust-tests")
    (version "0.11.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tests" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-insta" ,rust-insta-1)
         ("rust-lsp-server" ,rust-lsp-server-0.7)
         ("rust-lsp-types" ,rust-lsp-types-0.95)
         ("rust-reflexo" ,rust-reflexo-0.5)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-thiserror-1
  (package
    (name "rust-thiserror")
    (version "1.0.61")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "028prh962l16cmjivwb1g9xalbpqip0305zhq006mg74dc6whin5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror-impl" ,rust-thiserror-impl-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-thiserror-impl-1
  (package
    (name "rust-thiserror-impl")
    (version "1.0.61")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cvm37hp0kbcyk1xac1z0chpbd9pbn2g456iyid6sah0a113ihs6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-time-0.3
  (package
    (name "rust-time")
    (version "0.3.36")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11g8hdpahgrf1wwl2rpsg5nxq3aj7ri6xr672v4qcij6cgjqizax"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-deranged" ,rust-deranged-0.3)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-num-conv" ,rust-num-conv-0.1)
         ("rust-num-threads" ,rust-num-threads-0.1)
         ("rust-powerfmt" ,rust-powerfmt-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-time-core" ,rust-time-core-0.1)
         ("rust-time-macros" ,rust-time-macros-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-time-macros-0.2
  (package
    (name "rust-time-macros")
    (version "0.2.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kqwxvfh2jkpg38fy673d6danh1bhcmmbsmffww3mphgail2l99z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-conv" ,rust-num-conv-0.1)
         ("rust-time-core" ,rust-time-core-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tiny-skia-0.11
  (package
    (name "rust-tiny-skia")
    (version "0.11.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tiny-skia" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1aq9gd4qh4418g8v08qzakqqggx8hl66qcianl3k5bjdsja37lc3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arrayref" ,rust-arrayref-0.3)
         ("rust-arrayvec" ,rust-arrayvec-0.7)
         ("rust-bytemuck" ,rust-bytemuck-1)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-png" ,rust-png-0.17)
         ("rust-tiny-skia-path" ,rust-tiny-skia-path-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tiny-skia-path-0.11
  (package
    (name "rust-tiny-skia-path")
    (version "0.11.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tiny-skia-path" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14ywbdfakvacl6rxxmzbnycplaxpc6i2linh2yqk0sp8qb07z7lw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arrayref" ,rust-arrayref-0.3)
         ("rust-bytemuck" ,rust-bytemuck-1)
         ("rust-strict-num" ,rust-strict-num-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tinymist-assets-0.11
  (package
    (name "rust-tinymist-assets")
    (version "0.11.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinymist-assets" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tinymist-assets-0.11
  (package
    (name "rust-tinymist-assets")
    (version "0.11.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinymist-assets" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1p9q3vpikblzjixhdbdxmyyl817s6viwq8dbd8m6i0yh0mibq1d6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

; (define rust-tinymist-query-0.11
;   (package
;     (name "rust-tinymist-query")
;     (version "0.11.18")
;     (source
;       (origin
;         (method url-fetch)
;         (uri (crate-uri "tinymist-query" version))
;         (file-name
;           (string-append name "-" version ".tar.gz"))
;         (sha256
;           (base32
;             "0000000000000000000000000000000000000000000000000000"))))
;     (build-system cargo-build-system)
;     (arguments
;       `(#:skip-build?
;         #t
;         #:cargo-inputs
;         (("rust-anyhow" ,rust-anyhow-1)
;          ("rust-biblatex" ,rust-biblatex-0.9)
;          ("rust-chrono" ,rust-chrono-0.4)
;          ("rust-comemo" ,rust-comemo-0.4)
;          ("rust-dashmap" ,rust-dashmap-5)
;          ("rust-ecow" ,rust-ecow-0.2)
;          ("rust-ena" ,rust-ena-0.14)
;          ("rust-hashbrown" ,rust-hashbrown-0.14)
;          ("rust-hex" ,rust-hex-0.4)
;          ("rust-if-chain" ,rust-if-chain-1)
;          ("rust-indexmap" ,rust-indexmap-2)
;          ("rust-insta" ,rust-insta-1)
;          ("rust-itertools" ,rust-itertools-0.13)
;          ("rust-log" ,rust-log-0.4)
;          ("rust-lsp-types" ,rust-lsp-types-0.95)
;          ("rust-once-cell" ,rust-once-cell-1)
;          ("rust-parking-lot" ,rust-parking-lot-0.12)
;          ("rust-pathdiff" ,rust-pathdiff-0.2)
;          ("rust-percent-encoding"
;           ,rust-percent-encoding-2)
;          ("rust-reflexo" ,rust-reflexo-0.5)
;          ("rust-regex" ,rust-regex-1)
;          ("rust-rust-iso3166" ,rust-rust-iso3166-0.1)
;          ("rust-rust-iso639" ,rust-rust-iso639-0.0.1)
;          ("rust-rustc-hash" ,rust-rustc-hash-2)
;          ("rust-serde" ,rust-serde-1)
;          ("rust-serde-json" ,rust-serde-json-1)
;          ("rust-serde-yaml" ,rust-serde-yaml-0.9)
;          ("rust-sha2" ,rust-sha2-0.10)
;          ("rust-siphasher" ,rust-siphasher-1)
;          ("rust-strum" ,rust-strum-0.26)
;          ("rust-toml" ,rust-toml-0.8)
;          ("rust-triomphe" ,rust-triomphe-0.1)
;          ("rust-ttf-parser" ,rust-ttf-parser-0.20)
;          ("rust-typst" ,rust-typst-0.11)
;          ("rust-typst-assets" ,rust-typst-assets-0.11)
;          ("rust-typst-ts-compiler"
;           ,rust-typst-ts-compiler-0.5)
;          ("rust-typst-ts-core" ,rust-typst-ts-core-0.5)
;          ("rust-unscanny" ,rust-unscanny-0.1)
;          ("rust-walkdir" ,rust-walkdir-2)
;          ("rust-yaml-rust2" ,rust-yaml-rust2-0.8))))
;     (home-page "")
;     (synopsis "")
;     (description "")
;     (license #f)))

; (define rust-tinymist-render-0.11
;   (package
;     (name "rust-tinymist-render")
;     (version "0.11.18")
;     (source
;       (origin
;         (method url-fetch)
;         (uri (crate-uri "tinymist-render" version))
;         (file-name
;           (string-append name "-" version ".tar.gz"))
;         (sha256
;           (base32
;             "0000000000000000000000000000000000000000000000000000"))))
;     (build-system cargo-build-system)
;     (arguments
;       `(#:skip-build?
;         #t
;         #:cargo-inputs
;         (("rust-base64" ,rust-base64-0.22)
;          ("rust-log" ,rust-log-0.4)
;          ("rust-serde" ,rust-serde-1)
;          ("rust-tinymist-query" ,rust-tinymist-query-0.11)
;          ("rust-typst-ts-svg-exporter"
;           ,rust-typst-ts-svg-exporter-0.5))))
;     (home-page "")
;     (synopsis "")
;     (description "")
;     (license #f)))

(define rust-tinystr-0.7
  (package
    (name "rust-tinystr")
    (version "0.7.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinystr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bxqaw7z8r2kzngxlzlgvld1r6jbnwyylyvyjbv1q71rvgaga5wi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tinyvec-1
  (package
    (name "rust-tinyvec")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinyvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0f5rf6a2wzyv6w4jmfga9iw7rp9fp5gf4d604xgjsf3d9wgqhpj4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinyvec-macros" ,rust-tinyvec-macros-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tokio-1
  (package
    (name "rust-tokio")
    (version "1.38.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jjsb2c9dqi93yij3rqzsh9bk0z3qyasmw1n8qkny3d8lw14lkxs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-mio" ,rust-mio-0.8)
         ("rust-num-cpus" ,rust-num-cpus-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-signal-hook-registry"
          ,rust-signal-hook-registry-1)
         ("rust-socket2" ,rust-socket2-0.5)
         ("rust-tokio-macros" ,rust-tokio-macros-2)
         ("rust-windows-sys" ,rust-windows-sys-0.48))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tokio-macros-2
  (package
    (name "rust-tokio-macros")
    (version "2.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16nkan0x9b62hnqmjqcyd71j1mgpda2sv7gfm2mvbm39l2cfjnjz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tokio-util-0.7
  (package
    (name "rust-tokio-util")
    (version "0.7.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qcz30db6m8lxkl61b3nic4bim1symi636nhbb3rmi3i6xxv9xlw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-toml-0.8
  (package
    (name "rust-toml")
    (version "0.8.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "toml" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dgk8bacrza09npifba1xsx7wyjjvhz3igxpdnyjcbqxn8mfnjbg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-serde-spanned" ,rust-serde-spanned-0.6)
         ("rust-toml-datetime" ,rust-toml-datetime-0.6)
         ("rust-toml-edit" ,rust-toml-edit-0.22))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-toml-datetime-0.6
  (package
    (name "rust-toml-datetime")
    (version "0.6.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "toml_datetime" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1grcrr3gh7id3cy3j700kczwwfbn04p5ncrrj369prjaj9bgvbab"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-toml-edit-0.22
  (package
    (name "rust-toml-edit")
    (version "0.22.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "toml_edit" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1w9yb85mxz3mx3bhc7zm6sxzl1wddy4zl4diw9jgalw455r3m6nm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-indexmap" ,rust-indexmap-2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-spanned" ,rust-serde-spanned-0.6)
         ("rust-toml-datetime" ,rust-toml-datetime-0.6)
         ("rust-winnow" ,rust-winnow-0.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-triomphe-0.1
  (package
    (name "rust-triomphe")
    (version "0.1.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "triomphe" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sakxpp9d3nc2xgx348393diyvpypi740bzr1dlw0h0bw511wqz6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-try-lock-0.2
  (package
    (name "rust-try-lock")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "try-lock" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jqijrrvm1pyq34zn1jmy2vihd4jcrjlvsh4alkjahhssjnsn8g4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ttf-parser-0.20
  (package
    (name "rust-ttf-parser")
    (version "0.20.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ttf-parser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1d4n3p9ccjvy4mj72700i0c2q6d49dxjpwflw47q79rpv1v7vxqp"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-two-face-0.3
  (package
    (name "rust-two-face")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "two-face" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ssydfj3cdjf28pmy84wjhayp5s66xnr0b57zgpcfn94bc9x5gip"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-syntect" ,rust-syntect-5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typenum-1
  (package
    (name "rust-typenum")
    (version "1.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typenum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09dqxv69m9lj9zvv6xw5vxaqx15ps0vxyy5myg33i0kbqvq0pzs2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

; (define rust-typlite-0.11
;   (package
;     (name "rust-typlite")
;     (version "0.11.18")
;     (source
;       (origin
;         (method url-fetch)
;         (uri (crate-uri "typlite" version))
;         (file-name
;           (string-append name "-" version ".tar.gz"))
;         (sha256
;           (base32
;             "0000000000000000000000000000000000000000000000000000"))))
;     (build-system cargo-build-system)
;     (arguments
;       `(#:skip-build?
;         #t
;         #:cargo-inputs
;         (("rust-base64" ,rust-base64-0.22)
;          ("rust-comemo" ,rust-comemo-0.4)
;          ("rust-ecow" ,rust-ecow-0.2)
;          ("rust-insta" ,rust-insta-1)
;          ("rust-regex" ,rust-regex-1)
;          ("rust-typst" ,rust-typst-0.11)
;          ("rust-typst-assets" ,rust-typst-assets-0.11)
;          ("rust-typst-svg" ,rust-typst-svg-0.11)
;          ("rust-typst-syntax" ,rust-typst-syntax-0.11))))
;     (home-page "")
;     (synopsis "")
;     (description "")
;     (license #f)))

(define rust-typst-0.11
  (package
    (name "rust-typst")
    (version "0.11.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Myriad-Dreamin/typst")
               (commit "tinymist-v0.11.1-2")))
        (file-name (string-append (git-file-name name version) ".tar.gz"))
        (sha256
          (base32 "1k5mipd3ini6hhra3gvpikmfgd34q9smblc8q3ijz7lfhslzh1vm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #f
        #:cargo-inputs
        (("rust-az" ,rust-az-1)
         ("rust-bitflags" ,rust-bitflags-2)
         ("rust-chinese-number" ,rust-chinese-number-0.7)
         ("rust-ciborium" ,rust-ciborium-0.2)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-csv" ,rust-csv-1)
         ("rust-ecow" ,rust-ecow-0.2)
         ("rust-fontdb" ,rust-fontdb-0.16)
         ("rust-hayagriva" ,rust-hayagriva-0.5)
         ("rust-hypher" ,rust-hypher-0.1)
         ("rust-icu-properties" ,rust-icu-properties-1)
         ("rust-icu-provider" ,rust-icu-provider-1)
         ("rust-icu-provider-adapters"
          ,rust-icu-provider-adapters-1)
         ("rust-icu-provider-blob"
          ,rust-icu-provider-blob-1)
         ("rust-icu-segmenter" ,rust-icu-segmenter-1)
         ("rust-if-chain" ,rust-if-chain-1)
         ("rust-image" ,rust-image-0.24)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-kamadak-exif" ,rust-kamadak-exif-0.5)
         ("rust-kurbo" ,rust-kurbo-0.9)
         ("rust-lipsum" ,rust-lipsum-0.9)
         ("rust-log" ,rust-log-0.4)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-palette" ,rust-palette-0.7)
         ("rust-phf" ,rust-phf-0.11)
         ("rust-png" ,rust-png-0.17)
         ("rust-portable-atomic" ,rust-portable-atomic-1)
         ("rust-qcms" ,rust-qcms-0.3)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-roxmltree" ,rust-roxmltree-0.19)
         ("rust-rustybuzz" ,rust-rustybuzz-0.12)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-yaml" ,rust-serde-yaml-0.9)
         ("rust-siphasher" ,rust-siphasher-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-stacker" ,rust-stacker-0.1)
         ("rust-syntect" ,rust-syntect-5)
         ("rust-time" ,rust-time-0.3)
         ("rust-toml" ,rust-toml-0.8)
         ("rust-ttf-parser" ,rust-ttf-parser-0.20)
         ("rust-two-face" ,rust-two-face-0.3)
         ("rust-typed-arena" ,rust-typed-arena-2)
         ("rust-typst-assets" ,rust-typst-assets-0.11)
         ("rust-typst-macros" ,rust-typst-macros-0.11)
         ("rust-typst-syntax" ,rust-typst-syntax-0.11)
         ("rust-typst-timing" ,rust-typst-timing-0.11)
         ("rust-unicode-bidi" ,rust-unicode-bidi-0.3)
         ("rust-unicode-math-class"
          ,rust-unicode-math-class-0.1)
         ("rust-unicode-script" ,rust-unicode-script-0.5)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1)
         ("rust-usvg" ,rust-usvg-0.38)
         ("rust-wasmi" ,rust-wasmi-0.31))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-assets-0.11
  (package
    (name "rust-typst-assets")
    (version "0.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typst-assets" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "172dxzw8nvps72p9mi43568v8p25812v56hw933yxs38sbw62c1b"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-macros-0.11
  (package
    (name "rust-typst-macros")
    (version "0.11.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Myriad-Dreamin/typst")
               (commit "tinymist-v0.11.1-2")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1k5mipd3ini6hhra3gvpikmfgd34q9smblc8q3ijz7lfhslzh1vm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-heck" ,rust-heck-0.4)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-pdf-0.11
  (package
    (name "rust-typst-pdf")
    (version "0.11.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Myriad-Dreamin/typst")
               (commit "tinymist-v0.11.1-2")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1k5mipd3ini6hhra3gvpikmfgd34q9smblc8q3ijz7lfhslzh1vm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.22)
         ("rust-bytemuck" ,rust-bytemuck-1)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-ecow" ,rust-ecow-0.2)
         ("rust-image" ,rust-image-0.24)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.7)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-pdf-writer" ,rust-pdf-writer-0.9)
         ("rust-subsetter" ,rust-subsetter-0.1)
         ("rust-svg2pdf" ,rust-svg2pdf-0.10)
         ("rust-ttf-parser" ,rust-ttf-parser-0.20)
         ("rust-typst" ,rust-typst-0.11)
         ("rust-typst-assets" ,rust-typst-assets-0.11)
         ("rust-typst-macros" ,rust-typst-macros-0.11)
         ("rust-typst-timing" ,rust-typst-timing-0.11)
         ("rust-unicode-properties"
          ,rust-unicode-properties-0.1)
         ("rust-unscanny" ,rust-unscanny-0.1)
         ("rust-xmp-writer" ,rust-xmp-writer-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

; (define rust-typst-preview-0.11
;   (package
;     (name "rust-typst-preview")
;     (version "0.11.18")
;     (source
;       (origin
;         (method url-fetch)
;         (uri (crate-uri "typst-preview" version))
;         (file-name
;           (string-append name "-" version ".tar.gz"))
;         (sha256
;           (base32
;             "0000000000000000000000000000000000000000000000000000"))))
;     (build-system cargo-build-system)
;     (arguments
;       `(#:skip-build?
;         #t
;         #:cargo-inputs
;         (("rust-clap" ,rust-clap-4)
;          ("rust-comemo" ,rust-comemo-0.4)
;          ("rust-env-logger" ,rust-env-logger-0.11)
;          ("rust-futures" ,rust-futures-0.3)
;          ("rust-indexmap" ,rust-indexmap-2)
;          ("rust-log" ,rust-log-0.4)
;          ("rust-once-cell" ,rust-once-cell-1)
;          ("rust-serde" ,rust-serde-1)
;          ("rust-serde-json" ,rust-serde-json-1)
;          ("rust-tinymist-assets"
;           ,rust-tinymist-assets-0.11)
;          ("rust-tokio" ,rust-tokio-1)
;          ("rust-tokio-tungstenite"
;           ,rust-tokio-tungstenite-0.21)
;          ("rust-typst" ,rust-typst-0.11)
;          ("rust-typst-assets" ,rust-typst-assets-0.11)
;          ("rust-typst-ts-compiler"
;           ,rust-typst-ts-compiler-0.5)
;          ("rust-typst-ts-core" ,rust-typst-ts-core-0.5)
;          ("rust-typst-ts-svg-exporter"
;           ,rust-typst-ts-svg-exporter-0.5))))
;     (home-page "")
;     (synopsis "")
;     (description "")
;     (license #f)))

(define rust-typst-render-0.11
  (package
    (name "rust-typst-render")
    (version "0.11.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Myriad-Dreamin/typst")
               (commit "tinymist-v0.11.1-2")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1k5mipd3ini6hhra3gvpikmfgd34q9smblc8q3ijz7lfhslzh1vm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytemuck" ,rust-bytemuck-1)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-image" ,rust-image-0.24)
         ("rust-pixglyph" ,rust-pixglyph-0.3)
         ("rust-resvg" ,rust-resvg-0.38)
         ("rust-roxmltree" ,rust-roxmltree-0.19)
         ("rust-tiny-skia" ,rust-tiny-skia-0.11)
         ("rust-ttf-parser" ,rust-ttf-parser-0.20)
         ("rust-typst" ,rust-typst-0.11)
         ("rust-typst-macros" ,rust-typst-macros-0.11)
         ("rust-typst-timing" ,rust-typst-timing-0.11)
         ("rust-usvg" ,rust-usvg-0.38))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-svg-0.11
  (package
    (name "rust-typst-svg")
    (version "0.11.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Myriad-Dreamin/typst")
               (commit "tinymist-v0.11.1-2")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1k5mipd3ini6hhra3gvpikmfgd34q9smblc8q3ijz7lfhslzh1vm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.22)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-ecow" ,rust-ecow-0.2)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-ttf-parser" ,rust-ttf-parser-0.20)
         ("rust-typst" ,rust-typst-0.11)
         ("rust-typst-macros" ,rust-typst-macros-0.11)
         ("rust-typst-timing" ,rust-typst-timing-0.11)
         ("rust-xmlparser" ,rust-xmlparser-0.13)
         ("rust-xmlwriter" ,rust-xmlwriter-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-syntax-0.7
  (package
    (name "rust-typst-syntax")
    (version "0.7.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/typst/typst")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0f21zm37a0pswnn2jny39njr08s00xyyx3qhka0fla20a6d4xfya"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-comemo" ,rust-comemo-0.3)
         ("rust-ecow" ,rust-ecow-0.1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-unicode-ident" ,rust-unicode-ident-1)
         ("rust-unicode-math-class"
          ,rust-unicode-math-class-0.1)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1)
         ("rust-unscanny" ,rust-unscanny-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-syntax-0.11
  (package
    (name "rust-typst-syntax")
    (version "0.11.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Myriad-Dreamin/typst")
               (commit "tinymist-v0.11.1-2")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1k5mipd3ini6hhra3gvpikmfgd34q9smblc8q3ijz7lfhslzh1vm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-comemo" ,rust-comemo-0.4)
         ("rust-ecow" ,rust-ecow-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-unicode-ident" ,rust-unicode-ident-1)
         ("rust-unicode-math-class"
          ,rust-unicode-math-class-0.1)
         ("rust-unicode-script" ,rust-unicode-script-0.5)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1)
         ("rust-unscanny" ,rust-unscanny-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-timing-0.11
  (package
    (name "rust-typst-timing")
    (version "0.11.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Myriad-Dreamin/typst")
               (commit "tinymist-v0.11.1-2")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1k5mipd3ini6hhra3gvpikmfgd34q9smblc8q3ijz7lfhslzh1vm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-typst-syntax" ,rust-typst-syntax-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-ts-compiler-0.5
  (package
    (name "rust-typst-ts-compiler")
    (version "0.5.0-rc5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typst-ts-compiler" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k8gwa3f6mqmjl5r74chs1dgqljjilg34kk65m5k2a0wgd6klgxx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.22)
         ("rust-codespan-reporting"
          ,rust-codespan-reporting-0.11)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-fst" ,rust-fst-0.4)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-log" ,rust-log-0.4)
         ("rust-nohash-hasher" ,rust-nohash-hasher-0.2)
         ("rust-notify" ,rust-notify-6)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-pathdiff" ,rust-pathdiff-0.2)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-reflexo" ,rust-reflexo-0.5)
         ("rust-reflexo-vfs" ,rust-reflexo-vfs-0.5)
         ("rust-reflexo-world" ,rust-reflexo-world-0.5)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-typst" ,rust-typst-0.11)
         ("rust-typst-ts-core" ,rust-typst-ts-core-0.5)
         ("rust-typst-ts-svg-exporter"
          ,rust-typst-ts-svg-exporter-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-ts-core-0.5
  (package
    (name "rust-typst-ts-core")
    (version "0.5.0-rc5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typst-ts-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0qij2mjnbpasqhaj2v1mijlpjhy69wkpcbiac5fx3d3b6nfny0l1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.22)
         ("rust-base64-serde" ,rust-base64-serde-0.7)
         ("rust-bitvec" ,rust-bitvec-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-crossbeam-queue"
          ,rust-crossbeam-queue-0.3)
         ("rust-dashmap" ,rust-dashmap-5)
         ("rust-ecow" ,rust-ecow-0.2)
         ("rust-elsa" ,rust-elsa-1)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-fxhash" ,rust-fxhash-0.2)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-log" ,rust-log-0.4)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-path-clean" ,rust-path-clean-1)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-reflexo" ,rust-reflexo-0.5)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-repr" ,rust-serde-repr-0.1)
         ("rust-serde-with" ,rust-serde-with-3)
         ("rust-sha2" ,rust-sha2-0.10)
         ("rust-siphasher" ,rust-siphasher-1)
         ("rust-svgtypes" ,rust-svgtypes-0.13)
         ("rust-tiny-skia" ,rust-tiny-skia-0.11)
         ("rust-tiny-skia-path" ,rust-tiny-skia-path-0.11)
         ("rust-ttf-parser" ,rust-ttf-parser-0.20)
         ("rust-typst" ,rust-typst-0.11)
         ("rust-xmlparser" ,rust-xmlparser-0.13))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typst-ts-svg-exporter-0.5
  (package
    (name "rust-typst-ts-svg-exporter")
    (version "0.5.0-rc5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typst-ts-svg-exporter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "067k482h2rxhk7knkf9s197pgafw2wbsdbkmvb8z7gd1ddcdc9b4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.22)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-log" ,rust-log-0.4)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-reflexo" ,rust-reflexo-0.5)
         ("rust-siphasher" ,rust-siphasher-1)
         ("rust-tiny-skia" ,rust-tiny-skia-0.11)
         ("rust-typst" ,rust-typst-0.11)
         ("rust-typst-ts-core" ,rust-typst-ts-core-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typstfmt-lib-0.2
  (package
    (name "rust-typstfmt-lib")
    (version "0.2.7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/astrale-sharp/typstfmt")
               (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32 "04my3xl6vfny7nlygch7xaqv1i85diyxrnfyh6jlzyfr5162q5ic"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #f
        #:cargo-inputs
        (("rust-globmatch" ,rust-globmatch-0.2)
         ("rust-itertools" ,rust-itertools-0.10)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-toml" ,rust-toml-0.7)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-typst-syntax" ,rust-typst-syntax-0.7)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typstyle-0.11
  (package
    (name "rust-typstyle")
    (version "0.11.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typstyle" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1f4219gvb8bfa1m8kpy7ci3k3zvw1h0szwm08c7vqxam33dypyp9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-itertools" ,rust-itertools-0.13)
         ("rust-pretty" ,rust-pretty-0.12)
         ("rust-typst-syntax" ,rust-typst-syntax-0.11)
         ("rust-vergen" ,rust-vergen-8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unic-langid-0.9
  (package
    (name "rust-unic-langid")
    (version "0.9.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0i2s024frmpfa68lzy8y8vnb1rz3m9v0ga13f7h2afx7f8g9vp93"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unic-langid-impl"
          ,rust-unic-langid-impl-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unic-langid-impl-0.9
  (package
    (name "rust-unic-langid-impl")
    (version "0.9.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rckyn5wqd5h8jxhbzlbbagr459zkzg822r4k5n30jaryv0j4m0a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-tinystr" ,rust-tinystr-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-bidi-0.3
  (package
    (name "rust-unicode-bidi")
    (version "0.3.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-bidi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xcdxm7h0ydyprwpcbh436rbs6s6lph7f3gr527lzgv6lw053y88"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-bidi-mirroring-0.1
  (package
    (name "rust-unicode-bidi-mirroring")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-bidi-mirroring" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "150navn2n6barkzchv96n877i17m1754nzmy1282zmcjzdh25lan"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-ccc-0.1
  (package
    (name "rust-unicode-ccc")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-ccc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wbwny92wzmck2cix5h3r97h9z57x9831kadrs6jdy24lvpj09fc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-math-class-0.1
  (package
    (name "rust-unicode-math-class")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-math-class" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rbxcjirldpdrpxv1l7qiadbib8rnl7b413fsp4f7ynmk7snq93x"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-normalization-0.1
  (package
    (name "rust-unicode-normalization")
    (version "0.1.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-normalization" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1x81a50h2zxigj74b9bqjsirxxbyhmis54kg600xj213vf31cvd5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinyvec" ,rust-tinyvec-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-properties-0.1
  (package
    (name "rust-unicode-properties")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-properties" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14cjbmfs64qw1m4qzpfa673a8rpyhp5h9f412mkg1n958jfrs9g4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-script-0.5
  (package
    (name "rust-unicode-script")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-script" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kf9v1yfxazxjx07g9g9nqg4kw2kzpnyi7syjdd2hpvffbsp33dd"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-vo-0.1
  (package
    (name "rust-unicode-vo")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-vo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "151sha088v9jyfvbg5164xh4dk72g53b82xm4zzbf5dlagzqdlxi"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-width-0.1
  (package
    (name "rust-unicode-width")
    (version "0.1.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-width" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p92vl8n7qc8mxz45xn6qbgi0259z96n32a158l6vj5bywwdadh3"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unsafe-libyaml-0.2
  (package
    (name "rust-unsafe-libyaml")
    (version "0.2.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unsafe-libyaml" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0qdq69ffl3v5pzx9kzxbghzn0fzn266i1xn70y88maybz9csqfk7"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unscanny-0.1
  (package
    (name "rust-unscanny")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unscanny" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ivbipc1rnq15fhzgna41p1h01ncq4shycii72f3x5d7czq2mpz9"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-url-2
  (package
    (name "rust-url")
    (version "2.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "url" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0v2dx50mx7xzl9454cl5qmpjnhkbahmn59gd3apyipbgyyylsy12"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-form-urlencoded" ,rust-form-urlencoded-1)
         ("rust-idna" ,rust-idna-0.5)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-usvg-0.38
  (package
    (name "rust-usvg")
    (version "0.38.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "usvg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "098n1l980dz58fcrlxrsy7k584dc3nmq1an1aj3dwwy1lfs64zrp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.21)
         ("rust-log" ,rust-log-0.4)
         ("rust-pico-args" ,rust-pico-args-0.5)
         ("rust-usvg-parser" ,rust-usvg-parser-0.38)
         ("rust-usvg-text-layout"
          ,rust-usvg-text-layout-0.38)
         ("rust-usvg-tree" ,rust-usvg-tree-0.38)
         ("rust-xmlwriter" ,rust-xmlwriter-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-usvg-parser-0.38
  (package
    (name "rust-usvg-parser")
    (version "0.38.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "usvg-parser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c0lk23aij52hwjx9mrhz7ffyyljld044wvgji76ng82ybk0a6im"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-data-url" ,rust-data-url-0.3)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-imagesize" ,rust-imagesize-0.12)
         ("rust-kurbo" ,rust-kurbo-0.9)
         ("rust-log" ,rust-log-0.4)
         ("rust-roxmltree" ,rust-roxmltree-0.19)
         ("rust-simplecss" ,rust-simplecss-0.2)
         ("rust-siphasher" ,rust-siphasher-0.3)
         ("rust-svgtypes" ,rust-svgtypes-0.13)
         ("rust-usvg-tree" ,rust-usvg-tree-0.38))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-usvg-text-layout-0.38
  (package
    (name "rust-usvg-text-layout")
    (version "0.38.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "usvg-text-layout" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qj6vgk7244by314j5wgp58k5s5xgc29vbrfhpz33x2wkn5qhhcc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fontdb" ,rust-fontdb-0.16)
         ("rust-kurbo" ,rust-kurbo-0.9)
         ("rust-log" ,rust-log-0.4)
         ("rust-rustybuzz" ,rust-rustybuzz-0.12)
         ("rust-unicode-bidi" ,rust-unicode-bidi-0.3)
         ("rust-unicode-script" ,rust-unicode-script-0.5)
         ("rust-unicode-vo" ,rust-unicode-vo-0.1)
         ("rust-usvg-tree" ,rust-usvg-tree-0.38))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-usvg-tree-0.38
  (package
    (name "rust-usvg-tree")
    (version "0.38.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "usvg-tree" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01czj22qlzdy5p7y6i1j58k4z7yv8q8mnb1narp3s5gd0h23x1hq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-strict-num" ,rust-strict-num-0.1)
         ("rust-svgtypes" ,rust-svgtypes-0.13)
         ("rust-tiny-skia-path" ,rust-tiny-skia-path-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-utf8-iter-1
  (package
    (name "rust-utf8-iter")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "utf8_iter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gmna9flnj8dbyd8ba17zigrp9c4c3zclngf5lnb5yvz1ri41hdn"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-utf8parse-0.2
  (package
    (name "rust-utf8parse")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "utf8parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "088807qwjq46azicqwbhlmzwrbkz7l4hpw43sdkdyyk524vdxaq6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-uuid-1
  (package
    (name "rust-uuid")
    (version "1.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uuid" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0503gvp08dh5mnm3f0ffqgisj6x3mbs53dmnn1lm19pga43a1pw1"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-vergen-8
  (package
    (name "rust-vergen")
    (version "8.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vergen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ri5n4k1g4z6gnllkjx9zny3vaa2bjma84zlrjh6w9k7b7mdk419"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-cargo-metadata" ,rust-cargo-metadata-0.18)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-rustc-version" ,rust-rustc-version-0.4)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-time" ,rust-time-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-0.2
  (package
    (name "rust-wasm-bindgen")
    (version "0.2.92")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1a4mcw13nsk3fr8fxjzf9kk1wj88xkfsmnm0pjraw01ryqfm7qjb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-wasm-bindgen-macro"
          ,rust-wasm-bindgen-macro-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-backend-0.2
  (package
    (name "rust-wasm-bindgen-backend")
    (version "0.2.92")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-backend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nj7wxbi49f0rw9d44rjzms26xlw6r76b2mrggx8jfbdjrxphkb1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bumpalo" ,rust-bumpalo-3)
         ("rust-log" ,rust-log-0.4)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2)
         ("rust-wasm-bindgen-shared"
          ,rust-wasm-bindgen-shared-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-futures-0.4
  (package
    (name "rust-wasm-bindgen-futures")
    (version "0.4.42")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-futures" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1h322zjvpjllcpj7dahfxjsv6inkr6y0baw7nkdwivr1c4v19g3n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-web-sys" ,rust-web-sys-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-macro-0.2
  (package
    (name "rust-wasm-bindgen-macro")
    (version "0.2.92")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09npa1srjjabd6nfph5yc03jb26sycjlxhy0c2a1pdrpx4yq5y51"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-quote" ,rust-quote-1)
         ("rust-wasm-bindgen-macro-support"
          ,rust-wasm-bindgen-macro-support-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-macro-support-0.2
  (package
    (name "rust-wasm-bindgen-macro-support")
    (version "0.2.92")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro-support" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1dqv2xs8zcyw4kjgzj84bknp2h76phmsb3n7j6hn396h4ssifkz9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2)
         ("rust-wasm-bindgen-backend"
          ,rust-wasm-bindgen-backend-0.2)
         ("rust-wasm-bindgen-shared"
          ,rust-wasm-bindgen-shared-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-shared-0.2
  (package
    (name "rust-wasm-bindgen-shared")
    (version "0.2.92")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15kyavsrna2cvy30kg03va257fraf9x00ny554vxngvpyaa0q6dg"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmi-0.31
  (package
    (name "rust-wasmi")
    (version "0.31.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1isnchb3vd6cndfrjfiql6nhq9qcvnfzlgkaqxady3363lfjia3p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-smallvec" ,rust-smallvec-1)
         ("rust-spin" ,rust-spin-0.9)
         ("rust-wasmi-arena" ,rust-wasmi-arena-0.4)
         ("rust-wasmi-core" ,rust-wasmi-core-0.13)
         ("rust-wasmparser-nostd"
          ,rust-wasmparser-nostd-0.100))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmi-arena-0.4
  (package
    (name "rust-wasmi-arena")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmi_arena" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wvhfah2ccvhl4vwycbncfcnb78ndgbkac3v56n0qms4prrpyjhh"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmi-core-0.13
  (package
    (name "rust-wasmi-core")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmi_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sl5356hb8iz3l297jvd4ml62fhcq0h2f031qa2mpydz6kdsgwfw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-downcast-rs" ,rust-downcast-rs-1)
         ("rust-libm" ,rust-libm-0.2)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-paste" ,rust-paste-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmparser-nostd-0.100
  (package
    (name "rust-wasmparser-nostd")
    (version "0.100.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmparser-nostd" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ak4bi9k9jb223xw7mlxkgim6lp7m8bwfqhlpfa4ll7kjpz1b86m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-indexmap-nostd" ,rust-indexmap-nostd-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-web-sys-0.3
  (package
    (name "rust-web-sys")
    (version "0.3.69")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "web-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vqkxk935xa8zcnsi4bd88sb267ly2i24xl1yiq26d1n32hskbvp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-weezl-0.1
  (package
    (name "rust-weezl")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "weezl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10lhndjgs6y5djpg3b420xngcr6jkmv70q8rb1qcicbily35pa2k"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winapi-util-0.1
  (package
    (name "rust-winapi-util")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0svcgddd2rw06mj4r76gj655qsa1ikgz3d3gzax96fz7w62c6k2d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-targets-0.52
  (package
    (name "rust-windows-targets")
    (version "0.52.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-targets" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wwrx625nwlfp7k93r2rra568gad1mwd888h1jwnl0vfg5r4ywlv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-aarch64-gnullvm"
          ,rust-windows-aarch64-gnullvm-0.52)
         ("rust-windows-aarch64-msvc"
          ,rust-windows-aarch64-msvc-0.52)
         ("rust-windows-i686-gnu"
          ,rust-windows-i686-gnu-0.52)
         ("rust-windows-i686-gnullvm"
          ,rust-windows-i686-gnullvm-0.52)
         ("rust-windows-i686-msvc"
          ,rust-windows-i686-msvc-0.52)
         ("rust-windows-x86-64-gnu"
          ,rust-windows-x86-64-gnu-0.52)
         ("rust-windows-x86-64-gnullvm"
          ,rust-windows-x86-64-gnullvm-0.52)
         ("rust-windows-x86-64-msvc"
          ,rust-windows-x86-64-msvc-0.52))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-gnullvm-0.52
  (package
    (name "rust-windows-aarch64-gnullvm")
    (version "0.52.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1lrcq38cr2arvmz19v32qaggvj8bh1640mdm9c2fr877h0hn591j"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-msvc-0.52
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.52.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sfl0nysnz32yyfh773hpi49b1q700ah6y7sacmjbqjjn5xjmv09"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-gnu-0.52
  (package
    (name "rust-windows-i686-gnu")
    (version "0.52.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02zspglbykh1jh9pi7gn8g1f97jh1rrccni9ivmrfbl0mgamm6wf"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-gnullvm-0.52
  (package
    (name "rust-windows-i686-gnullvm")
    (version "0.52.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rpdx1537mw6slcpqa0rm3qixmsb79nbhqy5fsm3q2q9ik9m5vhf"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-msvc-0.52
  (package
    (name "rust-windows-i686-msvc")
    (version "0.52.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rkcqmp4zzmfvrrrx01260q3xkpzi6fzi2x2pgdcdry50ny4h294"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnu-0.52
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.52.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0y0sifqcb56a56mvn7xjgs8g43p33mfqkd8wj1yhrgxzma05qyhl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnullvm-0.52
  (package
    (name "rust-windows-x86-64-gnullvm")
    (version "0.52.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03gda7zjx1qh8k9nnlgb7m3w3s1xkysg55hkd1wjch8pqhyv5m94"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-msvc-0.52
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.52.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1v7rb5cibyzx8vak29pdrk8nx9hycsjs4w0jgms08qk49jl6v7sq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winnow-0.6
  (package
    (name "rust-winnow")
    (version "0.6.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winnow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "189b0mrr9lkckdyr0177hwj1c59igxc2lsl71f4wg8wrqbvfbdar"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-writeable-0.5
  (package
    (name "rust-writeable")
    (version "0.5.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "writeable" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lawr6y0bwqfyayf3z8zmqlhpnzhdx0ahs54isacbhyjwa7g778y"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-xmlparser-0.13
  (package
    (name "rust-xmlparser")
    (version "0.13.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "xmlparser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1r796g21c70p983ax0j6rmhzmalg4rhx61mvd4farxdhfyvy1zk6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-xmlwriter-0.1
  (package
    (name "rust-xmlwriter")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "xmlwriter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fg0ldmkgiis6hnxpi1c9gy7v23y0lpi824bp8yp12fi3r82lypc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-xmp-writer-0.2
  (package
    (name "rust-xmp-writer")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "xmp-writer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "128r1sfrs9zafv5ndh71f3a06znabcb6rip9w4clpab4iw9vlhs5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-yaml-rust2-0.8
  (package
    (name "rust-yaml-rust2")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yaml-rust2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1y0mf6n1jnf88xqfv5ppicnm5jg3fl57dmp9vd2v2bvg9q61c0l9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arraydeque" ,rust-arraydeque-0.5)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-hashlink" ,rust-hashlink-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-yoke-0.7
  (package
    (name "rust-yoke")
    (version "0.7.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yoke" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "198c4jkh6i3hxijia7mfa4cpnxg1iqym9bz364697c3rn0a16nvc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-stable-deref-trait"
          ,rust-stable-deref-trait-1)
         ("rust-yoke-derive" ,rust-yoke-derive-0.7)
         ("rust-zerofrom" ,rust-zerofrom-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-yoke-derive-0.7
  (package
    (name "rust-yoke-derive")
    (version "0.7.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yoke-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15cvhkci2mchfffx3fmva84fpmp34dsmnbzibwfnzjqq3ds33k18"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2)
         ("rust-synstructure" ,rust-synstructure-0.13))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zerocopy-0.7
  (package
    (name "rust-zerocopy")
    (version "0.7.35")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerocopy" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1w36q7b9il2flg0qskapgi9ymgg7p985vniqd09vi0mwib8lz6qv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zerocopy-derive"
          ,rust-zerocopy-derive-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zerocopy-derive-0.7
  (package
    (name "rust-zerocopy-derive")
    (version "0.7.35")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerocopy-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gnf2ap2y92nwdalzz3x7142f2b83sni66l39vxp2ijd6j080kzs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zerofrom-0.1
  (package
    (name "rust-zerofrom")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerofrom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mdbjd7vmbix2ynxbrbrrli47a5yrpfx05hi99wf1l4pwwf13v4i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zerofrom-derive"
          ,rust-zerofrom-derive-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zerofrom-derive-0.1
  (package
    (name "rust-zerofrom-derive")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerofrom-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19b31rrs2ry1lrq5mpdqjzgg65va51fgvwghxnf6da3ycfiv99qf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2)
         ("rust-synstructure" ,rust-synstructure-0.13))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zerotrie-0.1
  (package
    (name "rust-zerotrie")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerotrie" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07qa5ljss8j706iy0rd023naamwly4jfwz0pc1gmqcw7bpalsngv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-litemap" ,rust-litemap-0.7)
         ("rust-serde" ,rust-serde-1)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zerovec-0.10
  (package
    (name "rust-zerovec")
    (version "0.10.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerovec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yghix7n3fjfdppwghknzvx9v8cf826h2qal5nqvy8yzg4yqjaxa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-yoke" ,rust-yoke-0.7)
         ("rust-zerofrom" ,rust-zerofrom-0.1)
         ("rust-zerovec-derive" ,rust-zerovec-derive-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zerovec-derive-0.10
  (package
    (name "rust-zerovec-derive")
    (version "0.10.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerovec-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ik322dys6wnap5d3gcsn09azmssq466xryn5czfm13mn7gsdbvf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-syntax substitute-cargo-git
  (syntax-rules ()
    [(_ (pkg-name0 v0))
     (((string-append pkg-name0 " = \\{ git = .+")
       (string-append pkg-name0 " = \"" v0 "\"\n")))]
    [(_ (pkg-name1 v1)
        (pkg-name2 v2)
        ...)
     (substitute* "Cargo.toml"
      (((string-append pkg-name1 " = \\{ git = .+"))
       (string-append pkg-name1 " = \"" v1 "\"\n"))
      (((string-append pkg-name2 " = \\{ git = .+"))
       (string-append pkg-name2 " = \"" v2 "\"\n"))
      ...)]))

(define tinymist
  (package
    (name "tinymist")
    (version "0.11.18")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Myriad-Dreamin/tinymist")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0nzd431mdfv3afxbg84c8ahhpfag9p92mk888b3rlz471q7gdmys"))
        (snippet
        #~(begin (use-modules (guix build utils)
                              (ice-9 match)
                              (srfi srfi-1))
                 (substitute* "Cargo.toml"
                   (("typstfmt_lib = \\{ git =.+")
                    "typstfmt_lib = \"0.2.7\"\n"))))
        ))
    (build-system cargo-build-system)
    (arguments
      `(
        #:modules ((guix build utils)
                   (guix build cargo-build-system)
                   (ice-9 match))
        #:phases
        ,#~(modify-phases %standard-phases
             (add-after 'unpack 'unpack-additional-sources
               (lambda _
                 (for-each make-file-writable (find-files "."))
                 (for-each
                   (match-lambda
                     ((dst src)
                      (let* ((dest (string-append "guix-vendor/" dst)))
                        (copy-recursively src dest)
                        (for-each make-file-writable (find-files dest)))))
                   '(("typst"    #$additional-typst)))
                 )))
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-base64" ,rust-base64-0.22)
         ("rust-cargo-metadata" ,rust-cargo-metadata-0.18)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-clap" ,rust-clap-4)
         ("rust-clap-builder" ,rust-clap-builder-4)
         ("rust-clap-complete" ,rust-clap-complete-4)
         ("rust-clap-complete-fig" ,rust-clap-complete-fig-4)
         ("rust-clap-mangen" ,rust-clap-mangen-0.2)
         ("rust-codespan-reporting" ,rust-codespan-reporting-0.11)
         ("rust-comemo" ,rust-comemo-0.4)
         ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
         ("rust-dhat" ,rust-dhat-0.3)
         ("rust-env-logger" ,rust-env-logger-0.11)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-itertools" ,rust-itertools-0.13)
         ("rust-log" ,rust-log-0.4)
         ("rust-lsp-server" ,rust-lsp-server-0.7)
         ("rust-lsp-types" ,rust-lsp-types-0.95)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-open" ,rust-open-5)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-paste" ,rust-paste-1)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-reflexo" ,rust-reflexo-0.5)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-yaml" ,rust-serde-yaml-0.9)
         ; ("rust-sync-lsp" ,rust-sync-lsp-0.11)
         ("rust-tinymist-assets" ,rust-tinymist-assets-0.11)
         ; ("rust-tinymist-query" ,rust-tinymist-query-0.11)
         ; ("rust-tinymist-render" ,rust-tinymist-render-0.11)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-toml" ,rust-toml-0.8)
         ("rust-tower-layer" ,rust-tower-layer-0.3)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ; ("rust-typlite" ,rust-typlite-0.11)
         ("rust-typst" ,rust-typst-0.11)
         ("rust-typst-assets" ,rust-typst-assets-0.11)
         ("rust-typst-pdf" ,rust-typst-pdf-0.11)
         ; ("rust-typst-preview" ,rust-typst-preview-0.11)
         ("rust-typst-render" ,rust-typst-render-0.11)
         ("rust-typst-svg" ,rust-typst-svg-0.11)
         ("rust-typst-timing" ,rust-typst-timing-0.11)
         ("rust-typst-ts-compiler" ,rust-typst-ts-compiler-0.5)
         ("rust-typst-ts-core" ,rust-typst-ts-core-0.5)
         ("rust-typst-ts-svg-exporter" ,rust-typst-ts-svg-exporter-0.5)
         ("rust-typstfmt-lib" ,rust-typstfmt-lib-0.2)
         ("rust-typstyle" ,rust-typstyle-0.11)
         ("rust-unicode-script" ,rust-unicode-script-0.5)
         ("rust-vergen" ,rust-vergen-8)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

tinymist
