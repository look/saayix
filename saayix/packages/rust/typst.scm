;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages rust typst)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-check)
  #:use-module (gnu packages crates-compression)
  #:use-module (gnu packages crates-crypto)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-tls)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages tls)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define rust-xmlparser-0.13
  (package
    (name "rust-xmlparser")
    (version "0.13.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "xmlparser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1r796g21c70p983ax0j6rmhzmalg4rhx61mvd4farxdhfyvy1zk6"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/RazrFalcon/xmlparser")
    (synopsis "Pull-based, zero-allocation XML parser")
    (description
     "This package provides Pull-based, zero-allocation XML parser.")
    (license (list license:expat license:asl2.0))))

(define rust-typst-svg-0.11
  (package
    (name "rust-typst-svg")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-svg" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0fz2maws498x19ai7h421s4jab2b6zlazq09zgm1rj3909jf7x6f"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-base64" ,rust-base64-0.22)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.20)
                       ("rust-typst" ,rust-typst-0.11)
                       ("rust-typst-macros" ,rust-typst-macros-0.11)
                       ("rust-typst-timing" ,rust-typst-timing-0.11)
                       ("rust-xmlparser" ,rust-xmlparser-0.13)
                       ("rust-xmlwriter" ,rust-xmlwriter-0.1))))
    (home-page "https://typst.app")
    (synopsis "SVG exporter for Typst")
    (description "This package provides SVG exporter for Typst.")
    (license license:asl2.0)))

(define rust-pixglyph-0.3
  (package
    (name "rust-pixglyph")
    (version "0.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pixglyph" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "04m7l6y49nimb8pw9x6mqxjqcy248p2c705q4n0v6z8r9jnziq72"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-ttf-parser" ,rust-ttf-parser-0.20))))
    (home-page "https://github.com/typst/pixglyph")
    (synopsis "Font-rendering with subpixel positioning")
    (description
     "This package provides Font-rendering with subpixel positioning.")
    (license license:asl2.0)))

(define rust-typst-render-0.11
  (package
    (name "rust-typst-render")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-render" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0zmdnsv5nd3qw9zm7anv494av9f1pp7lwi5cwpyq6mp6znljy4kg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-image" ,rust-image-0.24)
                       ("rust-pixglyph" ,rust-pixglyph-0.3)
                       ("rust-resvg" ,rust-resvg-0.38)
                       ("rust-roxmltree" ,rust-roxmltree-0.19)
                       ("rust-tiny-skia" ,rust-tiny-skia-0.11)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.20)
                       ("rust-typst" ,rust-typst-0.11)
                       ("rust-typst-macros" ,rust-typst-macros-0.11)
                       ("rust-typst-timing" ,rust-typst-timing-0.11)
                       ("rust-usvg" ,rust-usvg-0.38))))
    (home-page "https://typst.app")
    (synopsis "Raster image exporter for Typst")
    (description "This package provides Raster image exporter for Typst.")
    (license license:asl2.0)))

(define rust-xmp-writer-0.2
  (package
    (name "rust-xmp-writer")
    (version "0.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "xmp-writer" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "128r1sfrs9zafv5ndh71f3a06znabcb6rip9w4clpab4iw9vlhs5"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/typst/xmp-writer")
    (synopsis "Write XMP metadata, step by step")
    (description "This package provides Write XMP metadata, step by step.")
    (license (list license:expat license:asl2.0))))

(define rust-resvg-0.38
  (package
    (name "rust-resvg")
    (version "0.38.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "resvg" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "07hcwi72y0cybyj45n2hq95gj58wyg3jsam61d3hd7lm8q850d2w"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-gif" ,rust-gif-0.12)
                       ("rust-jpeg-decoder" ,rust-jpeg-decoder-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-pico-args" ,rust-pico-args-0.5)
                       ("rust-png" ,rust-png-0.17)
                       ("rust-rgb" ,rust-rgb-0.8)
                       ("rust-svgtypes" ,rust-svgtypes-0.13)
                       ("rust-tiny-skia" ,rust-tiny-skia-0.11)
                       ("rust-usvg" ,rust-usvg-0.38))
       #:cargo-development-inputs (("rust-once-cell" ,rust-once-cell-1))))
    (home-page "https://github.com/RazrFalcon/resvg")
    (synopsis "An SVG rendering library")
    (description "This package provides An SVG rendering library.")
    (license license:mpl2.0)))

(define rust-svg2pdf-0.10
  (package
    (name "rust-svg2pdf")
    (version "0.10.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "svg2pdf" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0gk9nd92nlp40r5ickfjqj31nqkvfmx255v5v5zlks1b0qqb6dms"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-image" ,rust-image-0.24)
                       ("rust-miniz-oxide" ,rust-miniz-oxide-0.7)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pdf-writer" ,rust-pdf-writer-0.9)
                       ("rust-resvg" ,rust-resvg-0.38)
                       ("rust-tiny-skia" ,rust-tiny-skia-0.11)
                       ("rust-usvg" ,rust-usvg-0.38))))
    (home-page "https://github.com/typst/svg2pdf")
    (synopsis "Convert SVG files to PDFs")
    (description "This package provides Convert SVG files to PDFs.")
    (license (list license:expat license:asl2.0))))

(define rust-ttf-parser-0.15
  (package
    (name "rust-ttf-parser")
    (version "0.15.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ttf-parser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1pfqn06vjlr6pvlljjmkmcb2kb8ind09q5f78nvxc3nqp74hcgkv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-base64" ,rust-base64-0.13)
                                   ("rust-pico-args" ,rust-pico-args-0.5)
                                   ("rust-xmlwriter" ,rust-xmlwriter-0.1))))
    (home-page "https://github.com/RazrFalcon/ttf-parser")
    (synopsis
     "high-level, safe, zero-allocation font parser for TrueType, OpenType, and AAT.")
    (description
     "This package provides a high-level, safe, zero-allocation font parser for
@code{TrueType}, @code{OpenType}, and AAT.")
    (license (list license:expat license:asl2.0))))

(define rust-subsetter-0.1
  (package
    (name "rust-subsetter")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "subsetter" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ygjaz4nd8m5k5h966s3i3wqgiy78nz5jk5x00ibm2gz7flbish9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-ttf-parser" ,rust-ttf-parser-0.15))))
    (home-page "https://github.com/typst/subsetter")
    (synopsis "Reduces the size and coverage of OpenType fonts")
    (description
     "This package provides Reduces the size and coverage of @code{OpenType} fonts.")
    (license (list license:expat license:asl2.0))))

(define rust-pdf-writer-0.9
  (package
    (name "rust-pdf-writer")
    (version "0.9.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pdf-writer" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0arksb7ahf389q0r7gjm3y3x4fh4v7nckahwcrp82g06ams15s94"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-itoa" ,rust-itoa-1)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-ryu" ,rust-ryu-1))
       #:cargo-development-inputs (("rust-iai" ,rust-iai-0.1)
                                   ("rust-image" ,rust-image-0.24)
                                   ("rust-miniz-oxide" ,rust-miniz-oxide-0.5))))
    (home-page "https://github.com/typst/pdf-writer")
    (synopsis "step-by-step PDF writer.")
    (description "This package provides a step-by-step PDF writer.")
    (license (list license:expat license:asl2.0))))

(define rust-typst-pdf-0.11
  (package
    (name "rust-typst-pdf")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-pdf" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1mg0lhkk5zyf6rsk0a5vnizvs7xfj1j642113g9pdr9h8fv47xsl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-base64" ,rust-base64-0.22)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-image" ,rust-image-0.24)
                       ("rust-miniz-oxide" ,rust-miniz-oxide-0.7)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pdf-writer" ,rust-pdf-writer-0.9)
                       ("rust-subsetter" ,rust-subsetter-0.1)
                       ("rust-svg2pdf" ,rust-svg2pdf-0.10)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.20)
                       ("rust-typst" ,rust-typst-0.11)
                       ("rust-typst-assets" ,rust-typst-assets-0.11)
                       ("rust-typst-macros" ,rust-typst-macros-0.11)
                       ("rust-typst-timing" ,rust-typst-timing-0.11)
                       ("rust-unicode-properties" ,rust-unicode-properties-0.1)
                       ("rust-unscanny" ,rust-unscanny-0.1)
                       ("rust-xmp-writer" ,rust-xmp-writer-0.2))))
    (home-page "https://typst.app")
    (synopsis "PDF exporter for Typst")
    (description "This package provides PDF exporter for Typst.")
    (license license:asl2.0)))

(define rust-pcg-mwc-0.2
  (package
    (name "rust-pcg-mwc")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pcg-mwc" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0z6cpz8x387iqcx8kjnqfihgggi0yngqx73zwjz132y56f38a5i2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-rand-core" ,rust-rand-core-0.6)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/tkaitchuck/Mwc256XXA64")
    (synopsis "fast non-cryptographic psudo random number generator")
    (description
     "This package provides a fast non-cryptographic psudo random number generator.")
    (license (list license:expat license:asl2.0))))

(define rust-ahash-0.8
  (package
    (name "rust-ahash")
    (version "0.8.11")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ahash" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "04chdfkls5xmhp1d48gnjsmglbqibizs3bpbj6rsj604m10si7g8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-atomic-polyfill" ,rust-atomic-polyfill-1)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-const-random" ,rust-const-random-0.1)
                       ("rust-getrandom" ,rust-getrandom-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-version-check" ,rust-version-check-0.9)
                       ("rust-zerocopy" ,rust-zerocopy-0.7))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-fnv" ,rust-fnv-1)
                                   ("rust-fxhash" ,rust-fxhash-0.2)
                                   ("rust-hashbrown" ,rust-hashbrown-0.14)
                                   ("rust-hex" ,rust-hex-0.4)
                                   ("rust-no-panic" ,rust-no-panic-0.1)
                                   ("rust-pcg-mwc" ,rust-pcg-mwc-0.2)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-seahash" ,rust-seahash-4)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-smallvec" ,rust-smallvec-1))))
    (home-page "https://github.com/tkaitchuck/ahash")
    (synopsis
     "non-cryptographic hash function using AES-NI for high performance")
    (description
     "This package provides a non-cryptographic hash function using AES-NI for high
performance.")
    (license (list license:expat license:asl2.0))))

(define rust-wasmparser-0.215
  (package
    (name "rust-wasmparser")
    (version "0.215.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmparser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "03nryws9f2awvfd47z5pn67aqif1z7w6z6zl3jw9jhgjh44dxysk"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-ahash" ,rust-ahash-0.8)
                       ("rust-bitflags" ,rust-bitflags-2)
                       ("rust-hashbrown" ,rust-hashbrown-0.14)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-anyhow" ,rust-anyhow-1)
                                   ("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-env-logger" ,rust-env-logger-0.11)
                                   ("rust-log" ,rust-log-0.4)
                                   ("rust-once-cell" ,rust-once-cell-1)
                                   ("rust-rayon" ,rust-rayon-1))))
    (home-page
     "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wasmparser")
    (synopsis
     "simple event-driven library for parsing WebAssembly binary files.")
    (description
     "This package provides a simple event-driven library for parsing
@code{WebAssembly} binary files.")
    (license (list license:asl2.0 license:asl2.0
                   license:expat))))

(define rust-wasm-encoder-0.215
  (package
    (name "rust-wasm-encoder")
    (version "0.215.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasm-encoder" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0iw886v4jzvbwxakiswsk81824m51fjnjabxwdvnp3kbw3rnvdag"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-leb128" ,rust-leb128-0.2)
                       ("rust-wasmparser" ,rust-wasmparser-0.215))
       #:cargo-development-inputs (("rust-anyhow" ,rust-anyhow-1)
                                   ("rust-tempfile" ,rust-tempfile-3))))
    (home-page
     "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wasm-encoder")
    (synopsis "low-level WebAssembly encoder.")
    (description
     "This package provides a low-level @code{WebAssembly} encoder.")
    (license (list license:asl2.0 license:asl2.0
                   license:expat))))

(define rust-gimli-0.30
  (package
    (name "rust-gimli")
    (version "0.30.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gimli" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1jfz1sj9h0rnlhjnmy315db2drsn54f0b5qcpndvl8lpprzxkqg2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
                       ("rust-fallible-iterator" ,rust-fallible-iterator-0.3)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-rustc-std-workspace-alloc" ,rust-rustc-std-workspace-alloc-1)
                       ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
                       ("rust-stable-deref-trait" ,rust-stable-deref-trait-1))
       #:cargo-development-inputs (("rust-test-assembler" ,rust-test-assembler-0.1))))
    (home-page "https://github.com/gimli-rs/gimli")
    (synopsis "library for reading and writing the DWARF debugging format.")
    (description
     "This package provides a library for reading and writing the DWARF debugging
format.")
    (license (list license:expat license:asl2.0))))

(define rust-wast-215
  (package
    (name "rust-wast")
    (version "215.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wast" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1jslxdhsnrjfky3a41iqvk2g2hhzgi5f02vjc2g294rmi46x1w8z"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bumpalo" ,rust-bumpalo-3)
                       ("rust-gimli" ,rust-gimli-0.30)
                       ("rust-leb128" ,rust-leb128-0.2)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-unicode-width" ,rust-unicode-width-0.1)
                       ("rust-wasm-encoder" ,rust-wasm-encoder-0.215))
       #:cargo-development-inputs (("rust-anyhow" ,rust-anyhow-1)
                                   ("rust-libtest-mimic" ,rust-libtest-mimic-0.7)
                                   ("rust-rand" ,rust-rand-0.8))))
    (home-page
     "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wast")
    (synopsis
     "Customizable Rust parsers for the WebAssembly Text formats WAT and WAST")
    (description
     "This package provides Customizable Rust parsers for the @code{WebAssembly} Text formats WAT and WAST.")
    (license (list license:asl2.0 license:asl2.0
                   license:expat))))

(define rust-wat-1
  (package
    (name "rust-wat")
    (version "1.215.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wat" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0gaf5fzl8lq6rfdiz1x6zbdbc9b5aiyd9pkh5ljawxngr3cz82v7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-wast" ,rust-wast-215))))
    (home-page
     "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wat")
    (synopsis "Rust parser for the WebAssembly Text format, WAT")
    (description
     "This package provides Rust parser for the @code{WebAssembly} Text format, WAT.")
    (license (list license:asl2.0 license:asl2.0
                   license:expat))))

(define rust-wasm-encoder-0.31
  (package
    (name "rust-wasm-encoder")
    (version "0.31.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasm-encoder" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "05nyajlmqgvyq7ynqhs1hbsmkafkjrj4dyqszygkklgyx8h3yxj1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-leb128" ,rust-leb128-0.2))
       #:cargo-development-inputs (("rust-anyhow" ,rust-anyhow-1)
                                   ("rust-tempfile" ,rust-tempfile-3))))
    (home-page
     "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wasm-encoder")
    (synopsis "low-level WebAssembly encoder.")
    (description
     "This package provides a low-level @code{WebAssembly} encoder.")
    (license (list license:asl2.0))))

(define rust-wast-62
  (package
    (name "rust-wast")
    (version "62.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wast" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "07wzrdq9igydq21ljdlx3nfx9q91zbw0yqmxky47ndxykpq0dbmq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-leb128" ,rust-leb128-0.2)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-unicode-width" ,rust-unicode-width-0.1)
                       ("rust-wasm-encoder" ,rust-wasm-encoder-0.31))
       #:cargo-development-inputs (("rust-anyhow" ,rust-anyhow-1)
                                   ("rust-rayon" ,rust-rayon-1))))
    (home-page
     "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wast")
    (synopsis
     "Customizable Rust parsers for the WebAssembly Text formats WAT and WAST")
    (description
     "This package provides Customizable Rust parsers for the @code{WebAssembly} Text formats WAT and WAST.")
    (license (list license:asl2.0))))

(define rust-indexmap-nostd-0.4
  (package
    (name "rust-indexmap-nostd")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "indexmap-nostd" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "145mrkrrnzzg8xbv6si8j3b8cw1pi3g13vrjgf1fm2415gyy414f"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/robbepop/indexmap-nostd")
    (synopsis "no_std compatible implementation of the indexmap crate")
    (description
     "This package provides a no_std compatible implementation of the indexmap crate.")
    (license license:asl2.0)))

(define rust-wasmparser-nostd-0.100
  (package
    (name "rust-wasmparser-nostd")
    (version "0.100.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmparser-nostd" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ak4bi9k9jb223xw7mlxkgim6lp7m8bwfqhlpfa4ll7kjpz1b86m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-indexmap-nostd" ,rust-indexmap-nostd-0.4))
       #:cargo-development-inputs (("rust-anyhow" ,rust-anyhow-1)
                                   ("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-once-cell" ,rust-once-cell-1)
                                   ("rust-rayon" ,rust-rayon-1))))
    (home-page
     "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wasmparser")
    (synopsis
     "simple event-driven library for parsing WebAssembly binary files.")
    (description
     "This package provides a simple event-driven library for parsing
@code{WebAssembly} binary files.")
    (license (list license:asl2.0))))

(define rust-wasmi-core-0.13
  (package
    (name "rust-wasmi-core")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmi_core" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0sl5356hb8iz3l297jvd4ml62fhcq0h2f031qa2mpydz6kdsgwfw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-downcast-rs" ,rust-downcast-rs-1)
                       ("rust-libm" ,rust-libm-0.2)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-paste" ,rust-paste-1))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.8))))
    (home-page "https://github.com/wasmi-labs/wasmi")
    (synopsis "Core primitives for the wasmi WebAssembly interpreter")
    (description
     "This package provides Core primitives for the wasmi @code{WebAssembly} interpreter.")
    (license (list license:expat license:asl2.0))))

(define rust-wasmi-arena-0.4
  (package
    (name "rust-wasmi-arena")
    (version "0.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmi_arena" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0wvhfah2ccvhl4vwycbncfcnb78ndgbkac3v56n0qms4prrpyjhh"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/paritytech/wasmi")
    (synopsis "Entity arena data structures for wasmi")
    (description
     "This package provides Entity arena data structures for wasmi.")
    (license (list license:expat license:asl2.0))))

(define rust-wasmi-0.31
  (package
    (name "rust-wasmi")
    (version "0.31.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmi" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1isnchb3vd6cndfrjfiql6nhq9qcvnfzlgkaqxady3363lfjia3p"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-smallvec" ,rust-smallvec-1)
                       ("rust-spin" ,rust-spin-0.9)
                       ("rust-wasmi-arena" ,rust-wasmi-arena-0.4)
                       ("rust-wasmi-core" ,rust-wasmi-core-0.13)
                       ("rust-wasmparser-nostd" ,rust-wasmparser-nostd-0.100))
       #:cargo-development-inputs (("rust-anyhow" ,rust-anyhow-1)
                                   ("rust-assert-matches" ,rust-assert-matches-1)
                                   ("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-wast" ,rust-wast-62)
                                   ("rust-wat" ,rust-wat-1))))
    (home-page "https://github.com/wasmi-labs/wasmi")
    (synopsis "WebAssembly interpreter")
    (description "This package provides @code{WebAssembly} interpreter.")
    (license (list license:expat license:asl2.0))))

(define rust-unicode-vo-0.1
  (package
    (name "rust-unicode-vo")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-vo" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "151sha088v9jyfvbg5164xh4dk72g53b82xm4zzbf5dlagzqdlxi"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/RazrFalcon/unicode-vo")
    (synopsis "Unicode vertical orientation detection")
    (description
     "This package provides Unicode vertical orientation detection.")
    (license (list license:expat license:asl2.0))))

(define rust-usvg-text-layout-0.38
  (package
    (name "rust-usvg-text-layout")
    (version "0.38.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "usvg-text-layout" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1qj6vgk7244by314j5wgp58k5s5xgc29vbrfhpz33x2wkn5qhhcc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-fontdb" ,rust-fontdb-0.16)
                       ("rust-kurbo" ,rust-kurbo-0.9)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-rustybuzz" ,rust-rustybuzz-0.12)
                       ("rust-unicode-bidi" ,rust-unicode-bidi-0.3)
                       ("rust-unicode-script" ,rust-unicode-script-0.5)
                       ("rust-unicode-vo" ,rust-unicode-vo-0.1)
                       ("rust-usvg-tree" ,rust-usvg-tree-0.38))))
    (home-page "https://github.com/RazrFalcon/resvg")
    (synopsis "An SVG text layout implementation")
    (description "This package provides An SVG text layout implementation.")
    (license license:mpl2.0)))

(define rust-strict-num-0.1
  (package
    (name "rust-strict-num")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "strict-num" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0cb7l1vhb8zj90mzm8avlk815k40sql9515s865rqdrdfavvldv6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-float-cmp" ,rust-float-cmp-0.9))))
    (home-page "https://github.com/RazrFalcon/strict-num")
    (synopsis "collection of bounded numeric types")
    (description
     "This package provides a collection of bounded numeric types.")
    (license license:expat)))

(define rust-usvg-tree-0.38
  (package
    (name "rust-usvg-tree")
    (version "0.38.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "usvg-tree" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "01czj22qlzdy5p7y6i1j58k4z7yv8q8mnb1narp3s5gd0h23x1hq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-strict-num" ,rust-strict-num-0.1)
                       ("rust-svgtypes" ,rust-svgtypes-0.13)
                       ("rust-tiny-skia-path" ,rust-tiny-skia-path-0.11))))
    (home-page "https://github.com/RazrFalcon/resvg")
    (synopsis "An SVG tree representation used by usvg")
    (description
     "This package provides An SVG tree representation used by usvg.")
    (license license:mpl2.0)))

(define rust-svgtypes-0.13
  (package
    (name "rust-svgtypes")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "svgtypes" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0w4xknlff1np8l9if7y8ig6bx44bjr006m5xgj8ih0wnrn4f4i3f"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-kurbo" ,rust-kurbo-0.9)
                       ("rust-siphasher" ,rust-siphasher-0.3))))
    (home-page "https://github.com/RazrFalcon/svgtypes")
    (synopsis "SVG types parser")
    (description "This package provides SVG types parser.")
    (license (list license:expat license:asl2.0))))

(define rust-xmlparser-0.10
  (package
    (name "rust-xmlparser")
    (version "0.10.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "xmlparser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1sr1d602ng26lx07h3mqn3p3k88sg5d8v8rd1dmrc3fcbdn4j441"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/RazrFalcon/xmlparser")
    (synopsis "Pull-based, zero-allocation XML parser")
    (description
     "This package provides Pull-based, zero-allocation XML parser.")
    (license (list license:expat license:asl2.0))))

(define rust-roxmltree-0.7
  (package
    (name "rust-roxmltree")
    (version "0.7.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "roxmltree" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ay04flfbwz6a205qwpsl922g73nwzzv77bbqsh9ddn1axr40lh8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-xmlparser" ,rust-xmlparser-0.10))
       #:cargo-development-inputs (("rust-pretty-assertions" ,rust-pretty-assertions-0.5))))
    (home-page "https://github.com/RazrFalcon/roxmltree")
    (synopsis "Represent an XML as a read-only tree")
    (description "This package provides Represent an XML as a read-only tree.")
    (license (list license:expat license:asl2.0))))

(define rust-simplecss-0.2
  (package
    (name "rust-simplecss")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "simplecss" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "17g8q1z9xrkd27ic9nrfirj6in4rai6l9ws0kxz45n97573ff6x1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.6)
                                   ("rust-roxmltree" ,rust-roxmltree-0.7))))
    (home-page "https://github.com/RazrFalcon/simplecss")
    (synopsis "simple CSS 2 parser and selector.")
    (description "This package provides a simple CSS 2 parser and selector.")
    (license (list license:expat license:asl2.0))))

(define rust-imagesize-0.12
  (package
    (name "rust-imagesize")
    (version "0.12.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "imagesize" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "114jvqiyv13il1qghv2xm0xqrcjm68fh282hdlzdds6qfgsp7782"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/Roughsketch/imagesize")
    (synopsis
     "Quick probing of image dimensions without loading the entire file")
    (description
     "This package provides Quick probing of image dimensions without loading the entire file.")
    (license license:expat)))

(define rust-usvg-parser-0.38
  (package
    (name "rust-usvg-parser")
    (version "0.38.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "usvg-parser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1c0lk23aij52hwjx9mrhz7ffyyljld044wvgji76ng82ybk0a6im"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-data-url" ,rust-data-url-0.3)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-imagesize" ,rust-imagesize-0.12)
                       ("rust-kurbo" ,rust-kurbo-0.9)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-roxmltree" ,rust-roxmltree-0.19)
                       ("rust-simplecss" ,rust-simplecss-0.2)
                       ("rust-siphasher" ,rust-siphasher-0.3)
                       ("rust-svgtypes" ,rust-svgtypes-0.13)
                       ("rust-usvg-tree" ,rust-usvg-tree-0.38))))
    (home-page "https://github.com/RazrFalcon/resvg")
    (synopsis "An SVG parser used by usvg")
    (description "This package provides An SVG parser used by usvg.")
    (license license:mpl2.0)))

(define rust-usvg-0.38
  (package
    (name "rust-usvg")
    (version "0.38.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "usvg" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "098n1l980dz58fcrlxrsy7k584dc3nmq1an1aj3dwwy1lfs64zrp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-base64" ,rust-base64-0.21)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-pico-args" ,rust-pico-args-0.5)
                       ("rust-usvg-parser" ,rust-usvg-parser-0.38)
                       ("rust-usvg-text-layout" ,rust-usvg-text-layout-0.38)
                       ("rust-usvg-tree" ,rust-usvg-tree-0.38)
                       ("rust-xmlwriter" ,rust-xmlwriter-0.1))
       #:cargo-development-inputs (("rust-once-cell" ,rust-once-cell-1))))
    (home-page "https://github.com/RazrFalcon/resvg")
    (synopsis "An SVG simplification library")
    (description "This package provides An SVG simplification library.")
    (license license:mpl2.0)))

(define rust-typst-timing-0.11
  (package
    (name "rust-typst-timing")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-timing" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0rzfmxzkan0sa2n89qwwjjr0iw3hrvpd7rmckaiv5b5wj9qy2n2v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.11))))
    (home-page "https://typst.app")
    (synopsis "Performance timing for Typst")
    (description "This package provides Performance timing for Typst.")
    (license license:asl2.0)))

(define rust-unicode-math-class-0.1
  (package
    (name "rust-unicode-math-class")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-math-class" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0rbxcjirldpdrpxv1l7qiadbib8rnl7b413fsp4f7ynmk7snq93x"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/typst/unicode-math-class")
    (synopsis "Determine the Unicode class of a mathematical character")
    (description
     "This package provides Determine the Unicode class of a mathematical character.")
    (license (list license:expat license:asl2.0))))

(define rust-typst-syntax-0.11
  (package
    (name "rust-typst-syntax")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-syntax" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1i20vfmdxkk0ym36rqwy15zi6965gpyl9v6vdvzv24qnykr6knz3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-unicode-ident" ,rust-unicode-ident-1)
                       ("rust-unicode-math-class" ,rust-unicode-math-class-0.1)
                       ("rust-unicode-script" ,rust-unicode-script-0.5)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-unscanny" ,rust-unscanny-0.1))))
    (home-page "https://typst.app")
    (synopsis "Parser and syntax tree for Typst")
    (description "This package provides Parser and syntax tree for Typst.")
    (license license:asl2.0)))

(define rust-typst-macros-0.11
  (package
    (name "rust-typst-macros")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1z33hci300cvllg6i2j9qf5pwwwc8fwfa5a2iq7hp4ml8vyzv875"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-heck" ,rust-heck-0.4)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://typst.app")
    (synopsis "Proc-macros for Typst")
    (description "This package provides Proc-macros for Typst.")
    (license license:asl2.0)))

(define rust-typst-assets-0.11
  (package
    (name "rust-typst-assets")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-assets" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "172dxzw8nvps72p9mi43568v8p25812v56hw933yxs38sbw62c1b"))))
    (build-system cargo-build-system)
    (home-page "https://typst.app")
    (synopsis "Assets for the Typst compiler")
    (description "This package provides Assets for the Typst compiler.")
    (license license:asl2.0)))

(define rust-two-face-0.3
  (package
    (name "rust-two-face")
    (version "0.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "two-face" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ssydfj3cdjf28pmy84wjhayp5s66xnr0b57zgpcfn94bc9x5gip"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-once-cell" ,rust-once-cell-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-syntect" ,rust-syntect-5))
       #:cargo-development-inputs (("rust-strum" ,rust-strum-0.25)
                                   ("rust-syntect" ,rust-syntect-5))))
    (home-page "https://github.com/CosmicHorrorDev/two-face")
    (synopsis "Extra syntax and theme definitions for syntect")
    (description
     "This package provides Extra syntax and theme definitions for syntect.")
    (license (list license:expat license:asl2.0))))

(define rust-unicode-script-0.5
  (package
    (name "rust-unicode-script")
    (version "0.5.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-script" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1kf9v1yfxazxjx07g9g9nqg4kw2kzpnyi7syjdd2hpvffbsp33dd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
                       ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
                       ("rust-rustc-std-workspace-std" ,rust-rustc-std-workspace-std-1))))
    (home-page "https://github.com/unicode-rs/unicode-script")
    (synopsis
     "This crate exposes the Unicode `Script` and `Script_Extension` properties from [UAX #24](http://www.unicode.org/reports/tr24/)")
    (description
     "This crate exposes the Unicode `Script` and `Script_Extension` properties from
[UAX #24](http://www.unicode.org/reports/tr24/).")
    (license (list license:expat license:asl2.0))))

(define rust-unicode-properties-0.1
  (package
    (name "rust-unicode-properties")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-properties" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "14cjbmfs64qw1m4qzpfa673a8rpyhp5h9f412mkg1n958jfrs9g4"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/unicode-rs/unicode-properties")
    (synopsis "Query character Unicode properties according to
UAX #44 and UTR #51.")
    (description
     "This package provides Query character Unicode properties according to UAX #44 and UTR #51.")
    (license (list license:expat license:asl2.0))))

(define rust-unicode-ccc-0.1
  (package
    (name "rust-unicode-ccc")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-ccc" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1wbwny92wzmck2cix5h3r97h9z57x9831kadrs6jdy24lvpj09fc"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/RazrFalcon/unicode-ccc")
    (synopsis "Unicode Canonical Combining Class detection")
    (description
     "This package provides Unicode Canonical Combining Class detection.")
    (license (list license:expat license:asl2.0))))

(define rust-unicode-bidi-mirroring-0.1
  (package
    (name "rust-unicode-bidi-mirroring")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-bidi-mirroring" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "150navn2n6barkzchv96n877i17m1754nzmy1282zmcjzdh25lan"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/RazrFalcon/unicode-bidi-mirroring")
    (synopsis "Unicode Bidi Mirroring property detection")
    (description
     "This package provides Unicode Bidi Mirroring property detection.")
    (license (list license:expat license:asl2.0))))

(define rust-rustybuzz-0.12
  (package
    (name "rust-rustybuzz")
    (version "0.12.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rustybuzz" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0b12arlca4lfniphg91v9s5awkl7szpdwc18walxdamyqn95dbph"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-2)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-libm" ,rust-libm-0.2)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.20)
                       ("rust-unicode-bidi-mirroring" ,rust-unicode-bidi-mirroring-0.1)
                       ("rust-unicode-ccc" ,rust-unicode-ccc-0.1)
                       ("rust-unicode-properties" ,rust-unicode-properties-0.1)
                       ("rust-unicode-script" ,rust-unicode-script-0.5))
       #:cargo-development-inputs (("rust-libc" ,rust-libc-0.2)
                                   ("rust-pico-args" ,rust-pico-args-0.5))))
    (home-page "https://github.com/RazrFalcon/rustybuzz")
    (synopsis "complete harfbuzz shaping algorithm port to Rust.")
    (description
     "This package provides a complete harfbuzz shaping algorithm port to Rust.")
    (license license:expat)))

(define rust-qcms-0.3
  (package
    (name "rust-qcms")
    (version "0.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "qcms" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1yihv9rsa0qc4mmhzp8f0xdfrnkw7q8l7kr4ivcyb9amszazrv7d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/FirefoxGraphics/qcms")
    (synopsis "lightweight color management")
    (description "This package provides lightweight color management.")
    (license license:expat)))

(define rust-lipsum-0.9
  (package
    (name "rust-lipsum")
    (version "0.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "lipsum" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0r40mf2cwh4fp9pdfcc1n8hjxw05w7galjvb1z23r5pq38jn0s33"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-rand" ,rust-rand-0.8)
                       ("rust-rand-chacha" ,rust-rand-chacha-0.3))
       #:cargo-development-inputs (("rust-rand" ,rust-rand-0.8)
                                   ("rust-version-sync" ,rust-version-sync-0.9))))
    (home-page "https://github.com/mgeisler/lipsum/")
    (synopsis
     "Lipsum is a lorem ipsum text generation library. It generates
pseudo-random Latin text. Use this if you need filler or dummy text
for your application.

The text is generated using a simple Markov chain, which you can
instantiate to generate your own pieces of pseudo-random text.")
    (description
     "This package provides Lipsum is a lorem ipsum text generation library.  It generates pseudo-random
Latin text.  Use this if you need filler or dummy text for your application.
The text is generated using a simple Markov chain, which you can instantiate to
generate your own pieces of pseudo-random text.")
    (license license:expat)))

(define rust-kurbo-0.9
  (package
    (name "rust-kurbo")
    (version "0.9.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "kurbo" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "16w4k313z8smic4zifpwnxk8alh17dncgj2r40p0ql6rdivsb1dx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-libm" ,rust-libm-0.2)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-schemars" ,rust-schemars-0.8)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-getrandom" ,rust-getrandom-0.2)
                                   ("rust-rand" ,rust-rand-0.8))))
    (home-page "https://github.com/linebender/kurbo")
    (synopsis "2D curves library")
    (description "This package provides a 2D curves library.")
    (license (list license:expat license:asl2.0))))

(define rust-mutate-once-0.1
  (package
    (name "rust-mutate-once")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "mutate_once" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ys9mpjhwsj5md10ykmkin0wv7bz8dvc292hqczs9l5l4cd6ikqn"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/kamadak/mutate_once-rs")
    (synopsis "Interior mutability, write-once and borrowable as plain &T")
    (description
     "This package provides Interior mutability, write-once and borrowable as plain &T.")
    (license license:bsd-2)))

(define rust-kamadak-exif-0.5
  (package
    (name "rust-kamadak-exif")
    (version "0.5.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "kamadak-exif" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0xw0lpmra8j1y98c0agwrmjajpkh91mnl89hzaxbdrdp186wfkzg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-mutate-once" ,rust-mutate-once-0.1))))
    (home-page "https://github.com/kamadak/exif-rs")
    (synopsis "Exif parsing library written in pure Rust")
    (description
     "This package provides Exif parsing library written in pure Rust.")
    (license license:bsd-2)))

(define rust-utf8-iter-1
  (package
    (name "rust-utf8-iter")
    (version "1.0.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "utf8_iter" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1gmna9flnj8dbyd8ba17zigrp9c4c3zclngf5lnb5yvz1ri41hdn"))))
    (build-system cargo-build-system)
    (home-page "https://docs.rs/utf8_iter/")
    (synopsis "Iterator by char over potentially-invalid UTF-8 in &[u8]")
    (description
     "This package provides Iterator by char over potentially-invalid UTF-8 in &[u8].")
    (license (list license:asl2.0 license:expat))))

(define rust-icu-segmenter-data-1
  (package
    (name "rust-icu-segmenter-data")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_segmenter_data" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1pvrgnxi7fq47hfpc66jgvxzfc8nmzmgv0xw63imbnb0f9rywfgp"))))
    (build-system cargo-build-system)
    (home-page "https://icu4x.unicode.org")
    (synopsis "Data for the icu_segmenter crate")
    (description "This package provides Data for the icu_segmenter crate.")
    (license #f)))

(define rust-core-maths-0.1
  (package
    (name "rust-core-maths")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "core_maths" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "18q9fwy80lk1lccam375skmsslryik00zkhsl850pidqrh2jbc73"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libm" ,rust-libm-0.2))))
    (home-page "https://github.com/robertbastian/core_maths")
    (synopsis
     "Extension trait for full float functionality in `#[no_std]` backed by `libm`")
    (description
     "This package provides Extension trait for full float functionality in `#[no_std]` backed by `libm`.")
    (license license:expat)))

(define rust-icu-segmenter-1
  (package
    (name "rust-icu-segmenter")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_segmenter" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1pmharib9s1hn5650d4lyd48145n1n14pja2gcnzqvrl29b745x7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-core-maths" ,rust-core-maths-0.1)
                       ("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-icu-collections" ,rust-icu-collections-1)
                       ("rust-icu-locid" ,rust-icu-locid-1)
                       ("rust-icu-locid-transform" ,rust-icu-locid-transform-1)
                       ("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-icu-segmenter-data" ,rust-icu-segmenter-data-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-utf8-iter" ,rust-utf8-iter-1)
                       ("rust-zerovec" ,rust-zerovec-0.10))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-itertools" ,rust-itertools-0.10)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://icu4x.unicode.org")
    (synopsis
     "Unicode line breaking and text segmentation algorithms for text boundaries analysis")
    (description
     "This package provides Unicode line breaking and text segmentation algorithms for text boundaries
analysis.")
    (license #f)))

(define rust-zerotrie-0.1
  (package
    (name "rust-zerotrie")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "zerotrie" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "07qa5ljss8j706iy0rd023naamwly4jfwz0pc1gmqcw7bpalsngv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-litemap" ,rust-litemap-0.7)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-yoke" ,rust-yoke-0.7)
                       ("rust-zerofrom" ,rust-zerofrom-0.1)
                       ("rust-zerovec" ,rust-zerovec-0.10))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-postcard" ,rust-postcard-1)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-rand-pcg" ,rust-rand-pcg-0.3)
                                   ("rust-rmp-serde" ,rust-rmp-serde-1)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "data structure that efficiently maps strings to integers")
    (description
     "This package provides a data structure that efficiently maps strings to
integers.")
    (license #f)))

(define rust-icu-provider-blob-1
  (package
    (name "rust-icu-provider-blob")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_provider_blob" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0lfgvia5v76gkpfgbga5ga6z1b5465v821f2hs0xfmaz6v8rhjy2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-postcard" ,rust-postcard-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-writeable" ,rust-writeable-0.5)
                       ("rust-zerotrie" ,rust-zerotrie-0.1)
                       ("rust-zerovec" ,rust-zerovec-0.10))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-twox-hash" ,rust-twox-hash-1))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "ICU4X data provider that reads from blobs in memory")
    (description
     "This package provides ICU4X data provider that reads from blobs in memory.")
    (license #f)))

(define rust-icu-provider-adapters-1
  (package
    (name "rust-icu-provider-adapters")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_provider_adapters" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1g60fydak0i4rxf8vfrr31mpck846k9ynix4fh1qx2il13ylscnn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-icu-locid" ,rust-icu-locid-1)
                       ("rust-icu-locid-transform" ,rust-icu-locid-transform-1)
                       ("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-tinystr" ,rust-tinystr-0.7)
                       ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Adapters for composing and manipulating data providers")
    (description
     "This package provides Adapters for composing and manipulating data providers.")
    (license #f)))

(define rust-icu-properties-data-1
  (package
    (name "rust-icu-properties-data")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_properties_data" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0scms7pd5a7yxx9hfl167f5qdf44as6r3bd8myhlngnxqgxyza37"))))
    (build-system cargo-build-system)
    (home-page "https://icu4x.unicode.org")
    (synopsis "Data for the icu_properties crate")
    (description "This package provides Data for the icu_properties crate.")
    (license #f)))

(define rust-icu-provider-macros-1
  (package
    (name "rust-icu-provider-macros")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_provider_macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1mjs0w7fcm2lcqmbakhninzrjwqs485lkps4hz0cv3k36y9rxj0y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Proc macros for ICU data providers")
    (description "This package provides Proc macros for ICU data providers.")
    (license #f)))

(define rust-icu-provider-1
  (package
    (name "rust-icu-provider")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_provider" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1nb8vvgw8dv2inqklvk05fs0qxzkw8xrg2n9vgid6y7gm3423m3f"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bincode" ,rust-bincode-1)
                       ("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-erased-serde" ,rust-erased-serde-0.3)
                       ("rust-icu-locid" ,rust-icu-locid-1)
                       ("rust-icu-provider-macros" ,rust-icu-provider-macros-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-postcard" ,rust-postcard-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-stable-deref-trait" ,rust-stable-deref-trait-1)
                       ("rust-tinystr" ,rust-tinystr-0.7)
                       ("rust-writeable" ,rust-writeable-0.5)
                       ("rust-yoke" ,rust-yoke-0.7)
                       ("rust-zerofrom" ,rust-zerofrom-0.1)
                       ("rust-zerovec" ,rust-zerovec-0.10))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Trait and struct definitions for the ICU data provider")
    (description
     "This package provides Trait and struct definitions for the ICU data provider.")
    (license #f)))

(define rust-icu-locid-transform-data-1
  (package
    (name "rust-icu-locid-transform-data")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_locid_transform_data" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0vkgjixm0wzp2n3v5mw4j89ly05bg3lx96jpdggbwlpqi0rzzj7x"))))
    (build-system cargo-build-system)
    (home-page "https://icu4x.unicode.org")
    (synopsis "Data for the icu_locid_transform crate")
    (description
     "This package provides Data for the icu_locid_transform crate.")
    (license #f)))

(define rust-writeable-0.5
  (package
    (name "rust-writeable")
    (version "0.5.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "writeable" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0lawr6y0bwqfyayf3z8zmqlhpnzhdx0ahs54isacbhyjwa7g778y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-either" ,rust-either-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-rand" ,rust-rand-0.8))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "more efficient alternative to fmt::Display")
    (description
     "This package provides a more efficient alternative to fmt::Display.")
    (license #f)))

(define rust-litemap-0.7
  (package
    (name "rust-litemap")
    (version "0.7.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "litemap" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0157lf44c3s2piqiwpppnynzzpv1rxyddl2z9l089hpwsjwb0g34"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-yoke" ,rust-yoke-0.7))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-postcard" ,rust-postcard-1)
                                   ("rust-rkyv" ,rust-rkyv-0.7)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "key-value Map implementation based on a flat, sorted Vec.")
    (description
     "This package provides a key-value Map implementation based on a flat, sorted
Vec.")
    (license #f)))

(define rust-icu-locid-1
  (package
    (name "rust-icu-locid")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_locid" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0dznvd1c5b02iilqm044q4hvar0sqibq1z46prqwjzwif61vpb0k"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-litemap" ,rust-litemap-0.7)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-tinystr" ,rust-tinystr-0.7)
                       ("rust-writeable" ,rust-writeable-0.5)
                       ("rust-zerovec" ,rust-zerovec-0.10))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-iai" ,rust-iai-0.1)
                                   ("rust-postcard" ,rust-postcard-1)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "API for managing Unicode Language and Locale Identifiers")
    (description
     "This package provides API for managing Unicode Language and Locale Identifiers.")
    (license #f)))

(define rust-icu-locid-transform-1
  (package
    (name "rust-icu-locid-transform")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_locid_transform" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0kmmi1kmj9yph6mdgkc7v3wz6995v7ly3n80vbg0zr78bp1iml81"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-icu-locid" ,rust-icu-locid-1)
                       ("rust-icu-locid-transform-data" ,rust-icu-locid-transform-data-1)
                       ("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-tinystr" ,rust-tinystr-0.7)
                       ("rust-zerovec" ,rust-zerovec-0.10))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://icu4x.unicode.org")
    (synopsis
     "API for Unicode Language and Locale Identifiers canonicalization")
    (description
     "This package provides API for Unicode Language and Locale Identifiers canonicalization.")
    (license #f)))

(define rust-icu-collections-1
  (package
    (name "rust-icu-collections")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_collections" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "09j5kskirl59mvqc8kabhy7005yyy7dp88jw9f6f3gkf419a8byv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-yoke" ,rust-yoke-0.7)
                       ("rust-zerofrom" ,rust-zerofrom-0.1)
                       ("rust-zerovec" ,rust-zerovec-0.10))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-iai" ,rust-iai-0.1)
                                   ("rust-postcard" ,rust-postcard-1)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-toml" ,rust-toml-0.5))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Collection of API for use in ICU libraries")
    (description
     "This package provides Collection of API for use in ICU libraries.")
    (license #f)))

(define rust-icu-properties-1
  (package
    (name "rust-icu-properties")
    (version "1.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_properties" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1xgf584rx10xc1p7zjr78k0n4zn3g23rrg6v2ln31ingcq3h5mlk"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-icu-collections" ,rust-icu-collections-1)
                       ("rust-icu-locid-transform" ,rust-icu-locid-transform-1)
                       ("rust-icu-properties-data" ,rust-icu-properties-data-1)
                       ("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-tinystr" ,rust-tinystr-0.7)
                       ("rust-unicode-bidi" ,rust-unicode-bidi-0.3)
                       ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Definitions for Unicode properties")
    (description "This package provides Definitions for Unicode properties.")
    (license #f)))

(define rust-hypher-0.1
  (package
    (name "rust-hypher")
    (version "0.1.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "hypher" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ra2kxbpi033jy42wkr7m7rgg6yhy69xad0hmc0z43936xbas91v"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/typst/hypher")
    (synopsis "hypher separates words into syllables")
    (description
     "This package provides hypher separates words into syllables.")
    (license (list license:expat license:asl2.0))))

(define rust-unic-langid-macros-impl-0.9
  (package
    (name "rust-unic-langid-macros-impl")
    (version "0.9.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unic-langid-macros-impl" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0nsm0hky2sawgkwz511br06mkm3ba70rfc05jm0l54x3gciz9mqy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2)
                       ("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description
     "This package provides API for managing Unicode Language Identifiers.")
    (license (list license:expat license:asl2.0))))

(define rust-unic-langid-macros-0.9
  (package
    (name "rust-unic-langid-macros")
    (version "0.9.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unic-langid-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0pi71r5474n7sdmyky7qpnia9rrr42q0d200l5lpag1d0hncv88d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
                       ("rust-tinystr" ,rust-tinystr-0.7)
                       ("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9)
                       ("rust-unic-langid-macros-impl" ,rust-unic-langid-macros-impl-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description
     "This package provides API for managing Unicode Language Identifiers.")
    (license (list license:expat license:asl2.0))))

(define rust-rmp-0.8
  (package
    (name "rust-rmp")
    (version "0.8.14")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rmp" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1i1l6dhv7vws5vp0ikakj44fk597xi59g3j6ng1q55x3dz0xg3i2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-paste" ,rust-paste-1))
       #:cargo-development-inputs (("rust-quickcheck" ,rust-quickcheck-1))))
    (home-page "https://github.com/3Hren/msgpack-rust")
    (synopsis "Pure Rust MessagePack serialization implementation")
    (description
     "This package provides Pure Rust @code{MessagePack} serialization implementation.")
    (license license:expat)))

(define rust-rmp-serde-1
  (package
    (name "rust-rmp-serde")
    (version "1.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rmp-serde" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1nylmh7w2vpa1bwrnx1jfp2l4yz6i5qrmpic5zll166gfyj9kraj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-rmp" ,rust-rmp-0.8)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-serde" ,rust-serde-1)
                                   ("rust-serde-bytes" ,rust-serde-bytes-0.11))))
    (home-page "https://github.com/3Hren/msgpack-rust")
    (synopsis "Serde bindings for RMP")
    (description "This package provides Serde bindings for RMP.")
    (license license:expat)))

(define rust-zerovec-derive-0.10
  (package
    (name "rust-zerovec-derive")
    (version "0.10.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "zerovec-derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ik322dys6wnap5d3gcsn09azmssq466xryn5czfm13mn7gsdbvf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "Custom derive for the zerovec crate")
    (description "This package provides Custom derive for the zerovec crate.")
    (license #f)))

(define rust-zerofrom-derive-0.1
  (package
    (name "rust-zerofrom-derive")
    (version "0.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "zerofrom-derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "19b31rrs2ry1lrq5mpdqjzgg65va51fgvwghxnf6da3ycfiv99qf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2)
                       ("rust-synstructure" ,rust-synstructure-0.13))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "Custom derive for the zerofrom crate")
    (description "This package provides Custom derive for the zerofrom crate.")
    (license #f)))

(define rust-zerofrom-0.1
  (package
    (name "rust-zerofrom")
    (version "0.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "zerofrom" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0mdbjd7vmbix2ynxbrbrrli47a5yrpfx05hi99wf1l4pwwf13v4i"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-zerofrom-derive" ,rust-zerofrom-derive-0.1))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "ZeroFrom trait for constructing")
    (description
     "This package provides @code{ZeroFrom} trait for constructing.")
    (license #f)))

(define rust-yoke-derive-0.7
  (package
    (name "rust-yoke-derive")
    (version "0.7.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "yoke-derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "15cvhkci2mchfffx3fmva84fpmp34dsmnbzibwfnzjqq3ds33k18"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2)
                       ("rust-synstructure" ,rust-synstructure-0.13))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "Custom derive for the yoke crate")
    (description "This package provides Custom derive for the yoke crate.")
    (license #f)))

(define rust-yoke-0.7
  (package
    (name "rust-yoke")
    (version "0.7.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "yoke" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "198c4jkh6i3hxijia7mfa4cpnxg1iqym9bz364697c3rn0a16nvc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-serde" ,rust-serde-1)
                       ("rust-stable-deref-trait" ,rust-stable-deref-trait-1)
                       ("rust-yoke-derive" ,rust-yoke-derive-0.7)
                       ("rust-zerofrom" ,rust-zerofrom-0.1))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-postcard" ,rust-postcard-1)
                                   ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis
     "Abstraction allowing borrowed data to be carried along with the backing data it borrows from")
    (description
     "This package provides Abstraction allowing borrowed data to be carried along with the backing data it
borrows from.")
    (license #f)))

(define rust-zerovec-0.10
  (package
    (name "rust-zerovec")
    (version "0.10.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "zerovec" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0yghix7n3fjfdppwghknzvx9v8cf826h2qal5nqvy8yzg4yqjaxa"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-twox-hash" ,rust-twox-hash-1)
                       ("rust-yoke" ,rust-yoke-0.7)
                       ("rust-zerofrom" ,rust-zerofrom-0.1)
                       ("rust-zerovec-derive" ,rust-zerovec-derive-0.10))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-getrandom" ,rust-getrandom-0.2)
                                   ("rust-iai" ,rust-iai-0.1)
                                   ("rust-postcard" ,rust-postcard-1)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-rand-distr" ,rust-rand-distr-0.4)
                                   ("rust-rand-pcg" ,rust-rand-pcg-0.3)
                                   ("rust-rmp-serde" ,rust-rmp-serde-1)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "Zero-copy vector backed by a byte array")
    (description
     "This package provides Zero-copy vector backed by a byte array.")
    (license #f)))

(define rust-synstructure-0.13
  (package
    (name "rust-synstructure")
    (version "0.13.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "synstructure" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0wc9f002ia2zqcbj0q2id5x6n7g1zjqba7qkg2mr0qvvmdk7dby8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))
       #:cargo-development-inputs (("rust-synstructure-test-traits" ,rust-synstructure-test-traits-0.1))))
    (home-page "https://github.com/mystor/synstructure")
    (synopsis "Helper methods and macros for custom derives")
    (description
     "This package provides Helper methods and macros for custom derives.")
    (license license:expat)))

(define rust-databake-derive-0.1
  (package
    (name "rust-databake-derive")
    (version "0.1.8")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "databake-derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0yymbr1z93k7lg0pl5mw9mjhw8fpsfykg7bmkvmir9h1wmfjfy20"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2)
                       ("rust-synstructure" ,rust-synstructure-0.13))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "Custom derive for the databake crate")
    (description "This package provides Custom derive for the databake crate.")
    (license #f)))

(define rust-databake-0.1
  (package
    (name "rust-databake")
    (version "0.1.8")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "databake" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0kb0lnhka1fklrii3qaj40zcrbclfn8fyvy0r1whd3yaxkxzn13a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake-derive" ,rust-databake-derive-0.1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis
     "Trait that lets structs represent themselves as (const) Rust expressions")
    (description
     "This package provides Trait that lets structs represent themselves as (const) Rust expressions.")
    (license #f)))

(define rust-tinystr-0.7
  (package
    (name "rust-tinystr")
    (version "0.7.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tinystr" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0bxqaw7z8r2kzngxlzlgvld1r6jbnwyylyvyjbv1q71rvgaga5wi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-zerovec" ,rust-zerovec-0.10))
       #:cargo-development-inputs (("rust-bincode" ,rust-bincode-1)
                                   ("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-postcard" ,rust-postcard-1)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "small ASCII-only bounded length string representation.")
    (description
     "This package provides a small ASCII-only bounded length string representation.")
    (license #f)))

(define rust-unic-langid-impl-0.9
  (package
    (name "rust-unic-langid-impl")
    (version "0.9.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unic-langid-impl" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1rckyn5wqd5h8jxhbzlbbagr459zkzg822r4k5n30jaryv0j4m0a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-tinystr" ,rust-tinystr-0.7))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description
     "This package provides API for managing Unicode Language Identifiers.")
    (license (list license:expat license:asl2.0))))

(define rust-unic-langid-0.9
  (package
    (name "rust-unic-langid")
    (version "0.9.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unic-langid" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0i2s024frmpfa68lzy8y8vnb1rz3m9v0ga13f7h2afx7f8g9vp93"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9)
                       ("rust-unic-langid-macros" ,rust-unic-langid-macros-0.9))
       #:cargo-development-inputs (("rust-unic-langid-macros" ,rust-unic-langid-macros-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description
     "This package provides API for managing Unicode Language Identifiers.")
    (license (list license:expat license:asl2.0))))

(define rust-citationberg-0.3
  (package
    (name "rust-citationberg")
    (version "0.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "citationberg" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "02rx28ar1yqzfh97n18ihd1bmgshvgyj07aq36hhbylgsygzwnfj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-quick-xml" ,rust-quick-xml-0.31)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-unscanny" ,rust-unscanny-0.1))
       #:cargo-development-inputs (("rust-ciborium" ,rust-ciborium-0.2)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-serde-path-to-error" ,rust-serde-path-to-error-0.1))))
    (home-page "https://github.com/typst/citationberg")
    (synopsis "parser for CSL files")
    (description "This package provides a parser for CSL files.")
    (license (list license:expat license:asl2.0))))

(define rust-unscanny-0.1
  (package
    (name "rust-unscanny")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unscanny" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ivbipc1rnq15fhzgna41p1h01ncq4shycii72f3x5d7czq2mpz9"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/typst/unscanny")
    (synopsis "Painless string scanning")
    (description "This package provides Painless string scanning.")
    (license (list license:expat license:asl2.0))))

(define rust-numerals-0.1
  (package
    (name "rust-numerals")
    (version "0.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "numerals" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0cdx6yf5zcx2nvmzavr4qk9m35ha6i2rhy5fjxgx2wm7fq9y4nz2"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/ogham/rust-numerals")
    (synopsis "Library for numeric systems, both ancient and modern")
    (description
     "This package provides Library for numeric systems, both ancient and modern.")
    (license license:expat)))

(define rust-biblatex-0.9
  (package
    (name "rust-biblatex")
    (version "0.9.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "biblatex" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1f5py80y8aj3yibsd9gcffvg0i1zg32f35akig6jf0hd0j2p5zi7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-numerals" ,rust-numerals-0.1)
                       ("rust-paste" ,rust-paste-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-strum" ,rust-strum-0.26)
                       ("rust-unicode-normalization" ,rust-unicode-normalization-0.1)
                       ("rust-unscanny" ,rust-unscanny-0.1))))
    (home-page "https://github.com/typst/biblatex")
    (synopsis "Parsing, writing, and evaluating BibTeX and BibLaTeX files")
    (description
     "This package provides Parsing, writing, and evaluating @code{BibTeX} and @code{BibLaTeX} files.")
    (license (list license:expat license:asl2.0))))

(define rust-hayagriva-0.5
  (package
    (name "rust-hayagriva")
    (version "0.5.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "hayagriva" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0yn9j48hj491swwvpbw4xszaprp2acb2lsw76zknrf3pig4j038x"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-biblatex" ,rust-biblatex-0.9)
                       ("rust-ciborium" ,rust-ciborium-0.2)
                       ("rust-citationberg" ,rust-citationberg-0.3)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-numerals" ,rust-numerals-0.1)
                       ("rust-paste" ,rust-paste-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-yaml" ,rust-serde-yaml-0.9)
                       ("rust-strum" ,rust-strum-0.26)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-unic-langid" ,rust-unic-langid-0.9)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-unscanny" ,rust-unscanny-0.1)
                       ("rust-url" ,rust-url-2))
       #:cargo-development-inputs (("rust-heck" ,rust-heck-0.4)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/typst/hayagriva")
    (synopsis
     "Work with references: Literature database management, storage, and citation formatting")
    (description
     "This package provides Work with references: Literature database management, storage, and citation
formatting.")
    (license (list license:expat license:asl2.0))))

(define rust-assert-eq-float-0.1
  (package
    (name "rust-assert-eq-float")
    (version "0.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "assert-eq-float" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1y2hb46hm4cvxxy0bwng9gv4vq48i4scynflhqr5d2ijwzsd3c8w"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "https://magiclen.org/assert-eq-float")
    (synopsis
     "This crate provides the `assert_eq_float!` macros that support floats")
    (description
     "This crate provides the `assert_eq_float!` macros that support floats.")
    (license license:expat)))

(define rust-enum-ordinalize-derive-4
  (package
    (name "rust-enum-ordinalize-derive")
    (version "4.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "enum-ordinalize-derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1zy53fabazimwv5cl0366k834ybixzl84lxj9mfavbnlfn532a0d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://magiclen.org/enum-ordinalize")
    (synopsis
     "This crate provides a procedural macro that enables enums to not only obtain the ordinal values of their variants but also allows for the construction of enums from an ordinal value")
    (description
     "This crate provides a procedural macro that enables enums to not only obtain the
ordinal values of their variants but also allows for the construction of enums
from an ordinal value.")
    (license license:expat)))

(define rust-enum-ordinalize-4
  (package
    (name "rust-enum-ordinalize")
    (version "4.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "enum-ordinalize" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1max64z9giii61qcwl56rndd7pakaylkaij5zqbbbvjl9vxdr87y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-enum-ordinalize-derive" ,rust-enum-ordinalize-derive-4))))
    (home-page "https://magiclen.org/enum-ordinalize")
    (synopsis
     "This library enables enums to not only obtain the ordinal values of their variants but also allows for the construction of enums from an ordinal value")
    (description
     "This library enables enums to not only obtain the ordinal values of their
variants but also allows for the construction of enums from an ordinal value.")
    (license license:expat)))

(define rust-chinese-variant-1
  (package
    (name "rust-chinese-variant")
    (version "1.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "chinese-variant" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "12s91vg2m9wfs9b3f0q2alj9am08y7r2prb0szg3fwjh8m8lg23m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-enum-ordinalize" ,rust-enum-ordinalize-4))))
    (home-page "https://magiclen.org/chinese-variant")
    (synopsis "An enum to represent the variants of the Chinese Language")
    (description
     "This package provides An enum to represent the variants of the Chinese Language.")
    (license license:expat)))

(define rust-chinese-number-0.7
  (package
    (name "rust-chinese-number")
    (version "0.7.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "chinese-number" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0v5va8drix8gs2kv6pmv5yzdxhlpzrwkp3ch86kxdxj6cgpwmz29"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-chinese-variant" ,rust-chinese-variant-1)
                       ("rust-enum-ordinalize" ,rust-enum-ordinalize-4)
                       ("rust-num-bigint" ,rust-num-bigint-0.4)
                       ("rust-num-traits" ,rust-num-traits-0.2))
       #:cargo-development-inputs (("rust-assert-eq-float" ,rust-assert-eq-float-0.1))))
    (home-page "https://magiclen.org/chinese-number")
    (synopsis
     "Convert primitive numbers to Chinese numbers, or parse Chinese numbers to primitive numbers")
    (description
     "This package provides Convert primitive numbers to Chinese numbers, or parse Chinese numbers to
primitive numbers.")
    (license license:expat)))

(define rust-typst-0.11
  (package
    (name "rust-typst")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "16psqnwkzrrgqpqnl2dl4zcx6pwy6gpm1bj3057ljdq9sabj4j8j"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-az" ,rust-az-1)
                       ("rust-bitflags" ,rust-bitflags-2)
                       ("rust-chinese-number" ,rust-chinese-number-0.7)
                       ("rust-ciborium" ,rust-ciborium-0.2)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-csv" ,rust-csv-1)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-fontdb" ,rust-fontdb-0.16)
                       ("rust-hayagriva" ,rust-hayagriva-0.5)
                       ("rust-hypher" ,rust-hypher-0.1)
                       ("rust-icu-properties" ,rust-icu-properties-1)
                       ("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-icu-provider-adapters" ,rust-icu-provider-adapters-1)
                       ("rust-icu-provider-blob" ,rust-icu-provider-blob-1)
                       ("rust-icu-segmenter" ,rust-icu-segmenter-1)
                       ("rust-if-chain" ,rust-if-chain-1)
                       ("rust-image" ,rust-image-0.24)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-kamadak-exif" ,rust-kamadak-exif-0.5)
                       ("rust-kurbo" ,rust-kurbo-0.9)
                       ("rust-lipsum" ,rust-lipsum-0.9)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-palette" ,rust-palette-0.7)
                       ("rust-phf" ,rust-phf-0.11)
                       ("rust-png" ,rust-png-0.17)
                       ("rust-portable-atomic" ,rust-portable-atomic-1)
                       ("rust-qcms" ,rust-qcms-0.3)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-roxmltree" ,rust-roxmltree-0.19)
                       ("rust-rustybuzz" ,rust-rustybuzz-0.12)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-serde-yaml" ,rust-serde-yaml-0.9)
                       ("rust-siphasher" ,rust-siphasher-1)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-stacker" ,rust-stacker-0.1)
                       ("rust-syntect" ,rust-syntect-5)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-toml" ,rust-toml-0.8)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.20)
                       ("rust-two-face" ,rust-two-face-0.3)
                       ("rust-typed-arena" ,rust-typed-arena-2)
                       ("rust-typst-assets" ,rust-typst-assets-0.11)
                       ("rust-typst-macros" ,rust-typst-macros-0.11)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.11)
                       ("rust-typst-timing" ,rust-typst-timing-0.11)
                       ("rust-unicode-bidi" ,rust-unicode-bidi-0.3)
                       ("rust-unicode-math-class" ,rust-unicode-math-class-0.1)
                       ("rust-unicode-script" ,rust-unicode-script-0.5)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-usvg" ,rust-usvg-0.38)
                       ("rust-wasmi" ,rust-wasmi-0.31))))
    (home-page "https://typst.app")
    (synopsis
     "new markup-based typesetting system that is powerful and easy to learn.")
    (description
     "This package provides a new markup-based typesetting system that is powerful and
easy to learn.")
    (license license:asl2.0)))

(define rust-self-replace-1
  (package
    (name "rust-self-replace")
    (version "1.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "self-replace" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "05c5b55v90328gpc6a08qa4p51m5g4rplpiwbazxi1c6k5c8m0pp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-fastrand" ,rust-fastrand-1)
                       ("rust-tempfile" ,rust-tempfile-3)
                       ("rust-windows-sys" ,rust-windows-sys-0.48))))
    (home-page "https://github.com/mitsuhiko/self-replace")
    (synopsis
     "Utility crate that allows executables to replace or uninstall themselves")
    (description
     "This package provides Utility crate that allows executables to replace or uninstall themselves.")
    (license license:asl2.0)))

(define rust-xmlwriter-0.1
  (package
    (name "rust-xmlwriter")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "xmlwriter" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1fg0ldmkgiis6hnxpi1c9gy7v23y0lpi824bp8yp12fi3r82lypc"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/RazrFalcon/xmlwriter")
    (synopsis "simple, streaming XML writer.")
    (description "This package provides a simple, streaming XML writer.")
    (license license:expat)))

(define rust-ttf-parser-0.20
  (package
    (name "rust-ttf-parser")
    (version "0.20.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ttf-parser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1d4n3p9ccjvy4mj72700i0c2q6d49dxjpwflw47q79rpv1v7vxqp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-development-inputs (("rust-base64" ,rust-base64-0.13)
                                   ("rust-pico-args" ,rust-pico-args-0.5)
                                   ("rust-xmlwriter" ,rust-xmlwriter-0.1))))
    (home-page "https://github.com/RazrFalcon/ttf-parser")
    (synopsis
     "high-level, safe, zero-allocation font parser for TrueType, OpenType, and AAT.")
    (description
     "This package provides a high-level, safe, zero-allocation font parser for
@code{TrueType}, @code{OpenType}, and AAT.")
    (license (list license:expat license:asl2.0))))

(define rust-roxmltree-0.20
  (package
    (name "rust-roxmltree")
    (version "0.20.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "roxmltree" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "15vw91ps91wkmmgy62khf9zb63bdinvm80957dascbsw7dwvc83c"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/RazrFalcon/roxmltree")
    (synopsis "Represent an XML as a read-only tree")
    (description "This package provides Represent an XML as a read-only tree.")
    (license (list license:expat license:asl2.0))))

(define rust-fontconfig-parser-0.5
  (package
    (name "rust-fontconfig-parser")
    (version "0.5.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "fontconfig-parser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "19xdfsvl9sjha9n1lk3s6bqixcihsmjsd7zf3y90rsd69kagrz61"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-roxmltree" ,rust-roxmltree-0.20)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-glob" ,rust-glob-0.3)
                                   ("rust-k9" ,rust-k9-0.12)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/Riey/fontconfig-parser")
    (synopsis "fontconfig file parser in pure Rust")
    (description "This package provides fontconfig file parser in pure Rust.")
    (license license:expat)))

(define rust-fontdb-0.16
  (package
    (name "rust-fontdb")
    (version "0.16.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "fontdb" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1hqxv3jnh06s4bflrwnb39mi3knllfs4mxm44vsn0gzgqch90adh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-fontconfig-parser" ,rust-fontconfig-parser-0.5)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-memmap2" ,rust-memmap2-0.9)
                       ("rust-slotmap" ,rust-slotmap-1)
                       ("rust-tinyvec" ,rust-tinyvec-1)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.20))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.10))))
    (home-page "https://github.com/RazrFalcon/fontdb")
    (synopsis "simple, in-memory font database with CSS-like queries.")
    (description
     "This package provides a simple, in-memory font database with CSS-like queries.")
    (license license:expat)))

(define rust-ecow-0.2
  (package
    (name "rust-ecow")
    (version "0.2.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ecow" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0jvg2jrjgczy05mbsnirqqh3rxghxbdbwkbc18cj71lq10bvpgsl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-loom" ,rust-loom-0.7)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/typst/ecow")
    (synopsis "Compact, clone-on-write vector and string")
    (description
     "This package provides Compact, clone-on-write vector and string.")
    (license (list license:expat license:asl2.0))))

(define rust-siphasher-1
  (package
    (name "rust-siphasher")
    (version "1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "siphasher" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "17f35782ma3fn6sh21c027kjmd227xyrx06ffi8gw4xzv9yry6an"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://docs.rs/siphasher")
    (synopsis "SipHash-2-4, SipHash-1-3 and 128-bit variants in pure Rust")
    (description
     "This package provides @code{SipHash-2-4}, @code{SipHash-1-3} and 128-bit variants in pure Rust.")
    (license (list license:expat license:asl2.0))))

(define rust-comemo-macros-0.4
  (package
    (name "rust-comemo-macros")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "comemo-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1nr8w81hkzg49s515v61shxb077iq6d6001pybxbvxdlz516x4y8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/typst/comemo")
    (synopsis "Procedural macros for comemo")
    (description "This package provides Procedural macros for comemo.")
    (license (list license:expat license:asl2.0))))

(define rust-comemo-0.4
  (package
    (name "rust-comemo")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "comemo" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "14bsiayib4lhz3jrbf1fqh2fpwsm6cii90mifym3jhvji901csfz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-comemo-macros" ,rust-comemo-macros-0.4)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-siphasher" ,rust-siphasher-1))
       #:cargo-development-inputs (("rust-serial-test" ,rust-serial-test-3))))
    (home-page "https://github.com/typst/comemo")
    (synopsis "Incremental computation through constrained memoization")
    (description
     "This package provides Incremental computation through constrained memoization.")
    (license (list license:expat license:asl2.0))))

(define rust-typst-dev-assets-0.11
  (package
    (name "rust-typst-dev-assets")
    (version "0.11.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/typst/typst-dev-assets")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1x996a85x0n4k05bx1bskz4m646xq8ckrbl32969f5n7s256vi28"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #f))
    (home-page "https://github.com/typst/typst-dev-assets")
    (synopsis "Assets for testing & docs generation of the Typst compiler")
    (description "A crate with bundled assets for testing & docs generation of the Typst compiler.")
    (license license:asl2.0)))

(define typst
  (package
    (name "typst")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-cli" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0s4y5vl54m89w970bwx7033svl30868d0f6h0shr2zs9cghvjb5y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-chrono" ,rust-chrono-0.4)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-clap-complete" ,rust-clap-complete-4)
                       ("rust-clap-mangen" ,rust-clap-mangen-0.2)
                       ("rust-codespan-reporting" ,rust-codespan-reporting-0.11)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-dirs" ,rust-dirs-5)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-env-proxy" ,rust-env-proxy-0.4)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-fontdb" ,rust-fontdb-0.16)
                       ("rust-fs-extra" ,rust-fs-extra-1)
                       ("rust-native-tls" ,rust-native-tls-0.2)
                       ("rust-notify" ,rust-notify-6)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-open" ,rust-open-5)
                       ("rust-openssl" ,rust-openssl-0.10)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-pathdiff" ,rust-pathdiff-0.2)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-same-file" ,rust-same-file-1)
                       ("rust-self-replace" ,rust-self-replace-1)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-serde-yaml" ,rust-serde-yaml-0.9)
                       ("rust-shell-escape" ,rust-shell-escape-0.1)
                       ("rust-tar" ,rust-tar-0.4)
                       ("rust-tempfile" ,rust-tempfile-3)
                       ("rust-toml" ,rust-toml-0.8)
                       ("rust-typst" ,rust-typst-0.11)
                       ("rust-typst-assets" ,rust-typst-assets-0.11)
                       ("rust-typst-macros" ,rust-typst-macros-0.11)
                       ("rust-typst-pdf" ,rust-typst-pdf-0.11)
                       ("rust-typst-render" ,rust-typst-render-0.11)
                       ("rust-typst-svg" ,rust-typst-svg-0.11)
                       ("rust-typst-timing" ,rust-typst-timing-0.11)
                       ("rust-ureq" ,rust-ureq-2)
                       ("rust-xz2" ,rust-xz2-0.1)
                       ("rust-zip" ,rust-zip-0.6))))
    (inputs (list openssl
                  pkg-config))
    (home-page "https://typst.app")
    (synopsis "The command line interface for Typst")
    (description "This package provides The command line interface for Typst.")
    (license license:asl2.0)))
