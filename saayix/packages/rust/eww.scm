(define-module (saayix packages rust eww)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-gtk)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-crypto)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define %eww-version "0.6.0")
(define %eww-hash "0fd5ycfccrcwnmmdb6bc48rcj4qj9hb6xwbnz7g22dj9a6kffc5g")

(define rust-anstyle-parse-0.2
  (package
    (name "rust-anstyle-parse")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jy12rvgbldflnb2x7mcww9dcffw1mx22nyv6p3n7d62h0gdwizb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-utf8parse" ,rust-utf8parse-0.2))))
    (home-page "https://github.com/rust-cli/anstyle")
    (synopsis "Parse ANSI Style Escapes")
    (description
      "This package provides Parse ANSI Style Escapes.")
    (license (list license:expat license:asl2.0))))

(define rust-anstyle-query-1
  (package
    (name "rust-anstyle-query")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-query" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0aj22iy4pzk6mz745sfrm1ym14r0y892jhcrbs8nkj7nqx9gqdkd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page
      "https://github.com/rust-cli/anstyle.git")
    (synopsis "Look up colored console capabilities")
    (description
      "This package provides Look up colored console capabilities.")
    (license (list license:expat license:asl2.0))))

(define rust-atk-0.18
  (package
    (name "rust-atk")
    (version "0.18.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "atk" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1r0pdczm9jyh6lpn5hc3nmpbmp8ilbab52d6kypyi06x2x5h3bxl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atk-sys" ,rust-atk-sys-0.18)
         ("rust-glib" ,rust-glib-0.18)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://gtk-rs.org/")
    (synopsis
      "UNMAINTAINED Rust bindings for the ATK library")
    (description
      "This package provides UNMAINTAINED Rust bindings for the ATK library.")
    (license license:expat)))

(define rust-bit-set-0.6
  (package
    (name "rust-bit-set")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bit-set" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kzlffh7gp1ks2jjh3cyvkrqhgg99qc9b81k26di0hi70c71lj7h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bit-vec" ,rust-bit-vec-0.7))))
    (home-page
      "https://github.com/contain-rs/bit-set")
    (synopsis "set of bits")
    (description
      "This package provides a set of bits.")
    (license (list license:asl2.0 license:expat))))

(define rust-bit-vec-0.7
  (package
    (name "rust-bit-vec")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bit-vec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08kzw2z8hr4yhm80mr5k62ilir6q3gm35f56717s7h6ghzr4zifj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/contain-rs/bit-vec")
    (synopsis "vector of bits")
    (description
      "This package provides a vector of bits.")
    (license (list license:asl2.0 license:expat))))

(define rust-cached-0.53
  (package
    (name "rust-cached")
    (version "0.53.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cached" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ik8fr14wlfd2mfh3jw3p3dcd8dq0bmrmhngwifwya3bmrak3mxl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ahash" ,rust-ahash-0.8)
         ("rust-cached-proc-macro"
          ,rust-cached-proc-macro-0.23)
         ("rust-cached-proc-macro-types"
          ,rust-cached-proc-macro-types-0.1)
         ("rust-hashbrown" ,rust-hashbrown-0.14)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-web-time" ,rust-web-time-1))))
    (home-page "https://github.com/jaemk/cached")
    (synopsis
      "Generic cache implementations and simplified function memoization")
    (description
      "This package provides Generic cache implementations and simplified function memoization.")
    (license license:expat)))

(define rust-cached-proc-macro-0.23
  (package
    (name "rust-cached-proc-macro")
    (version "0.23.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cached_proc_macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ajxgl0w9vm55dk47qb0cq1akzncrwqcy78y37idq41dxm2s2hig"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling" ,rust-darling-0.20)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/jaemk/cached")
    (synopsis
      "Generic cache implementations and simplified function memoization")
    (description
      "This package provides Generic cache implementations and simplified function memoization.")
    (license license:expat)))

(define rust-cached-proc-macro-types-0.1
  (package
    (name "rust-cached-proc-macro-types")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cached_proc_macro_types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1h3gw61v1inay4g3b8pirxlz18m81k63dw2q18zj9fnmidmkds5d"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/jaemk/cached")
    (synopsis
      "Generic cache implementations and simplified function memoization")
    (description
      "This package provides Generic cache implementations and simplified function memoization.")
    (license license:expat)))

(define rust-chumsky-0.9
  (package
    (name "rust-chumsky")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chumsky" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jcnafc8rjfs1al08gqzyn0kpbaizgdwrd0ajqafspd18ikxdswf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hashbrown" ,rust-hashbrown-0.14))))
    (home-page "https://github.com/zesterer/chumsky")
    (synopsis
      "parser library for humans with powerful error recovery")
    (description
      "This package provides a parser library for humans with powerful error recovery.")
    (license license:expat)))

(define rust-codemap-0.1
  (package
    (name "rust-codemap")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "codemap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "091azkslwkcijj3lp9ymb084y9a0wm4fkil7m613ja68r2snkrxr"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/kevinmehall/codemap")
    (synopsis
      "data structure for efficiently storing source code position and span information\n(e.g. in a compiler AST), and mapping it back to file/line/column locations for error\nreporting and suggestions.")
    (description
      "This package provides a data structure for efficiently storing source code\nposition and span information (e.g. in a compiler AST), and mapping it back to\nfile/line/column locations for error reporting and suggestions.")
    (license (list license:expat license:asl2.0))))

(define rust-colorchoice-1
  (package
    (name "rust-colorchoice")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "colorchoice" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1h18ph538y8yjmbpaf8li98l0ifms2xmh3rax9666c5qfjfi3zfk"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/rust-cli/anstyle.git")
    (synopsis "Global override of color control")
    (description
      "This package provides Global override of color control.")
    (license (list license:expat license:asl2.0))))

(define rust-dbusmenu-glib-0.1
  (package
    (name "rust-dbusmenu-glib")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dbusmenu-glib" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "155q311s8zn830x06d7w7sk950xqs5w7xw5rirkf0xaprkf2j5px"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dbusmenu-glib-sys"
          ,rust-dbusmenu-glib-sys-0.1)
         ("rust-glib" ,rust-glib-0.18)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/ralismark/dbusmenu-rs")
    (synopsis "Rust bindings to dbusmenu-glib")
    (description
      "This package provides Rust bindings to dbusmenu-glib.")
    (license license:lgpl3)))

(define rust-dbusmenu-glib-sys-0.1
  (package
    (name "rust-dbusmenu-glib-sys")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dbusmenu-glib-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1synl6ix79a5bgihywd70zdl1n0rmjbwjlxr891wj6076d0fvybz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-glib-sys" ,rust-glib-sys-0.18)
         ("rust-gobject-sys" ,rust-gobject-sys-0.18)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-system-deps" ,rust-system-deps-6))))
    (home-page
      "https://github.com/ralismark/dbusmenu-rs")
    (synopsis "FFI bindings to dbusmenu-glib")
    (description
      "This package provides FFI bindings to dbusmenu-glib.")
    (license license:lgpl3)))

(define rust-dbusmenu-gtk3-0.1
  (package
    (name "rust-dbusmenu-gtk3")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dbusmenu-gtk3" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vj3k0pn2m6j0wj28qcy5jwgvwlva91ic4fnwk791mzfafzh4pmi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atk" ,rust-atk-0.18)
         ("rust-dbusmenu-glib" ,rust-dbusmenu-glib-0.1)
         ("rust-dbusmenu-gtk3-sys"
          ,rust-dbusmenu-gtk3-sys-0.1)
         ("rust-glib" ,rust-glib-0.18)
         ("rust-gtk" ,rust-gtk-0.18)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/ralismark/dbusmenu-rs")
    (synopsis "Rust bindings to dbusmenu-gtk3")
    (description
      "This package provides Rust bindings to dbusmenu-gtk3.")
    (license license:lgpl3)))

(define rust-dbusmenu-gtk3-sys-0.1
  (package
    (name "rust-dbusmenu-gtk3-sys")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dbusmenu-gtk3-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jkfrdhiygd2jvfrywhy41xl3xmn7ppci6sp9jl3h3pci9gvlc3g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dbusmenu-glib-sys"
          ,rust-dbusmenu-glib-sys-0.1)
         ("rust-gdk-pixbuf-sys" ,rust-gdk-pixbuf-sys-0.18)
         ("rust-gdk-sys" ,rust-gdk-sys-0.18)
         ("rust-glib-sys" ,rust-glib-sys-0.18)
         ("rust-gobject-sys" ,rust-gobject-sys-0.18)
         ("rust-gtk-sys" ,rust-gtk-sys-0.18)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-system-deps" ,rust-system-deps-6))))
    (home-page
      "https://github.com/ralismark/dbusmenu-rs")
    (synopsis "FFI bindings to dbusmenu-gtk3")
    (description
      "This package provides FFI bindings to dbusmenu-gtk3.")
    (license license:lgpl3)))

(define rust-derive-more-1
  (package
    (name "rust-derive-more")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "derive_more" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01cd8pskdjg10dvfchi6b8a9pa1ja1ic0kbn45dl8jdyrfwrk6sa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-derive-more-impl"
          ,rust-derive-more-impl-1))))
    (home-page
      "https://github.com/JelteF/derive_more")
    (synopsis
      "Adds #[derive(x)] macros for more traits")
    (description
      "This package provides Adds #[derive(x)] macros for more traits.")
    (license license:expat)))

(define rust-derive-more-impl-1
  (package
    (name "rust-derive-more-impl")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "derive_more-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08mxyd456ygk68v5nfn4dyisn82k647w9ri2jl19dqpvmnp30wyb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2)
         ("rust-unicode-xid" ,rust-unicode-xid-0.2))))
    (home-page
      "https://github.com/JelteF/derive_more")
    (synopsis
      "Internal implementation of `derive_more` crate")
    (description
      "This package provides Internal implementation of `derive_more` crate.")
    (license license:expat)))

(define rust-extend-1
  (package
    (name "rust-extend")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "extend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "003dnm6vdfx5ja0j6p7ifabgf9zdjyps0y1c7pvvyq4x3wpns6ii"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/davidpdrsn/extend")
    (synopsis
      "Create extensions for types you don't own with extension traits but without the boilerplate")
    (description
      "This package provides Create extensions for types you don't own with extension traits but without the\nboilerplate.")
    (license license:expat)))

(define rust-gdk-0.18
  (package
    (name "rust-gdk")
    (version "0.18.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gdk" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0iinlc369mrp5aj4fcw5ppb1fx1dxnckd5gwvfy5xdzkvqdhifpm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cairo-rs" ,rust-cairo-rs-0.18)
         ("rust-gdk-pixbuf" ,rust-gdk-pixbuf-0.18)
         ("rust-gdk-sys" ,rust-gdk-sys-0.18)
         ("rust-gio" ,rust-gio-0.18)
         ("rust-glib" ,rust-glib-0.18)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pango" ,rust-pango-0.18))))
    (home-page "https://gtk-rs.org/")
    (synopsis
      "UNMAINTAINED Rust bindings for the GDK 3 library (use gdk4 instead)")
    (description
      "This package provides UNMAINTAINED Rust bindings for the GDK 3 library (use gdk4 instead).")
    (license license:expat)))

(define rust-gdkx11-0.18
  (package
    (name "rust-gdkx11")
    (version "0.18.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gdkx11" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1klwrpwsap76m879gzivcafwnd3wpnf3i40jj9whylwxj2jahbnv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gdk" ,rust-gdk-0.18)
         ("rust-gdkx11-sys" ,rust-gdkx11-sys-0.18)
         ("rust-gio" ,rust-gio-0.18)
         ("rust-glib" ,rust-glib-0.18)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-x11" ,rust-x11-2))))
    (home-page "https://gtk-rs.org/")
    (synopsis
      "UNMAINTAINED Rust bindings for the GDK X11 library (use gdk4-x11 instead)")
    (description
      "This package provides UNMAINTAINED Rust bindings for the GDK X11 library (use gdk4-x11 instead).")
    (license license:expat)))

(define rust-gdkx11-sys-0.18
  (package
    (name "rust-gdkx11-sys")
    (version "0.18.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gdkx11-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hrhnm5gll7rmg2kr3q2qf1ghky9f1f0z6dq74lssv749q7z1s7y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gdk-sys" ,rust-gdk-sys-0.18)
         ("rust-glib-sys" ,rust-glib-sys-0.18)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-system-deps" ,rust-system-deps-6)
         ("rust-x11" ,rust-x11-2))))
    (home-page "https://gtk-rs.org/")
    (synopsis
      "UNMAINTAINED FFI binding for libgdkx11 (use gdk4-x11-sys instead)")
    (description
      "This package provides UNMAINTAINED FFI binding for libgdkx11 (use gdk4-x11-sys instead).")
    (license license:expat)))

(define rust-grass-0.13
  (package
    (name "rust-grass")
    (version "0.13.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "grass" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "151wydlb0543hcyv00g06wiakrp6p5xnr3g4703qzxky8cb859pp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-4)
         ("rust-getrandom" ,rust-getrandom-0.2)
         ("rust-grass-compiler" ,rust-grass-compiler-0.13))))
    (home-page
      "https://github.com/connorskees/grass")
    (synopsis "Sass compiler written purely in Rust")
    (description
      "This package provides a Sass compiler written purely in Rust.")
    (license license:expat)))

(define rust-grass-compiler-0.13
  (package
    (name "rust-grass-compiler")
    (version "0.13.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "grass_compiler" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xngzb4h04fkdhyagrwcqanrl7arghj3v5sl84cfab12y3vkv7id"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-codemap" ,rust-codemap-0.1)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-lasso" ,rust-lasso-0.7)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-phf" ,rust-phf-0.11)
         ("rust-rand" ,rust-rand-0.8))))
    (home-page
      "https://github.com/connorskees/grass")
    (synopsis
      "Internal implementation of the grass compiler")
    (description
      "This package provides Internal implementation of the grass compiler.")
    (license license:expat)))

(define rust-gtk-0.18
  (package
    (name "rust-gtk")
    (version "0.18.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gtk" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0349cpwasirgxqgsf3md0m5p9nixzsknvw55643f2q0bwbhgbi4k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atk" ,rust-atk-0.18)
         ("rust-cairo-rs" ,rust-cairo-rs-0.18)
         ("rust-field-offset" ,rust-field-offset-0.3)
         ("rust-futures-channel"
          ,rust-futures-channel-0.3)
         ("rust-gdk" ,rust-gdk-0.18)
         ("rust-gdk-pixbuf" ,rust-gdk-pixbuf-0.18)
         ("rust-gio" ,rust-gio-0.18)
         ("rust-glib" ,rust-glib-0.18)
         ("rust-gtk-sys" ,rust-gtk-sys-0.18)
         ("rust-gtk3-macros" ,rust-gtk3-macros-0.18)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pango" ,rust-pango-0.18)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://gtk-rs.org/")
    (synopsis
      "UNMAINTAINED Rust bindings for the GTK+ 3 library (use gtk4 instead)")
    (description
      "This package provides UNMAINTAINED Rust bindings for the GTK+ 3 library (use gtk4 instead).")
    (license license:expat)))

(define rust-gtk-layer-shell-0.8
  (package
    (name "rust-gtk-layer-shell")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gtk-layer-shell" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10vk23j43wq29isjnd2y752l7zqca8wa642abbdcsm8b0x1idd5d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-gdk" ,rust-gdk-0.18)
         ("rust-glib" ,rust-glib-0.18)
         ("rust-glib-sys" ,rust-glib-sys-0.18)
         ("rust-gtk" ,rust-gtk-0.18)
         ("rust-gtk-layer-shell-sys"
          ,rust-gtk-layer-shell-sys-0.7)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/pentamassiv/gtk-layer-shell-gir/tree/main/gtk-layer-shell")
    (synopsis
      "UNMAINTAINED Save gir-generated wrapper for gtk-layer-shell")
    (description
      "This package provides UNMAINTAINED Save gir-generated wrapper for gtk-layer-shell.")
    (license license:expat)))

(define rust-gtk-layer-shell-sys-0.7
  (package
    (name "rust-gtk-layer-shell-sys")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gtk-layer-shell-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0d7yj7i29qdwy188q63p6wifmjrn5agii6r3pvc9aldznp5pbamr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gdk-sys" ,rust-gdk-sys-0.18)
         ("rust-glib-sys" ,rust-glib-sys-0.18)
         ("rust-gtk-sys" ,rust-gtk-sys-0.18)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-system-deps" ,rust-system-deps-7))))
    (home-page
      "https://github.com/pentamassiv/gtk-layer-shell-gir/tree/main/gtk-layer-shell-sys")
    (synopsis
      "UNMAINTAINED Unsave gir-generated FFI bindings for gtk-layer-shell")
    (description
      "This package provides UNMAINTAINED Unsave gir-generated FFI bindings for gtk-layer-shell.")
    (license license:expat)))

(define rust-gtk3-macros-0.18
  (package
    (name "rust-gtk3-macros")
    (version "0.18.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gtk3-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0pnpp8bddisgyd1m39xhrw6kd9lawqd2xxvxzdl2jn6vcgxkw1n6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate"
          ,rust-proc-macro-crate-1)
         ("rust-proc-macro-error"
          ,rust-proc-macro-error-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://gtk-rs.org/")
    (synopsis
      "UNMAINTAINED Rust bindings for the GTK 3 library (use gtk4-macros instead)")
    (description
      "This package provides UNMAINTAINED Rust bindings for the GTK 3 library (use gtk4-macros instead).")
    (license license:expat)))

(define rust-hifijson-0.2
  (package
    (name "rust-hifijson")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hifijson" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hjwcn3hj7bmwqw084lbbvmwx7lnkfyid6b74wd0c30pwcyann4r"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/01mf02/hifijson")
    (synopsis "High-fidelity JSON lexer and parser")
    (description
      "This package provides High-fidelity JSON lexer and parser.")
    (license license:expat)))

(define rust-iana-time-zone-0.1
  (package
    (name "rust-iana-time-zone")
    (version "0.1.61")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "iana-time-zone" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "085jjsls330yj1fnwykfzmb2f10zp6l7w4fhq81ng81574ghhpi3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-android-system-properties"
          ,rust-android-system-properties-0.1)
         ("rust-core-foundation-sys"
          ,rust-core-foundation-sys-0.8)
         ("rust-iana-time-zone-haiku"
          ,rust-iana-time-zone-haiku-0.1)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-windows-core" ,rust-windows-core-0.52))))
    (home-page
      "https://github.com/strawlab/iana-time-zone")
    (synopsis
      "get the IANA time zone for the current system")
    (description
      "This package provides get the IANA time zone for the current system.")
    (license (list license:expat license:asl2.0))))

(define rust-instant-0.1
  (package
    (name "rust-instant")
    (version "0.1.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "instant" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08h27kzvb5jw74mh0ajv0nv9ggwvgqm8ynjsn2sa9jsks4cjh970"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page
      "https://github.com/sebcrozet/instant")
    (synopsis
      "Unmaintained, consider using web-time instead - A partial replacement for std::time::Instant that works on WASM to")
    (description
      "This package provides Unmaintained, consider using web-time instead - A partial replacement for\nstd::time::Instant that works on WASM to.")
    (license license:bsd-3)))

(define rust-jaq-core-1
  (package
    (name "rust-jaq-core")
    (version "1.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jaq-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1m6xq8ldf9n9j3r92lvvalkv71xaxgci3y7xjc9ci14cw2ga1zfn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-1)
         ("rust-base64" ,rust-base64-0.22)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-hifijson" ,rust-hifijson-0.2)
         ("rust-jaq-interpret" ,rust-jaq-interpret-1)
         ("rust-libm" ,rust-libm-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-regex" ,rust-regex-1)
         ("rust-urlencoding" ,rust-urlencoding-2))))
    (home-page "https://github.com/01mf02/jaq")
    (synopsis "Interpreter for the jaq language")
    (description
      "This package provides Interpreter for the jaq language.")
    (license license:expat)))

(define rust-jaq-interpret-1
  (package
    (name "rust-jaq-interpret")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jaq-interpret" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yz1dahd0r6p76mai5n4cqx016z4yn9ia2fi7ngzvwsaqb1mxs9g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ahash" ,rust-ahash-0.8)
         ("rust-dyn-clone" ,rust-dyn-clone-1)
         ("rust-hifijson" ,rust-hifijson-0.2)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-jaq-syn" ,rust-jaq-syn-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/01mf02/jaq")
    (synopsis "Interpreter for the jaq language")
    (description
      "This package provides Interpreter for the jaq language.")
    (license license:expat)))

(define rust-jaq-parse-1
  (package
    (name "rust-jaq-parse")
    (version "1.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jaq-parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10pfdpmw5apzx9dckmbldhsk58r6cqyiyn19v6naipbc2k9xfih3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chumsky" ,rust-chumsky-0.9)
         ("rust-jaq-syn" ,rust-jaq-syn-1))))
    (home-page "https://github.com/01mf02/jaq")
    (synopsis "Parser for the jaq language")
    (description
      "This package provides Parser for the jaq language.")
    (license license:expat)))

(define rust-jaq-std-1
  (package
    (name "rust-jaq-std")
    (version "1.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jaq-std" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1h8v663wwgv2k40cr4pzmzj69hz0843kfjjr7d1p0fzxg1asbfmz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-jaq-syn" ,rust-jaq-syn-1))))
    (home-page "https://github.com/01mf02/jaq")
    (synopsis "Standard library for jaq")
    (description
      "This package provides Standard library for jaq.")
    (license license:expat)))

(define rust-jaq-syn-1
  (package
    (name "rust-jaq-syn")
    (version "1.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jaq-syn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0j1rdmr33fzb4b96n6jg1jvczsby0jpcn7i60i330wcc8bj4z90v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/01mf02/jaq")
    (synopsis "Parser for the jq language")
    (description
      "This package provides Parser for the jq language.")
    (license license:expat)))

(define rust-lalrpop-0.21
  (package
    (name "rust-lalrpop")
    (version "0.21.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lalrpop" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15ypxmqkzfv3340ngvc9g6n78ikpi8b3kpav3y6n446nw8iz6mkf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ascii-canvas" ,rust-ascii-canvas-3)
         ("rust-bit-set" ,rust-bit-set-0.6)
         ("rust-ena" ,rust-ena-0.14)
         ("rust-itertools" ,rust-itertools-0.13)
         ("rust-lalrpop-util" ,rust-lalrpop-util-0.21)
         ("rust-petgraph" ,rust-petgraph-0.6)
         ("rust-pico-args" ,rust-pico-args-0.5)
         ("rust-regex" ,rust-regex-1)
         ("rust-regex-syntax" ,rust-regex-syntax-0.8)
         ("rust-sha3" ,rust-sha3-0.10)
         ("rust-string-cache" ,rust-string-cache-0.8)
         ("rust-term" ,rust-term-0.7)
         ("rust-unicode-xid" ,rust-unicode-xid-0.2)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/lalrpop/lalrpop")
    (synopsis "convenient LR(1) parser generator")
    (description
      "This package provides convenient LR(1) parser generator.")
    (license (list license:asl2.0 license:expat))))

(define rust-lalrpop-util-0.21
  (package
    (name "rust-lalrpop-util")
    (version "0.21.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lalrpop-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qcg3ph4hkdwd0x7sghgq3fgaiyqfxaha8rml1jjrndsvbswi38h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-regex-automata" ,rust-regex-automata-0.4))))
    (home-page "https://github.com/lalrpop/lalrpop")
    (synopsis
      "Runtime library for parsers generated by LALRPOP")
    (description
      "This package provides Runtime library for parsers generated by LALRPOP.")
    (license (list license:asl2.0 license:expat))))

(define rust-lasso-0.7
  (package
    (name "rust-lasso")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lasso" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yz92fy2zv6wslfwwf3j7lw1wxja8d91rrcwgfzv751l1ajys53f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hashbrown" ,rust-hashbrown-0.14))))
    (home-page "https://github.com/Kixiron/lasso")
    (synopsis
      "multithreaded and single threaded string interner that allows strings to be cached with a\nminimal memory footprint, associating them with a unique key that can be used to retrieve them at any time.")
    (description
      "This package provides a multithreaded and single threaded string interner that\nallows strings to be cached with a minimal memory footprint, associating them\nwith a unique key that can be used to retrieve them at any time.")
    (license (list license:expat license:asl2.0))))

(define rust-memoffset-0.9
  (package
    (name "rust-memoffset")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memoffset" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12i17wh9a9plx869g7j4whf62xw68k5zd4k0k5nh6ys5mszid028"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1))))
    (home-page "https://github.com/Gilnaa/memoffset")
    (synopsis
      "offset_of functionality for Rust structs")
    (description
      "This package provides offset_of functionality for Rust structs.")
    (license license:expat)))

(define rust-new-debug-unreachable-1
  (package
    (name "rust-new-debug-unreachable")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "new_debug_unreachable" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11phpf1mjxq6khk91yzcbd3ympm78m3ivl7xg6lg2c0lf66fy3k5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/mbrubeck/rust-debug-unreachable")
    (synopsis
      "panic in debug, intrinsics::unreachable() in release (fork of debug_unreachable)")
    (description
      "This package provides panic in debug, @code{intrinsics::unreachable()} in release (fork of\ndebug_unreachable).")
    (license license:expat)))

(define rust-parking-2
  (package
    (name "rust-parking")
    (version "2.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fnfgmzkfpjd69v4j9x737b1k8pnn054bvzcn5dm3pkgq595d3gk"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/smol-rs/parking")
    (synopsis "Thread parking and unparking")
    (description
      "This package provides Thread parking and unparking.")
    (license (list license:asl2.0 license:expat))))

(define rust-parse-zoneinfo-0.3
  (package
    (name "rust-parse-zoneinfo")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parse-zoneinfo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "093cs8slbd6kyfi6h12isz0mnaayf5ha8szri1xrbqj4inqhaahz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-regex" ,rust-regex-1))))
    (home-page
      "https://github.com/chronotope/chrono-tz")
    (synopsis
      "Parse zoneinfo files from the IANA database")
    (description
      "This package provides Parse zoneinfo files from the IANA database.")
    (license license:expat)))

(define rust-phf-codegen-0.11
  (package
    (name "rust-phf-codegen")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_codegen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nia6h4qfwaypvfch3pnq1nd2qj64dif4a6kai3b7rjrsf49dlz8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-generator" ,rust-phf-generator-0.11)
         ("rust-phf-shared" ,rust-phf-shared-0.11))))
    (home-page
      "https://github.com/rust-phf/rust-phf")
    (synopsis "Codegen library for PHF types")
    (description
      "This package provides Codegen library for PHF types.")
    (license license:expat)))

(define rust-phf-generator-0.11
  (package
    (name "rust-phf-generator")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_generator" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c14pjyxbcpwkdgw109f7581cc5fa3fnkzdq1ikvx7mdq9jcrr28"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-shared" ,rust-phf-shared-0.11)
         ("rust-rand" ,rust-rand-0.8))))
    (home-page
      "https://github.com/rust-phf/rust-phf")
    (synopsis "PHF generation logic")
    (description
      "This package provides PHF generation logic.")
    (license license:expat)))

(define rust-piper-0.2
  (package
    (name "rust-piper")
    (version "0.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "piper" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rn0mjjm0cwagdkay77wgmz3sqf8fqmv9d9czm79mvr2yj8c9j4n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atomic-waker" ,rust-atomic-waker-1)
         ("rust-fastrand" ,rust-fastrand-2)
         ("rust-futures-io" ,rust-futures-io-0.3))))
    (home-page "https://github.com/smol-rs/piper")
    (synopsis
      "Async pipes, channels, mutexes, and more")
    (description
      "This package provides Async pipes, channels, mutexes, and more.")
    (license (list license:expat license:asl2.0))))

(define rust-proc-macro-crate-1
  (package
    (name "rust-proc-macro-crate")
    (version "1.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-crate" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "069r1k56bvgk0f58dm5swlssfcp79im230affwk6d9ck20g04k3z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-toml-edit" ,rust-toml-edit-0.19))))
    (home-page
      "https://github.com/bkchr/proc-macro-crate")
    (synopsis
      "Replacement for crate (macro_rules keyword) in proc-macros")
    (description
      "This package provides Replacement for crate (macro_rules keyword) in proc-macros.")
    (license (list license:expat license:asl2.0))))

(define rust-proc-macro-crate-2
  (package
    (name "rust-proc-macro-crate")
    (version "2.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-crate" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "092x5acqnic14cw6vacqap5kgknq3jn4c6jij9zi6j85839jc3xh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-toml-datetime" ,rust-toml-datetime-0.6)
         ("rust-toml-edit" ,rust-toml-edit-0.20))))
    (home-page
      "https://github.com/bkchr/proc-macro-crate")
    (synopsis
      "Replacement for crate (macro_rules keyword) in proc-macros")
    (description
      "This package provides Replacement for crate (macro_rules keyword) in proc-macros.")
    (license (list license:expat license:asl2.0))))

(define rust-redox-syscall-0.5
  (package
    (name "rust-redox-syscall")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0fx1c883b6jqw5v5b4cdv1sfm2bnss36w97qscal786krhay8nim"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2))))
    (home-page
      "https://gitlab.redox-os.org/redox-os/syscall")
    (synopsis
      "Rust library to access raw Redox system calls")
    (description
      "This package provides a Rust library to access raw Redox system calls.")
    (license license:expat)))

(define rust-redox-users-0.4
  (package
    (name "rust-redox-users")
    (version "0.4.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_users" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hya2cxx6hxmjfxzv9n8rjl5igpychav7zfi1f81pz6i4krry05s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-getrandom" ,rust-getrandom-0.2)
         ("rust-libredox" ,rust-libredox-0.1)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page
      "https://gitlab.redox-os.org/redox-os/users")
    (synopsis
      "Rust library to access Redox users and groups functionality")
    (description
      "This package provides a Rust library to access Redox users and groups\nfunctionality.")
    (license license:expat)))

(define rust-ref-cast-1
  (package
    (name "rust-ref-cast")
    (version "1.0.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ref-cast" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ca9fi5jsdibaidi2a55y9i1k1q0hvn4f6xlm0fmh7az9pwadw6c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ref-cast-impl" ,rust-ref-cast-impl-1))))
    (home-page "https://github.com/dtolnay/ref-cast")
    (synopsis
      "Safely cast &T to &U where the struct U contains a single field of type T")
    (description
      "This package provides Safely cast &T to &U where the struct U contains a single field of type T.")
    (license (list license:expat license:asl2.0))))

(define rust-ref-cast-impl-1
  (package
    (name "rust-ref-cast-impl")
    (version "1.0.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ref-cast-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rpkjlsr99g8nb5ripffz9n9rb3g32dmw83x724l8wykjgkh7hxw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/dtolnay/ref-cast")
    (synopsis
      "Derive implementation for ref_cast::RefCast")
    (description
      "This package provides Derive implementation for ref_cast::@code{RefCast}.")
    (license (list license:expat license:asl2.0))))

(define rust-serde-spanned-0.6
  (package
    (name "rust-serde-spanned")
    (version "0.6.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_spanned" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1q89g70azwi4ybilz5jb8prfpa575165lmrffd49vmcf76qpqq47"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/toml-rs/toml")
    (synopsis "Serde-compatible spanned Value")
    (description
      "This package provides Serde-compatible spanned Value.")
    (license (list license:expat license:asl2.0))))

(define rust-signal-hook-registry-1
  (package
    (name "rust-signal-hook-registry")
    (version "1.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook-registry" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cb5akgq8ajnd5spyn587srvs4n26ryq0p78nswffwhv46sf1sd9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/vorner/signal-hook")
    (synopsis "Backend crate for signal-hook")
    (description
      "This package provides Backend crate for signal-hook.")
    (license (list license:asl2.0 license:expat))))

(define rust-simple-signal-1
  (package
    (name "rust-simple-signal")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "simple-signal" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12r82dpipdkkfmslp04pd3b2fpr9h4zxjfs8axynchncmm2dmxsk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/swizard0/rust-simple-signal")
    (synopsis
      "Easy unix signals handler for Rust projects")
    (description
      "This package provides Easy unix signals handler for Rust projects.")
    (license license:expat)))

(define rust-socket2-0.4
  (package
    (name "rust-socket2")
    (version "0.4.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "socket2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03ack54dxhgfifzsj14k7qa3r5c9wqy3v6mqhlim99cc03y1cycz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page
      "https://github.com/rust-lang/socket2")
    (synopsis
      "Utilities for handling networking sockets with a maximal amount of configuration\npossible intended.")
    (description
      "This package provides Utilities for handling networking sockets with a maximal amount of configuration\npossible intended.")
    (license (list license:expat license:asl2.0))))

(define rust-termcolor-1
  (package
    (name "rust-termcolor")
    (version "1.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "termcolor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mappjh3fj3p2nmrg4y7qv94rchwi9mzmgmfflr8p2awdj7lyy86"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-util" ,rust-winapi-util-0.1))))
    (home-page
      "https://github.com/BurntSushi/termcolor")
    (synopsis
      "simple cross platform library for writing colored text to a terminal.")
    (description
      "This package provides a simple cross platform library for writing colored text\nto a terminal.")
    (license (list license:unlicense license:expat))))

(define rust-tokio-util-0.7
  (package
    (name "rust-tokio-util")
    (version "0.7.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0spc0g4irbnf2flgag22gfii87avqzibwfm0si0d1g0k9ijw7rv1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://tokio.rs")
    (synopsis
      "Additional utilities for working with Tokio.")
    (description
      "This package provides Additional utilities for working with Tokio.")
    (license license:expat)))

(define rust-typenum-1
  (package
    (name "rust-typenum")
    (version "1.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typenum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09dqxv69m9lj9zvv6xw5vxaqx15ps0vxyy5myg33i0kbqvq0pzs2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/paholg/typenum")
    (synopsis
      "Typenum is a Rust library for type-level numbers evaluated at\n    compile time. It currently supports bits, unsigned integers, and signed\n    integers. It also provides a type-level array of type-level numbers, but its\n    implementation is incomplete")
    (description
      "This package provides Typenum is a Rust library for type-level numbers evaluated at compile time.  It\ncurrently supports bits, unsigned integers, and signed integers.  It also\nprovides a type-level array of type-level numbers, but its implementation is\nincomplete.")
    (license (list license:expat license:asl2.0))))

(define rust-unescape-0.1
  (package
    (name "rust-unescape")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unescape" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vlgws15n4kz8xq4igzr1f80nbiyr838k687hn6ly8a36an7vffc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/saghm/unescape-rs")
    (synopsis
      "Unescapes strings with escape sequences written out as literal characters")
    (description
      "This package provides Unescapes strings with escape sequences written out as literal characters.")
    (license license:expat)))

(define rust-utf8parse-0.2
  (package
    (name "rust-utf8parse")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "utf8parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "088807qwjq46azicqwbhlmzwrbkz7l4hpw43sdkdyyk524vdxaq6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/alacritty/vte")
    (synopsis "Table-driven UTF-8 parser")
    (description
      "This package provides Table-driven UTF-8 parser.")
    (license (list license:asl2.0 license:expat))))

(define eww
  (let ((version "0.6.0")
        (commit "a7bd80ac1ec77f0c473c7ec70240f8329bffa07b")
        (revision "0"))
    (package
      (name "eww")
      (version (git-version version revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://github.com/elkowar/eww")
                 (commit commit)))
          (sha256
            (base32 "1wx0541xv7bpgsprqv0w8pxmh87cvaswgz4xs6k5ds6shdr6f5ql"))))
      (build-system cargo-build-system)
      (arguments
        `(#:skip-build? #f
          #:install-source? #f
          #:tests? #f
          #:imported-modules (,@%cargo-build-system-modules
                              (guix build cargo-build-system))
          #:modules ((guix build utils)
                     (guix build cargo-build-system)
                     (ice-9 match))
          #:phases
          ,#~(modify-phases %standard-phases
               (replace 'install
                 (lambda _
                   (install-file "target/release/eww"
                                 (string-append #$output "/bin")))))
          #:cargo-inputs
          (("rust-anyhow" ,rust-anyhow-1)
           ("rust-bincode" ,rust-bincode-1)
           ("rust-cached" ,rust-cached-0.53)
           ("rust-chrono" ,rust-chrono-0.4)
           ("rust-chrono-tz" ,rust-chrono-tz-0.10)
           ("rust-clap" ,rust-clap-4)
           ("rust-clap-complete" ,rust-clap-complete-4)
           ("rust-codespan-reporting" ,rust-codespan-reporting-0.11)
           ("rust-dbusmenu-gtk3" ,rust-dbusmenu-gtk3-0.1)
           ("rust-derive-more" ,rust-derive-more-1)
           ("rust-extend" ,rust-extend-1)
           ("rust-futures" ,rust-futures-0.3)
           ("rust-gdk-sys" ,rust-gdk-sys-0.18)
           ("rust-gdkx11" ,rust-gdkx11-0.18)
           ("rust-grass" ,rust-grass-0.13)
           ("rust-gtk" ,rust-gtk-0.18)
           ("rust-gtk-layer-shell" ,rust-gtk-layer-shell-0.8)
           ("rust-insta" ,rust-insta-1)
           ("rust-itertools" ,rust-itertools-0.13)
           ("rust-jaq-core" ,rust-jaq-core-1)
           ("rust-jaq-interpret" ,rust-jaq-interpret-1)
           ("rust-jaq-parse" ,rust-jaq-parse-1)
           ("rust-jaq-std" ,rust-jaq-std-1)
           ("rust-jaq-syn" ,rust-jaq-syn-1)
           ("rust-lalrpop" ,rust-lalrpop-0.21)
           ("rust-lalrpop-util" ,rust-lalrpop-util-0.21)
           ("rust-libc" ,rust-libc-0.2)
           ("rust-log" ,rust-log-0.4)
           ("rust-maplit" ,rust-maplit-1)
           ("rust-nix" ,rust-nix-0.29)
           ("rust-notify" ,rust-notify-6)
           ("rust-once-cell" ,rust-once-cell-1)
           ("rust-ordered-stream" ,rust-ordered-stream-0.2)
           ("rust-pretty-assertions" ,rust-pretty-assertions-1)
           ("rust-pretty-env-logger" ,rust-pretty-env-logger-0.5)
           ("rust-ref-cast" ,rust-ref-cast-1)
           ("rust-regex" ,rust-regex-1)
           ("rust-serde" ,rust-serde-1)
           ("rust-serde-json" ,rust-serde-json-1)
           ("rust-simple-signal" ,rust-simple-signal-1)
           ("rust-smart-default" ,rust-smart-default-0.7)
           ("rust-static-assertions" ,rust-static-assertions-1)
           ("rust-strsim" ,rust-strsim-0.11)
           ("rust-strum" ,rust-strum-0.26)
           ("rust-sysinfo" ,rust-sysinfo-0.31)
           ("rust-thiserror" ,rust-thiserror-1)
           ("rust-tokio" ,rust-tokio-1)
           ("rust-tokio-util" ,rust-tokio-util-0.7)
           ("rust-unescape" ,rust-unescape-0.1)
           ("rust-wait-timeout" ,rust-wait-timeout-0.2)
           ("rust-x11rb" ,rust-x11rb-0.13)
           ("rust-zbus" ,rust-zbus-3))))
      (native-inputs (list pkg-config
                           glib
                           gtk+
                           libdbusmenu
                           gtk-layer-shell))
      (home-page "https://elkowar.github.io/eww")
      (synopsis "ElKowars wacky widgets")
      (description "Elkowars Wacky Widgets is a standalone widget system made in Rust that allows you to implement your own, custom widgets in any window manager.")
      (license license:expat))))

(define eww/wayland
  (package/inherit eww
    (name "eww-wayland")
    (arguments
      (substitute-keyword-arguments (package-arguments eww)
        ((#:cargo-build-flags flags)
         #~(cons* "--release"
                  "--no-default-features"
                  "--features=wayland"
                  #$flags))))))

(define eww/x11
  (package/inherit eww
    (name "eww-x11")
    (arguments
      (substitute-keyword-arguments (package-arguments eww)
        ((#:cargo-build-flags flags)
         #~(cons* "--release"
                  "--no-default-features"
                  "--features=x11"
                  #$flags))))))
