;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages rust yazi)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages assembly)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-compression)
  #:use-module (gnu packages crates-crypto)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages jemalloc)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages video)
  #:use-module (gnu packages web)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define rust-addr2line-0.24
  (package
    (name "rust-addr2line")
    (version "0.24.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "addr2line" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hd1i57zxgz08j6h5qrhsnm2fi0bcqvsh389fw400xm3arz2ggnz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gimli" ,rust-gimli-0.31))))
    (home-page
      "https://github.com/gimli-rs/addr2line")
    (synopsis
      "cross-platform symbolication library written in Rust, using `gimli`")
    (description
      "This package provides a cross-platform symbolication library written in Rust,\nusing `gimli`.")
    (license (list license:asl2.0 license:expat))))

(define rust-aligned-vec-0.5
  (package
    (name "rust-aligned-vec")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aligned-vec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1lb8qjqfap028ylf8zap6rkwrnrqimc3v6h3cixycjrdx1y0vaaa"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/sarah-ek/aligned-vec/")
    (synopsis "Aligned vector and box containers")
    (description
      "This package provides Aligned vector and box containers.")
    (license license:expat)))

(define rust-allocator-api2-0.2
  (package
    (name "rust-allocator-api2")
    (version "0.2.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "allocator-api2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08zrzs022xwndihvzdn78yqarv2b9696y67i6h78nla3ww87jgb8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/zakarumych/allocator-api2")
    (synopsis "Mirror of Rust's allocator API")
    (description
      "This package provides Mirror of Rust's allocator API.")
    (license (list license:expat license:asl2.0))))

(define rust-ansi-to-tui-7
  (package
    (name "rust-ansi-to-tui")
    (version "7.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ansi-to-tui" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0b4iynqcqaav8i55w8lk7ypm6xr845vh32lcw8vxffff3qgmwmb7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-nom" ,rust-nom-7)
         ("rust-ratatui" ,rust-ratatui-0.29)
         ("rust-simdutf8" ,rust-simdutf8-0.1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page
      "https://github.com/uttarayan21/ansi-to-tui")
    (synopsis
      "library to convert ansi color coded text into ratatui::text::Text type from ratatui library")
    (description
      "This package provides a library to convert ansi color coded text into\nratatui::text::Text type from ratatui library.")
    (license license:expat)))

(define rust-anstyle-1
  (package
    (name "rust-anstyle")
    (version "1.0.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yai2vppmd7zlvlrp9grwll60knrmscalf8l2qpfz8b7y5lkpk2m"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/rust-cli/anstyle")
    (synopsis "ANSI text styling")
    (description
      "This package provides ANSI text styling.")
    (license (list license:expat license:asl2.0))))

(define rust-anstyle-parse-0.2
  (package
    (name "rust-anstyle-parse")
    (version "0.2.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1acqayy22fwzsrvr6n0lz6a4zvjjcvgr5sm941m7m0b2fr81cb9v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-utf8parse" ,rust-utf8parse-0.2))))
    (home-page "https://github.com/rust-cli/anstyle")
    (synopsis "Parse ANSI Style Escapes")
    (description
      "This package provides Parse ANSI Style Escapes.")
    (license (list license:expat license:asl2.0))))

(define rust-anstyle-query-1
  (package
    (name "rust-anstyle-query")
    (version "1.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-query" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "036nm3lkyk43xbps1yql3583fp4hg3b1600is7mcyxs1gzrpm53r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page
      "https://github.com/rust-cli/anstyle.git")
    (synopsis "Look up colored console capabilities")
    (description
      "This package provides Look up colored console capabilities.")
    (license (list license:expat license:asl2.0))))

(define rust-anyhow-1
  (package
    (name "rust-anyhow")
    (version "1.0.94")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anyhow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xqz3j4h3dxiqi37k8dwl5cc2sb3rlzy7rywfqiblf7g52h07zf1"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/dtolnay/anyhow")
    (synopsis
      "Flexible concrete Error type built on std::error::Error")
    (description
      "This package provides Flexible concrete Error type built on std::error::Error.")
    (license (list license:expat license:asl2.0))))

(define rust-arbitrary-1
  (package
    (name "rust-arbitrary")
    (version "1.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "arbitrary" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08zj2yanll5s5gsbmvgwvbq39iqzy3nia3yx3db3zwba08yhpqnx"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/rust-fuzz/arbitrary/")
    (synopsis
      "The trait for generating structured data from unstructured data")
    (description
      "This package provides The trait for generating structured data from unstructured data.")
    (license (list license:expat license:asl2.0))))

(define rust-async-priority-channel-0.2
  (package
    (name "rust-async-priority-channel")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-priority-channel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0n4pk6dgx0w3i43dsjgfn8f3wzdrhv3kvi65c3vk246k8ks9dpmc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-event-listener" ,rust-event-listener-4))))
    (home-page
      "https://github.com/rmcgibbo/async-priority-channel")
    (synopsis
      "An async channel where pending messages are delivered in order of priority")
    (description
      "This package provides An async channel where pending messages are delivered in order of priority.")
    (license (list license:asl2.0 license:expat))))

(define rust-autocfg-1
  (package
    (name "rust-autocfg")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "autocfg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09lz3by90d2hphbq56znag9v87gfpd9gb8nr82hll8z6x2nhprdc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/cuviper/autocfg")
    (synopsis
      "Automatic cfg for Rust compiler features")
    (description
      "This package provides Automatic cfg for Rust compiler features.")
    (license (list license:asl2.0 license:expat))))

(define rust-bit-field-0.10
  (package
    (name "rust-bit-field")
    (version "0.10.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bit_field" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0qav5rpm4hqc33vmf4vc4r0mh51yjx5vmd9zhih26n9yjs3730nw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/phil-opp/rust-bit-field")
    (synopsis
      "Simple bit field trait providing get_bit, get_bits, set_bit, and set_bits methods for Rust's integral types")
    (description
      "This package provides Simple bit field trait providing get_bit, get_bits, set_bit, and set_bits\nmethods for Rust's integral types.")
    (license (list license:asl2.0 license:expat))))

(define rust-bitstream-io-2
  (package
    (name "rust-bitstream-io")
    (version "2.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitstream-io" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cli390l1dhp9skygyjjnqvczp36b7f31mkx9ry3dg26330cv6b0"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/tuffy/bitstream-io")
    (synopsis
      "Library for reading/writing un-aligned values from/to streams in big-endian and little-endian formats")
    (description
      "This package provides Library for reading/writing un-aligned values from/to streams in big-endian and\nlittle-endian formats.")
    (license (list license:expat license:asl2.0))))

(define rust-bstr-1
  (package
    (name "rust-bstr")
    (version "1.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bstr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08my0pjgnqxc17fw061inxnpkjw05bp2w6wl9gpch3nzgksg2s0s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/BurntSushi/bstr")
    (synopsis
      "string type that is not required to be valid UTF-8.")
    (description
      "This package provides a string type that is not required to be valid UTF-8.")
    (license (list license:expat license:asl2.0))))

(define rust-built-0.7
  (package
    (name "rust-built")
    (version "0.7.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "built" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0fyhzjgymls3qylggd6rs4vkq44rkl1kyv33lfbfrdsjxmd50q63"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/lukaslueg/built")
    (synopsis
      "Provides a crate with information from the time it was built")
    (description
      "This package provides a crate with information from the time it was built.")
    (license license:expat)))

(define rust-bytemuck-1
  (package
    (name "rust-bytemuck")
    (version "1.20.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytemuck" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nk9llwmvjpjlsrlga1qg3spqvci8g1nr286nhamvn7zcf5chdwb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/Lokathor/bytemuck")
    (synopsis
      "crate for mucking around with piles of bytes.")
    (description
      "This package provides a crate for mucking around with piles of bytes.")
    (license
      (list license:zlib license:asl2.0 license:expat))))

(define rust-bytes-1
  (package
    (name "rust-bytes")
    (version "1.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16ykzx24v1x4f42v2lxyvlczqhdfji3v7r4ghwckpwijzvb1hn9j"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/tokio-rs/bytes")
    (synopsis
      "Types and traits for working with bytes")
    (description
      "This package provides Types and traits for working with bytes.")
    (license license:expat)))

(define rust-castaway-0.2
  (package
    (name "rust-castaway")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "castaway" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mf0wypwnkpa1hi0058vp8g7bjh2qraip2qv7dmak7mg1azfkfha"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustversion" ,rust-rustversion-1))))
    (home-page
      "https://github.com/sagebind/castaway")
    (synopsis
      "Safe, zero-cost downcasting for limited compile-time specialization")
    (description
      "This package provides Safe, zero-cost downcasting for limited compile-time specialization.")
    (license license:expat)))

(define rust-cc-1
  (package
    (name "rust-cc")
    (version "1.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bcga5xf5fgvddfamsjhg89hlydzbdk1fwvcym5kkxfggdj5gxi7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-jobserver" ,rust-jobserver-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-shlex" ,rust-shlex-1))))
    (home-page "https://github.com/rust-lang/cc-rs")
    (synopsis
      "build-time dependency for Cargo build scripts to assist in invoking the native\nC compiler to compile native C code into a static archive to be linked into Rust\ncode.")
    (description
      "This package provides a build-time dependency for Cargo build scripts to assist\nin invoking the native C compiler to compile native C code into a static archive\nto be linked into Rust code.")
    (license (list license:expat license:asl2.0))))

(define rust-chrono-0.4
  (package
    (name "rust-chrono")
    (version "0.4.39")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chrono" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09g8nf409lb184kl9j4s85k0kn8wzgjkp5ls9zid50b886fwqdky"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-android-tzdata" ,rust-android-tzdata-0.1)
         ("rust-iana-time-zone" ,rust-iana-time-zone-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-windows-targets"
          ,rust-windows-targets-0.52))))
    (home-page
      "https://github.com/chronotope/chrono")
    (synopsis "Date and time library for Rust")
    (description
      "This package provides Date and time library for Rust.")
    (license (list license:expat license:asl2.0))))

(define rust-clap-4
  (package
    (name "rust-clap")
    (version "4.5.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "110cf0i9fmkfqzqhi1h8za9y0vnr5rwhy3wmv1p0rcgp5vnffd9i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap-builder" ,rust-clap-builder-4)
         ("rust-clap-derive" ,rust-clap-derive-4))))
    (home-page "https://github.com/clap-rs/clap")
    (synopsis
      "simple to use, efficient, and full-featured Command Line Argument Parser")
    (description
      "This package provides a simple to use, efficient, and full-featured Command Line\nArgument Parser.")
    (license (list license:expat license:asl2.0))))

(define rust-clap-builder-4
  (package
    (name "rust-clap-builder")
    (version "4.5.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_builder" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0f28rgc09kdgfq1hgg1bb1ydaw243w6dwyw74syz439k6b32yn1h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anstream" ,rust-anstream-0.6)
         ("rust-anstyle" ,rust-anstyle-1)
         ("rust-clap-lex" ,rust-clap-lex-0.7)
         ("rust-strsim" ,rust-strsim-0.11))))
    (home-page "https://github.com/clap-rs/clap")
    (synopsis
      "simple to use, efficient, and full-featured Command Line Argument Parser")
    (description
      "This package provides a simple to use, efficient, and full-featured Command Line\nArgument Parser.")
    (license (list license:expat license:asl2.0))))

(define rust-clap-complete-4
  (package
    (name "rust-clap-complete")
    (version "4.5.38")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_complete" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00bsiq6jn6br4wrq3165hfl80mk4sdrdq97pgkqpa88ikiaplr6r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-4))))
    (home-page "https://github.com/clap-rs/clap")
    (synopsis
      "Generate shell completion scripts for your clap::Command")
    (description
      "This package provides Generate shell completion scripts for your clap::Command.")
    (license (list license:expat license:asl2.0))))

(define rust-clap-complete-nushell-4
  (package
    (name "rust-clap-complete-nushell")
    (version "4.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_complete_nushell" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xvnl61gnc3j76b9y50y35zvg7fls30i7lyb43fmsvncj3kh4n9i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-4)
         ("rust-clap-complete" ,rust-clap-complete-4))))
    (home-page "https://github.com/clap-rs/clap")
    (synopsis
      "generator library used with clap for Nushell completion scripts")
    (description
      "This package provides a generator library used with clap for Nushell completion\nscripts.")
    (license (list license:expat license:asl2.0))))

(define rust-clap-lex-0.7
  (package
    (name "rust-clap-lex")
    (version "0.7.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_lex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19nwfls5db269js5n822vkc8dw0wjq2h1wf0hgr06ld2g52d2spl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/clap-rs/clap")
    (synopsis
      "Minimal, flexible command line parser")
    (description
      "This package provides Minimal, flexible command line parser.")
    (license (list license:expat license:asl2.0))))

(define rust-clipboard-win-5
  (package
    (name "rust-clipboard-win")
    (version "5.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clipboard-win" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14n87fc0vzbd0wdhqzvcs1lqgafsncplzcanhpik93xhhalfgvqm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-error-code" ,rust-error-code-3))))
    (home-page
      "https://github.com/DoumanAsh/clipboard-win")
    (synopsis
      "Provides simple way to interact with Windows clipboard")
    (description
      "This package provides simple way to interact with Windows clipboard.")
    (license license:boost1.0)))

(define rust-colorchoice-1
  (package
    (name "rust-colorchoice")
    (version "1.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "colorchoice" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1439m3r3jy3xqck8aa13q658visn71ki76qa93cy55wkmalwlqsv"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/rust-cli/anstyle.git")
    (synopsis "Global override of color control")
    (description
      "This package provides Global override of color control.")
    (license (list license:expat license:asl2.0))))

(define rust-compact-str-0.8
  (package
    (name "rust-compact-str")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "compact_str" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0i7nscqyc2szdqpzfqnakcih8mq1f74g4c8b2q9f9cnsdnhw6l30"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-castaway" ,rust-castaway-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-ryu" ,rust-ryu-1)
         ("rust-static-assertions"
          ,rust-static-assertions-1))))
    (home-page
      "https://github.com/ParkMyCar/compact_str")
    (synopsis
      "memory efficient string type that transparently stores strings on the stack, when possible")
    (description
      "This package provides a memory efficient string type that transparently stores\nstrings on the stack, when possible.")
    (license license:expat)))

(define rust-console-0.15
  (package
    (name "rust-console")
    (version "0.15.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "console" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1sz4nl9nz8pkmapqni6py7jxzi7nzqjxzb3ya4kxvmkb0zy867qf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-encode-unicode" ,rust-encode-unicode-0.3)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page
      "https://github.com/console-rs/console")
    (synopsis
      "terminal and console abstraction for Rust")
    (description
      "This package provides a terminal and console abstraction for Rust.")
    (license license:expat)))

(define rust-crossterm-0.28
  (package
    (name "rust-crossterm")
    (version "0.28.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossterm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1im9vs6fvkql0sr378dfr4wdm1rrkrvr22v4i8byz05k1dd9b7c2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-crossterm-winapi"
          ,rust-crossterm-winapi-0.9)
         ("rust-filedescriptor" ,rust-filedescriptor-0.8)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-mio" ,rust-mio-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-rustix" ,rust-rustix-0.38)
         ("rust-signal-hook" ,rust-signal-hook-0.3)
         ("rust-signal-hook-mio"
          ,rust-signal-hook-mio-0.2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page
      "https://github.com/crossterm-rs/crossterm")
    (synopsis
      "crossplatform terminal library for manipulating terminals.")
    (description
      "This package provides a crossplatform terminal library for manipulating\nterminals.")
    (license license:expat)))

(define rust-darling-0.20
  (package
    (name "rust-darling")
    (version "0.20.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1299h2z88qn71mizhh05j26yr3ik0wnqmw11ijds89l8i9nbhqvg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling-core" ,rust-darling-core-0.20)
         ("rust-darling-macro" ,rust-darling-macro-0.20))))
    (home-page
      "https://github.com/TedDriggs/darling")
    (synopsis
      "proc-macro library for reading attributes into structs when\nimplementing custom derives.")
    (description
      "This package provides a proc-macro library for reading attributes into structs\nwhen implementing custom derives.")
    (license license:expat)))

(define rust-darling-core-0.20
  (package
    (name "rust-darling-core")
    (version "0.20.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rgr9nci61ahnim93yh3xy6fkfayh7sk4447hahawah3m1hkh4wm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fnv" ,rust-fnv-1)
         ("rust-ident-case" ,rust-ident-case-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-strsim" ,rust-strsim-0.11)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/TedDriggs/darling")
    (synopsis
      "Helper crate for proc-macro library for reading attributes into structs when\nimplementing custom derives. Use https://crates.io/crates/darling in your code.")
    (description
      "This package provides Helper crate for proc-macro library for reading attributes into structs when\nimplementing custom derives.  Use https://crates.io/crates/darling in your code.")
    (license license:expat)))

(define rust-darling-macro-0.20
  (package
    (name "rust-darling-macro")
    (version "0.20.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling_macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01kq3ibbn47czijj39h3vxyw0c2ksd0jvc097smcrk7n2jjs4dnk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling-core" ,rust-darling-core-0.20)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/TedDriggs/darling")
    (synopsis
      "Internal support for a proc-macro library for reading attributes into structs when\nimplementing custom derives. Use https://crates.io/crates/darling in your code.")
    (description
      "This package provides Internal support for a proc-macro library for reading attributes into structs\nwhen implementing custom derives.  Use https://crates.io/crates/darling in your\ncode.")
    (license license:expat)))

(define rust-derive-builder-0.20
  (package
    (name "rust-derive-builder")
    (version "0.20.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "derive_builder" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0is9z7v3kznziqsxa5jqji3ja6ay9wzravppzhcaczwbx84znzah"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-derive-builder-macro"
          ,rust-derive-builder-macro-0.20))))
    (home-page
      "https://github.com/colin-kiegel/rust-derive-builder")
    (synopsis
      "Rust macro to automatically implement the builder pattern for arbitrary structs")
    (description
      "This package provides Rust macro to automatically implement the builder pattern for arbitrary structs.")
    (license (list license:expat license:asl2.0))))

(define rust-derive-builder-core-0.20
  (package
    (name "rust-derive-builder-core")
    (version "0.20.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "derive_builder_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1s640r6q46c2iiz25sgvxw3lk6b6v5y8hwylng7kas2d09xwynrd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling" ,rust-darling-0.20)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/colin-kiegel/rust-derive-builder")
    (synopsis
      "Internal helper library for the derive_builder crate")
    (description
      "This package provides Internal helper library for the derive_builder crate.")
    (license (list license:expat license:asl2.0))))

(define rust-derive-builder-macro-0.20
  (package
    (name "rust-derive-builder-macro")
    (version "0.20.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "derive_builder_macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0g1zznpqrmvjlp2w7p0jzsjvpmw5rvdag0rfyypjhnadpzib0qxb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-derive-builder-core"
          ,rust-derive-builder-core-0.20)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/colin-kiegel/rust-derive-builder")
    (synopsis
      "Rust macro to automatically implement the builder pattern for arbitrary structs")
    (description
      "This package provides Rust macro to automatically implement the builder pattern for arbitrary structs.")
    (license (list license:expat license:asl2.0))))

(define rust-either-1
  (package
    (name "rust-either")
    (version "1.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "either" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1w2c1mybrd7vljyxk77y9f4w9dyjrmp3yp82mk7bcm8848fazcb0"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/rayon-rs/either")
    (synopsis
      "The enum `Either` with variants `Left` and `Right` is a general purpose sum type with two cases.")
    (description
      "This package provides The enum `Either` with variants `Left` and `Right` is a general purpose sum type\nwith two cases.")
    (license (list license:expat license:asl2.0))))

(define rust-errno-0.3
  (package
    (name "rust-errno")
    (version "0.3.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "errno" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0pgblicz1kjz9wa9m0sghkhh2zw1fhq1mxzj7ndjm746kg5m5n1k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page
      "https://github.com/lambda-fairy/rust-errno")
    (synopsis
      "Cross-platform interface to the `errno` variable")
    (description
      "This package provides Cross-platform interface to the `errno` variable.")
    (license (list license:expat license:asl2.0))))

(define rust-error-code-3
  (package
    (name "rust-error-code")
    (version "3.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "error-code" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bx9hw3pahzqym8jvb0ln4qsabnysgn98mikyh2afhk9rif31nd5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/DoumanAsh/error-code")
    (synopsis "Error code")
    (description "This package provides Error code.")
    (license license:boost1.0)))

(define rust-exr-1
  (package
    (name "rust-exr")
    (version "1.73.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "exr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1q47yq78q9k210r6jy1wwrilxwwxqavik9l3l426rd17k7srfcgq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bit-field" ,rust-bit-field-0.10)
         ("rust-half" ,rust-half-2)
         ("rust-lebe" ,rust-lebe-0.5)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.8)
         ("rust-rayon-core" ,rust-rayon-core-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-zune-inflate" ,rust-zune-inflate-0.2))))
    (home-page
      "https://github.com/johannesvollmer/exrs")
    (synopsis
      "Read and write OpenEXR files without any unsafe code")
    (description
      "This package provides Read and write @code{OpenEXR} files without any unsafe code.")
    (license license:bsd-3)))

(define rust-fdeflate-0.3
  (package
    (name "rust-fdeflate")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fdeflate" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "130ga18vyxbb5idbgi07njymdaavvk6j08yh1dfarm294ssm6s0y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-simd-adler32" ,rust-simd-adler32-0.3))))
    (home-page
      "https://github.com/image-rs/fdeflate")
    (synopsis
      "Fast specialized deflate implementation")
    (description
      "This package provides Fast specialized deflate implementation.")
    (license (list license:expat license:asl2.0))))

(define rust-fdlimit-0.3
  (package
    (name "rust-fdlimit")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fdlimit" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rbz4wpf6a2csjfwxlv2hg22jiq8xaxmy71mczpxjwzgqbdzg0p1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page
      "https://github.com/paritytech/fdlimit")
    (synopsis
      "Utility crate for raising file descriptors limit for OSX and Linux")
    (description
      "This package provides Utility crate for raising file descriptors limit for OSX and Linux.")
    (license license:asl2.0)))

(define rust-flate2-1
  (package
    (name "rust-flate2")
    (version "1.0.35")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "flate2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0z6h0wa095wncpfngx75wyhyjnqwld7wax401gsvnzjhzgdbydn9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crc32fast" ,rust-crc32fast-1)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.8))))
    (home-page
      "https://github.com/rust-lang/flate2-rs")
    (synopsis
      "DEFLATE compression and decompression exposed as Read/BufRead/Write streams.\nSupports miniz_oxide and multiple zlib implementations. Supports zlib, gzip,\nand raw deflate streams.")
    (description
      "This package provides DEFLATE compression and decompression exposed as Read/@code{BufRead/Write}\nstreams.  Supports miniz_oxide and multiple zlib implementations.  Supports\nzlib, gzip, and raw deflate streams.")
    (license (list license:expat license:asl2.0))))

(define rust-futures-0.3
  (package
    (name "rust-futures")
    (version "0.3.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xh8ddbkm9jy8kc5gbvjp9a4b6rqqxvc8471yb2qaz5wm2qhgg35"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-channel"
          ,rust-futures-channel-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-executor"
          ,rust-futures-executor-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-task" ,rust-futures-task-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3))))
    (home-page
      "https://rust-lang.github.io/futures-rs")
    (synopsis
      "An implementation of futures and streams featuring zero allocations,\ncomposability, and iterator-like interfaces.")
    (description
      "This package provides An implementation of futures and streams featuring zero allocations,\ncomposability, and iterator-like interfaces.")
    (license (list license:expat license:asl2.0))))

(define rust-futures-channel-0.3
  (package
    (name "rust-futures-channel")
    (version "0.3.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-channel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "040vpqpqlbk099razq8lyn74m0f161zd0rp36hciqrwcg2zibzrd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3))))
    (home-page
      "https://rust-lang.github.io/futures-rs")
    (synopsis
      "Channels for asynchronous communication using futures-rs.")
    (description
      "This package provides Channels for asynchronous communication using futures-rs.")
    (license (list license:expat license:asl2.0))))

(define rust-futures-core-0.3
  (package
    (name "rust-futures-core")
    (version "0.3.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gk6yrxgi5ihfanm2y431jadrll00n5ifhnpx090c2f2q1cr1wh5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://rust-lang.github.io/futures-rs")
    (synopsis
      "The core traits and types in for the `futures` library.")
    (description
      "This package provides The core traits and types in for the `futures` library.")
    (license (list license:expat license:asl2.0))))

(define rust-futures-executor-0.3
  (package
    (name "rust-futures-executor")
    (version "0.3.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-executor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17vcci6mdfzx4gbk0wx64chr2f13wwwpvyf3xd5fb1gmjzcx2a0y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-task" ,rust-futures-task-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3))))
    (home-page
      "https://rust-lang.github.io/futures-rs")
    (synopsis
      "Executors for asynchronous tasks based on the futures-rs library.")
    (description
      "This package provides Executors for asynchronous tasks based on the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define rust-futures-io-0.3
  (package
    (name "rust-futures-io")
    (version "0.3.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-io" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ikmw1yfbgvsychmsihdkwa8a1knank2d9a8dk01mbjar9w1np4y"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://rust-lang.github.io/futures-rs")
    (synopsis
      "The `AsyncRead`, `AsyncWrite`, `AsyncSeek`, and `AsyncBufRead` traits for the futures-rs library.")
    (description
      "This package provides The `@code{AsyncRead`}, `@code{AsyncWrite`}, `@code{AsyncSeek`}, and\n`@code{AsyncBufRead`} traits for the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define rust-futures-macro-0.3
  (package
    (name "rust-futures-macro")
    (version "0.3.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0l1n7kqzwwmgiznn0ywdc5i24z72zvh9q1dwps54mimppi7f6bhn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://rust-lang.github.io/futures-rs")
    (synopsis
      "The futures-rs procedural macro implementations.")
    (description
      "This package provides The futures-rs procedural macro implementations.")
    (license (list license:expat license:asl2.0))))

(define rust-futures-sink-0.3
  (package
    (name "rust-futures-sink")
    (version "0.3.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-sink" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xyly6naq6aqm52d5rh236snm08kw8zadydwqz8bip70s6vzlxg5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://rust-lang.github.io/futures-rs")
    (synopsis
      "The asynchronous `Sink` trait for the futures-rs library.")
    (description
      "This package provides The asynchronous `Sink` trait for the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define rust-futures-task-0.3
  (package
    (name "rust-futures-task")
    (version "0.3.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-task" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "124rv4n90f5xwfsm9qw6y99755y021cmi5dhzh253s920z77s3zr"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://rust-lang.github.io/futures-rs")
    (synopsis "Tools for working with tasks.")
    (description
      "This package provides tools for working with tasks.")
    (license (list license:expat license:asl2.0))))

(define rust-futures-util-0.3
  (package
    (name "rust-futures-util")
    (version "0.3.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10aa1ar8bgkgbr4wzxlidkqkcxf77gffyj8j7768h831pcaq784z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-channel"
          ,rust-futures-channel-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-macro" ,rust-futures-macro-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-task" ,rust-futures-task-0.3)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-pin-utils" ,rust-pin-utils-0.1)
         ("rust-slab" ,rust-slab-0.4))))
    (home-page
      "https://rust-lang.github.io/futures-rs")
    (synopsis
      "Common utilities and extension traits for the futures-rs library.")
    (description
      "This package provides Common utilities and extension traits for the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define rust-gimli-0.31
  (package
    (name "rust-gimli")
    (version "0.31.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gimli" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gvqc0ramx8szv76jhfd4dms0zyamvlg4whhiz11j34hh3dqxqh7"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/gimli-rs/gimli")
    (synopsis
      "library for reading and writing the DWARF debugging format.")
    (description
      "This package provides a library for reading and writing the DWARF debugging\nformat.")
    (license (list license:expat license:asl2.0))))

(define rust-half-2
  (package
    (name "rust-half")
    (version "2.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "half" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "123q4zzw1x4309961i69igzd1wb7pj04aaii3kwasrz3599qrl3d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-crunchy" ,rust-crunchy-0.2))))
    (home-page
      "https://github.com/starkat99/half-rs")
    (synopsis
      "Half-precision floating point f16 and bf16 types for Rust implementing the IEEE 754-2008 standard binary16 and bfloat16 types")
    (description
      "This package provides Half-precision floating point f16 and bf16 types for Rust implementing the IEEE\n754-2008 standard binary16 and bfloat16 types.")
    (license (list license:expat license:asl2.0))))

(define rust-hashbrown-0.15
  (package
    (name "rust-hashbrown")
    (version "0.15.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashbrown" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12dj0yfn59p3kh3679ac0w1fagvzf4z2zp87a13gbbqbzw0185dz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-allocator-api2" ,rust-allocator-api2-0.2)
         ("rust-equivalent" ,rust-equivalent-1)
         ("rust-foldhash" ,rust-foldhash-0.1))))
    (home-page
      "https://github.com/rust-lang/hashbrown")
    (synopsis
      "Rust port of Google's SwissTable hash map")
    (description
      "This package provides a Rust port of Google's @code{SwissTable} hash map.")
    (license (list license:expat license:asl2.0))))

(define rust-iana-time-zone-0.1
  (package
    (name "rust-iana-time-zone")
    (version "0.1.61")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "iana-time-zone" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "085jjsls330yj1fnwykfzmb2f10zp6l7w4fhq81ng81574ghhpi3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-android-system-properties"
          ,rust-android-system-properties-0.1)
         ("rust-core-foundation-sys"
          ,rust-core-foundation-sys-0.8)
         ("rust-iana-time-zone-haiku"
          ,rust-iana-time-zone-haiku-0.1)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-windows-core" ,rust-windows-core-0.52))))
    (home-page
      "https://github.com/strawlab/iana-time-zone")
    (synopsis
      "get the IANA time zone for the current system")
    (description
      "This package provides get the IANA time zone for the current system.")
    (license (list license:expat license:asl2.0))))

(define rust-icu-collections-1
  (package
    (name "rust-icu-collections")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_collections" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09j5kskirl59mvqc8kabhy7005yyy7dp88jw9f6f3gkf419a8byv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-yoke" ,rust-yoke-0.7)
         ("rust-zerofrom" ,rust-zerofrom-0.1)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis
      "Collection of API for use in ICU libraries")
    (description
      "This package provides Collection of API for use in ICU libraries.")
    (license license:unicode)))

(define rust-icu-locid-1
  (package
    (name "rust-icu-locid")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_locid" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dznvd1c5b02iilqm044q4hvar0sqibq1z46prqwjzwif61vpb0k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-litemap" ,rust-litemap-0.7)
         ("rust-tinystr" ,rust-tinystr-0.7)
         ("rust-writeable" ,rust-writeable-0.5)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis
      "API for managing Unicode Language and Locale Identifiers")
    (description
      "This package provides API for managing Unicode Language and Locale Identifiers.")
    (license license:unicode)))

(define rust-icu-locid-transform-1
  (package
    (name "rust-icu-locid-transform")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_locid_transform" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kmmi1kmj9yph6mdgkc7v3wz6995v7ly3n80vbg0zr78bp1iml81"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-icu-locid" ,rust-icu-locid-1)
         ("rust-icu-locid-transform-data"
          ,rust-icu-locid-transform-data-1)
         ("rust-icu-provider" ,rust-icu-provider-1)
         ("rust-tinystr" ,rust-tinystr-0.7)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis
      "API for Unicode Language and Locale Identifiers canonicalization")
    (description
      "This package provides API for Unicode Language and Locale Identifiers canonicalization.")
    (license license:unicode)))

(define rust-icu-locid-transform-data-1
  (package
    (name "rust-icu-locid-transform-data")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_locid_transform_data" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vkgjixm0wzp2n3v5mw4j89ly05bg3lx96jpdggbwlpqi0rzzj7x"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://icu4x.unicode.org")
    (synopsis
      "Data for the icu_locid_transform crate")
    (description
      "This package provides Data for the icu_locid_transform crate.")
    (license license:unicode)))

(define rust-icu-normalizer-1
  (package
    (name "rust-icu-normalizer")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_normalizer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kx8qryp8ma8fw1vijbgbnf7zz9f2j4d14rw36fmjs7cl86kxkhr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-icu-collections" ,rust-icu-collections-1)
         ("rust-icu-normalizer-data"
          ,rust-icu-normalizer-data-1)
         ("rust-icu-properties" ,rust-icu-properties-1)
         ("rust-icu-provider" ,rust-icu-provider-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-utf16-iter" ,rust-utf16-iter-1)
         ("rust-utf8-iter" ,rust-utf8-iter-1)
         ("rust-write16" ,rust-write16-1)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis
      "API for normalizing text into Unicode Normalization Forms")
    (description
      "This package provides API for normalizing text into Unicode Normalization Forms.")
    (license license:unicode)))

(define rust-icu-normalizer-data-1
  (package
    (name "rust-icu-normalizer-data")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_normalizer_data" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05lmk0zf0q7nzjnj5kbmsigj3qgr0rwicnn5pqi9n7krmbvzpjpq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Data for the icu_normalizer crate")
    (description
      "This package provides Data for the icu_normalizer crate.")
    (license license:unicode)))

(define rust-icu-properties-1
  (package
    (name "rust-icu-properties")
    (version "1.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_properties" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xgf584rx10xc1p7zjr78k0n4zn3g23rrg6v2ln31ingcq3h5mlk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-icu-collections" ,rust-icu-collections-1)
         ("rust-icu-locid-transform"
          ,rust-icu-locid-transform-1)
         ("rust-icu-properties-data"
          ,rust-icu-properties-data-1)
         ("rust-icu-provider" ,rust-icu-provider-1)
         ("rust-tinystr" ,rust-tinystr-0.7)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Definitions for Unicode properties")
    (description
      "This package provides Definitions for Unicode properties.")
    (license license:unicode)))

(define rust-icu-properties-data-1
  (package
    (name "rust-icu-properties-data")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_properties_data" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0scms7pd5a7yxx9hfl167f5qdf44as6r3bd8myhlngnxqgxyza37"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Data for the icu_properties crate")
    (description
      "This package provides Data for the icu_properties crate.")
    (license license:unicode)))

(define rust-icu-provider-1
  (package
    (name "rust-icu-provider")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_provider" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nb8vvgw8dv2inqklvk05fs0qxzkw8xrg2n9vgid6y7gm3423m3f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-icu-locid" ,rust-icu-locid-1)
         ("rust-icu-provider-macros"
          ,rust-icu-provider-macros-1)
         ("rust-stable-deref-trait"
          ,rust-stable-deref-trait-1)
         ("rust-tinystr" ,rust-tinystr-0.7)
         ("rust-writeable" ,rust-writeable-0.5)
         ("rust-yoke" ,rust-yoke-0.7)
         ("rust-zerofrom" ,rust-zerofrom-0.1)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis
      "Trait and struct definitions for the ICU data provider")
    (description
      "This package provides Trait and struct definitions for the ICU data provider.")
    (license license:unicode)))

(define rust-icu-provider-macros-1
  (package
    (name "rust-icu-provider-macros")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "icu_provider_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mjs0w7fcm2lcqmbakhninzrjwqs485lkps4hz0cv3k36y9rxj0y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Proc macros for ICU data providers")
    (description
      "This package provides Proc macros for ICU data providers.")
    (license license:unicode)))

(define rust-idna-1
  (package
    (name "rust-idna")
    (version "1.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "idna" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zlajvm2k3wy0ay8plr07w22hxkkmrxkffa6ah57ac6nci984vv8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-idna-adapter" ,rust-idna-adapter-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-utf8-iter" ,rust-utf8-iter-1))))
    (home-page "https://github.com/servo/rust-url/")
    (synopsis
      "IDNA (Internationalizing Domain Names in Applications) and Punycode")
    (description
      "This package provides IDNA (Internationalizing Domain Names in Applications) and Punycode.")
    (license (list license:expat license:asl2.0))))

(define rust-idna-adapter-1
  (package
    (name "rust-idna-adapter")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "idna_adapter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wggnkiivaj5lw0g0384ql2d7zk4ppkn3b1ry4n0ncjpr7qivjns"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-icu-normalizer" ,rust-icu-normalizer-1)
         ("rust-icu-properties" ,rust-icu-properties-1))))
    (home-page
      "https://docs.rs/crate/idna_adapter/latest")
    (synopsis "Back end adapter for idna")
    (description
      "This package provides Back end adapter for idna.")
    (license (list license:asl2.0 license:expat))))

(define rust-imgref-1
  (package
    (name "rust-imgref")
    (version "1.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "imgref" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0254wzkakm31fdix6diqng0fkggknibh0b1iv570ap0djwykl9nh"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://lib.rs/crates/imgref")
    (synopsis
      "basic 2-dimensional slice for safe and convenient handling of pixel buffers with width, height & stride")
    (description
      "This package provides a basic 2-dimensional slice for safe and convenient\nhandling of pixel buffers with width, height & stride.")
    (license (list license:cc0 license:asl2.0))))

(define rust-indexmap-2
  (package
    (name "rust-indexmap")
    (version "2.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "indexmap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07s7jmdymvd0rm4yswp0j3napx57hkjm9gs9n55lvs2g78vj5y32"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-equivalent" ,rust-equivalent-1)
         ("rust-hashbrown" ,rust-hashbrown-0.15)
         ("rust-serde" ,rust-serde-1))))
    (home-page
      "https://github.com/indexmap-rs/indexmap")
    (synopsis
      "hash table with consistent order and fast iteration.")
    (description
      "This package provides a hash table with consistent order and fast iteration.")
    (license (list license:asl2.0 license:expat))))

(define rust-instability-0.3
  (package
    (name "rust-instability")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "instability" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vhc7im0kp1h7ifzalqzfynd5pwz2xn3gly283gkkp6rx9yz6adq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling" ,rust-darling-0.20)
         ("rust-indoc" ,rust-indoc-2)
         ("rust-pretty-assertions"
          ,rust-pretty-assertions-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/ratatui-org/instability")
    (synopsis
      "Rust API stability attributes for the rest of us. A fork of the `stability` crate")
    (description
      "This package provides Rust API stability attributes for the rest of us.  A fork of the `stability`\ncrate.")
    (license license:expat)))

(define rust-instant-0.1
  (package
    (name "rust-instant")
    (version "0.1.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "instant" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08h27kzvb5jw74mh0ajv0nv9ggwvgqm8ynjsn2sa9jsks4cjh970"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page
      "https://github.com/sebcrozet/instant")
    (synopsis
      "Unmaintained, consider using web-time instead - A partial replacement for std::time::Instant that works on WASM to")
    (description
      "This package provides Unmaintained, consider using web-time instead - A partial replacement for\nstd::time::Instant that works on WASM to.")
    (license license:bsd-3)))

(define rust-itoa-1
  (package
    (name "rust-itoa")
    (version "1.0.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itoa" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0x26kr9m062mafaxgcf2p6h2x7cmixm0zw95aipzn2hr3d5jlnnp"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/dtolnay/itoa")
    (synopsis
      "Fast integer primitive to string conversion")
    (description
      "This package provides Fast integer primitive to string conversion.")
    (license (list license:expat license:asl2.0))))

(define rust-js-sys-0.3
  (package
    (name "rust-js-sys")
    (version "0.3.76")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "js-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1dz7v777h2j38wkf8k5iwkfxskn6nff2cdv2jsslyxkpn2svc5v7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page
      "https://rustwasm.github.io/wasm-bindgen/")
    (synopsis
      "Bindings for all JS global objects and functions in all JS environments like\nNode.js and browsers, built on `#[wasm_bindgen]` using the `wasm-bindgen` crate.")
    (description
      "This package provides Bindings for all JS global objects and functions in all JS environments like\nNode.js and browsers, built on `#[wasm_bindgen]` using the `wasm-bindgen` crate.")
    (license (list license:expat license:asl2.0))))

(define rust-kqueue-1
  (package
    (name "rust-kqueue")
    (version "1.0.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kqueue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "033x2knkbv8d3jy6i9r32jcgsq6zm3g97zh5la43amkv3g5g2ivl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-kqueue-sys" ,rust-kqueue-sys-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://gitlab.com/rust-kqueue/rust-kqueue")
    (synopsis "kqueue interface for BSDs")
    (description
      "This package provides kqueue interface for BSDs.")
    (license license:expat)))

(define rust-kqueue-sys-1
  (package
    (name "rust-kqueue-sys")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kqueue-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12w3wi90y4kwis4k9g6fp0kqjdmc6l00j16g8mgbhac7vbzjb5pd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://gitlab.com/rust-kqueue/rust-kqueue-sys")
    (synopsis "Low-level kqueue interface for BSDs")
    (description
      "This package provides Low-level kqueue interface for BSDs.")
    (license license:expat)))

(define rust-libc-0.2
  (package
    (name "rust-libc")
    (version "0.2.168")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vab4inpw0dz78nii02hsxp1skqn06xzh64psw8wl1h63scb5bjs"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/rust-lang/libc")
    (synopsis
      "Raw FFI bindings to platform libraries like libc.")
    (description
      "This package provides Raw FFI bindings to platform libraries like libc.")
    (license (list license:expat license:asl2.0))))

(define rust-libfuzzer-sys-0.4
  (package
    (name "rust-libfuzzer-sys")
    (version "0.4.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libfuzzer-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yhpxlxsli2rhb6axpai3s2npd05nlzsggy6v1v709afyz96k5cv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-1)
         ("rust-cc" ,rust-cc-1))))
    (home-page
      "https://github.com/rust-fuzz/libfuzzer")
    (synopsis
      "wrapper around LLVM's libFuzzer runtime.")
    (description
      "This package provides a wrapper around LLVM's @code{libFuzzer} runtime.")
    (license
      (list license:expat license:asl2.0 license:ncsa))))

(define rust-litemap-0.7
  (package
    (name "rust-litemap")
    (version "0.7.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "litemap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "012ili3vppd4952sh6y3qwcd0jkd0bq2qpr9h7cppc8sj11k7saf"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/unicode-org/icu4x")
    (synopsis
      "key-value Map implementation based on a flat, sorted Vec.")
    (description
      "This package provides a key-value Map implementation based on a flat, sorted\nVec.")
    (license license:unicode)))

(define rust-lock-api-0.4
  (package
    (name "rust-lock-api")
    (version "0.4.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lock_api" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05qvxa6g27yyva25a5ghsg85apdxkvr77yhkyhapj6r8vnf8pbq7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1)
         ("rust-scopeguard" ,rust-scopeguard-1))))
    (home-page
      "https://github.com/Amanieu/parking_lot")
    (synopsis
      "Wrappers to create fully-featured Mutex and RwLock types. Compatible with no_std")
    (description
      "This package provides Wrappers to create fully-featured Mutex and @code{RwLock} types.  Compatible\nwith no_std.")
    (license (list license:expat license:asl2.0))))

(define rust-lru-0.12
  (package
    (name "rust-lru")
    (version "0.12.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lru" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0f1a7cgqxbyhrmgaqqa11m3azwhcc36w0v5r4izgbhadl3sg8k13"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hashbrown" ,rust-hashbrown-0.15))))
    (home-page
      "https://github.com/jeromefroe/lru-rs")
    (synopsis "LRU cache implementation")
    (description
      "This package provides a LRU cache implementation.")
    (license license:expat)))

(define rust-lua-src-547
  (package
    (name "rust-lua-src")
    (version "547.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lua-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hjy276xbidvp8n0c6f1w76qamybiijfa0b7fj5rpd0p6ngg5nhy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1))))
    (home-page
      "https://github.com/khvzak/lua-src-rs")
    (synopsis
      "Sources of Lua 5.1/5.2/5.3/5.4 and logic to build them.")
    (description
      "This package provides Sources of Lua 5.1/5.2/5.3/5.4 and logic to build them.")
    (license license:expat)))

(define rust-luajit-src-210
  (package
    (name "rust-luajit-src")
    (version "210.5.11+97813fb")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "luajit-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fs0fmy106j52nizrxnij3lrxjzrh08gqnf561ydn5a550f5a59h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-which" ,rust-which-6))))
    (home-page
      "https://github.com/khvzak/luajit-src-rs")
    (synopsis
      "Sources of LuaJIT 2.1 and logic to build it.")
    (description
      "This package provides Sources of @code{LuaJIT} 2.1 and logic to build it.")
    (license license:expat)))

(define rust-mio-1
  (package
    (name "rust-mio")
    (version "1.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gah0h4ia3avxbwym0b6bi6lr6rpysmj9zvw6zis5yq0z0xq91i8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-wasi" ,rust-wasi-0.11)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "https://github.com/tokio-rs/mio")
    (synopsis "Lightweight non-blocking I/O")
    (description
      "This package provides Lightweight non-blocking I/O.")
    (license license:expat)))

(define rust-mlua-0.10
  (package
    (name "rust-mlua")
    (version "0.10.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mlua" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13bbggf17vxb5n87m6r9dcqri36p5lhib20jsy5pkl62z8zkr94y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bstr" ,rust-bstr-1)
         ("rust-either" ,rust-either-1)
         ("rust-erased-serde" ,rust-erased-serde-0.4)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-mlua-sys" ,rust-mlua-sys-0.6)
         ("rust-mlua-derive" ,rust-mlua-derive-0.10)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-rustc-hash" ,rust-rustc-hash-2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-value" ,rust-serde-value-0.7))))
    (home-page "https://github.com/khvzak/mlua")
    (synopsis
      "High level bindings to Lua 5.4/5.3/5.2/5.1 (including LuaJIT) and Roblox Luau\nwith async/await features and support of writing native Lua modules in Rust.")
    (description
      "This package provides High level bindings to Lua 5.4/5.3/5.2/5.1 (including @code{LuaJIT}) and Roblox\nLuau with async/await features and support of writing native Lua modules in\nRust.")
    (license license:expat)))

(define rust-mlua-sys-0.6
  (package
    (name "rust-mlua-sys")
    (version "0.6.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mlua-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w8plgh521jkwb9yczhsysf2j3fmqwv5sq88ll23y3yzbr41v8b3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-lua-src" ,rust-lua-src-547)
         ("rust-luajit-src" ,rust-luajit-src-210)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/khvzak/mlua")
    (synopsis
      "Low level (FFI) bindings to Lua 5.4/5.3/5.2/5.1 (including LuaJIT) and Roblox Luau")
    (description
      "This package provides Low level (FFI) bindings to Lua 5.4/5.3/5.2/5.1 (including @code{LuaJIT}) and\nRoblox Luau.")
    (license license:expat)))

(define rust-mlua-derive-0.10
  (package
    (name "rust-mlua-derive")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mlua_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ssdvpqwnsy6d4i418a2b8zfx8hr0qb08k7vnp393x7wfb0p23c7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-itertools" ,rust-itertools-0.13)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-proc-macro-error2"
          ,rust-proc-macro-error2-2)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/khvzak/mlua")
    (synopsis "Procedural macros for the mlua crate")
    (description
      "This package provides Procedural macros for the mlua crate.")
    (license license:expat)))

(define rust-new-debug-unreachable-1
  (package
    (name "rust-new-debug-unreachable")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "new_debug_unreachable" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11phpf1mjxq6khk91yzcbd3ympm78m3ivl7xg6lg2c0lf66fy3k5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/mbrubeck/rust-debug-unreachable")
    (synopsis
      "panic in debug, intrinsics::unreachable() in release (fork of debug_unreachable)")
    (description
      "This package provides panic in debug, @code{intrinsics::unreachable()} in release (fork of\ndebug_unreachable).")
    (license license:expat)))

(define rust-notify-7
  (package
    (name "rust-notify")
    (version "7.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "notify" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02a0a1n0raxqslwhfprwmz7w34v54r42006q0m8bmy89jz1v8cy5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-filetime" ,rust-filetime-0.2)
         ("rust-fsevent-sys" ,rust-fsevent-sys-4)
         ("rust-inotify" ,rust-inotify-0.10)
         ("rust-kqueue" ,rust-kqueue-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-mio" ,rust-mio-1)
         ("rust-notify-types" ,rust-notify-types-1)
         ("rust-walkdir" ,rust-walkdir-2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "https://github.com/notify-rs/notify")
    (synopsis
      "Cross-platform filesystem notification library")
    (description
      "This package provides Cross-platform filesystem notification library.")
    (license license:cc0)))

(define rust-notify-types-1
  (package
    (name "rust-notify-types")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "notify-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pw5hwvigcn1qaf4hjq2idx117jhz425gp5kzxj7k08zc8kc54vk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-instant" ,rust-instant-0.1))))
    (home-page "https://github.com/notify-rs/notify")
    (synopsis "Types used by the notify crate")
    (description
      "This package provides Types used by the notify crate.")
    (license (list license:expat license:asl2.0))))

(define rust-nu-ansi-term-0.46
  (package
    (name "rust-nu-ansi-term")
    (version "0.46.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nu-ansi-term" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "115sywxh53p190lyw97alm14nc004qj5jm5lvdj608z84rbida3p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-overload" ,rust-overload-0.1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page
      "https://github.com/nushell/nu-ansi-term")
    (synopsis
      "Library for ANSI terminal colors and styles (bold, underline)")
    (description
      "This package provides Library for ANSI terminal colors and styles (bold, underline).")
    (license license:expat)))

(define rust-num-derive-0.4
  (package
    (name "rust-num-derive")
    (version "0.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00p2am9ma8jgd2v6xpsz621wc7wbn1yqi71g15gc3h67m7qmafgd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/rust-num/num-derive")
    (synopsis "Numeric syntax extensions")
    (description
      "This package provides Numeric syntax extensions.")
    (license (list license:expat license:asl2.0))))

(define rust-num-rational-0.4
  (package
    (name "rust-num-rational")
    (version "0.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-rational" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "093qndy02817vpgcqjnj139im3jl7vkq4h68kykdqqh577d18ggq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page
      "https://github.com/rust-num/num-rational")
    (synopsis
      "Rational numbers implementation for Rust")
    (description
      "This package provides Rational numbers implementation for Rust.")
    (license (list license:expat license:asl2.0))))

(define rust-num-threads-0.1
  (package
    (name "rust-num-threads")
    (version "0.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_threads" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ngajbmhrgyhzrlc4d5ga9ych1vrfcvfsiqz6zv0h2dpr2wrhwsw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/jhpratt/num_threads")
    (synopsis
      "minimal library that determines the number of running threads for the current process.")
    (description
      "This package provides a minimal library that determines the number of running\nthreads for the current process.")
    (license (list license:expat license:asl2.0))))

(define rust-object-0.36
  (package
    (name "rust-object")
    (version "0.36.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "object" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gk8lhbs229c68lapq6w6qmnm4jkj48hrcw5ilfyswy514nhmpxf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2))))
    (home-page "https://github.com/gimli-rs/object")
    (synopsis
      "unified interface for reading and writing object file formats.")
    (description
      "This package provides a unified interface for reading and writing object file\nformats.")
    (license (list license:asl2.0 license:expat))))

(define rust-onig-6
  (package
    (name "rust-onig")
    (version "6.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "onig" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kyaz2fwa5dkr04rvk5ga2yv5jkqn1ymblvpdlf1gn9afb432jwc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-onig-sys" ,rust-onig-sys-69))))
    (home-page
      "http://github.com/iwillspeak/rust-onig")
    (synopsis
      "Rust-Onig is a set of Rust bindings for the\nOniguruma regular expression library. Oniguruma\nis a modern regex library with support for\nmultiple character encodings and regex syntaxes.")
    (description
      "This package provides Rust-Onig is a set of Rust bindings for the Oniguruma regular expression\nlibrary.  Oniguruma is a modern regex library with support for multiple\ncharacter encodings and regex syntaxes.")
    (license license:expat)))

(define rust-onig-sys-69
  (package
    (name "rust-onig-sys")
    (version "69.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "onig_sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rw6y2qkb765gzylmrydbbd90hdzhnqyvs2y65z4riwwgqyrx0kv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page
      "http://github.com/iwillspeak/rust-onig")
    (synopsis
      "The `onig_sys` crate contains raw rust bindings to the\noniguruma library. This crate exposes a set of unsafe\nfunctions which can then be used by other crates to\ncreate safe wrappers around Oniguruma.\n\nYou probably don't want to link to this crate directly;\ninstead check out the `onig` crate.")
    (description
      "This package provides The `onig_sys` crate contains raw rust bindings to the oniguruma library.  This\ncrate exposes a set of unsafe functions which can then be used by other crates\nto create safe wrappers around Oniguruma.  You probably don't want to link to\nthis crate directly; instead check out the `onig` crate.")
    (license license:expat)))

(define rust-parking-2
  (package
    (name "rust-parking")
    (version "2.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fnfgmzkfpjd69v4j9x737b1k8pnn054bvzcn5dm3pkgq595d3gk"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/smol-rs/parking")
    (synopsis "Thread parking and unparking")
    (description
      "This package provides Thread parking and unparking.")
    (license (list license:asl2.0 license:expat))))

(define rust-pin-project-lite-0.2
  (package
    (name "rust-pin-project-lite")
    (version "0.2.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pin-project-lite" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zz4xif3iknfrpmvqmh0pcc9mx4cxm28jywqydir3pimcla1wnli"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/taiki-e/pin-project-lite")
    (synopsis
      "lightweight version of pin-project written with declarative macros.")
    (description
      "This package provides a lightweight version of pin-project written with\ndeclarative macros.")
    (license (list license:asl2.0 license:expat))))

(define rust-png-0.17
  (package
    (name "rust-png")
    (version "0.17.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "png" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07918iqrf9m1grgixk252bvvw5fgl64ymqkh89hzzgb5bfyq4xdn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-crc32fast" ,rust-crc32fast-1)
         ("rust-fdeflate" ,rust-fdeflate-0.3)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.8))))
    (home-page
      "https://github.com/image-rs/image-png")
    (synopsis
      "PNG decoding and encoding library in pure Rust")
    (description
      "This package provides PNG decoding and encoding library in pure Rust.")
    (license (list license:expat license:asl2.0))))

(define rust-pretty-assertions-1
  (package
    (name "rust-pretty-assertions")
    (version "1.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pretty_assertions" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0v8iq35ca4rw3rza5is3wjxwsf88303ivys07anc5yviybi31q9s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-diff" ,rust-diff-0.1)
         ("rust-yansi" ,rust-yansi-1))))
    (home-page
      "https://github.com/rust-pretty-assertions/rust-pretty-assertions")
    (synopsis
      "Overwrite `assert_eq!` and `assert_ne!` with drop-in replacements, adding colorful diffs")
    (description
      "This package provides Overwrite `assert_eq!` and `assert_ne!` with drop-in replacements, adding\ncolorful diffs.")
    (license (list license:expat license:asl2.0))))

(define rust-proc-macro-error-attr2-2
  (package
    (name "rust-proc-macro-error-attr2")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-error-attr2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ifzi763l7swl258d8ar4wbpxj4c9c2im7zy89avm6xv6vgl5pln"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1))))
    (home-page
      "https://github.com/GnomedDev/proc-macro-error-2")
    (synopsis
      "Attribute macro for the proc-macro-error2 crate")
    (description
      "This package provides Attribute macro for the proc-macro-error2 crate.")
    (license (list license:expat license:asl2.0))))

(define rust-proc-macro-error2-2
  (package
    (name "rust-proc-macro-error2")
    (version "2.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-error2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00lq21vgh7mvyx51nwxwf822w2fpww1x0z8z0q47p8705g2hbv0i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-error-attr2"
          ,rust-proc-macro-error-attr2-2)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/GnomedDev/proc-macro-error-2")
    (synopsis
      "Almost drop-in replacement to panics in proc-macros")
    (description
      "This package provides Almost drop-in replacement to panics in proc-macros.")
    (license (list license:expat license:asl2.0))))

(define rust-proc-macro2-1
  (package
    (name "rust-proc-macro2")
    (version "1.0.92")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c1vjy5wg8iy7kxsxda564qf4ljp0asysmbn2i7caj177x5m9lrp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-ident" ,rust-unicode-ident-1))))
    (home-page
      "https://github.com/dtolnay/proc-macro2")
    (synopsis
      "substitute implementation of the compiler's `proc_macro` API to decouple token-based libraries from the procedural macro use case.")
    (description
      "This package provides a substitute implementation of the compiler's `proc_macro`\nAPI to decouple token-based libraries from the procedural macro use case.")
    (license (list license:expat license:asl2.0))))

(define rust-profiling-1
  (package
    (name "rust-profiling")
    (version "1.0.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "profiling" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kcz2xzg4qx01r5az8cf9ffjasi2srj56sna32igddh0vi7cggdg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-profiling-procmacros"
          ,rust-profiling-procmacros-1))))
    (home-page
      "https://github.com/aclysma/profiling")
    (synopsis
      "This crate provides a very thin abstraction over other profiler crates")
    (description
      "This crate provides a very thin abstraction over other profiler crates.")
    (license (list license:expat license:asl2.0))))

(define rust-profiling-procmacros-1
  (package
    (name "rust-profiling-procmacros")
    (version "1.0.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "profiling-procmacros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0c7y2k4mz5dp2ksj1h4zbxsxq4plmjzccscdaml3h1pizdh2wpx6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/aclysma/profiling")
    (synopsis
      "This crate provides a very thin abstraction over other profiler crates")
    (description
      "This crate provides a very thin abstraction over other profiler crates.")
    (license (list license:expat license:asl2.0))))

(define rust-ratatui-0.29
  (package
    (name "rust-ratatui")
    (version "0.29.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ratatui" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yqiccg1wmqqxpb2sz3q2v3nifmhsrfdsjgwhc2w40bqyg199gga"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-cassowary" ,rust-cassowary-0.3)
         ("rust-compact-str" ,rust-compact-str-0.8)
         ("rust-crossterm" ,rust-crossterm-0.28)
         ("rust-indoc" ,rust-indoc-2)
         ("rust-instability" ,rust-instability-0.3)
         ("rust-itertools" ,rust-itertools-0.13)
         ("rust-lru" ,rust-lru-0.12)
         ("rust-paste" ,rust-paste-1)
         ("rust-strum" ,rust-strum-0.26)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1)
         ("rust-unicode-truncate"
          ,rust-unicode-truncate-1)
         ("rust-unicode-width" ,rust-unicode-width-0.2))))
    (home-page "https://ratatui.rs")
    (synopsis
      "library that's all about cooking up terminal user interfaces")
    (description
      "This package provides a library that's all about cooking up terminal user\ninterfaces.")
    (license license:expat)))

(define rust-rav1e-0.7
  (package
    (name "rust-rav1e")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rav1e" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1sawva6nmj2fvynydbcirr3nb7wjyg0id2hz2771qnv6ly0cx1yd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-1)
         ("rust-arg-enum-proc-macro"
          ,rust-arg-enum-proc-macro-0.3)
         ("rust-arrayvec" ,rust-arrayvec-0.7)
         ("rust-av1-grain" ,rust-av1-grain-0.2)
         ("rust-bitstream-io" ,rust-bitstream-io-2)
         ("rust-built" ,rust-built-0.7)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-interpolate-name"
          ,rust-interpolate-name-0.2)
         ("rust-itertools" ,rust-itertools-0.12)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libfuzzer-sys" ,rust-libfuzzer-sys-0.4)
         ("rust-log" ,rust-log-0.4)
         ("rust-maybe-rayon" ,rust-maybe-rayon-0.1)
         ("rust-new-debug-unreachable"
          ,rust-new-debug-unreachable-1)
         ("rust-noop-proc-macro"
          ,rust-noop-proc-macro-0.3)
         ("rust-num-derive" ,rust-num-derive-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-paste" ,rust-paste-1)
         ("rust-profiling" ,rust-profiling-1)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-rand-chacha" ,rust-rand-chacha-0.3)
         ("rust-simd-helpers" ,rust-simd-helpers-0.1)
         ("rust-system-deps" ,rust-system-deps-6)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-v-frame" ,rust-v-frame-0.3)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page "https://github.com/xiph/rav1e/")
    (synopsis "The fastest and safest AV1 encoder")
    (description
      "This package provides The fastest and safest AV1 encoder.")
    (license license:bsd-2)))

(define rust-redox-syscall-0.5
  (package
    (name "rust-redox-syscall")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07vpgfr6a04k0x19zqr1xdlqm6fncik3zydbdi3f5g3l5k7zwvcv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2))))
    (home-page
      "https://gitlab.redox-os.org/redox-os/syscall")
    (synopsis
      "Rust library to access raw Redox system calls")
    (description
      "This package provides a Rust library to access raw Redox system calls.")
    (license license:expat)))

(define rust-redox-users-0.4
  (package
    (name "rust-redox-users")
    (version "0.4.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_users" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hya2cxx6hxmjfxzv9n8rjl5igpychav7zfi1f81pz6i4krry05s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-getrandom" ,rust-getrandom-0.2)
         ("rust-libredox" ,rust-libredox-0.1)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page
      "https://gitlab.redox-os.org/redox-os/users")
    (synopsis
      "Rust library to access Redox users and groups functionality")
    (description
      "This package provides a Rust library to access Redox users and groups\nfunctionality.")
    (license license:expat)))

(define rust-regex-1
  (package
    (name "rust-regex")
    (version "1.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "148i41mzbx8bmq32hsj1q4karkzzx5m60qza6gdw4pdc9qdyyi5m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-regex-automata" ,rust-regex-automata-0.4)
         ("rust-regex-syntax" ,rust-regex-syntax-0.8))))
    (home-page "https://github.com/rust-lang/regex")
    (synopsis
      "An implementation of regular expressions for Rust. This implementation uses\nfinite automata and guarantees linear time matching on all inputs.")
    (description
      "This package provides An implementation of regular expressions for Rust.  This implementation uses\nfinite automata and guarantees linear time matching on all inputs.")
    (license (list license:expat license:asl2.0))))

(define rust-regex-automata-0.1
  (package
    (name "rust-regex-automata")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-automata" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ci1hvbzhrfby5fdpf4ganhf7kla58acad9i1ff1p34dzdrhs8vc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-regex-syntax" ,rust-regex-syntax-0.6))))
    (home-page
      "https://github.com/rust-lang/regex/tree/master/regex-automata")
    (synopsis
      "Automata construction and matching using regular expressions")
    (description
      "This package provides Automata construction and matching using regular expressions.")
    (license (list license:expat license:asl2.0))))

(define rust-regex-automata-0.4
  (package
    (name "rust-regex-automata")
    (version "0.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-automata" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02092l8zfh3vkmk47yjc8d631zhhcd49ck2zr133prvd3z38v7l0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-regex-syntax" ,rust-regex-syntax-0.8))))
    (home-page
      "https://github.com/rust-lang/regex/tree/master/regex-automata")
    (synopsis
      "Automata construction and matching using regular expressions")
    (description
      "This package provides Automata construction and matching using regular expressions.")
    (license (list license:expat license:asl2.0))))

(define rust-regex-syntax-0.6
  (package
    (name "rust-regex-syntax")
    (version "0.6.29")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-syntax" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qgj49vm6y3zn1hi09x91jvgkl2b1fiaq402skj83280ggfwcqpi"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/rust-lang/regex/tree/master/regex-syntax")
    (synopsis "regular expression parser.")
    (description
      "This package provides a regular expression parser.")
    (license (list license:expat license:asl2.0))))

(define rust-rustc-hash-2
  (package
    (name "rust-rustc-hash")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc-hash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15yln6fmqlbg0k35r748h8g9xsd637ri23xihq81jb03ncwq1yy7"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/rust-lang/rustc-hash")
    (synopsis
      "speedy, non-cryptographic hashing algorithm used by rustc")
    (description
      "This package provides a speedy, non-cryptographic hashing algorithm used by\nrustc.")
    (license (list license:asl2.0 license:expat))))

(define rust-rustc-version-0.4
  (package
    (name "rust-rustc-version")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc_version" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14lvdsmr5si5qbqzrajgb6vfn69k0sfygrvfvr2mps26xwi3mjyg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-semver" ,rust-semver-1))))
    (home-page
      "https://github.com/djc/rustc-version-rs")
    (synopsis
      "library for querying the version of a installed rustc compiler")
    (description
      "This package provides a library for querying the version of a installed rustc\ncompiler.")
    (license (list license:expat license:asl2.0))))

(define rust-rustix-0.38
  (package
    (name "rust-rustix")
    (version "0.38.42")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustix" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11fvprv3p450ggyqacp7sdpjbbsgm5zvfjwnzy8bfbmbrf7c6ggr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-errno" ,rust-errno-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-linux-raw-sys" ,rust-linux-raw-sys-0.4)
         ("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page
      "https://github.com/bytecodealliance/rustix")
    (synopsis
      "Safe Rust bindings to POSIX/Unix/Linux/Winsock-like syscalls")
    (description
      "This package provides Safe Rust bindings to POSIX/Unix/Linux/Winsock-like syscalls.")
    (license
      (list license:asl2.0
            license:asl2.0
            license:expat))))

(define rust-rustversion-1
  (package
    (name "rust-rustversion")
    (version "1.0.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustversion" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0j2207vmgrcxwwwvknfn3lwv4i8djhjnxlvwdnz8bwijqqmrz08f"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/dtolnay/rustversion")
    (synopsis
      "Conditional compilation according to rustc compiler version")
    (description
      "This package provides Conditional compilation according to rustc compiler version.")
    (license (list license:expat license:asl2.0))))

(define rust-serde-json-1
  (package
    (name "rust-serde-json")
    (version "1.0.133")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_json" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xz3bswa527wln3fy0qb7y081nx3cp5yy1ggjhi6n5mrfcjfpz67"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-itoa" ,rust-itoa-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-ryu" ,rust-ryu-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/serde-rs/json")
    (synopsis "JSON serialization file format")
    (description
      "This package provides a JSON serialization file format.")
    (license (list license:expat license:asl2.0))))

(define rust-serde-spanned-0.6
  (package
    (name "rust-serde-spanned")
    (version "0.6.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_spanned" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1q89g70azwi4ybilz5jb8prfpa575165lmrffd49vmcf76qpqq47"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/toml-rs/toml")
    (synopsis "Serde-compatible spanned Value")
    (description
      "This package provides Serde-compatible spanned Value.")
    (license (list license:expat license:asl2.0))))

(define rust-sharded-slab-0.1
  (package
    (name "rust-sharded-slab")
    (version "0.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sharded-slab" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xipjr4nqsgw34k7a2cgj9zaasl2ds6jwn89886kww93d32a637l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lazy-static" ,rust-lazy-static-1))))
    (home-page
      "https://github.com/hawkw/sharded-slab")
    (synopsis "lock-free concurrent slab.")
    (description
      "This package provides a lock-free concurrent slab.")
    (license license:expat)))

(define rust-signal-hook-mio-0.2
  (package
    (name "rust-signal-hook-mio")
    (version "0.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook-mio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k8pl9aafiadr4czsg8zal9b4jdk6kq5985p90i19jc5sh31mnrl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-mio" ,rust-mio-1)
         ("rust-signal-hook" ,rust-signal-hook-0.3))))
    (home-page
      "https://github.com/vorner/signal-hook")
    (synopsis "MIO support for signal-hook")
    (description
      "This package provides MIO support for signal-hook.")
    (license (list license:asl2.0 license:expat))))

(define rust-signal-hook-registry-1
  (package
    (name "rust-signal-hook-registry")
    (version "1.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook-registry" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cb5akgq8ajnd5spyn587srvs4n26ryq0p78nswffwhv46sf1sd9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/vorner/signal-hook")
    (synopsis "Backend crate for signal-hook")
    (description
      "This package provides Backend crate for signal-hook.")
    (license (list license:asl2.0 license:expat))))

(define rust-simdutf8-0.1
  (package
    (name "rust-simdutf8")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "simdutf8" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vmpf7xaa0dnaikib5jlx6y4dxd3hxqz6l830qb079g7wcsgxag3"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/rusticstuff/simdutf8")
    (synopsis "SIMD-accelerated UTF-8 validation")
    (description
      "This package provides SIMD-accelerated UTF-8 validation.")
    (license (list license:expat license:asl2.0))))

(define rust-slab-0.4
  (package
    (name "rust-slab")
    (version "0.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slab" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rxvsgir0qw5lkycrqgb1cxsvxzjv9bmx73bk5y42svnzfba94lg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1))))
    (home-page "https://github.com/tokio-rs/slab")
    (synopsis
      "Pre-allocated storage for a uniform data type")
    (description
      "This package provides Pre-allocated storage for a uniform data type.")
    (license license:expat)))

(define rust-smallvec-1
  (package
    (name "rust-smallvec")
    (version "1.13.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smallvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rsw5samawl3wsw6glrsb127rx6sh89a8wyikicw6dkdcjd1lpiw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/servo/rust-smallvec")
    (synopsis
      "'Small vector' optimization: store up to a small number of items on the stack")
    (description
      "This package provides Small vector optimization: store up to a small number of items on the stack.")
    (license (list license:expat license:asl2.0))))

(define rust-socket2-0.5
  (package
    (name "rust-socket2")
    (version "0.5.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "socket2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1s7vjmb5gzp3iaqi94rh9r63k9cj00kjgbfn7gn60kmnk6fjcw69"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page
      "https://github.com/rust-lang/socket2")
    (synopsis
      "Utilities for handling networking sockets with a maximal amount of configuration\npossible intended.")
    (description
      "This package provides Utilities for handling networking sockets with a maximal amount of configuration\npossible intended.")
    (license (list license:expat license:asl2.0))))

(define rust-strsim-0.11
  (package
    (name "rust-strsim")
    (version "0.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strsim" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kzvqlw8hxqb7y598w1s0hxlnmi84sg5vsipp3yg5na5d1rvba3x"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/rapidfuzz/strsim-rs")
    (synopsis
      "Implementations of string similarity metrics. Includes Hamming, Levenshtein,\nOSA, Damerau-Levenshtein, Jaro, Jaro-Winkler, and SÃ¸rensen-Dice.")
    (description
      "This package provides Implementations of string similarity metrics.  Includes Hamming, Levenshtein,\nOSA, Damerau-Levenshtein, Jaro, Jaro-Winkler, and SÃ¸rensen-Dice.")
    (license license:expat)))

(define rust-strum-0.26
  (package
    (name "rust-strum")
    (version "0.26.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01lgl6jvrf4j28v5kmx9bp480ygf1nhvac8b4p7rcj9hxw50zv4g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-strum-macros" ,rust-strum-macros-0.26))))
    (home-page
      "https://github.com/Peternator7/strum")
    (synopsis
      "Helpful macros for working with enums and strings")
    (description
      "This package provides Helpful macros for working with enums and strings.")
    (license license:expat)))

(define rust-strum-macros-0.26
  (package
    (name "rust-strum-macros")
    (version "0.26.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gl1wmq24b8md527cpyd5bw9rkbqldd7k1h38kf5ajd2ln2ywssc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-heck" ,rust-heck-0.5)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/Peternator7/strum")
    (synopsis
      "Helpful macros for working with enums and strings")
    (description
      "This package provides Helpful macros for working with enums and strings.")
    (license license:expat)))

(define rust-syn-2
  (package
    (name "rust-syn")
    (version "2.0.90")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cfg5dsr1x0hl6b9hz08jp1197mx0rq3xydqmqaws36xlms3p7ci"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-unicode-ident" ,rust-unicode-ident-1))))
    (home-page "https://github.com/dtolnay/syn")
    (synopsis "Parser for Rust source code")
    (description
      "This package provides Parser for Rust source code.")
    (license (list license:expat license:asl2.0))))

(define rust-thiserror-1
  (package
    (name "rust-thiserror")
    (version "1.0.69")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lizjay08agcr5hs9yfzzj6axs53a2rgx070a1dsi3jpkcrzbamn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror-impl" ,rust-thiserror-impl-1))))
    (home-page
      "https://github.com/dtolnay/thiserror")
    (synopsis "derive(Error)")
    (description
      "This package provides derive(Error).")
    (license (list license:expat license:asl2.0))))

(define rust-thiserror-impl-1
  (package
    (name "rust-thiserror-impl")
    (version "1.0.69")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1h84fmn2nai41cxbhk6pqf46bxqq1b344v8yz089w1chzi76rvjg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/dtolnay/thiserror")
    (synopsis
      "Implementation detail of the `thiserror` crate")
    (description
      "This package provides Implementation detail of the `thiserror` crate.")
    (license (list license:expat license:asl2.0))))

(define rust-thread-local-1
  (package
    (name "rust-thread-local")
    (version "1.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thread_local" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "173i5lyjh011gsimk21np9jn8al18rxsrkjli20a7b8ks2xgk7lb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-once-cell" ,rust-once-cell-1))))
    (home-page
      "https://github.com/Amanieu/thread_local-rs")
    (synopsis "Per-object thread-local storage")
    (description
      "This package provides Per-object thread-local storage.")
    (license (list license:expat license:asl2.0))))

(define rust-time-0.3
  (package
    (name "rust-time")
    (version "0.3.37")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08bvydyc14plkwhchzia5bcdbmm0mk5fzilsdpjx06w6hf48drrm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-deranged" ,rust-deranged-0.3)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-num-conv" ,rust-num-conv-0.1)
         ("rust-num-threads" ,rust-num-threads-0.1)
         ("rust-powerfmt" ,rust-powerfmt-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-time-core" ,rust-time-core-0.1)
         ("rust-time-macros" ,rust-time-macros-0.2))))
    (home-page "https://time-rs.github.io")
    (synopsis
      "Date and time library. Fully interoperable with the standard library. Mostly compatible with #![no_std]")
    (description
      "This package provides Date and time library.  Fully interoperable with the standard library.  Mostly\ncompatible with #![no_std].")
    (license (list license:expat license:asl2.0))))

(define rust-time-macros-0.2
  (package
    (name "rust-time-macros")
    (version "0.2.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pl558z26pp342l5y91n6dxb60xwhar975wk6jc4npiygq0ycd18"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-conv" ,rust-num-conv-0.1)
         ("rust-time-core" ,rust-time-core-0.1))))
    (home-page "https://github.com/time-rs/time")
    (synopsis
      "Procedural macros for the time crate.\n    This crate is an implementation detail and should not be relied upon directly.")
    (description
      "This package provides Procedural macros for the time crate.  This crate is an implementation detail\nand should not be relied upon directly.")
    (license (list license:expat license:asl2.0))))

(define rust-tinystr-0.7
  (package
    (name "rust-tinystr")
    (version "0.7.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinystr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bxqaw7z8r2kzngxlzlgvld1r6jbnwyylyvyjbv1q71rvgaga5wi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page
      "https://github.com/unicode-org/icu4x")
    (synopsis
      "small ASCII-only bounded length string representation.")
    (description
      "This package provides a small ASCII-only bounded length string representation.")
    (license license:unicode)))

(define rust-tokio-1
  (package
    (name "rust-tokio")
    (version "1.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lbmf21h16iibdy7m63ck66grkwa7b1x8yy9gwvp60j5n0hrpv2w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-mio" ,rust-mio-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-signal-hook-registry"
          ,rust-signal-hook-registry-1)
         ("rust-socket2" ,rust-socket2-0.5)
         ("rust-tokio-macros" ,rust-tokio-macros-2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "https://tokio.rs")
    (synopsis
      "An event-driven, non-blocking I/O platform for writing asynchronous I/O\nbacked applications.")
    (description
      "This package provides An event-driven, non-blocking I/O platform for writing asynchronous I/O backed\napplications.")
    (license license:expat)))

(define rust-tokio-stream-0.1
  (package
    (name "rust-tokio-stream")
    (version "0.1.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-stream" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ix0770hfp4x5rh5bl7vsnr3d4iz4ms43i522xw70xaap9xqv9gc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://tokio.rs")
    (synopsis
      "Utilities to work with `Stream` and `tokio`.")
    (description
      "This package provides Utilities to work with `Stream` and `tokio`.")
    (license license:expat)))

(define rust-tokio-util-0.7
  (package
    (name "rust-tokio-util")
    (version "0.7.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0y0h10a52c7hrldmr3410bp7j3fadq0jn9nf7awddgd2an6smz6p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://tokio.rs")
    (synopsis
      "Additional utilities for working with Tokio.")
    (description
      "This package provides Additional utilities for working with Tokio.")
    (license license:expat)))

(define rust-toml-edit-0.22
  (package
    (name "rust-toml-edit")
    (version "0.22.22")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "toml_edit" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xf7sxfzmnc45f75x302qrn5aph52vc8w226v59yhrm211i8vr2a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-indexmap" ,rust-indexmap-2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-spanned" ,rust-serde-spanned-0.6)
         ("rust-toml-datetime" ,rust-toml-datetime-0.6)
         ("rust-winnow" ,rust-winnow-0.6))))
    (home-page "https://github.com/toml-rs/toml")
    (synopsis
      "Yet another format-preserving TOML parser")
    (description
      "This package provides Yet another format-preserving TOML parser.")
    (license (list license:expat license:asl2.0))))

(define rust-tracing-0.1
  (package
    (name "rust-tracing")
    (version "0.1.41")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1l5xrzyjfyayrwhvhldfnwdyligi1mpqm8mzbi2m1d6y6p2hlkkq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-tracing-attributes"
          ,rust-tracing-attributes-0.1)
         ("rust-tracing-core" ,rust-tracing-core-0.1))))
    (home-page "https://tokio.rs")
    (synopsis "Application-level tracing for Rust.")
    (description
      "This package provides Application-level tracing for Rust.")
    (license license:expat)))

(define rust-tracing-appender-0.2
  (package
    (name "rust-tracing-appender")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-appender" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kq69qyjvb4dxch5c9zgii6cqhy9nkk81z0r4pj3y2nc537fhrim"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-time" ,rust-time-0.3)
         ("rust-tracing-subscriber"
          ,rust-tracing-subscriber-0.3))))
    (home-page "https://tokio.rs")
    (synopsis
      "Provides utilities for file appenders and making non-blocking writers.")
    (description
      "This package provides utilities for file appenders and making non-blocking\nwriters.")
    (license license:expat)))

(define rust-tracing-attributes-0.1
  (package
    (name "rust-tracing-attributes")
    (version "0.1.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-attributes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0v92l9cxs42rdm4m5hsa8z7ln1xsiw1zc2iil8c6k7lzq0jf2nir"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://tokio.rs")
    (synopsis
      "Procedural macro attributes for automatically instrumenting functions.")
    (description
      "This package provides Procedural macro attributes for automatically instrumenting functions.")
    (license license:expat)))

(define rust-tracing-core-0.1
  (package
    (name "rust-tracing-core")
    (version "0.1.33")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "170gc7cxyjx824r9kr17zc9gvzx89ypqfdzq259pr56gg5bwjwp6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-valuable" ,rust-valuable-0.1))))
    (home-page "https://tokio.rs")
    (synopsis
      "Core primitives for application-level tracing.")
    (description
      "This package provides Core primitives for application-level tracing.")
    (license license:expat)))

(define rust-tracing-subscriber-0.3
  (package
    (name "rust-tracing-subscriber")
    (version "0.3.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-subscriber" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0220rignck8072i89jjsh140vmh14ydwpdwnifyaf3xcnpn9s678"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-matchers" ,rust-matchers-0.1)
         ("rust-nu-ansi-term" ,rust-nu-ansi-term-0.46)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-sharded-slab" ,rust-sharded-slab-0.1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thread-local" ,rust-thread-local-1)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-tracing-core" ,rust-tracing-core-0.1)
         ("rust-tracing-log" ,rust-tracing-log-0.2))))
    (home-page "https://tokio.rs")
    (synopsis
      "Utilities for implementing and composing `tracing` subscribers.")
    (description
      "This package provides Utilities for implementing and composing `tracing` subscribers.")
    (license license:expat)))

(define rust-trash-5
  (package
    (name "rust-trash")
    (version "5.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "trash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bzxzyjywb6sdyfshr9fp3fbpjf7gfhznh9ybrlb8rh3q9icmrd8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chrono" ,rust-chrono-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-objc2" ,rust-objc2-0.5)
         ("rust-objc2-foundation"
          ,rust-objc2-foundation-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2)
         ("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-urlencoding" ,rust-urlencoding-2)
         ("rust-windows" ,rust-windows-0.56))))
    (home-page
      "https://github.com/ArturKovacs/trash")
    (synopsis
      "library for moving files and folders to the Recycle Bin")
    (description
      "This package provides a library for moving files and folders to the Recycle Bin.")
    (license license:expat)))

(define rust-typenum-1
  (package
    (name "rust-typenum")
    (version "1.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typenum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09dqxv69m9lj9zvv6xw5vxaqx15ps0vxyy5myg33i0kbqvq0pzs2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/paholg/typenum")
    (synopsis
      "Typenum is a Rust library for type-level numbers evaluated at\n    compile time. It currently supports bits, unsigned integers, and signed\n    integers. It also provides a type-level array of type-level numbers, but its\n    implementation is incomplete")
    (description
      "This package provides Typenum is a Rust library for type-level numbers evaluated at compile time.  It\ncurrently supports bits, unsigned integers, and signed integers.  It also\nprovides a type-level array of type-level numbers, but its implementation is\nincomplete.")
    (license (list license:expat license:asl2.0))))

(define rust-unicode-ident-1
  (package
    (name "rust-unicode-ident")
    (version "1.0.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-ident" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10ywa1pg0glgkr4l3dppjxizr9r2b7im0ycbfa0137l69z5fdfdd"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/dtolnay/unicode-ident")
    (synopsis
      "Determine whether characters have the XID_Start or XID_Continue properties according to Unicode Standard Annex #31")
    (description
      "This package provides Determine whether characters have the XID_Start or XID_Continue properties\naccording to Unicode Standard Annex #31.")
    (license
      (list license:unicode))))

(define rust-unicode-segmentation-1
  (package
    (name "rust-unicode-segmentation")
    (version "1.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-segmentation" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14qla2jfx74yyb9ds3d2mpwpa4l4lzb9z57c6d2ba511458z5k7n"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/unicode-rs/unicode-segmentation")
    (synopsis
      "This crate provides Grapheme Cluster, Word and Sentence boundaries\naccording to Unicode Standard Annex #29 rules.")
    (description
      "This crate provides Grapheme Cluster, Word and Sentence boundaries according to\nUnicode Standard Annex #29 rules.")
    (license (list license:expat license:asl2.0))))

(define rust-unicode-truncate-1
  (package
    (name "rust-unicode-truncate")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-truncate" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gr7arjjhrhy8dww7hj8qqlws97xf9d276svr4hs6pxgllklcr5k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-itertools" ,rust-itertools-0.13)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1)
         ("rust-unicode-width" ,rust-unicode-width-0.1))))
    (home-page
      "https://github.com/Aetf/unicode-truncate")
    (synopsis
      "Unicode-aware algorithm to pad or truncate `str` in terms of displayed width.")
    (description
      "This package provides Unicode-aware algorithm to pad or truncate `str` in terms of displayed width.")
    (license (list license:expat license:asl2.0))))

(define rust-unicode-width-0.1
  (package
    (name "rust-unicode-width")
    (version "0.1.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-width" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1bzn2zv0gp8xxbxbhifw778a7fc93pa6a1kj24jgg9msj07f7mkx"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/unicode-rs/unicode-width")
    (synopsis
      "Determine displayed width of `char` and `str` types\naccording to Unicode Standard Annex #11 rules.")
    (description
      "This package provides Determine displayed width of `char` and `str` types according to Unicode\nStandard Annex #11 rules.")
    (license (list license:expat license:asl2.0))))

(define rust-unicode-width-0.2
  (package
    (name "rust-unicode-width")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-width" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zd0r5vs52ifxn25rs06gxrgz8cmh4xpra922k0xlmrchib1kj0z"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/unicode-rs/unicode-width")
    (synopsis
      "Determine displayed width of `char` and `str` types\naccording to Unicode Standard Annex #11 rules.")
    (description
      "This package provides Determine displayed width of `char` and `str` types according to Unicode\nStandard Annex #11 rules.")
    (license (list license:expat license:asl2.0))))

(define rust-url-2
  (package
    (name "rust-url")
    (version "2.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "url" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0q6sgznyy2n4l5lm16zahkisvc9nip9aa5q1pps7656xra3bdy1j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-form-urlencoded" ,rust-form-urlencoded-1)
         ("rust-idna" ,rust-idna-1)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2))))
    (home-page "https://github.com/servo/rust-url")
    (synopsis
      "URL library for Rust, based on the WHATWG URL Standard")
    (description
      "This package provides URL library for Rust, based on the WHATWG URL Standard.")
    (license (list license:expat license:asl2.0))))

(define rust-utf16-iter-1
  (package
    (name "rust-utf16-iter")
    (version "1.0.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "utf16_iter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ik2krdr73hfgsdzw0218fn35fa09dg2hvbi1xp3bmdfrp9js8y8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://docs.rs/utf16_iter/")
    (synopsis
      "Iterator by char over potentially-invalid UTF-16 in &[u16]")
    (description
      "This package provides Iterator by char over potentially-invalid UTF-16 in &[u16].")
    (license (list license:asl2.0 license:expat))))

(define rust-utf8-iter-1
  (package
    (name "rust-utf8-iter")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "utf8_iter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gmna9flnj8dbyd8ba17zigrp9c4c3zclngf5lnb5yvz1ri41hdn"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://docs.rs/utf8_iter/")
    (synopsis
      "Iterator by char over potentially-invalid UTF-8 in &[u8]")
    (description
      "This package provides Iterator by char over potentially-invalid UTF-8 in &[u8].")
    (license (list license:asl2.0 license:expat))))

(define rust-utf8parse-0.2
  (package
    (name "rust-utf8parse")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "utf8parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "088807qwjq46azicqwbhlmzwrbkz7l4hpw43sdkdyyk524vdxaq6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/alacritty/vte")
    (synopsis "Table-driven UTF-8 parser")
    (description
      "This package provides Table-driven UTF-8 parser.")
    (license (list license:asl2.0 license:expat))))

(define rust-v-frame-0.3
  (package
    (name "rust-v-frame")
    (version "0.3.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "v_frame" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0az9nd6qi1gyikh9yb3lhm453kf7d5isd6xai3j13kds4jm2mwyn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aligned-vec" ,rust-aligned-vec-0.5)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page "https://github.com/rust-av/v_frame")
    (synopsis
      "Video Frame data structures, originally part of rav1e")
    (description
      "This package provides Video Frame data structures, originally part of rav1e.")
    (license license:bsd-2)))

(define rust-validator-0.19
  (package
    (name "rust-validator")
    (version "0.19.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "validator" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00v3pv9w1qgfkf4vjyn8j8rbfj8mwdzg4yckl200j889hyfs5d6h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-idna" ,rust-idna-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-url" ,rust-url-2)
         ("rust-validator-derive"
          ,rust-validator-derive-0.19))))
    (home-page "https://github.com/Keats/validator")
    (synopsis
      "Common validation functions (email, url, length, ...) and trait - to be used with `validator_derive`")
    (description
      "This package provides Common validation functions (email, url, length, ...) and trait - to be used\nwith `validator_derive`.")
    (license license:expat)))

(define rust-validator-derive-0.19
  (package
    (name "rust-validator-derive")
    (version "0.19.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "validator_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xxz85yp9ma8vm4z9r8mrdxq6bm4f3jycmwp4bmkp13grsi5bj5s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling" ,rust-darling-0.20)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-proc-macro-error2"
          ,rust-proc-macro-error2-2)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/Keats/validator")
    (synopsis
      "Macros 1.1 implementation of #[derive(Validate)]")
    (description
      "This package provides Macros 1.1 implementation of #[derive(Validate)].")
    (license license:expat)))

(define rust-vergen-9
  (package
    (name "rust-vergen")
    (version "9.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vergen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0f86dz7mm0blfw17lggkkwm548mdjgq8f7llqxaz8pghz345zwii"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-derive-builder" ,rust-derive-builder-0.20)
         ("rust-rustc-version" ,rust-rustc-version-0.4)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-time" ,rust-time-0.3)
         ("rust-vergen-lib" ,rust-vergen-lib-0.1))))
    (home-page
      "https://github.com/rustyhorde/vergen")
    (synopsis
      "Generate 'cargo:rustc-env' instructions via 'build.rs' for use in your code via the 'env!' macro")
    (description
      "This package provides Generate cargo:rustc-env instructions via build.rs for use in your code via the\nenv! macro.")
    (license (list license:expat license:asl2.0))))

(define rust-vergen-gitcl-1
  (package
    (name "rust-vergen-gitcl")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vergen-gitcl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15xrh1h6mj3nc6c34hpdzcy82sk7bq2mx6lylq7b12pr15nh09q2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-derive-builder" ,rust-derive-builder-0.20)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-time" ,rust-time-0.3)
         ("rust-vergen" ,rust-vergen-9)
         ("rust-vergen-lib" ,rust-vergen-lib-0.1))))
    (home-page
      "https://github.com/rustyhorde/vergen")
    (synopsis
      "Generate 'cargo:rustc-env' instructions via 'build.rs' for use in your code via the 'env!' macro")
    (description
      "This package provides Generate cargo:rustc-env instructions via build.rs for use in your code via the\nenv! macro.")
    (license (list license:expat license:asl2.0))))

(define rust-vergen-lib-0.1
  (package
    (name "rust-vergen-lib")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vergen-lib" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ix19k2kvn4hzhhl6nld9h7fh1qh5z7j51z5rn2zq28wfpk6giy0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-derive-builder" ,rust-derive-builder-0.20)
         ("rust-rustversion" ,rust-rustversion-1))))
    (home-page
      "https://github.com/rustyhorde/vergen")
    (synopsis
      "Common code used to support the vergen libraries")
    (description
      "This package provides Common code used to support the vergen libraries.")
    (license (list license:expat license:asl2.0))))

(define rust-version-check-0.9
  (package
    (name "rust-version-check")
    (version "0.9.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "version_check" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nhhi4i5x89gm911azqbn7avs9mdacw2i3vcz3cnmz3mv4rqz4hb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/SergioBenitez/version_check")
    (synopsis
      "Tiny crate to check the version of the installed/running rustc")
    (description
      "This package provides Tiny crate to check the version of the installed/running rustc.")
    (license (list license:expat license:asl2.0))))

(define rust-wasm-bindgen-0.2
  (package
    (name "rust-wasm-bindgen")
    (version "0.2.99")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15k3rzb3kjrxyqnh0916gq99mrpwhwy62smawxxc2w0x3llgcx54"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-wasm-bindgen-macro"
          ,rust-wasm-bindgen-macro-0.2))))
    (home-page "https://rustwasm.github.io/")
    (synopsis
      "Easy support for interacting between JS and Rust.")
    (description
      "This package provides Easy support for interacting between JS and Rust.")
    (license (list license:expat license:asl2.0))))

(define rust-wasm-bindgen-backend-0.2
  (package
    (name "rust-wasm-bindgen-backend")
    (version "0.2.99")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-backend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ycwa4c68j34687k513djgyy2asn3fw3yp4g9rkq2kvbchwbp2az"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bumpalo" ,rust-bumpalo-3)
         ("rust-log" ,rust-log-0.4)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2)
         ("rust-wasm-bindgen-shared"
          ,rust-wasm-bindgen-shared-0.2))))
    (home-page
      "https://rustwasm.github.io/wasm-bindgen/")
    (synopsis
      "Backend code generation of the wasm-bindgen tool")
    (description
      "This package provides Backend code generation of the wasm-bindgen tool.")
    (license (list license:expat license:asl2.0))))

(define rust-wasm-bindgen-macro-0.2
  (package
    (name "rust-wasm-bindgen-macro")
    (version "0.2.99")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1znlcrk5bvisr3vscwlqkdby959n3sb367zgdzpjwjd7v4giiiic"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-quote" ,rust-quote-1)
         ("rust-wasm-bindgen-macro-support"
          ,rust-wasm-bindgen-macro-support-0.2))))
    (home-page
      "https://rustwasm.github.io/wasm-bindgen/")
    (synopsis
      "Definition of the `#[wasm_bindgen]` attribute, an internal dependency")
    (description
      "This package provides Definition of the `#[wasm_bindgen]` attribute, an internal dependency.")
    (license (list license:expat license:asl2.0))))

(define rust-wasm-bindgen-macro-support-0.2
  (package
    (name "rust-wasm-bindgen-macro-support")
    (version "0.2.99")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro-support" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hihsgyg0kf46kjhgfv8x5g9x0q1d0aizj6n7s84ag1xfrdskmrh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2)
         ("rust-wasm-bindgen-backend"
          ,rust-wasm-bindgen-backend-0.2)
         ("rust-wasm-bindgen-shared"
          ,rust-wasm-bindgen-shared-0.2))))
    (home-page
      "https://rustwasm.github.io/wasm-bindgen/")
    (synopsis
      "The part of the implementation of the `#[wasm_bindgen]` attribute that is not in the shared backend crate")
    (description
      "This package provides The part of the implementation of the `#[wasm_bindgen]` attribute that is not in\nthe shared backend crate.")
    (license (list license:expat license:asl2.0))))

(define rust-wasm-bindgen-shared-0.2
  (package
    (name "rust-wasm-bindgen-shared")
    (version "0.2.99")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19h61snrhh1qhb5gz6zyb89l7fbj1fhmxcvi09p9l0mav8zsnfll"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://rustwasm.github.io/wasm-bindgen/")
    (synopsis
      "Shared support between wasm-bindgen and wasm-bindgen cli, an internal\ndependency.")
    (description
      "This package provides Shared support between wasm-bindgen and wasm-bindgen cli, an internal\ndependency.")
    (license (list license:expat license:asl2.0))))

(define rust-which-6
  (package
    (name "rust-which")
    (version "6.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "which" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07yg74dsq644hq5a35546c9mja6rsjdsg92rykr9hkflxf7r5vml"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-either" ,rust-either-1)
         ("rust-home" ,rust-home-0.5)
         ("rust-rustix" ,rust-rustix-0.38)
         ("rust-winsafe" ,rust-winsafe-0.0.19))))
    (home-page
      "https://github.com/harryfei/which-rs.git")
    (synopsis
      "Rust equivalent of Unix command \"which\". Locate installed executable in cross platforms.")
    (description
      "This package provides a Rust equivalent of Unix command \"which\".  Locate\ninstalled executable in cross platforms.")
    (license license:expat)))

(define rust-winapi-util-0.1
  (package
    (name "rust-winapi-util")
    (version "0.1.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fqhkcl9scd230cnfj8apfficpf5c9vhwnk4yy9xfc1sw69iq8ng"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page
      "https://github.com/BurntSushi/winapi-util")
    (synopsis
      "dumping ground for high level safe wrappers over windows-sys.")
    (description
      "This package provides a dumping ground for high level safe wrappers over\nwindows-sys.")
    (license (list license:unlicense license:expat))))

(define rust-windows-0.56
  (package
    (name "rust-windows")
    (version "0.56.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cp10nzrqgrlk91dpwxjcpzyy6imr5vxr5f898pss7nz3gq9vrhx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-core" ,rust-windows-core-0.56)
         ("rust-windows-targets"
          ,rust-windows-targets-0.52))))
    (home-page
      "https://github.com/microsoft/windows-rs")
    (synopsis "Rust for Windows")
    (description
      "This package provides Rust for Windows.")
    (license (list license:expat license:asl2.0))))

(define rust-windows-core-0.56
  (package
    (name "rust-windows-core")
    (version "0.56.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19pj57bm0rzhlk0ghrccd3i5zvh0ghm52f8cmdc8d3yhs8pfb626"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-implement"
          ,rust-windows-implement-0.56)
         ("rust-windows-interface"
          ,rust-windows-interface-0.56)
         ("rust-windows-result" ,rust-windows-result-0.1)
         ("rust-windows-targets"
          ,rust-windows-targets-0.52))))
    (home-page
      "https://github.com/microsoft/windows-rs")
    (synopsis "Rust for Windows")
    (description
      "This package provides Rust for Windows.")
    (license (list license:expat license:asl2.0))))

(define rust-windows-implement-0.56
  (package
    (name "rust-windows-implement")
    (version "0.56.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-implement" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16rgkvlx4syqmajfdwmkcvn6nvh126wjj8sg3jvsk5fdivskbz7n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/microsoft/windows-rs")
    (synopsis
      "The implement macro for the windows crate")
    (description
      "This package provides The implement macro for the windows crate.")
    (license (list license:expat license:asl2.0))))

(define rust-windows-interface-0.56
  (package
    (name "rust-windows-interface")
    (version "0.56.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-interface" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k2prfxna0mw47f8gi8qhw9jfpw66bh2cqzs67sgipjfpx30b688"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/microsoft/windows-rs")
    (synopsis
      "The interface macro for the windows crate")
    (description
      "This package provides The interface macro for the windows crate.")
    (license (list license:expat license:asl2.0))))

(define rust-winnow-0.6
  (package
    (name "rust-winnow")
    (version "0.6.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winnow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16y4i8z9vh8hazjxg5mvmq0c5i35wlk8rxi5gkq6cn5vlb0zxh9n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2))))
    (home-page "https://github.com/winnow-rs/winnow")
    (synopsis
      "byte-oriented, zero-copy, parser combinators library")
    (description
      "This package provides a byte-oriented, zero-copy, parser combinators library.")
    (license license:expat)))

(define rust-winsafe-0.0.19
  (package
    (name "rust-winsafe")
    (version "0.0.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winsafe" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0169xy9mjma8dys4m8v4x0xhw2gkbhv2v1wsbvcjl9bhnxxd2dfi"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/rodrigocfd/winsafe")
    (synopsis
      "Windows API and GUI in safe, idiomatic Rust")
    (description
      "This package provides Windows API and GUI in safe, idiomatic Rust.")
    (license license:expat)))

(define rust-write16-1
  (package
    (name "rust-write16")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "write16" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dnryvrrbrnl7vvf5vb1zkmwldhjkf2n5znliviam7bm4900z2fi"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://docs.rs/write16/")
    (synopsis "UTF-16 analog of the Write trait")
    (description
      "This package provides a UTF-16 analog of the Write trait.")
    (license (list license:asl2.0 license:expat))))

(define rust-writeable-0.5
  (package
    (name "rust-writeable")
    (version "0.5.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "writeable" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lawr6y0bwqfyayf3z8zmqlhpnzhdx0ahs54isacbhyjwa7g778y"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/unicode-org/icu4x")
    (synopsis
      "more efficient alternative to fmt::Display")
    (description
      "This package provides a more efficient alternative to fmt::Display.")
    (license license:unicode)))

(define rust-yansi-1
  (package
    (name "rust-yansi")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yansi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jdh55jyv0dpd38ij4qh60zglbw9aa8wafqai6m0wa7xaxk3mrfg"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/SergioBenitez/yansi")
    (synopsis
      "dead simple ANSI terminal color painting library.")
    (description
      "This package provides a dead simple ANSI terminal color painting library.")
    (license (list license:expat license:asl2.0))))

(define rust-yazi-adapter-0.4
  (package
    (name "rust-yazi-adapter")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-adapter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0haappfqvg98bb7mh8har6dc8dh4kia80z8lcr55am468pzfp6ca"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ansi-to-tui" ,rust-ansi-to-tui-7)
         ("rust-anyhow" ,rust-anyhow-1)
         ("rust-base64" ,rust-base64-0.22)
         ("rust-color-quant" ,rust-color-quant-1)
         ("rust-crossterm" ,rust-crossterm-0.28)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-image" ,rust-image-0.25)
         ("rust-ratatui" ,rust-ratatui-0.29)
         ("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-yazi-config" ,rust-yazi-config-0.4)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi image adapter")
    (description
      "This package provides Yazi image adapter.")
    (license license:expat)))

(define rust-yazi-boot-0.4
  (package
    (name "rust-yazi-boot")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-boot" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08jq25r4w8r55nkgds7rcgarcm5qabwysv0xvxfl60jwxbc2114x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-4)
         ("rust-clap-complete" ,rust-clap-complete-4)
         ("rust-clap-complete-fig"
          ,rust-clap-complete-fig-4)
         ("rust-clap-complete-nushell"
          ,rust-clap-complete-nushell-4)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-vergen-gitcl" ,rust-vergen-gitcl-1)
         ("rust-yazi-adapter" ,rust-yazi-adapter-0.4)
         ("rust-yazi-config" ,rust-yazi-config-0.4)
         ("rust-yazi-fs" ,rust-yazi-fs-0.4)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi bootstrapper")
    (description
      "This package provides Yazi bootstrapper.")
    (license license:expat)))

(define rust-yazi-cli-0.4
  (package
    (name "rust-yazi-cli")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-cli" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1p1zppppzchkzpzik9cqx9swyyjd8f73nmfkr5d2wjbj2xvjlqq9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-clap" ,rust-clap-4)
         ("rust-clap-complete" ,rust-clap-complete-4)
         ("rust-clap-complete-fig"
          ,rust-clap-complete-fig-4)
         ("rust-clap-complete-nushell"
          ,rust-clap-complete-nushell-4)
         ("rust-crossterm" ,rust-crossterm-0.28)
         ("rust-md-5" ,rust-md-5-0.10)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-toml-edit" ,rust-toml-edit-0.22)
         ("rust-vergen-gitcl" ,rust-vergen-gitcl-1)
         ("rust-yazi-boot" ,rust-yazi-boot-0.4)
         ("rust-yazi-dds" ,rust-yazi-dds-0.4)
         ("rust-yazi-fs" ,rust-yazi-fs-0.4)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi command-line interface")
    (description
      "This package provides Yazi command-line interface.")
    (license license:expat)))

(define rust-yazi-codegen-0.4
  (package
    (name "rust-yazi-codegen")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-codegen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08ndi5493n5ywck78hima5hlq0ch01xppalij4fi2drvi737696z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi codegen")
    (description
      "This package provides Yazi codegen.")
    (license license:expat)))

(define rust-yazi-config-0.4
  (package
    (name "rust-yazi-config")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-config" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19np0fr0dkx6kw32nbm9ygryaypi4jyx309rl1pni1gcljggz1ym"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bitflags" ,rust-bitflags-2)
         ("rust-crossterm" ,rust-crossterm-0.28)
         ("rust-globset" ,rust-globset-0.4)
         ("rust-indexmap" ,rust-indexmap-2)
         ("rust-ratatui" ,rust-ratatui-0.29)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-toml" ,rust-toml-0.8)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-validator" ,rust-validator-0.19)
         ("rust-yazi-fs" ,rust-yazi-fs-0.4)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi configuration file parser")
    (description
      "This package provides Yazi configuration file parser.")
    (license license:expat)))

(define rust-yazi-core-0.4
  (package
    (name "rust-yazi-core")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p4k6q59y4bsnwp16ba5fg9lx04hgqxs4ac9avha9ai65wgixwgx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bitflags" ,rust-bitflags-2)
         ("rust-crossterm" ,rust-crossterm-0.28)
         ("rust-dirs" ,rust-dirs-5)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-notify" ,rust-notify-7)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-ratatui" ,rust-ratatui-0.29)
         ("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-unicode-width" ,rust-unicode-width-0.2)
         ("rust-yazi-adapter" ,rust-yazi-adapter-0.4)
         ("rust-yazi-boot" ,rust-yazi-boot-0.4)
         ("rust-yazi-codegen" ,rust-yazi-codegen-0.4)
         ("rust-yazi-config" ,rust-yazi-config-0.4)
         ("rust-yazi-dds" ,rust-yazi-dds-0.4)
         ("rust-yazi-fs" ,rust-yazi-fs-0.4)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-plugin" ,rust-yazi-plugin-0.4)
         ("rust-yazi-proxy" ,rust-yazi-proxy-0.4)
         ("rust-yazi-scheduler" ,rust-yazi-scheduler-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi core logic")
    (description
      "This package provides Yazi core logic.")
    (license license:expat)))

(define rust-yazi-dds-0.4
  (package
    (name "rust-yazi-dds")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-dds" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vifhzmf2w3k44hsabdidi8z2nmlzqsia3mdks3hcm1gmj0lmfgb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-mlua" ,rust-mlua-0.10)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-uzers" ,rust-uzers-0.12)
         ("rust-vergen-gitcl" ,rust-vergen-gitcl-1)
         ("rust-yazi-boot" ,rust-yazi-boot-0.4)
         ("rust-yazi-fs" ,rust-yazi-fs-0.4)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi data distribution service")
    (description
      "This package provides Yazi data distribution service.")
    (license license:expat)))

(define rust-yazi-fs-0.4
  (package
    (name "rust-yazi-fs")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-fs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gmdwqvif0fpg081s21x4xym922fdjkmr2lhbllryampx464xnrp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-arc-swap" ,rust-arc-swap-1)
         ("rust-bitflags" ,rust-bitflags-2)
         ("rust-dirs" ,rust-dirs-5)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-uzers" ,rust-uzers-0.12)
         ("rust-windows-sys" ,rust-windows-sys-0.59)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi file system")
    (description
      "This package provides Yazi file system.")
    (license license:expat)))

(define rust-yazi-macro-0.4
  (package
    (name "rust-yazi-macro")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fpn1x0qcrl64p6fr1hr34z3j5hakkylv3g4yn8874a4pnwph879"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi macros")
    (description
      "This package provides Yazi macros.")
    (license license:expat)))

(define rust-yazi-plugin-0.4
  (package
    (name "rust-yazi-plugin")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-plugin" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vv2igr5mdk3r8z2gpqbn4lk7rzpks3jj4vqglgv0r297nypbkma"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ansi-to-tui" ,rust-ansi-to-tui-7)
         ("rust-anyhow" ,rust-anyhow-1)
         ("rust-base64" ,rust-base64-0.22)
         ("rust-clipboard-win" ,rust-clipboard-win-5)
         ("rust-crossterm" ,rust-crossterm-0.28)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-globset" ,rust-globset-0.4)
         ("rust-md-5" ,rust-md-5-0.10)
         ("rust-mlua" ,rust-mlua-0.10)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-ratatui" ,rust-ratatui-0.29)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-syntect" ,rust-syntect-5)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-unicode-width" ,rust-unicode-width-0.2)
         ("rust-uzers" ,rust-uzers-0.12)
         ("rust-yazi-adapter" ,rust-yazi-adapter-0.4)
         ("rust-yazi-boot" ,rust-yazi-boot-0.4)
         ("rust-yazi-config" ,rust-yazi-config-0.4)
         ("rust-yazi-dds" ,rust-yazi-dds-0.4)
         ("rust-yazi-fs" ,rust-yazi-fs-0.4)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-prebuild" ,rust-yazi-prebuild-0.1)
         ("rust-yazi-proxy" ,rust-yazi-proxy-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi plugin system")
    (description
      "This package provides Yazi plugin system.")
    (license license:expat)))

(define rust-yazi-prebuild-0.1
  (package
    (name "rust-yazi-prebuild")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-prebuild" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cpnfhyb82f7xqlxgawi0qiqvnp0i1dnxwwnz9whzb1r5vhwidpl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/sxyazi/yazi")
    (synopsis
      "Used to place the pre-built assets of yazi (https://github.com/sxyazi/yazi)")
    (description
      "This package provides Used to place the pre-built assets of yazi (https://github.com/sxyazi/yazi).")
    (license license:expat)))

(define rust-yazi-proxy-0.4
  (package
    (name "rust-yazi-proxy")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-proxy" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wjw0hsfc064kqx1zisggqgpvbpizvr3x7p38ypw8sdsz02d1i0w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-mlua" ,rust-mlua-0.10)
         ("rust-shell-words" ,rust-shell-words-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-yazi-config" ,rust-yazi-config-0.4)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi event proxy")
    (description
      "This package provides Yazi event proxy.")
    (license license:expat)))

(define rust-yazi-scheduler-0.4
  (package
    (name "rust-yazi-scheduler")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-scheduler" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0q0g1d7c2w8a15m0ydk73c97ax76zhly576y3d0ir9cz4v4rwi3q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-async-priority-channel"
          ,rust-async-priority-channel-0.2)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-trash" ,rust-trash-5)
         ("rust-yazi-config" ,rust-yazi-config-0.4)
         ("rust-yazi-dds" ,rust-yazi-dds-0.4)
         ("rust-yazi-fs" ,rust-yazi-fs-0.4)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4)
         ("rust-yazi-plugin" ,rust-yazi-plugin-0.4)
         ("rust-yazi-proxy" ,rust-yazi-proxy-0.4)
         ("rust-yazi-shared" ,rust-yazi-shared-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi task scheduler")
    (description
      "This package provides Yazi task scheduler.")
    (license license:expat)))

(define rust-yazi-shared-0.4
  (package
    (name "rust-yazi-shared")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yazi-shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1a4rj2l9rr02d804d16spqjhd5dfqdjy2f3qrsi23a6773rmkq9j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-crossterm" ,rust-crossterm-0.28)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2)
         ("rust-ratatui" ,rust-ratatui-0.29)
         ("rust-serde" ,rust-serde-1)
         ("rust-shell-words" ,rust-shell-words-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-uzers" ,rust-uzers-0.12)
         ("rust-windows-sys" ,rust-windows-sys-0.59)
         ("rust-yazi-macro" ,rust-yazi-macro-0.4))))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Yazi shared library")
    (description
      "This package provides Yazi shared library.")
    (license license:expat)))

(define rust-yoke-0.7
  (package
    (name "rust-yoke")
    (version "0.7.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yoke" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0h3znzrdmll0a7sglzf9ji0p5iqml11wrj1dypaf6ad6kbpnl3hj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-stable-deref-trait"
          ,rust-stable-deref-trait-1)
         ("rust-yoke-derive" ,rust-yoke-derive-0.7)
         ("rust-zerofrom" ,rust-zerofrom-0.1))))
    (home-page
      "https://github.com/unicode-org/icu4x")
    (synopsis
      "Abstraction allowing borrowed data to be carried along with the backing data it borrows from")
    (description
      "This package provides Abstraction allowing borrowed data to be carried along with the backing data it\nborrows from.")
    (license license:unicode)))

(define rust-yoke-derive-0.7
  (package
    (name "rust-yoke-derive")
    (version "0.7.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yoke-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0m4i4a7gy826bfvnqa9wy6sp90qf0as3wps3wb0smjaamn68g013"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2)
         ("rust-synstructure" ,rust-synstructure-0.13))))
    (home-page
      "https://github.com/unicode-org/icu4x")
    (synopsis "Custom derive for the yoke crate")
    (description
      "This package provides Custom derive for the yoke crate.")
    (license license:unicode)))

(define rust-zerofrom-0.1
  (package
    (name "rust-zerofrom")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerofrom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bnd8vjcllzrvr3wvn8x14k2hkrpyy1fm3crkn2y3plmr44fxwyg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zerofrom-derive"
          ,rust-zerofrom-derive-0.1))))
    (home-page
      "https://github.com/unicode-org/icu4x")
    (synopsis "ZeroFrom trait for constructing")
    (description
      "This package provides @code{ZeroFrom} trait for constructing.")
    (license license:unicode)))

(define rust-zerofrom-derive-0.1
  (package
    (name "rust-zerofrom-derive")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerofrom-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "022q55phhb44qbrcfbc48k0b741fl8gnazw3hpmmndbx5ycfspjr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2)
         ("rust-synstructure" ,rust-synstructure-0.13))))
    (home-page
      "https://github.com/unicode-org/icu4x")
    (synopsis "Custom derive for the zerofrom crate")
    (description
      "This package provides Custom derive for the zerofrom crate.")
    (license license:unicode)))

(define rust-zerovec-0.10
  (package
    (name "rust-zerovec")
    (version "0.10.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerovec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yghix7n3fjfdppwghknzvx9v8cf826h2qal5nqvy8yzg4yqjaxa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-yoke" ,rust-yoke-0.7)
         ("rust-zerofrom" ,rust-zerofrom-0.1)
         ("rust-zerovec-derive" ,rust-zerovec-derive-0.10))))
    (home-page
      "https://github.com/unicode-org/icu4x")
    (synopsis
      "Zero-copy vector backed by a byte array")
    (description
      "This package provides Zero-copy vector backed by a byte array.")
    (license license:unicode)))

(define rust-zerovec-derive-0.10
  (package
    (name "rust-zerovec-derive")
    (version "0.10.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerovec-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ik322dys6wnap5d3gcsn09azmssq466xryn5czfm13mn7gsdbvf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/unicode-org/icu4x")
    (synopsis "Custom derive for the zerovec crate")
    (description
      "This package provides Custom derive for the zerovec crate.")
    (license license:unicode)))

(define rust-zune-inflate-0.2
  (package
    (name "rust-zune-inflate")
    (version "0.2.54")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zune-inflate" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00kg24jh3zqa3i6rg6yksnb71bch9yi1casqydl00s7nw8pk7avk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-simd-adler32" ,rust-simd-adler32-0.3))))
    (home-page
      "https://github.com/etemesi254/zune-image/tree/main/zune-inflate")
    (synopsis
      "heavily optimized deflate decompressor in Pure Rust")
    (description
      "This package provides a heavily optimized deflate decompressor in Pure Rust.")
    (license
      (list license:expat license:asl2.0 license:zlib))))

(define yazi
  (package
    (name "yazi")
    (version "0.4.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/sxyazi/yazi")
             (commit (string-append "v" version))))
       (file-name (git-file-name "yazi" version))
       (sha256
        (base32 "1jqgvhwxrciiasvd9jcp9w9bc7xgkazmi5pjibapd2fb8zsgx8v0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:rust ,rust-1.82
        #:cargo-inputs (("rust-anyhow" ,rust-anyhow-1)
                        ("rust-better-panic" ,rust-better-panic-0.3)
                        ("rust-crossterm" ,rust-crossterm-0.28)
                        ("rust-fdlimit" ,rust-fdlimit-0.3)
                        ("rust-futures" ,rust-futures-0.3)
                        ("rust-libc" ,rust-libc-0.2)
                        ("rust-mlua" ,rust-mlua-0.10)
                        ("rust-ratatui" ,rust-ratatui-0.29)
                        ("rust-scopeguard" ,rust-scopeguard-1)
                        ("rust-signal-hook-tokio" ,rust-signal-hook-tokio-0.3)
                        ("rust-syntect" ,rust-syntect-5)
                        ("rust-tikv-jemallocator" ,rust-tikv-jemallocator-0.6)
                        ("rust-tokio" ,rust-tokio-1)
                        ("rust-tokio-stream" ,rust-tokio-stream-0.1)
                        ("rust-tracing" ,rust-tracing-0.1)
                        ("rust-tracing-appender" ,rust-tracing-appender-0.2)
                        ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3)
                        ("rust-yazi-adapter" ,rust-yazi-adapter-0.4)
                        ("rust-yazi-boot" ,rust-yazi-boot-0.4)
                        ("rust-yazi-codegen" ,rust-yazi-codegen-0.4)
                        ("rust-yazi-config" ,rust-yazi-config-0.4)
                        ("rust-yazi-core" ,rust-yazi-core-0.4)
                        ("rust-yazi-cli" ,rust-yazi-cli-0.4)
                        ("rust-yazi-dds" ,rust-yazi-dds-0.4)
                        ("rust-yazi-fs" ,rust-yazi-fs-0.4)
                        ("rust-yazi-macro" ,rust-yazi-macro-0.4)
                        ("rust-yazi-plugin" ,rust-yazi-plugin-0.4)
                        ("rust-yazi-proxy" ,rust-yazi-proxy-0.4)
                        ("rust-yazi-shared" ,rust-yazi-shared-0.4))
        #:phases
        ,#~(modify-phases %standard-phases
             (add-after 'unpack 'set-build-vars
               (lambda _
                 (setenv "YAZI_GEN_COMPLETIONS" "1")
                 (setenv "VERGEN_GIT_SHA" "Guix")
                 (setenv "VERGEN_BUILD_DATE" "2024-09-13")))
             (add-after 'unpack 'override-jemalloc
               (lambda _
                   ;; This flag is needed when not using the bundled jemalloc.
                   ;; https://github.com/tikv/jemallocator/issues/19
                   (setenv "CARGO_FEATURE_UNPREFIXED_MALLOC_ON_SUPPORTED_PLATFORMS" "1")
                   (setenv "JEMALLOC_OVERRIDE"
                           (string-append #$jemalloc "/lib/libjemalloc.so"))))
             (replace 'install
               (lambda _
                 (let* ((bin (string-append #$output "/bin"))
                        (yazi (string-append bin "/yazi"))
                        (ya (string-append bin "/ya"))
                        (applications (string-append #$output "/share/applications")))
                   (install-file "target/release/yazi" bin)
                   (install-file "target/release/ya" bin)
                   (install-file "assets/yazi.desktop" applications)))))))
    (native-inputs (list rust))
    (inputs (list jemalloc nasm))
    (propagated-inputs (list ffmpegthumbnailer jq poppler fd ripgrep fzf zoxide))
    (home-page "https://yazi-rs.github.io")
    (synopsis "Blazing Fast Terminal File Manager")
    (description
     "Yazi (duck in Chinese) is a terminal file manager written in
Rust, based on non-blocking async I/O.  It aims to provide an efficient,
user-friendly, and customizable file management experience.")
    (license (list license:expat))))

yazi
