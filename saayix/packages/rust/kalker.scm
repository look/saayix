;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages rust kalker)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages multiprecision)
  #:use-module (saayix packages)
  #:use-module (saayix packages python-xyz))

(define rust-rustyline-13
  (package
    (name "rust-rustyline")
    (version "13.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rustyline" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "11mywskk2jcxhanlsgzza5yx6ywpdlzr64qhbgpsx45clj1xd8h2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-test-flags '("--release" "--"
                            "--skip=binding::test::size_of_event")
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-clipboard-win" ,rust-clipboard-win-5)
                       ("rust-fd-lock" ,rust-fd-lock-4)
                       ("rust-home" ,rust-home-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-nix" ,rust-nix-0.27)
                       ("rust-radix-trie" ,rust-radix-trie-0.2)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-rusqlite" ,rust-rusqlite-0.30)
                       ("rust-rustyline-derive" ,rust-rustyline-derive-0.10)
                       ("rust-signal-hook" ,rust-signal-hook-0.3)
                       ("rust-skim" ,rust-skim-0.10)
                       ("rust-termios" ,rust-termios-0.3)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-unicode-width" ,rust-unicode-width-0.1)
                       ("rust-utf8parse" ,rust-utf8parse-0.2)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-assert-matches" ,rust-assert-matches-1)
                                   ("rust-doc-comment" ,rust-doc-comment-0.3)
                                   ("rust-env-logger" ,rust-env-logger-0.10)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/kkawakam/rustyline")
    (synopsis "Readline implementation in Rust")
    (description
     "Rustyline is a readline implementation based on the linenoise package.")
    (license license:expat)))

(define rust-rustyline-7
  (package
    (inherit rust-rustyline-13)
    (name "rust-rustyline")
    (version "7.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rustyline" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1v1czmy3ir7d06xldp8bg94l97hrm15hcgdxxkq3cwbizhdk09w2"))))
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs
       (("rust-bitflags" ,rust-bitflags-1)
        ("rust-cfg-if" ,rust-cfg-if-1)
        ("rust-dirs-next" ,rust-dirs-next-2)
        ("rust-fs2" ,rust-fs2-0.4)
        ("rust-libc" ,rust-libc-0.2)
        ("rust-log" ,rust-log-0.4)
        ("rust-memchr" ,rust-memchr-2)
        ("rust-nix" ,rust-nix-0.19)
        ("rust-scopeguard" ,rust-scopeguard-1)
        ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
        ("rust-unicode-width" ,rust-unicode-width-0.1)
        ("rust-utf8parse" ,rust-utf8parse-0.2)
        ("rust-winapi" ,rust-winapi-0.3)
        ("rust-skim" ,rust-skim-0.7))))))

(define gmp-for-kalker
  (package
    (inherit gmp)
    (name "gmp")
    (version "6.3.0")
    (source
      (origin
        (method url-fetch)
        (uri
         (string-append "mirror://gnu/gmp/gmp-"
                        version ".tar.xz"))
        (sha256
         (base32
          "1648ad1mr7c1r8lkkqshrv1jfjgfdb30plsadxhni7mq041bihm3"))
        (patches (search-patches "gmp-faulty-test.patch"))))))

(define rust-az-1
  (package
    (name "rust-az")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "az" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ww9k1w3al7x5qmb7f13v3s9c2pg1pdxbs8xshqy6zyrchj4qzkv"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://gitlab.com/tspiteri/az")
    (synopsis "Casts and checked casts")
    (description
      "This package provides Casts and checked casts.")
    (license (list license:expat license:asl2.0))))

(define rust-console-error-panic-hook-0.1
  (package
    (name "rust-console-error-panic-hook")
    (version "0.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "console_error_panic_hook" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1g5v8s0ndycc10mdn6igy914k645pgpcl8vjpz6nvxkhyirynsm0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page
      "https://github.com/rustwasm/console_error_panic_hook")
    (synopsis
      "panic hook for `wasm32-unknown-unknown` that logs panics to `console.error`")
    (description
      "This package provides a panic hook for `wasm32-unknown-unknown` that logs panics\nto `console.error`.")
    (license (list license:asl2.0 license:expat))))

(define rust-dirs-sys-0.3
  (package
    (name "rust-dirs-sys")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dirs-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19md1cnkazham8a6kh22v12d8hh3raqahfk6yb043vrjr68is78v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-redox-users" ,rust-redox-users-0.4)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page
      "https://github.com/dirs-dev/dirs-sys-rs")
    (synopsis
      "System-level helper functions for the dirs and directories crates")
    (description
      "This package provides System-level helper functions for the dirs and directories crates.")
    (license (list license:expat license:asl2.0))))

(define rust-gmp-mpfr-sys-1
  (package
    (name "rust-gmp-mpfr-sys")
    (version "1.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gmp-mpfr-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qq85s45m5pdyqrhbf9gsdlzra4gxqdbj0wwwkmimx79rg1nqain"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page
      "https://gitlab.com/tspiteri/gmp-mpfr-sys")
    (synopsis
      "Rust FFI bindings for GMP, MPFR and MPC")
    (description
      "This package provides Rust FFI bindings for GMP, MPFR and MPC.")
    (license license:lgpl3+)))

(define rust-kalk-3
  (package
    (name "rust-kalk")
    (version "3.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kalk" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1p7cvqbdhskb3zp7i0c8gnhzi88gpyb4rp430v7n2n4rqnqs7i3j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-rug" ,rust-rug-1)
         ("rust-test-case" ,rust-test-case-1)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-wasm-bindgen-test"
          ,rust-wasm-bindgen-test-0.3))))
    (home-page
      "https://github.com/PaddiM8/kalker/tree/master/kalk")
    (synopsis
      "math evaluator library that supports user-defined functions, variables and units, and can handle fairly ambiguous syntax.")
    (description
      "This package provides a math evaluator library that supports user-defined\nfunctions, variables and units, and can handle fairly ambiguous syntax.")
    (license license:expat)))

(define rust-regex-syntax-0.6
  (package
    (name "rust-regex-syntax")
    (version "0.6.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-syntax" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0j68z4jnxshfymb08j1drvxn9wgs1469047lfaq4im78wcxn0v25"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/rust-lang/regex/tree/master/regex-syntax")
    (synopsis "regular expression parser.")
    (description
      "This package provides a regular expression parser.")
    (license (list license:expat license:asl2.0))))

(define rust-rug-1
  (package
    (name "rust-rug")
    (version "1.24.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rug" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kgpxa0f62rq32j5kaf660468wbxqw84s42z0ynh7lahb7c2za3n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-az" ,rust-az-1)
         ("rust-gmp-mpfr-sys" ,rust-gmp-mpfr-sys-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libm" ,rust-libm-0.2))))
    (home-page "https://gitlab.com/tspiteri/rug")
    (synopsis
      "Arbitrary-precision integers, rational, floating-point and complex numbers based\non GMP, MPFR and MPC")
    (description
      "This package provides Arbitrary-precision integers, rational, floating-point and complex numbers based\non GMP, MPFR and MPC.")
    (license license:lgpl3+)))

(define rust-scoped-tls-1
  (package
    (name "rust-scoped-tls")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "scoped-tls" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15524h04mafihcvfpgxd8f4bgc3k95aclz8grjkg9a0rxcvn9kz1"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/alexcrichton/scoped-tls")
    (synopsis
      "Library implementation of the standard library's old `scoped_thread_local!`\nmacro for providing scoped access to thread local storage (TLS) so any type can\nbe stored into TLS.")
    (description
      "This package provides Library implementation of the standard library's old `scoped_thread_local!`\nmacro for providing scoped access to thread local storage (TLS) so any type can\nbe stored into TLS.")
    (license (list license:expat license:asl2.0))))

(define rust-seahorse-1
  (package
    (name "rust-seahorse")
    (version "1.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "seahorse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1garn1xr6ng0ip2kplj2pblrdjmsvhx3vjq7d8iisvvqakj1zvlb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/ksk001100/seahorse")
    (synopsis
      "minimal CLI framework written in Rust")
    (description
      "This package provides a minimal CLI framework written in Rust.")
    (license #f)))

(define kalker
  (package
    (name "kalker")
    (version "2.2.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/PaddiM8/kalker")
               (commit (string-append "v" version))))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1i18nlk2nd1jkkrp904hydxxmf6sg47i86z8mivl4qrmwhpqfmvw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:phases
        ,#~(modify-phases %standard-phases
             (add-after 'unpack 'disable-grammar-build
               (lambda _
                 (setenv "CARGO_FEATURE_USE_SYSTEM_LIBS" "1")))
             (delete 'package)
             (replace 'install
               (lambda* (#:key outputs #:allow-other-keys)
                 (let* ((out (assoc-ref outputs "out"))
                        (bin (string-append out "/bin"))
                        (lib (string-append out "/lib")))
                   (install-file "target/release/kalker" bin)
                   (install-file "target/release/libkalk.so" lib)))))
        #:cargo-inputs (("rust-ansi-term" ,rust-ansi-term-0.12)
                        ("rust-atty" ,rust-atty-0.2)
                        ("rust-dirs" ,rust-dirs-3)
                        ("rust-kalk" ,rust-kalk-3)
                        ("rust-lazy-static" ,rust-lazy-static-1)
                        ("rust-regex" ,rust-regex-1)
                        ("rust-rustyline" ,rust-rustyline-7)
                        ("rust-seahorse" ,rust-seahorse-1)
                        ("rust-winres" ,rust-winres-0.1))))
    (inputs (list gmp-for-kalker
                  mpfr
                  mpc))
    (home-page "https://github.com/PaddiM8/kalker")
    (synopsis "Scientific calculator with math syntax that supports user-defined variables and functions, complex numbers, and estimation of derivatives and integrals")
    (description "Kalker is a calculator program/website that supports user-defined variables, functions, differentiation, and integration. It runs on Windows, macOS, Linux, Android, and in web browsers (with WebAssembly).")
    (license license:expat)))
