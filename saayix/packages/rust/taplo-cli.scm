;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages rust taplo-cli)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages tls)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages))

(define rust-addr2line-0.17.0
  (package
    (name "rust-addr2line")
    (version "0.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "addr2line" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0sw16zqy6w0ar633z69m7lw6gb0k1y7xj3387a8wly43ij5div5r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-gimli" ,rust-gimli-0.26.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-adler-1.0.2
  (package
    (name "rust-adler")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "adler" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zim79cvzd5yrkzl3nyfx0avijwgk9fqv3yrscdy1cc79ih02qpj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-ahash-0.7.6
  (package
    (name "rust-ahash")
    (version "0.7.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ahash" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0isw672fiwx8cjl040jrck6pi85xcszkz6q0xsqkiy6qjl31mdgw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-getrandom" ,rust-getrandom-0.2.8)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-version-check" ,rust-version-check-0.9.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-ahash-0.8.3
  (package
    (name "rust-ahash")
    (version "0.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ahash" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0bzcsxdl2wd6j2p4214qh9sqkqn69gi7f9lk1xi8yj063r6zd69c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-getrandom" ,rust-getrandom-0.2.8)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-version-check" ,rust-version-check-0.9.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-aho-corasick-0.7.19
  (package
    (name "rust-aho-corasick")
    (version "0.7.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aho-corasick" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0knl5n9f396068qk4zrvhcf01d5qp9ja2my4j7ywny093bcmpxdl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-memchr" ,rust-memchr-2.5.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-ansi-term-0.12.1
  (package
    (name "rust-ansi-term")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ansi_term" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ljmkbilxgmhavxvxqa7qvm6f3fjggi7q2l3a72q9x0cxjvrnanm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-anyhow-1.0.66
  (package
    (name "rust-anyhow")
    (version "1.0.66")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anyhow" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xj3ahmwjlbiqsajhkaa0q6hqwb4l3l5rkfxa7jk1498r3fn2qi1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.3.66))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-arc-swap-1.5.1
  (package
    (name "rust-arc-swap")
    (version "1.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "arc-swap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0r5136lflczil0hzxzwg0fh2g2l5nlp2cmx5zz36samhsjwxhg4q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-arrayvec-0.7.2
  (package
    (name "rust-arrayvec")
    (version "0.7.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "arrayvec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mjl8jjqxpl0x7sm9cij61cppi7yi38cdrd1l8zjw7h7qxk2v9cd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-assert-json-diff-2.0.2
  (package
    (name "rust-assert-json-diff")
    (version "2.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "assert-json-diff" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04mg3w0rh3schpla51l18362hsirl23q93aisws2irrj32wg5r27"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-async-ctrlc-1.2.0
  (package
    (name "rust-async-ctrlc")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-ctrlc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1w9dxna5c4ky5z500xqcq6kivwh8hcg7295cgknchl8sx7v7jwlh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-ctrlc" ,rust-ctrlc-3.2.3)
                        ("rust-futures-core" ,rust-futures-core-0.3.25))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-async-recursion-1.0.0
  (package
    (name "rust-async-recursion")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-recursion" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1shvcikj7bbal61xq0m0bkx8rafcabsb6rmwhm74qqhhri5qznic"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-async-trait-0.1.58
  (package
    (name "rust-async-trait")
    (version "0.1.58")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-trait" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "031n0jlf07gn8k3bbfi7klqmzaxi8va4rkr62ijin05mwsa5v00y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-atomic-0.5.1
  (package
    (name "rust-atomic")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "atomic" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0k135q1qfmxxyzrlhr47r0j38r5fnd4163rgl552qxyagrk853dq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-atty-0.2.14
  (package
    (name "rust-atty")
    (version "0.2.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "atty" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1s7yslcs6a28c5vz7jwj63lkfgyx8mx99fdirlhi9lbhhzhrpcyr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-hermit-abi" ,rust-hermit-abi-0.1.19)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-autocfg-1.1.0
  (package
    (name "rust-autocfg")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "autocfg" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ylp3cb47ylzabimazvbz9ms6ap784zhb6syaz6c1jqpmcmq0s6l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-backtrace-0.3.66
  (package
    (name "rust-backtrace")
    (version "0.3.66")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "backtrace" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19yrfx0gprqmzphmf6qv32g93w76ny5g751ks1abdkqnsqcl7f6a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-addr2line" ,rust-addr2line-0.17.0)
                        ("rust-cc" ,rust-cc-1.0.73)
                        ("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-miniz-oxide" ,rust-miniz-oxide-0.5.4)
                        ("rust-object" ,rust-object-0.29.0)
                        ("rust-rustc-demangle" ,rust-rustc-demangle-0.1.21))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-base64-0.13.1
  (package
    (name "rust-base64")
    (version "0.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "base64" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1s494mqmzjb766fy1kqlccgfg2sdcjb6hzbvzqv2jw65fdi5h6wy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-base64-0.21.2
  (package
    (name "rust-base64")
    (version "0.21.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "base64" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gfffgp4jmk517hymckk5jn393dqvw78312papf047y2qpv7hhb0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-beef-0.5.2
  (package
    (name "rust-beef")
    (version "0.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "beef" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1c95lbnhld96iwwbyh5kzykbpysq0fnjfhwxa1mhap5qxgrl30is"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-bit-set-0.5.3
  (package
    (name "rust-bit-set")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bit-set" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1wcm9vxi00ma4rcxkl3pzzjli6ihrpn9cfdi0c5b4cvga2mxs007"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bit-vec" ,rust-bit-vec-0.6.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-bit-vec-0.6.3
  (package
    (name "rust-bit-vec")
    (version "0.6.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bit-vec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ywqjnv60cdh1slhz67psnp422md6jdliji6alq0gmly2xm9p7rl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-bitflags-1.3.2
  (package
    (name "rust-bitflags")
    (version "1.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitflags" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "12ki6w8gn1ldq7yz9y680llwk5gmrhrzszaa17g1sbrw2r2qvwxy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-bitflags-2.4.1
  (package
    (name "rust-bitflags")
    (version "2.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitflags" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01ryy3kd671b0ll4bhdvhsz67vwz1lz53fz504injrd7wpv64xrj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-block-buffer-0.10.3
  (package
    (name "rust-block-buffer")
    (version "0.10.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "block-buffer" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zmy5vjwa6pbrhlgk94jg2pz08w5dd9nw2j7jfwrg3s96w3y5k39"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-generic-array" ,rust-generic-array-0.14.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-bstr-0.2.17
  (package
    (name "rust-bstr")
    (version "0.2.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bstr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08rjbhysy6gg27db2h3pnhvr2mlr5vkj797i9625kwg8hgrnjdds"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1.4.0)
                        ("rust-memchr" ,rust-memchr-2.5.0)
                        ("rust-regex-automata" ,rust-regex-automata-0.1.10)
                        ("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-bumpalo-3.11.1
  (package
    (name "rust-bumpalo")
    (version "3.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bumpalo" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1fl072w8wia496byc2h6ck2159sir2jjrb8niwq8h4916r8njbsp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-bytecount-0.6.3
  (package
    (name "rust-bytecount")
    (version "0.6.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytecount" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "173wsvyagflb7ic3hpvp1db6q3dsigr452inslnzmsb3ix3nlrrc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-bytemuck-1.12.1
  (package
    (name "rust-bytemuck")
    (version "1.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytemuck" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nliallfh3f008ybs9d6ivac2pbvhh3adxdyqa7mk8dmj7j1amrg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-bytes-1.2.1
  (package
    (name "rust-bytes")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytes" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nsni0jbx1048inbrarn3hz6zxd000pp0rac2mr07s7xf1m7p2pc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-cast-0.3.0
  (package
    (name "rust-cast")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cast" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dbyngbyz2qkk0jn2sxil8vrz3rnpcj142y184p9l4nbl9radcip"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-cc-1.0.73
  (package
    (name "rust-cc")
    (version "1.0.73")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04ccylrjq94jssh8f7d7hxv64gs9f1m1jrsxb7wqgfxk4xljmzrg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-cfg-if-1.0.0
  (package
    (name "rust-cfg-if")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cfg-if" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1za0vb97n4brpzpv8lsbnzmq5r8f2b0cpqqr0sy8h5bn751xxwds"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-clap-2.34.0
  (package
    (name "rust-clap")
    (version "2.34.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "071q5d8jfwbazi6zhik9xwpacx5i6kb2vkzy060vhf0c3120aqd0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1.3.2)
                        ("rust-textwrap" ,rust-textwrap-0.11.0)
                        ("rust-unicode-width" ,rust-unicode-width-0.1.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-clap-3.2.23
  (package
    (name "rust-clap")
    (version "3.2.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19bkwkj49ha7mlip0gxsqb9xmd3jpr7ghvcx1hkx6icqrd2mqrbi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-atty" ,rust-atty-0.2.14)
                        ("rust-bitflags" ,rust-bitflags-1.3.2)
                        ("rust-clap-derive" ,rust-clap-derive-3.2.18)
                        ("rust-clap-lex" ,rust-clap-lex-0.2.4)
                        ("rust-indexmap" ,rust-indexmap-1.9.1)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-strsim" ,rust-strsim-0.10.0)
                        ("rust-termcolor" ,rust-termcolor-1.1.3)
                        ("rust-textwrap" ,rust-textwrap-0.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-clap-derive-3.2.18
  (package
    (name "rust-clap-derive")
    (version "3.2.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0r9az0cl33xx0i9g18l56l3vd5ayjvcflvza2gdf8jwcab78n37a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-heck" ,rust-heck-0.4.0)
                        ("rust-proc-macro-error" ,rust-proc-macro-error-1.0.4)
                        ("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-clap-lex-0.2.4
  (package
    (name "rust-clap-lex")
    (version "0.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_lex" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ib1a9v55ybnaws11l63az0jgz5xiy24jkdgsmyl7grcm3sz4l18"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-os-str-bytes" ,rust-os-str-bytes-6.3.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-codespan-reporting-0.11.1
  (package
    (name "rust-codespan-reporting")
    (version "0.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "codespan-reporting" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vkfay0aqk73d33kh79k1kqxx06ka22894xhqi89crnc6c6jff1m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-termcolor" ,rust-termcolor-1.1.3)
                        ("rust-unicode-width" ,rust-unicode-width-0.1.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-core-foundation-0.9.3
  (package
    (name "rust-core-foundation")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core-foundation" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ii1ihpjb30fk38gdikm5wqlkmyr8k46fh4k2r8sagz5dng7ljhr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8.3)
                        ("rust-libc" ,rust-libc-0.2.136))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-core-foundation-sys-0.8.3
  (package
    (name "rust-core-foundation-sys")
    (version "0.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core-foundation-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1p5r2wckarkpkyc4z83q08dwpvcafrb1h6fxfa3qnikh8szww9sq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-countme-3.0.1
  (package
    (name "rust-countme")
    (version "3.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "countme" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0dn62hhvgmwyxslh14r4nlbvz8h50cp5mnn1qhqsw63vs7yva13p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-cpp-demangle-0.3.5
  (package
    (name "rust-cpp-demangle")
    (version "0.3.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp_demangle" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "03vi33qz8x7lll0xd3acd3jp39nvzv174wg424qsb1nkm8z9bapf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-cpufeatures-0.2.5
  (package
    (name "rust-cpufeatures")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpufeatures" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08535izlz4kx8z1kkcp0gy80gqk7k19dqiiysj6r5994bsyrgn98"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-libc" ,rust-libc-0.2.136))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-criterion-0.3.6
  (package
    (name "rust-criterion")
    (version "0.3.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "criterion" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13yd64ah93gkbdv7qq4cr6rhgl9979jjcjk3gkhnav1b7glns7dh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-atty" ,rust-atty-0.2.14)
                        ("rust-cast" ,rust-cast-0.3.0)
                        ("rust-clap" ,rust-clap-2.34.0)
                        ("rust-criterion-plot" ,rust-criterion-plot-0.4.5)
                        ("rust-csv" ,rust-csv-1.1.6)
                        ("rust-itertools" ,rust-itertools-0.10.5)
                        ("rust-lazy-static" ,rust-lazy-static-1.4.0)
                        ("rust-num-traits" ,rust-num-traits-0.2.15)
                        ("rust-oorandom" ,rust-oorandom-11.1.3)
                        ("rust-plotters" ,rust-plotters-0.3.4)
                        ("rust-rayon" ,rust-rayon-1.5.3)
                        ("rust-regex" ,rust-regex-1.6.0)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-cbor" ,rust-serde-cbor-0.11.2)
                        ("rust-serde-derive" ,rust-serde-derive-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-tinytemplate" ,rust-tinytemplate-1.2.1)
                        ("rust-walkdir" ,rust-walkdir-2.3.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-criterion-plot-0.4.5
  (package
    (name "rust-criterion-plot")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "criterion-plot" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xhq0jz1603585h7xvm3s4x9irmifjliklszbzs4cda00y1cqwr6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cast" ,rust-cast-0.3.0)
                        ("rust-itertools" ,rust-itertools-0.10.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-crossbeam-channel-0.5.6
  (package
    (name "rust-crossbeam-channel")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-channel" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08f5f043rljl82a06d1inda6nl2b030s7yfqp31ps8w8mzfh9pf2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.8.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-crossbeam-deque-0.8.2
  (package
    (name "rust-crossbeam-deque")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-deque" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1z6ifz35lyk0mw818xcl3brgss2k8islhgdmfk9s5fwjnr982pki"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-crossbeam-epoch" ,rust-crossbeam-epoch-0.9.11)
                        ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.8.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-crossbeam-epoch-0.9.11
  (package
    (name "rust-crossbeam-epoch")
    (version "0.9.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-epoch" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0j33x0b3bzplrj4bjqi63n2s42kpr6gxpwb5msfyvc2nsg2xy5pr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.8.12)
                        ("rust-memoffset" ,rust-memoffset-0.6.5)
                        ("rust-scopeguard" ,rust-scopeguard-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-crossbeam-utils-0.8.12
  (package
    (name "rust-crossbeam-utils")
    (version "0.8.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-utils" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b0zs5ahnwkgky7svwah9fhmqx645qnb3h97cnk6q68zzb2zxfpd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-crypto-common-0.1.6
  (package
    (name "rust-crypto-common")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crypto-common" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1cvby95a6xg7kxdz5ln3rl9xh66nz66w46mm3g56ri1z5x815yqv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-generic-array" ,rust-generic-array-0.14.6)
                        ("rust-typenum" ,rust-typenum-1.15.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-csv-1.1.6
  (package
    (name "rust-csv")
    (version "1.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "csv" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1q9nqn0qlamwl18v57p82c8yhxy43lkzf2z1mndmycsvqinkm092"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bstr" ,rust-bstr-0.2.17)
                        ("rust-csv-core" ,rust-csv-core-0.1.10)
                        ("rust-itoa" ,rust-itoa-0.4.8)
                        ("rust-ryu" ,rust-ryu-1.0.11)
                        ("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-csv-core-0.1.10
  (package
    (name "rust-csv-core")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "csv-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "145wcc3560v1kmysqqspvddppiysr2rifqzy4nnlh3r6kxanc91b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-memchr" ,rust-memchr-2.5.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-ctrlc-3.2.3
  (package
    (name "rust-ctrlc")
    (version "3.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ctrlc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wr18qgdi56jj8mjn9z6waknfdjqk47z19141kgw33p8pd7rg48x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-nix" ,rust-nix-0.25.0)
                        ("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-debugid-0.7.3
  (package
    (name "rust-debugid")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "debugid" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c370jsmnb0ahssda6f11l72ns1xpqrcmswa6y2zhknq66pqgvnn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-uuid" ,rust-uuid-0.8.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-difference-2.0.0
  (package
    (name "rust-difference")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "difference" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1621wx4k8h452p6xzmzzvm7mz87kxh4yqz0kzxfjj9xmjxlbyk2j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-digest-0.10.5
  (package
    (name "rust-digest")
    (version "0.10.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "digest" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0v7qvhh0apbgagnj2dc1x8pnwxmvd5z4vdpjxg9cnym3cmrwbyxd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-block-buffer" ,rust-block-buffer-0.10.3)
                        ("rust-crypto-common" ,rust-crypto-common-0.1.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-dyn-clone-1.0.9
  (package
    (name "rust-dyn-clone")
    (version "1.0.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dyn-clone" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1cl8dn2qgvlpmhb22llwbg99yhmz86wbf5747645psmfq84zm52g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-either-1.8.0
  (package
    (name "rust-either")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "either" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "15z70yaivlkpx27vzv99ibf8d2x5jp24yn69y0xi20w86v4c3rch"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-encoding-rs-0.8.31
  (package
    (name "rust-encoding-rs")
    (version "0.8.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "encoding_rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0azc6rblf75vd862ymjahdfch27j1sshb7zynshrx7ywi5an6llq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-equivalent-1.0.1
  (package
    (name "rust-equivalent")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "equivalent" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1malmx5f4lkfvqasz319lq6gb3ddg19yzf9s8cykfsgzdmyq0hsl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-fancy-regex-0.11.0
  (package
    (name "rust-fancy-regex")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fancy-regex" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18j0mmzfycibhxhhhfja00dxd1vf8x5c28lbry224574h037qpxr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bit-set" ,rust-bit-set-0.5.3)
                        ("rust-regex" ,rust-regex-1.6.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-fastrand-1.8.0
  (package
    (name "rust-fastrand")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fastrand" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16b4z2rig7zmyxw1hsiydv89h9wcshilds13dfpc919kmb7hg957"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-instant" ,rust-instant-0.1.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-figment-0.10.8
  (package
    (name "rust-figment")
    (version "0.10.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "figment" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1jbcq7y8695bjs61pkcpxrhqg6ssximacrpc1m0028lv8qmn0mjf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-atomic" ,rust-atomic-0.5.1)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-uncased" ,rust-uncased-0.9.7)
                        ("rust-version-check" ,rust-version-check-0.9.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-findshlibs-0.10.2
  (package
    (name "rust-findshlibs")
    (version "0.10.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "findshlibs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0r3zy2r12rxzwqgz53830bk38r6b7rl8kq2br9n81q7ps2ffbfa0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cc" ,rust-cc-1.0.73)
                        ("rust-lazy-static" ,rust-lazy-static-1.4.0)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-fnv-1.0.7
  (package
    (name "rust-fnv")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fnv" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hc2mcqha06aibcaza94vbi81j6pr9a1bbxrxjfhc91zin8yr7iz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-foreign-types-0.3.2
  (package
    (name "rust-foreign-types")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "foreign-types" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1cgk0vyd7r45cj769jym4a6s7vwshvd0z4bqrb92q1fwibmkkwzn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-foreign-types-shared" ,rust-foreign-types-shared-0.1.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-foreign-types-shared-0.1.1
  (package
    (name "rust-foreign-types-shared")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "foreign-types-shared" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0jxgzd04ra4imjv8jgkmdq59kj8fsz6w4zxsbmlai34h26225c00"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-form-urlencoded-1.1.0
  (package
    (name "rust-form-urlencoded")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "form_urlencoded" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1y3bwavygjzv7b0yqsjqk33yi6wz25b7q2aaq9h54vqmc7qq9hx9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-percent-encoding" ,rust-percent-encoding-2.2.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-fraction-0.13.1
  (package
    (name "rust-fraction")
    (version "0.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fraction" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y6nh9qyfidm6hsp85wf1kv7l7nc9anzvj214bnln6ylz0fsw9rh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1.4.0)
                        ("rust-num" ,rust-num-0.4.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-futures-0.3.25
  (package
    (name "rust-futures")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1c0v0lv6fgr95k1nw26n2v9vb40j7k32jg558m8pmhrxfq202f9q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-futures-channel" ,rust-futures-channel-0.3.25)
                        ("rust-futures-core" ,rust-futures-core-0.3.25)
                        ("rust-futures-executor" ,rust-futures-executor-0.3.25)
                        ("rust-futures-io" ,rust-futures-io-0.3.25)
                        ("rust-futures-sink" ,rust-futures-sink-0.3.25)
                        ("rust-futures-task" ,rust-futures-task-0.3.25)
                        ("rust-futures-util" ,rust-futures-util-0.3.25))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-futures-channel-0.3.25
  (package
    (name "rust-futures-channel")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-channel" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vcyyxwdgh92nl277053zvqd3qpzf6jhb5kibgs0aq95j9d2dfjj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-futures-core" ,rust-futures-core-0.3.25)
                        ("rust-futures-sink" ,rust-futures-sink-0.3.25))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-futures-core-0.3.25
  (package
    (name "rust-futures-core")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b6k9fd6bkga9556jyx78di278bdp2p81cls99nawcs6grx9m404"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-futures-executor-0.3.25
  (package
    (name "rust-futures-executor")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-executor" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qkll0s12i4ry48yqh08ikl7n8gyz8in2f6zbsmpdh8lczgqbk3s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-futures-core" ,rust-futures-core-0.3.25)
                        ("rust-futures-task" ,rust-futures-task-0.3.25)
                        ("rust-futures-util" ,rust-futures-util-0.3.25))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-futures-io-0.3.25
  (package
    (name "rust-futures-io")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-io" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1szl4w206x2inliipf5hvjbrd8w8i0gnglz8akmsvp3bl19gpx80"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-futures-macro-0.3.25
  (package
    (name "rust-futures-macro")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-macro" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0pc3c5mydmwy50f0whcljcd41f0z1ci0r65dka8r2syqagh8ryxx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-futures-sink-0.3.25
  (package
    (name "rust-futures-sink")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-sink" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ygwh57nzinpj2sk6akc6sgavl3njsrjyimvy50dyydalkqmrh9r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-futures-task-0.3.25
  (package
    (name "rust-futures-task")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-task" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1sp6k18py8nv3dmy3j00w83bfmk6fzi7mwzxsflym9nrqlx3kyrg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-futures-util-0.3.25
  (package
    (name "rust-futures-util")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-util" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mk5vh8q5bkkvxji8r1nimh87hgi190nz4l4zynrqbxxgac7cxhr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-futures-channel" ,rust-futures-channel-0.3.25)
                        ("rust-futures-core" ,rust-futures-core-0.3.25)
                        ("rust-futures-io" ,rust-futures-io-0.3.25)
                        ("rust-futures-macro" ,rust-futures-macro-0.3.25)
                        ("rust-futures-sink" ,rust-futures-sink-0.3.25)
                        ("rust-futures-task" ,rust-futures-task-0.3.25)
                        ("rust-memchr" ,rust-memchr-2.5.0)
                        ("rust-pin-project-lite" ,rust-pin-project-lite-0.2.9)
                        ("rust-pin-utils" ,rust-pin-utils-0.1.0)
                        ("rust-slab" ,rust-slab-0.4.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-generic-array-0.14.6
  (package
    (name "rust-generic-array")
    (version "0.14.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "generic-array" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1fgi07v268jd0mr6xc42rjbq0wzl8ngsgp5b8wj33wwpfaa9xx5z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-typenum" ,rust-typenum-1.15.0)
                        ("rust-version-check" ,rust-version-check-0.9.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-getrandom-0.2.8
  (package
    (name "rust-getrandom")
    (version "0.2.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "getrandom" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0cbb766pcyi7sws0fnp1pxkz0nhiya0ckallq502bxmq49mfnnn0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-js-sys" ,rust-js-sys-0.3.60)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-wasi" ,rust-wasi-0.11.0+wasi-snapshot-preview1)
                        ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2.83))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-gimli-0.26.2
  (package
    (name "rust-gimli")
    (version "0.26.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gimli" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0pafbk64rznibgnvfidhm1pqxd14a5s9m50yvsgnbv38b8n0w0r2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-glob-0.3.0
  (package
    (name "rust-glob")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "glob" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0x25wfr7vg3mzxc9x05dcphvd3nwlcmbnxrvwcvrrdwplcrrk4cv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-globset-0.4.9
  (package
    (name "rust-globset")
    (version "0.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "globset" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02mx4yhpxfbv74pqfch0asicc868nszazhk4m4hvrv8r4qs1f7ha"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-aho-corasick" ,rust-aho-corasick-0.7.19)
                        ("rust-bstr" ,rust-bstr-0.2.17)
                        ("rust-fnv" ,rust-fnv-1.0.7)
                        ("rust-log" ,rust-log-0.4.17)
                        ("rust-regex" ,rust-regex-1.6.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-h2-0.3.19
  (package
    (name "rust-h2")
    (version "0.3.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "h2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10mpx8d00wnywxy9kc3q0q5l4qirjq5iv1ypyy122zcfk2pcfmyk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bytes" ,rust-bytes-1.2.1)
                        ("rust-fnv" ,rust-fnv-1.0.7)
                        ("rust-futures-core" ,rust-futures-core-0.3.25)
                        ("rust-futures-sink" ,rust-futures-sink-0.3.25)
                        ("rust-futures-util" ,rust-futures-util-0.3.25)
                        ("rust-http" ,rust-http-0.2.8)
                        ("rust-indexmap" ,rust-indexmap-1.9.1)
                        ("rust-slab" ,rust-slab-0.4.7)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-tokio-util" ,rust-tokio-util-0.7.4)
                        ("rust-tracing" ,rust-tracing-0.1.37))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-half-1.8.2
  (package
    (name "rust-half")
    (version "1.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "half" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mqbmx2m9qd4lslkb42fzgldsklhv9c4bxsc8j82r80d8m24mfza"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-hashbrown-0.12.3
  (package
    (name "rust-hashbrown")
    (version "0.12.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashbrown" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1268ka4750pyg2pbgsr43f0289l5zah4arir2k4igx5a8c6fg7la"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-ahash" ,rust-ahash-0.7.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-hashbrown-0.14.3
  (package
    (name "rust-hashbrown")
    (version "0.14.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashbrown" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "012nywlg0lj9kwanh69my5x67vjlfmzfi9a0rq4qvis2j8fil3r9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-heck-0.4.0
  (package
    (name "rust-heck")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "heck" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ygphsnfwl2xpa211vbqkz1db6ri1kvkg8p8sqybi37wclg7fh15"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-hermit-abi-0.1.19
  (package
    (name "rust-hermit-abi")
    (version "0.1.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hermit-abi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0cxcm8093nf5fyn114w8vxbrbcyvv91d4015rdnlgfll7cs6gd32"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-libc" ,rust-libc-0.2.136))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-hex-0.4.3
  (package
    (name "rust-hex")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hex" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0w1a4davm1lgzpamwnba907aysmlrnygbqmfis2mqjx5m552a93z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-http-0.2.8
  (package
    (name "rust-http")
    (version "0.2.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1693pkg43czk26fima0l0l5h2h9rvm8n84pff5zc35b9w90kvx3m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bytes" ,rust-bytes-1.2.1)
                        ("rust-fnv" ,rust-fnv-1.0.7)
                        ("rust-itoa" ,rust-itoa-1.0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-http-body-0.4.5
  (package
    (name "rust-http-body")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http-body" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l967qwwlvhp198xdrnc0p5d7jwfcp6q2lm510j6zqw4s4b8zwym"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bytes" ,rust-bytes-1.2.1)
                        ("rust-http" ,rust-http-0.2.8)
                        ("rust-pin-project-lite" ,rust-pin-project-lite-0.2.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-httparse-1.8.0
  (package
    (name "rust-httparse")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "httparse" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "010rrfahm1jss3p022fqf3j3jmm72vhn4iqhykahb9ynpaag75yq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-httpdate-1.0.2
  (package
    (name "rust-httpdate")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "httpdate" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08bln7b1ibdw26gl8h4dr6rlybvlkyhlha309xbh9ghxh9nf78f4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-hyper-0.14.20
  (package
    (name "rust-hyper")
    (version "0.14.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyper" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b7vm9dzs3hg5a6dk401n4hhg1hqh5r94lj07jh3bqrrbkf2kj82"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bytes" ,rust-bytes-1.2.1)
                        ("rust-futures-channel" ,rust-futures-channel-0.3.25)
                        ("rust-futures-core" ,rust-futures-core-0.3.25)
                        ("rust-futures-util" ,rust-futures-util-0.3.25)
                        ("rust-h2" ,rust-h2-0.3.19)
                        ("rust-http" ,rust-http-0.2.8)
                        ("rust-http-body" ,rust-http-body-0.4.5)
                        ("rust-httparse" ,rust-httparse-1.8.0)
                        ("rust-httpdate" ,rust-httpdate-1.0.2)
                        ("rust-itoa" ,rust-itoa-1.0.4)
                        ("rust-pin-project-lite" ,rust-pin-project-lite-0.2.9)
                        ("rust-socket2" ,rust-socket2-0.4.7)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-tower-service" ,rust-tower-service-0.3.2)
                        ("rust-tracing" ,rust-tracing-0.1.37)
                        ("rust-want" ,rust-want-0.3.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-hyper-rustls-0.23.0
  (package
    (name "rust-hyper-rustls")
    (version "0.23.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyper-rustls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b2wi75qrmwfpw3pqwcg1xjndl4z0aris15296wf7i8d5v04hz6q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-http" ,rust-http-0.2.8)
                        ("rust-hyper" ,rust-hyper-0.14.20)
                        ("rust-rustls" ,rust-rustls-0.20.7)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-tokio-rustls" ,rust-tokio-rustls-0.23.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-hyper-tls-0.5.0
  (package
    (name "rust-hyper-tls")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyper-tls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01crgy13102iagakf6q4mb75dprzr7ps1gj0l5hxm1cvm7gks66n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bytes" ,rust-bytes-1.2.1)
                        ("rust-hyper" ,rust-hyper-0.14.20)
                        ("rust-native-tls" ,rust-native-tls-0.2.10)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-idna-0.3.0
  (package
    (name "rust-idna")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "idna" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1rh9f9jls0jy3g8rh2bfpjhvvhh4q80348jc4jr2s844133xykg1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-unicode-bidi" ,rust-unicode-bidi-0.3.8)
                        ("rust-unicode-normalization" ,rust-unicode-normalization-0.1.22))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-indexmap-1.9.1
  (package
    (name "rust-indexmap")
    (version "1.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "indexmap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "07nli1wcz7m81svvig8l5j6vjycjnv9va46lwblgy803ffbmm8qh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-hashbrown" ,rust-hashbrown-0.12.3)
                        ("rust-rayon" ,rust-rayon-1.5.3)
                        ("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-indexmap-2.1.0
  (package
    (name "rust-indexmap")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "indexmap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "07rxrqmryr1xfnmhrjlz8ic6jw28v6h5cig3ws2c9d0wifhy2c6m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-equivalent" ,rust-equivalent-1.0.1)
                        ("rust-hashbrown" ,rust-hashbrown-0.14.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-inferno-0.11.11
  (package
    (name "rust-inferno")
    (version "0.11.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inferno" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "046wdg428bvyh59r6ri2v7z5rcpgsvybcsgc42x7nwz130384fsz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-ahash" ,rust-ahash-0.7.6)
                        ("rust-atty" ,rust-atty-0.2.14)
                        ("rust-indexmap" ,rust-indexmap-1.9.1)
                        ("rust-itoa" ,rust-itoa-1.0.4)
                        ("rust-log" ,rust-log-0.4.17)
                        ("rust-num-format" ,rust-num-format-0.4.3)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-quick-xml" ,rust-quick-xml-0.23.1)
                        ("rust-rgb" ,rust-rgb-0.8.34)
                        ("rust-str-stack" ,rust-str-stack-0.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-instant-0.1.12
  (package
    (name "rust-instant")
    (version "0.1.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "instant" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0b2bx5qdlwayriidhrag8vhy10kdfimfhmb3jnjmsz2h9j1bwnvs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-ipnet-2.5.0
  (package
    (name "rust-ipnet")
    (version "2.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ipnet" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0asr5bwhbfxgxwappmvs0rvb0ncc5adnhfi9yiz4axlc9j1m97c7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-iso8601-0.6.1
  (package
    (name "rust-iso8601")
    (version "0.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "iso8601" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lqif1zp19fjmrbhcdjx0ydnljax3090san5zq8r1x98x9rmsklj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-nom" ,rust-nom-7.1.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-itertools-0.10.5
  (package
    (name "rust-itertools")
    (version "0.10.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itertools" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ww45h7nxx5kj6z2y6chlskxd1igvs4j507anr6dzg99x1h25zdh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-either" ,rust-either-1.8.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-itoa-0.4.8
  (package
    (name "rust-itoa")
    (version "0.4.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itoa" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1m1dairwyx8kfxi7ab3b5jc71z1vigh9w4shnhiajji9avzr26dp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-itoa-1.0.4
  (package
    (name "rust-itoa")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itoa" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1k2y06z87wjxda4v751ybf7n9q4kwl9ly9jffa78vpxs3qsas5s2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-js-sys-0.3.60
  (package
    (name "rust-js-sys")
    (version "0.3.60")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "js-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0isslargvb1cd5xfk73xrxqni3p2ksharkp22swmc25zwgrrsh29"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2.83))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-json-value-merge-2.0.0
  (package
    (name "rust-json-value-merge")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "json_value_merge" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "059jgdszv17n25114z1bw964jqxwbpdvn8pxwqmzxbdav2nkl2ib"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-serde-json" ,rust-serde-json-1.0.87))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-jsonschema-0.17.1
  (package
    (name "rust-jsonschema")
    (version "0.17.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jsonschema" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y397fmb7qkah166lq5q39p9hizj9sls09xnvwc936pwgr7iy1ra"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-ahash" ,rust-ahash-0.8.3)
                        ("rust-anyhow" ,rust-anyhow-1.0.66)
                        ("rust-base64" ,rust-base64-0.21.2)
                        ("rust-bytecount" ,rust-bytecount-0.6.3)
                        ("rust-fancy-regex" ,rust-fancy-regex-0.11.0)
                        ("rust-fraction" ,rust-fraction-0.13.1)
                        ("rust-getrandom" ,rust-getrandom-0.2.8)
                        ("rust-iso8601" ,rust-iso8601-0.6.1)
                        ("rust-itoa" ,rust-itoa-1.0.4)
                        ("rust-memchr" ,rust-memchr-2.5.0)
                        ("rust-num-cmp" ,rust-num-cmp-0.1.0)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-parking-lot" ,rust-parking-lot-0.12.1)
                        ("rust-percent-encoding" ,rust-percent-encoding-2.2.0)
                        ("rust-regex" ,rust-regex-1.6.0)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-time" ,rust-time-0.3.16)
                        ("rust-url" ,rust-url-2.3.1)
                        ("rust-uuid" ,rust-uuid-1.2.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-lazy-static-1.4.0
  (package
    (name "rust-lazy-static")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lazy_static" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0in6ikhw8mgl33wjv6q6xfrb5b9jr16q8ygjy803fay4zcisvaz2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-libc-0.2.136
  (package
    (name "rust-libc")
    (version "0.2.136")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "15zi66mi5wvgmd3fplz8sl7phqdlkpwjqww4x8nha6dk1dnczvam"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-lock-api-0.4.9
  (package
    (name "rust-lock-api")
    (version "0.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lock_api" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1py41vk243hwk345nhkn5nw0bd4m03gzjmprdjqq6rg5dwv12l23"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-scopeguard" ,rust-scopeguard-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-log-0.4.17
  (package
    (name "rust-log")
    (version "0.4.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "log" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0biqlaaw1lsr8bpnmbcc0fvgjj34yy79ghqzyi0ali7vgil2xcdb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-logos-0.12.1
  (package
    (name "rust-logos")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "logos" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1w82qm3hck5cr6ax3j3yzrpf4zzbffahz126ahyqwyn6h8b072xz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-logos-derive" ,rust-logos-derive-0.12.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-logos-derive-0.12.1
  (package
    (name "rust-logos-derive")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "logos-derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0v295x78vcskab88hshl530w9d1vn61cmlaic4d6dydsila4kn51"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-beef" ,rust-beef-0.5.2)
                        ("rust-fnv" ,rust-fnv-1.0.7)
                        ("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-regex-syntax" ,rust-regex-syntax-0.6.27)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-lru-0.7.8
  (package
    (name "rust-lru")
    (version "0.7.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lru" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0yp4ai5rpr2czxklzxxx98p6l2aqv4g1906j3dr4b0vfgfxbx6g9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-hashbrown" ,rust-hashbrown-0.12.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-lsp-async-stub-0.6.1
  (package
    (name "rust-lsp-async-stub")
    (version "0.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lsp-async-stub" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "00sr38bfzs83vs1v358rw59qqz7jr3rqc9mvrq8fvsv4y7pqv7fk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-anyhow" ,rust-anyhow-1.0.66)
                        ("rust-async-trait" ,rust-async-trait-0.1.58)
                        ("rust-futures" ,rust-futures-0.3.25)
                        ("rust-getrandom" ,rust-getrandom-0.2.8)
                        ("rust-lsp-types" ,rust-lsp-types-0.93.2)
                        ("rust-rowan" ,rust-rowan-0.15.10)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-tracing" ,rust-tracing-0.1.37))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-lsp-types-0.93.2
  (package
    (name "rust-lsp-types")
    (version "0.93.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lsp-types" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lfawrvxq3k6jpblyikfvzh1759zf3zpl39pfhcnb3yiwb3ykrlv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1.3.2)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-serde-repr" ,rust-serde-repr-0.1.9)
                        ("rust-url" ,rust-url-2.3.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-matchers-0.1.0
  (package
    (name "rust-matchers")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "matchers" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0n2mbk7lg2vf962c8xwzdq96yrc9i0p8dbmm4wa1nnkcp1dhfqw2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-regex-automata" ,rust-regex-automata-0.1.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-memchr-2.5.0
  (package
    (name "rust-memchr")
    (version "2.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memchr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vanfk5mzs1g1syqnj03q8n0syggnhn55dq535h2wxr7rwpfbzrd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-memmap2-0.5.7
  (package
    (name "rust-memmap2")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memmap2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "163lwxgmjdsnjdxjfd8vxj66ydxwp07himpar3pz4ymi8pribbwm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-libc" ,rust-libc-0.2.136))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-memoffset-0.6.5
  (package
    (name "rust-memoffset")
    (version "0.6.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memoffset" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kkrzll58a3ayn5zdyy9i1f1v3mx0xgl29x0chq614zazba638ss"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-mime-0.3.16
  (package
    (name "rust-mime")
    (version "0.3.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mime" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13dcm9lh01hdwfjcg74ppljyjfj1c6w3a3cwkhxf0w8wa37cfq1a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-minimal-lexical-0.2.1
  (package
    (name "rust-minimal-lexical")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "minimal-lexical" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16ppc5g84aijpri4jzv14rvcnslvlpphbszc7zzp6vfkddf4qdb8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-miniz-oxide-0.5.4
  (package
    (name "rust-miniz-oxide")
    (version "0.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "miniz_oxide" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0d2xcypr8s0skd81dhlrylas1j794qyz74snm11jc8kmy6l0nncn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-adler" ,rust-adler-1.0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-mio-0.8.5
  (package
    (name "rust-mio")
    (version "0.8.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pjqn6jvmqkgyykf2z5danqka1rfs3il7w4d0qin8yi062y35mz5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-libc" ,rust-libc-0.2.136)
                        ("rust-log" ,rust-log-0.4.17)
                        ("rust-wasi" ,rust-wasi-0.11.0+wasi-snapshot-preview1)
                        ("rust-windows-sys" ,rust-windows-sys-0.42.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-native-tls-0.2.10
  (package
    (name "rust-native-tls")
    (version "0.2.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "native-tls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ad4dhkbc3r9rbqdym1cl5zwkqzfa9i8bs0p1c79hzsm30v2yzpx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1.4.0)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-log" ,rust-log-0.4.17)
                        ("rust-openssl" ,rust-openssl-0.10.60)
                        ("rust-openssl-probe" ,rust-openssl-probe-0.1.5)
                        ("rust-openssl-sys" ,rust-openssl-sys-0.9.96)
                        ("rust-schannel" ,rust-schannel-0.1.20)
                        ("rust-security-framework" ,rust-security-framework-2.7.0)
                        ("rust-security-framework-sys" ,rust-security-framework-sys-2.6.1)
                        ("rust-tempfile" ,rust-tempfile-3.3.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-nix-0.24.2
  (package
    (name "rust-nix")
    (version "0.24.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nix" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1z35n1bhzslr7zawy2c0fl90jjy9l5b3lnsidls3908vfk0xnp0r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1.3.2)
                        ("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-libc" ,rust-libc-0.2.136))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-nix-0.25.0
  (package
    (name "rust-nix")
    (version "0.25.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nix" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1frasbh0fps2fawrss5dl8ps74387skcidm7zhkw6h1lkr5c08p3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-bitflags" ,rust-bitflags-1.3.2)
                        ("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-libc" ,rust-libc-0.2.136))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-nom-7.1.1
  (package
    (name "rust-nom")
    (version "7.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nom" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0djc3lq5xihnwhrvkc4bj0fd58sjf632yh6hfiw545x355d3x458"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-memchr" ,rust-memchr-2.5.0)
                        ("rust-minimal-lexical" ,rust-minimal-lexical-0.2.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-nu-ansi-term-0.46.0
  (package
    (name "rust-nu-ansi-term")
    (version "0.46.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nu-ansi-term" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "115sywxh53p190lyw97alm14nc004qj5jm5lvdj608z84rbida3p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-overload" ,rust-overload-0.1.1)
                        ("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-0.4.0
  (package
    (name "rust-num")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01j6k8kjad0a96297j3qjhdhrc6cgmzhf52i0sd7yd0d2z8ndns3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-num-bigint" ,rust-num-bigint-0.4.3)
                        ("rust-num-complex" ,rust-num-complex-0.4.2)
                        ("rust-num-integer" ,rust-num-integer-0.1.45)
                        ("rust-num-iter" ,rust-num-iter-0.1.43)
                        ("rust-num-rational" ,rust-num-rational-0.4.1)
                        ("rust-num-traits" ,rust-num-traits-0.2.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-bigint-0.4.3
  (package
    (name "rust-num-bigint")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-bigint" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0py73wsa5j4izhd39nkqzqv260r0ma08vy30ky54ld3vkhlbcfpr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-num-integer" ,rust-num-integer-0.1.45)
                        ("rust-num-traits" ,rust-num-traits-0.2.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-cmp-0.1.0
  (package
    (name "rust-num-cmp")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-cmp" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1alavi36shn32b3cwbmkncj1wal3y3cwzkm21bxy5yil5hp5ncv3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-complex-0.4.2
  (package
    (name "rust-num-complex")
    (version "0.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-complex" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06ac87jsgdh845q4czdj5bypll83by9aj9y781zvspxwr1497qvs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-num-traits" ,rust-num-traits-0.2.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-format-0.4.3
  (package
    (name "rust-num-format")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-format" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fzzrckk4280d118ph2cjw7yv9vn6qc8pjaqj04cz47nipzn5f2l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7.2)
                        ("rust-itoa" ,rust-itoa-1.0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-integer-0.1.45
  (package
    (name "rust-num-integer")
    (version "0.1.45")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-integer" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ncwavvwdmsqzxnn65phv6c6nn72pnv9xhpmjd6a429mzf4k6p92"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-num-traits" ,rust-num-traits-0.2.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-iter-0.1.43
  (package
    (name "rust-num-iter")
    (version "0.1.43")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-iter" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lp22isvzmmnidbq9n5kbdh8gj0zm3yhxv1ddsn5rp65530fc0vx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-num-integer" ,rust-num-integer-0.1.45)
                        ("rust-num-traits" ,rust-num-traits-0.2.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-rational-0.4.1
  (package
    (name "rust-num-rational")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-rational" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1c0rb8x4avxy3jvvzv764yk7afipzxncfnqlb10r3h53s34s2f06"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-num-bigint" ,rust-num-bigint-0.4.3)
                        ("rust-num-integer" ,rust-num-integer-0.1.45)
                        ("rust-num-traits" ,rust-num-traits-0.2.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-traits-0.2.15
  (package
    (name "rust-num-traits")
    (version "0.2.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-traits" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kfdqqw2ndz0wx2j75v9nbjx7d3mh3150zs4p5595y02rwsdx3jp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-cpus-1.13.1
  (package
    (name "rust-num-cpus")
    (version "1.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_cpus" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18apx62z4j4lajj2fi6r1i8slr9rs2d0xrbj2ls85qfyxck4brhr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-hermit-abi" ,rust-hermit-abi-0.1.19)
                        ("rust-libc" ,rust-libc-0.2.136))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-num-threads-0.1.6
  (package
    (name "rust-num-threads")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_threads" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0i5vmffsv6g79z869flp1sja69g1gapddjagdw1k3q9f3l2cw698"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-libc" ,rust-libc-0.2.136))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-object-0.29.0
  (package
    (name "rust-object")
    (version "0.29.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "object" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lzblxwxcih7j4z2cfx9094caax97hlfm9n0y5hlavda6cn8n591"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-memchr" ,rust-memchr-2.5.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-once-cell-1.18.0
  (package
    (name "rust-once-cell")
    (version "1.18.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "once_cell" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vapcd5ambwck95wyz3ymlim35jirgnqn9a0qmi19msymv95v2yx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-oorandom-11.1.3
  (package
    (name "rust-oorandom")
    (version "11.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "oorandom" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xdm4vd89aiwnrk1xjwzklnchjqvib4klcihlc2bsd4x50mbrc8a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-openssl-0.10.60
  (package
    (name "rust-openssl")
    (version "0.10.60")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0068ywx6whs572jb9ny9dzgni853kyb5mz52ybwgfn5ilb1wd93r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2.4.1)
                        ("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-foreign-types" ,rust-foreign-types-0.3.2)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-openssl-macros" ,rust-openssl-macros-0.1.0)
                        ("rust-openssl-sys" ,rust-openssl-sys-0.9.96))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-openssl-macros-0.1.0
  (package
    (name "rust-openssl-macros")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl-macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0v3kgnzbadrf9c06q4cqmbjas53av73n5w7wwz3n0nb6257y80dm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-openssl-probe-0.1.5
  (package
    (name "rust-openssl-probe")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl-probe" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kq18qm48rvkwgcggfkqq6pm948190czqc94d6bm2sir5hq1l0gz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-openssl-sys-0.9.96
  (package
    (name "rust-openssl-sys")
    (version "0.9.96")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13w336l4am9mw2krfa3padasd299sjqvq4ncfxb8pnk0p9qw04iq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cc" ,rust-cc-1.0.73)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-pkg-config" ,rust-pkg-config-0.3.25)
                        ("rust-vcpkg" ,rust-vcpkg-0.2.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-os-str-bytes-6.3.0
  (package
    (name "rust-os-str-bytes")
    (version "6.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "os_str_bytes" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zqfx1ajwmv3g9kp95v18zwpjm2fkq6rxpsib0ig3zz3k9g43xwz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-overload-0.1.1
  (package
    (name "rust-overload")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "overload" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fdgbaqwknillagy1xq7xfgv60qdbk010diwl7s1p0qx7hb16n5i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-parking-lot-0.12.1
  (package
    (name "rust-parking-lot")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13r2xk7mnxfc5g0g6dkdxqdqad99j7s7z8zhzz4npw5r0g0v4hip"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-lock-api" ,rust-lock-api-0.4.9)
                        ("rust-parking-lot-core" ,rust-parking-lot-core-0.9.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-parking-lot-core-0.9.4
  (package
    (name "rust-parking-lot-core")
    (version "0.9.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot_core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1h5y9qbsyismsbk85b8dvzqc7i9h7n6z7bs3j786j76w5bff1jad"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-redox-syscall" ,rust-redox-syscall-0.2.16)
                        ("rust-smallvec" ,rust-smallvec-1.10.0)
                        ("rust-windows-sys" ,rust-windows-sys-0.42.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-percent-encoding-2.2.0
  (package
    (name "rust-percent-encoding")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "percent-encoding" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13nrpp6r1f4k14viksga3094krcrxgv4b42kqbriy63k7ln5g327"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-pin-project-lite-0.2.9
  (package
    (name "rust-pin-project-lite")
    (version "0.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pin-project-lite" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05n1z851l356hpgqadw4ar64mjanaxq1qlwqsf2k05ziq8xax9z0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-pin-utils-0.1.0
  (package
    (name "rust-pin-utils")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pin-utils" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "117ir7vslsl2z1a7qzhws4pd01cg2d3338c47swjyvqv2n60v1wb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-pkg-config-0.3.25
  (package
    (name "rust-pkg-config")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pkg-config" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1bh3vij79cshj884py4can1f8rvk52niaii1vwxya9q69gnc9y0x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-plotters-0.3.4
  (package
    (name "rust-plotters")
    (version "0.3.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "plotters" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "15xzbxnjcfsaf8lac846lgi4xmn9k18m9k8gqm35aaa2wqwvcf15"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-num-traits" ,rust-num-traits-0.2.15)
                        ("rust-plotters-backend" ,rust-plotters-backend-0.3.4)
                        ("rust-plotters-svg" ,rust-plotters-svg-0.3.3)
                        ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2.83)
                        ("rust-web-sys" ,rust-web-sys-0.3.60))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-plotters-backend-0.3.4
  (package
    (name "rust-plotters-backend")
    (version "0.3.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "plotters-backend" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hl1x8dqrzsjw1vabyw48gzp7g6z8rlyjqjc4b0wvzl1cdhjhchr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-plotters-svg-0.3.3
  (package
    (name "rust-plotters-svg")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "plotters-svg" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vx5wmm5mxip3fm4l67l3wcvv3jwph4c70zpd3kdmqdab4kiva7r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-plotters-backend" ,rust-plotters-backend-0.3.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-pprof-0.9.1
  (package
    (name "rust-pprof")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pprof" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0p93ipri1a614j68vppq43x1f19spdl9zafyrvwbi9naxb04jwd9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.3.66)
                        ("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-criterion" ,rust-criterion-0.3.6)
                        ("rust-findshlibs" ,rust-findshlibs-0.10.2)
                        ("rust-inferno" ,rust-inferno-0.11.11)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-log" ,rust-log-0.4.17)
                        ("rust-nix" ,rust-nix-0.24.2)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-parking-lot" ,rust-parking-lot-0.12.1)
                        ("rust-smallvec" ,rust-smallvec-1.10.0)
                        ("rust-symbolic-demangle" ,rust-symbolic-demangle-8.8.0)
                        ("rust-tempfile" ,rust-tempfile-3.3.0)
                        ("rust-thiserror" ,rust-thiserror-1.0.37))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-prettydiff-0.6.1
  (package
    (name "rust-prettydiff")
    (version "0.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "prettydiff" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1752bsq5wsax4p19z76s835pqwjqwzxq42w26ihd8dqn1wcpcq8b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-ansi-term" ,rust-ansi-term-0.12.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-proc-macro-error-1.0.4
  (package
    (name "rust-proc-macro-error")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-error" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1373bhxaf0pagd8zkyd03kkx6bchzf6g0dkwrwzsnal9z47lj9fs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro-error-attr" ,rust-proc-macro-error-attr-1.0.4)
                        ("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103)
                        ("rust-version-check" ,rust-version-check-0.9.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-proc-macro-error-attr-1.0.4
  (package
    (name "rust-proc-macro-error-attr")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-error-attr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0sgq6m5jfmasmwwy8x4mjygx5l7kp8s4j60bv25ckv2j1qc41gm1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-version-check" ,rust-version-check-0.9.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-proc-macro2-1.0.66
  (package
    (name "rust-proc-macro2")
    (version "1.0.66")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ngawak3lh5p63k5x2wk37qy65q1yylk1phwhbmb5pcv7zdk3yqq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-unicode-ident" ,rust-unicode-ident-1.0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-quick-xml-0.23.1
  (package
    (name "rust-quick-xml")
    (version "0.23.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quick-xml" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1slry2g2wrj38fnzj9ybzq9wjyknrfg25x5vzfpzn5b8kj2zrfhi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-memchr" ,rust-memchr-2.5.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-quote-1.0.21
  (package
    (name "rust-quote")
    (version "1.0.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quote" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0yai5cyd9h95n7hkwjcx8ig3yv0hindmz5gm60g9dmm7fzrlir5v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-rayon-1.5.3
  (package
    (name "rust-rayon")
    (version "1.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rayon" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0z9sjcy1hnnvgkwx3cn1x44pf24jpwarp3172m9am2xd5rvyb6dx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-crossbeam-deque" ,rust-crossbeam-deque-0.8.2)
                        ("rust-either" ,rust-either-1.8.0)
                        ("rust-rayon-core" ,rust-rayon-core-1.9.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-rayon-core-1.9.3
  (package
    (name "rust-rayon-core")
    (version "1.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rayon-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gv8k6612gc24kqqm4440f5qfx6gnyv2v6dj3d4libbdmjswv2r5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5.6)
                        ("rust-crossbeam-deque" ,rust-crossbeam-deque-0.8.2)
                        ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.8.12)
                        ("rust-num-cpus" ,rust-num-cpus-1.13.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-redox-syscall-0.2.16
  (package
    (name "rust-redox-syscall")
    (version "0.2.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16jicm96kjyzm802cxdd1k9jmcph0db1a4lhslcnhjsvhp0mhnpv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1.3.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-regex-1.6.0
  (package
    (name "rust-regex")
    (version "1.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "12wqvyh4i75j7pc8sgvmqh4yy3qaj4inc4alyv1cdf3lf4kb6kjc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-aho-corasick" ,rust-aho-corasick-0.7.19)
                        ("rust-memchr" ,rust-memchr-2.5.0)
                        ("rust-regex-syntax" ,rust-regex-syntax-0.6.27))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-regex-automata-0.1.10
  (package
    (name "rust-regex-automata")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-automata" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ci1hvbzhrfby5fdpf4ganhf7kla58acad9i1ff1p34dzdrhs8vc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-regex-syntax" ,rust-regex-syntax-0.6.27))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-regex-syntax-0.6.27
  (package
    (name "rust-regex-syntax")
    (version "0.6.27")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-syntax" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0i32nnvyzzkvz1rqp2qyfxrp2170859z8ck37jd63c8irrrppy53"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-remove-dir-all-0.5.3
  (package
    (name "rust-remove-dir-all")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "remove_dir_all" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1rzqbsgkmr053bxxl04vmvsd1njyz0nxvly97aip6aa2cmb15k9s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-reqwest-0.11.12
  (package
    (name "rust-reqwest")
    (version "0.11.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "reqwest" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1z29a4p9zcpsrivrakyxraswsblx3mnsbjjwc03sxqplhk1lj6a3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-base64" ,rust-base64-0.13.1)
                        ("rust-bytes" ,rust-bytes-1.2.1)
                        ("rust-encoding-rs" ,rust-encoding-rs-0.8.31)
                        ("rust-futures-core" ,rust-futures-core-0.3.25)
                        ("rust-futures-util" ,rust-futures-util-0.3.25)
                        ("rust-h2" ,rust-h2-0.3.19)
                        ("rust-http" ,rust-http-0.2.8)
                        ("rust-http-body" ,rust-http-body-0.4.5)
                        ("rust-hyper" ,rust-hyper-0.14.20)
                        ("rust-hyper-rustls" ,rust-hyper-rustls-0.23.0)
                        ("rust-hyper-tls" ,rust-hyper-tls-0.5.0)
                        ("rust-ipnet" ,rust-ipnet-2.5.0)
                        ("rust-js-sys" ,rust-js-sys-0.3.60)
                        ("rust-log" ,rust-log-0.4.17)
                        ("rust-mime" ,rust-mime-0.3.16)
                        ("rust-native-tls" ,rust-native-tls-0.2.10)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-percent-encoding" ,rust-percent-encoding-2.2.0)
                        ("rust-pin-project-lite" ,rust-pin-project-lite-0.2.9)
                        ("rust-rustls" ,rust-rustls-0.20.7)
                        ("rust-rustls-pemfile" ,rust-rustls-pemfile-1.0.1)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-serde-urlencoded" ,rust-serde-urlencoded-0.7.1)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3.0)
                        ("rust-tokio-rustls" ,rust-tokio-rustls-0.23.4)
                        ("rust-tower-service" ,rust-tower-service-0.3.2)
                        ("rust-url" ,rust-url-2.3.1)
                        ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2.83)
                        ("rust-wasm-bindgen-futures" ,rust-wasm-bindgen-futures-0.4.33)
                        ("rust-web-sys" ,rust-web-sys-0.3.60)
                        ("rust-webpki-roots" ,rust-webpki-roots-0.22.5)
                        ("rust-winreg" ,rust-winreg-0.10.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-rgb-0.8.34
  (package
    (name "rust-rgb")
    (version "0.8.34")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rgb" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1cwdif1zh6nbk3v5a8nk1gkfsn4s1qid21jskgvl89m83kbvf0rn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bytemuck" ,rust-bytemuck-1.12.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-ring-0.16.20
  (package
    (name "rust-ring")
    (version "0.16.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ring" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1z682xp7v38ayq9g9nkbhhfpj6ygralmlx7wdmsfv8rnw99cylrh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cc" ,rust-cc-1.0.73)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-spin" ,rust-spin-0.5.2)
                        ("rust-untrusted" ,rust-untrusted-0.7.1)
                        ("rust-web-sys" ,rust-web-sys-0.3.60)
                        ("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-rowan-0.15.10
  (package
    (name "rust-rowan")
    (version "0.15.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rowan" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0cpkf6m93kkwhhy5459x3w80mms01nqym34cwhzr07m3gdz584aq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-countme" ,rust-countme-3.0.1)
                        ("rust-hashbrown" ,rust-hashbrown-0.12.3)
                        ("rust-memoffset" ,rust-memoffset-0.6.5)
                        ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
                        ("rust-text-size" ,rust-text-size-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-rustc-demangle-0.1.21
  (package
    (name "rust-rustc-demangle")
    (version "0.1.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc-demangle" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hn3xyd2n3bg3jnc5a5jbzll32n4r5a65bqzs287l30m5c53xw3y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-rustc-hash-1.1.0
  (package
    (name "rust-rustc-hash")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc-hash" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qkc5khrmv5pqi5l5ca9p5nl5hs742cagrndhbrlk3dhlrx3zm08"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-rustls-0.20.7
  (package
    (name "rust-rustls")
    (version "0.20.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0b4cwi00llwrgzpq4prpdlbz5751nqgbsxiqjgx1niwgj3z2p6jk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-log" ,rust-log-0.4.17)
                        ("rust-ring" ,rust-ring-0.16.20)
                        ("rust-sct" ,rust-sct-0.7.0)
                        ("rust-webpki" ,rust-webpki-0.22.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-rustls-pemfile-1.0.1
  (package
    (name "rust-rustls-pemfile")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustls-pemfile" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0mdxhxp73vxh5pqk5nx2xdxg1z1xkn1yzrc6inh5mh7qagzswr08"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-base64" ,rust-base64-0.13.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-ryu-1.0.11
  (package
    (name "rust-ryu")
    (version "1.0.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ryu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02czvxrxhi2gmamw25drdvkfkkk9xd9758bpnk0s30mfyggsn0a5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-same-file-1.0.6
  (package
    (name "rust-same-file")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "same-file" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "00h5j1w87dmhnvbv9l8bic3y7xxsnjmssvifw2ayvgx9mb1ivz4k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-winapi-util" ,rust-winapi-util-0.1.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-schannel-0.1.20
  (package
    (name "rust-schannel")
    (version "0.1.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "schannel" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qnvajc4wg0gzfj4mmg4a9fd45nps5gyvcj4j9fs4bj68q8p7ml8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1.4.0)
                        ("rust-windows-sys" ,rust-windows-sys-0.36.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-schemars-0.8.11
  (package
    (name "rust-schemars")
    (version "0.8.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "schemars" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01xkqpp4hxssypjvpv5v58hi6hv99kcj7sf8dl127rr93z3bcpra"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-dyn-clone" ,rust-dyn-clone-1.0.9)
                        ("rust-schemars-derive" ,rust-schemars-derive-0.8.11)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-url" ,rust-url-2.3.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-schemars-derive-0.8.11
  (package
    (name "rust-schemars-derive")
    (version "0.8.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "schemars_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1saq013z864siaqw52g2nx14ldknxj1drf7k62a1albljwvd127i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-serde-derive-internals" ,rust-serde-derive-internals-0.26.0)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-scopeguard-1.1.0
  (package
    (name "rust-scopeguard")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "scopeguard" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kbqm85v43rq92vx7hfiay6pmcga03vrjbbfwqpyj3pwsg3b16nj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-sct-0.7.0
  (package
    (name "rust-sct")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sct" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "193w3dg2pcn7138ab4c586pl76nkryn4h6wqlwvqj5gqr6vwsgfm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-ring" ,rust-ring-0.16.20)
                        ("rust-untrusted" ,rust-untrusted-0.7.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-security-framework-2.7.0
  (package
    (name "rust-security-framework")
    (version "2.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "security-framework" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0v1m0vchbibfr1l0pqiyscp0y7h7f7vkjmy52cc67xjah2bvph9b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1.3.2)
                        ("rust-core-foundation" ,rust-core-foundation-0.9.3)
                        ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8.3)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-security-framework-sys" ,rust-security-framework-sys-2.6.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-security-framework-sys-2.6.1
  (package
    (name "rust-security-framework-sys")
    (version "2.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "security-framework-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0mn5lm0jip9nm6ydqm6qd9alyiwq15c027777jsbyibs2wxa2q01"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8.3)
                        ("rust-libc" ,rust-libc-0.2.136))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-semver-1.0.14
  (package
    (name "rust-semver")
    (version "1.0.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "semver" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1i193dd6xkhh2fi1x7rws9pvv2ff3jfl9qjvvd9y6y6pcg2glpg2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-serde-1.0.147
  (package
    (name "rust-serde")
    (version "1.0.147")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0rc9jj8bbhf3lkf07ln8kyljigyzc4kk90nzg4dc2gwqmsdxd4yi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-serde-derive" ,rust-serde-derive-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-serde-cbor-0.11.2
  (package
    (name "rust-serde-cbor")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_cbor" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xf1bq7ixha30914pd5jl3yw9v1x6car7xgrpimvfvs5vszjxvrb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-half" ,rust-half-1.8.2)
                        ("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-serde-derive-1.0.147
  (package
    (name "rust-serde-derive")
    (version "1.0.147")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ln8rqbybpxmk4fvh6lgm75acs1d8x90fi44fhx3x77wm0n3c7ag"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-serde-derive-internals-0.26.0
  (package
    (name "rust-serde-derive-internals")
    (version "0.26.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_derive_internals" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0g2zdr6s8i0r29yy7pdl6ahimq8w6ck70hvrciiry2ljwwlq5gw5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-serde-json-1.0.87
  (package
    (name "rust-serde-json")
    (version "1.0.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_json" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ibxrq43axvspv350wvx7w05l4s7b1gvaa0dysf6pmshn6vpgrvc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-indexmap" ,rust-indexmap-1.9.1)
                        ("rust-itoa" ,rust-itoa-1.0.4)
                        ("rust-ryu" ,rust-ryu-1.0.11)
                        ("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-serde-repr-0.1.9
  (package
    (name "rust-serde-repr")
    (version "0.1.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_repr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1jm7i6p9xba9msw6c4cz6z8zlqplwi3jlznbqyr5xgqfpfgrvqqz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-serde-spanned-0.6.5
  (package
    (name "rust-serde-spanned")
    (version "0.6.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_spanned" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hgh6s3jjwyzhfk3xwb6pnnr1misq9nflwq0f026jafi37s24dpb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-serde-urlencoded-0.7.1
  (package
    (name "rust-serde-urlencoded")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_urlencoded" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zgklbdaysj3230xivihs30qi5vkhigg323a9m62k8jwf4a1qjfk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-form-urlencoded" ,rust-form-urlencoded-1.1.0)
                        ("rust-itoa" ,rust-itoa-1.0.4)
                        ("rust-ryu" ,rust-ryu-1.0.11)
                        ("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-sha1-0.10.5
  (package
    (name "rust-sha1")
    (version "0.10.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sha1" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18zb80sxn31kxdpl1ly6w17hkrvyf08zbxnpy8ckb6f3h3f96hph"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-cpufeatures" ,rust-cpufeatures-0.2.5)
                        ("rust-digest" ,rust-digest-0.10.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-sharded-slab-0.1.4
  (package
    (name "rust-sharded-slab")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sharded-slab" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0cbb8kgwsyr3zzhsv8jrs3y1j3vsw4jxil42lfq31ikhdy0bl3wh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1.4.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-slab-0.4.7
  (package
    (name "rust-slab")
    (version "0.4.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slab" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vyw3rkdfdfkzfa1mh83s237sll8v5kazfwxma60bq4b59msf526"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-smallvec-1.10.0
  (package
    (name "rust-smallvec")
    (version "1.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smallvec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1q2k15fzxgwjpcdv3f323w24rbbfyv711ayz85ila12lg7zbw1x5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-socket2-0.4.7
  (package
    (name "rust-socket2")
    (version "0.4.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "socket2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gaf57dc16s1lfyv388w9vdl9qay15xds78jcwakml9kj3dx5qh2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-libc" ,rust-libc-0.2.136)
                        ("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-spin-0.5.2
  (package
    (name "rust-spin")
    (version "0.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "spin" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0b84m6dbzrwf2kxylnw82d3dr8w06av7rfkr8s85fb5f43rwyqvf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-stable-deref-trait-1.2.0
  (package
    (name "rust-stable-deref-trait")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stable_deref_trait" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1lxjr8q2n534b2lhkxd6l6wcddzjvnksi58zv11f9y0jjmr15wd8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-str-stack-0.1.0
  (package
    (name "rust-str-stack")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "str_stack" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1sxl8xd8kiaffsryqpfwcb02lnd3djfin7gf38ag5980908vd4ch"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-strsim-0.10.0
  (package
    (name "rust-strsim")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strsim" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08s69r4rcrahwnickvi0kq49z524ci50capybln83mg6b473qivk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-symbolic-common-8.8.0
  (package
    (name "rust-symbolic-common")
    (version "8.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "symbolic-common" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0h7mcqwrhi0n6qb1h1vfz5m94dqhlqhr0spfk81mhbk4sl1gjlgm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-debugid" ,rust-debugid-0.7.3)
                        ("rust-memmap2" ,rust-memmap2-0.5.7)
                        ("rust-stable-deref-trait" ,rust-stable-deref-trait-1.2.0)
                        ("rust-uuid" ,rust-uuid-0.8.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-symbolic-demangle-8.8.0
  (package
    (name "rust-symbolic-demangle")
    (version "8.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "symbolic-demangle" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0l1p8j37n7xkg8b6fgipqkcvbxl0w0kcxfwbm82l3cbf9rxwlr25"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cpp-demangle" ,rust-cpp-demangle-0.3.5)
                        ("rust-rustc-demangle" ,rust-rustc-demangle-0.1.21)
                        ("rust-symbolic-common" ,rust-symbolic-common-8.8.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-syn-1.0.103
  (package
    (name "rust-syn")
    (version "1.0.103")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syn" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0pa4b6g938drphblgdhmjnzclp7gcbf4zdgkmfaxlfhk54i08r58"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-unicode-ident" ,rust-unicode-ident-1.0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tap-1.0.1
  (package
    (name "rust-tap")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0sc3gl4nldqpvyhqi3bbd0l9k7fngrcl4zs47n314nqqk4bpx4sm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-taplo-0.13.0
  (package
    (name "rust-taplo")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "taplo" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hkrg76l62lgxa6n6i6h9b5qvv8dyw9si371lng40r1i7007bbsj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-ahash" ,rust-ahash-0.8.3)
                        ("rust-arc-swap" ,rust-arc-swap-1.5.1)
                        ("rust-assert-json-diff" ,rust-assert-json-diff-2.0.2)
                        ("rust-criterion" ,rust-criterion-0.3.6)
                        ("rust-difference" ,rust-difference-2.0.0)
                        ("rust-either" ,rust-either-1.8.0)
                        ("rust-globset" ,rust-globset-0.4.9)
                        ("rust-itertools" ,rust-itertools-0.10.5)
                        ("rust-logos" ,rust-logos-0.12.1)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-pprof" ,rust-pprof-0.9.1)
                        ("rust-rowan" ,rust-rowan-0.15.10)
                        ("rust-schemars" ,rust-schemars-0.8.11)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-thiserror" ,rust-thiserror-1.0.37)
                        ("rust-time" ,rust-time-0.3.16)
                        ("rust-toml" ,rust-toml-0.7.8)
                        ("rust-tracing" ,rust-tracing-0.1.37))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))


(define rust-taplo-common-0.5.0
  (package
    (name "rust-taplo-common")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "taplo-common" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nzav0lbjmxv38r0q437i77hfz7bc9myz5ggnp8905c165mwzlxa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-ahash" ,rust-ahash-0.8.3)
                        ("rust-anyhow" ,rust-anyhow-1.0.66)
                        ("rust-arc-swap" ,rust-arc-swap-1.5.1)
                        ("rust-async-recursion" ,rust-async-recursion-1.0.0)
                        ("rust-async-trait" ,rust-async-trait-0.1.58)
                        ("rust-atty" ,rust-atty-0.2.14)
                        ("rust-futures" ,rust-futures-0.3.25)
                        ("rust-glob" ,rust-glob-0.3.0)
                        ("rust-globset" ,rust-globset-0.4.9)
                        ("rust-hex" ,rust-hex-0.4.3)
                        ("rust-indexmap" ,rust-indexmap-1.9.1)
                        ("rust-itertools" ,rust-itertools-0.10.5)
                        ("rust-json-value-merge" ,rust-json-value-merge-2.0.0)
                        ("rust-jsonschema" ,rust-jsonschema-0.17.1)
                        ("rust-lru" ,rust-lru-0.7.8)
                        ("rust-parking-lot" ,rust-parking-lot-0.12.1)
                        ("rust-percent-encoding" ,rust-percent-encoding-2.2.0)
                        ("rust-regex" ,rust-regex-1.6.0)
                        ("rust-reqwest" ,rust-reqwest-0.11.12)
                        ("rust-schemars" ,rust-schemars-0.8.11)
                        ("rust-semver" ,rust-semver-1.0.14)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-sha1" ,rust-sha1-0.10.5)
                        ("rust-tap" ,rust-tap-1.0.1)
                        ("rust-taplo" ,rust-taplo-0.13.0)
                        ("rust-thiserror" ,rust-thiserror-1.0.37)
                        ("rust-time" ,rust-time-0.3.16)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-tracing" ,rust-tracing-0.1.37)
                        ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3.16)
                        ("rust-url" ,rust-url-2.3.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-taplo-lsp-0.7.0
  (package
    (name "rust-taplo-lsp")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "taplo-lsp" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "003p597ppyfs6i4dkgqd8jbyw368rg65sw0jwnpp2lhhvklzxs6f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-anyhow" ,rust-anyhow-1.0.66)
                        ("rust-arc-swap" ,rust-arc-swap-1.5.1)
                        ("rust-either" ,rust-either-1.8.0)
                        ("rust-figment" ,rust-figment-0.10.8)
                        ("rust-futures" ,rust-futures-0.3.25)
                        ("rust-indexmap" ,rust-indexmap-1.9.1)
                        ("rust-itertools" ,rust-itertools-0.10.5)
                        ("rust-lsp-async-stub" ,rust-lsp-async-stub-0.6.1)
                        ("rust-lsp-types" ,rust-lsp-types-0.93.2)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-parking-lot" ,rust-parking-lot-0.12.1)
                        ("rust-regex" ,rust-regex-1.6.0)
                        ("rust-reqwest" ,rust-reqwest-0.11.12)
                        ("rust-schemars" ,rust-schemars-0.8.11)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-tap" ,rust-tap-1.0.1)
                        ("rust-taplo" ,rust-taplo-0.13.0)
                        ("rust-taplo-common" ,rust-taplo-common-0.5.0)
                        ("rust-time" ,rust-time-0.3.16)
                        ("rust-toml" ,rust-toml-0.7.8)
                        ("rust-tracing" ,rust-tracing-0.1.37))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tempfile-3.3.0
  (package
    (name "rust-tempfile")
    (version "3.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tempfile" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1r3rdp66f7w075mz6blh244syr3h0lbm07ippn7xrbgfxbs1xnsw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-fastrand" ,rust-fastrand-1.8.0)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-redox-syscall" ,rust-redox-syscall-0.2.16)
                        ("rust-remove-dir-all" ,rust-remove-dir-all-0.5.3)
                        ("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-termcolor-1.1.3
  (package
    (name "rust-termcolor")
    (version "1.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "termcolor" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0mbpflskhnz3jf312k50vn0hqbql8ga2rk0k79pkgchip4q4vcms"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-winapi-util" ,rust-winapi-util-0.1.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-text-size-1.1.0
  (package
    (name "rust-text-size")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "text-size" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02jn26l5wcdjqpy80ycnk9ha10flyc0p4yga8ci6aaz7vd4bb318"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-textwrap-0.11.0
  (package
    (name "rust-textwrap")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "textwrap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0q5hky03ik3y50s9sz25r438bc4nwhqc6dqwynv4wylc807n29nk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-unicode-width" ,rust-unicode-width-0.1.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-textwrap-0.16.0
  (package
    (name "rust-textwrap")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "textwrap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gbwkjf15l6p3x2rkr75fa4cpcs1ly4c8pmlfx5bl6zybcm24ai2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-thiserror-1.0.37
  (package
    (name "rust-thiserror")
    (version "1.0.37")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gky83x4i87gd87w3fknnp920wvk9yycp7dgkf5h3jg364vb7phh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-thiserror-impl" ,rust-thiserror-impl-1.0.37))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-thiserror-impl-1.0.37
  (package
    (name "rust-thiserror-impl")
    (version "1.0.37")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1fydmpksd14x1mkc24zas01qjssz8q43sbn2ywl6n527dda1fbcq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-thread-local-1.1.4
  (package
    (name "rust-thread-local")
    (version "1.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thread_local" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1001bvz6a688wf3izcrh3jqrkiqaarf44wf08azm071ig1xw45jm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-once-cell" ,rust-once-cell-1.18.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-time-0.3.16
  (package
    (name "rust-time")
    (version "0.3.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1jpv33lwhvxxibxw09rr3y02q1lwhfmy7nrdv430x1c0k65mraqg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-itoa" ,rust-itoa-1.0.4)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-num-threads" ,rust-num-threads-0.1.6)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-time-core" ,rust-time-core-0.1.0)
                        ("rust-time-macros" ,rust-time-macros-0.2.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-time-core-0.1.0
  (package
    (name "rust-time-core")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1z803zwzyh16nk3c4nmkw8v69nyj0r4v8s3yag68mvya38gkw59f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-time-macros-0.2.5
  (package
    (name "rust-time-macros")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time-macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0nqff5j5170llixk05vb3x76xri63x9znavxmrica4nq64c81fv5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-time-core" ,rust-time-core-0.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tinytemplate-1.2.1
  (package
    (name "rust-tinytemplate")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinytemplate" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1g5n77cqkdh9hy75zdb01adxn45mkh9y40wdr7l68xpz35gnnkdy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tinyvec-1.6.0
  (package
    (name "rust-tinyvec")
    (version "1.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinyvec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0l6bl2h62a5m44jdnpn7lmj14rd44via8180i7121fvm73mmrk47"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-tinyvec-macros" ,rust-tinyvec-macros-0.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tinyvec-macros-0.1.0
  (package
    (name "rust-tinyvec-macros")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinyvec_macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0p5zvgbas5nh403fbxica819mf3g83n8g2hzpfazfr56w6klv9yd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tokio-1.24.2
  (package
    (name "rust-tokio")
    (version "1.24.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1fxhf69ih3v28wh73l782pjgd6bkqfq8arr1ip1y7nc1k6ji4yjr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1.1.0)
                        ("rust-bytes" ,rust-bytes-1.2.1)
                        ("rust-libc" ,rust-libc-0.2.136)
                        ("rust-memchr" ,rust-memchr-2.5.0)
                        ("rust-mio" ,rust-mio-0.8.5)
                        ("rust-num-cpus" ,rust-num-cpus-1.13.1)
                        ("rust-parking-lot" ,rust-parking-lot-0.12.1)
                        ("rust-pin-project-lite" ,rust-pin-project-lite-0.2.9)
                        ("rust-socket2" ,rust-socket2-0.4.7)
                        ("rust-tokio-macros" ,rust-tokio-macros-1.8.0)
                        ("rust-windows-sys" ,rust-windows-sys-0.42.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tokio-macros-1.8.0
  (package
    (name "rust-tokio-macros")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11140lnx88qycdx8ynxgk0317gnw1qsy16ydlgvpx67vfnlzj94p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tokio-native-tls-0.3.0
  (package
    (name "rust-tokio-native-tls")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-native-tls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0yvikgmph2qjq0ni2h2wfaxkzhbnc09c2544av0zidyj1dk9bngp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-native-tls" ,rust-native-tls-0.2.10)
                        ("rust-tokio" ,rust-tokio-1.24.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tokio-rustls-0.23.4
  (package
    (name "rust-tokio-rustls")
    (version "0.23.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-rustls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0nfsmmi8l1lgpbfy6079d5i13984djzcxrdr9jc06ghi0cwyhgn4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-rustls" ,rust-rustls-0.20.7)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-webpki" ,rust-webpki-0.22.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tokio-util-0.7.4
  (package
    (name "rust-tokio-util")
    (version "0.7.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-util" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0h67jb56bsxy4pi1a41pda8d52569ci5clvqv3c6cg9vy1sy1chb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bytes" ,rust-bytes-1.2.1)
                        ("rust-futures-core" ,rust-futures-core-0.3.25)
                        ("rust-futures-sink" ,rust-futures-sink-0.3.25)
                        ("rust-pin-project-lite" ,rust-pin-project-lite-0.2.9)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-tracing" ,rust-tracing-0.1.37))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-toml-0.7.8
  (package
    (name "rust-toml")
    (version "0.7.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "toml" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0mr2dpmzw4ndvzpnnli2dprcx61pdk62fq4mzw0b6zb27ffycyfx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-spanned" ,rust-serde-spanned-0.6.5)
                        ("rust-toml-datetime" ,rust-toml-datetime-0.6.5)
                        ("rust-toml-edit" ,rust-toml-edit-0.19.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-toml-datetime-0.6.5
  (package
    (name "rust-toml-datetime")
    (version "0.6.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "toml_datetime" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1wds4pm2cn6agd38f0ivm65xnc7c7bmk9m0fllcaq82nd3lz8l1m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-toml-edit-0.19.15
  (package
    (name "rust-toml-edit")
    (version "0.19.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "toml_edit" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08bl7rp5g6jwmfpad9s8jpw8wjrciadpnbaswgywpr9hv9qbfnqv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-indexmap" ,rust-indexmap-2.1.0)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-spanned" ,rust-serde-spanned-0.6.5)
                        ("rust-toml-datetime" ,rust-toml-datetime-0.6.5)
                        ("rust-winnow" ,rust-winnow-0.5.35))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tower-service-0.3.2
  (package
    (name "rust-tower-service")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tower-service" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lmfzmmvid2yp2l36mbavhmqgsvzqf7r2wiwz73ml4xmwaf1rg5n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tracing-0.1.37
  (package
    (name "rust-tracing")
    (version "0.1.37")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1f2fylc79xmbh7v53kak6qyw27njbx227rd64kb4bga8ilxc7s4c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-pin-project-lite" ,rust-pin-project-lite-0.2.9)
                        ("rust-tracing-attributes" ,rust-tracing-attributes-0.1.23)
                        ("rust-tracing-core" ,rust-tracing-core-0.1.30))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tracing-attributes-0.1.23
  (package
    (name "rust-tracing-attributes")
    (version "0.1.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-attributes" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06h80cy0i1kilvnj8j9dw2kcfwbwj49n2s3jwskhr1rra7sgh5s0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tracing-core-0.1.30
  (package
    (name "rust-tracing-core")
    (version "0.1.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fi1jz3jbzk3n7k379pwv3wfhn35c5gcwn000m2xh7xb1sx07sr4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-valuable" ,rust-valuable-0.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tracing-log-0.1.3
  (package
    (name "rust-tracing-log")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-log" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08prnkxq8yas6jvvjnvyx5v3hwblas5527wxxgbiw2yis8rsvpbq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1.4.0)
                        ("rust-log" ,rust-log-0.4.17)
                        ("rust-tracing-core" ,rust-tracing-core-0.1.30))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-tracing-subscriber-0.3.16
  (package
    (name "rust-tracing-subscriber")
    (version "0.3.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-subscriber" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0w2sdf97g1ynbmk3j4q6sxmjgaalgf4pg4vl374x0w6x4sp6w5x6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-matchers" ,rust-matchers-0.1.0)
                        ("rust-nu-ansi-term" ,rust-nu-ansi-term-0.46.0)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-regex" ,rust-regex-1.6.0)
                        ("rust-sharded-slab" ,rust-sharded-slab-0.1.4)
                        ("rust-smallvec" ,rust-smallvec-1.10.0)
                        ("rust-thread-local" ,rust-thread-local-1.1.4)
                        ("rust-tracing" ,rust-tracing-0.1.37)
                        ("rust-tracing-core" ,rust-tracing-core-0.1.30)
                        ("rust-tracing-log" ,rust-tracing-log-0.1.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-try-lock-0.2.3
  (package
    (name "rust-try-lock")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "try-lock" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hkn1ksmg5hdqgqdw1ahy5qk69f4crh2psf0v61qphyrf777nm2r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-typenum-1.15.0
  (package
    (name "rust-typenum")
    (version "1.15.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typenum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11yrvz1vd43gqv738yw1v75rzngjbs7iwcgzjy3cq5ywkv2imy6w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-uncased-0.9.7
  (package
    (name "rust-uncased")
    (version "0.9.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uncased" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08inqy50xnfa2mk6g9zlsi18gnmd1dw9iq4qrynky2zxn011gc09"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-version-check" ,rust-version-check-0.9.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-unicode-bidi-0.3.8
  (package
    (name "rust-unicode-bidi")
    (version "0.3.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-bidi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "14p95n9kw9p7psp0vsp0j9yfkfg6sn1rlnymvmwmya0x60l736q9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-unicode-ident-1.0.5
  (package
    (name "rust-unicode-ident")
    (version "1.0.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-ident" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1wznr6ax3jl09vxkvj4a62vip2avfgif13js9sflkjg4b6fv7skc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-unicode-normalization-0.1.22
  (package
    (name "rust-unicode-normalization")
    (version "0.1.22")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-normalization" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08d95g7b1irc578b2iyhzv4xhsa4pfvwsqxcl9lbcpabzkq16msw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-tinyvec" ,rust-tinyvec-1.6.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-unicode-width-0.1.10
  (package
    (name "rust-unicode-width")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-width" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "12vc3wv0qwg8rzcgb9bhaf5119dlmd6lmkhbfy1zfls6n7jx3vf0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-untrusted-0.7.1
  (package
    (name "rust-untrusted")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "untrusted" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0jkbqaj9d3v5a91pp3wp9mffvng1nhycx6sh4qkdd9qyr62ccmm1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-url-2.3.1
  (package
    (name "rust-url")
    (version "2.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "url" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hs67jw257y0a7mj2p9wi0n61x8fc2vgwxg37y62nxkmmscwfs0d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-form-urlencoded" ,rust-form-urlencoded-1.1.0)
                        ("rust-idna" ,rust-idna-0.3.0)
                        ("rust-percent-encoding" ,rust-percent-encoding-2.2.0)
                        ("rust-serde" ,rust-serde-1.0.147))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-uuid-0.8.2
  (package
    (name "rust-uuid")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uuid" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dy4ldcp7rnzjy56dxh7d2sgrcvn4q77y0a8r0a48946h66zjp5w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-uuid-1.2.1
  (package
    (name "rust-uuid")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uuid" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10xyg4zzjz3m1mwhrshnx837iv8flcn6ms5hz0nvnqrkz5w1xd7y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-valuable-0.1.0
  (package
    (name "rust-valuable")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "valuable" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0v9gp3nkjbl30z0fd56d8mx7w1csk86wwjhfjhr400wh9mfpw2w3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-vcpkg-0.2.15
  (package
    (name "rust-vcpkg")
    (version "0.2.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vcpkg" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "09i4nf5y8lig6xgj3f7fyrvzd3nlaw4znrihw8psidvv5yk4xkdc"))
        (snippet
         #~(begin (use-modules (guix build utils))
                  (for-each delete-file-recursively
                            '("test-data/normalized/installed/arm64-ios"
                              "test-data/normalized/installed/x64-osx"
                              "test-data/normalized/installed/x64-windows"
                              "test-data/normalized/installed/x64-windows-static"
                              "test-data/normalized/installed/x86-windows"
                              "test-data/no-status/installed/x64-windows"))))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-version-check-0.9.4
  (package
    (name "rust-version-check")
    (version "0.9.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "version_check" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gs8grwdlgh0xq660d7wr80x14vxbizmd8dbp29p2pdncx8lp1s9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-walkdir-2.3.2
  (package
    (name "rust-walkdir")
    (version "2.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "walkdir" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0mnszy33685v8y9js8mw6x2p3iddqs8vfj7n2dhqddnlbirz5340"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-same-file" ,rust-same-file-1.0.6)
                        ("rust-winapi" ,rust-winapi-0.3.9)
                        ("rust-winapi-util" ,rust-winapi-util-0.1.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-want-0.3.0
  (package
    (name "rust-want")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "want" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "181b2zmwfq389x9n2g1n37cvcvvdand832zz6v8i1l8wrdlaks0w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-log" ,rust-log-0.4.17)
                        ("rust-try-lock" ,rust-try-lock-0.2.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-wasi-0.11.0+wasi-snapshot-preview1
  (package
    (name "rust-wasi")
    (version "0.11.0+wasi-snapshot-preview1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08z4hxwkpdpalxjps1ai9y7ihin26y9f476i53dv98v45gkqg3cw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-wasm-bindgen-0.2.83
  (package
    (name "rust-wasm-bindgen")
    (version "0.2.83")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0s3ji0k8p261glnsxi5rkd34v2pv67h96blb29yf32zcxsngbyga"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-wasm-bindgen-macro" ,rust-wasm-bindgen-macro-0.2.83))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-wasm-bindgen-backend-0.2.83
  (package
    (name "rust-wasm-bindgen-backend")
    (version "0.2.83")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-backend" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hhigjqrb31axh7jgmb5y8akdpxqx8gvjs6ja9xmbc3r4lrzp3sc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-bumpalo" ,rust-bumpalo-3.11.1)
                        ("rust-log" ,rust-log-0.4.17)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103)
                        ("rust-wasm-bindgen-shared" ,rust-wasm-bindgen-shared-0.2.83))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-wasm-bindgen-futures-0.4.33
  (package
    (name "rust-wasm-bindgen-futures")
    (version "0.4.33")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-futures" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zdxgrpdmdw7516amyv0jlqq1bipnyb8h7dfhvgab9aw2r398qr3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1.0.0)
                        ("rust-js-sys" ,rust-js-sys-0.3.60)
                        ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2.83)
                        ("rust-web-sys" ,rust-web-sys-0.3.60))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-wasm-bindgen-macro-0.2.83
  (package
    (name "rust-wasm-bindgen-macro")
    (version "0.2.83")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0468wshk7bp78mnglcpmrb6m4q7x2fp9pz6ybk3wpri683wy0aq5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-quote" ,rust-quote-1.0.21)
                        ("rust-wasm-bindgen-macro-support" ,rust-wasm-bindgen-macro-support-0.2.83))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-wasm-bindgen-macro-support-0.2.83
  (package
    (name "rust-wasm-bindgen-macro-support")
    (version "0.2.83")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro-support" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0g0rmawgkhfyfgjj2mvch7gvz1nzfnfmya0kgcq3xwn53l2hrg07"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1.0.66)
                        ("rust-quote" ,rust-quote-1.0.21)
                        ("rust-syn" ,rust-syn-1.0.103)
                        ("rust-wasm-bindgen-backend" ,rust-wasm-bindgen-backend-0.2.83)
                        ("rust-wasm-bindgen-shared" ,rust-wasm-bindgen-shared-0.2.83))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-wasm-bindgen-shared-0.2.83
  (package
    (name "rust-wasm-bindgen-shared")
    (version "0.2.83")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-shared" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zzz9xfi3fp2n5ihhlq8ws7674a2ir2frvsd1d7yr4sxad2w0f0w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-web-sys-0.3.60
  (package
    (name "rust-web-sys")
    (version "0.3.60")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "web-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "03sic7x58vi5qz8qrcka21748kmdmwlvgiddsn7p4vp1idnr1nmw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-js-sys" ,rust-js-sys-0.3.60)
                        ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2.83))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-webpki-0.22.2
  (package
    (name "rust-webpki")
    (version "0.22.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "webpki" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zv0bw9vw7cmncw1y644gdhrvxygn4cglppchbv1n2dcgk6w1v07"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-ring" ,rust-ring-0.16.20)
                        ("rust-untrusted" ,rust-untrusted-0.7.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-webpki-roots-0.22.5
  (package
    (name "rust-webpki-roots")
    (version "0.22.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "webpki-roots" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gkmk4baan9knbf3dggwyvh8bqmdvi8x6mmpicih3yv9g5jzx2rn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-webpki" ,rust-webpki-0.22.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-winapi-0.3.9
  (package
    (name "rust-winapi")
    (version "0.3.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06gl025x418lchw1wxj64ycr7gha83m44cjr5sarhynd9xkrm0sw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-winapi-i686-pc-windows-gnu" ,rust-winapi-i686-pc-windows-gnu-0.4.0)
                        ("rust-winapi-x86-64-pc-windows-gnu" ,rust-winapi-x86-64-pc-windows-gnu-0.4.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-winapi-i686-pc-windows-gnu-0.4.0
  (package
    (name "rust-winapi-i686-pc-windows-gnu")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi-i686-pc-windows-gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dmpa6mvcvzz16zg6d5vrfy4bxgg541wxrcip7cnshi06v38ffxc"))
        (modules '((guix build utils)))
        (snippet
         '(begin
            (for-each delete-file (find-files "." "\\.a$"))))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-winapi-util-0.1.5
  (package
    (name "rust-winapi-util")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi-util" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y71bp7f6d536czj40dhqk0d55wfbbwqfp2ymqf1an5ibgl6rv3h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-winapi-x86-64-pc-windows-gnu-0.4.0
  (package
    (name "rust-winapi-x86-64-pc-windows-gnu")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi-x86_64-pc-windows-gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gqq64czqb64kskjryj8isp62m2sgvx25yyj3kpc2myh85w24bki"))
        (modules '((guix build utils)))
        (snippet
         '(begin
            (for-each delete-file (find-files "." "\\.a$"))))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-sys-0.36.1
  (package
    (name "rust-windows-sys")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1lmqangv0zg1l46xiq7rfnqwsx8f8m52mqbgg2mrx7x52rd1a17a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-windows-aarch64-msvc" ,rust-windows-aarch64-msvc-0.36.1)
                        ("rust-windows-i686-gnu" ,rust-windows-i686-gnu-0.36.1)
                        ("rust-windows-i686-msvc" ,rust-windows-i686-msvc-0.36.1)
                        ("rust-windows-x86-64-gnu" ,rust-windows-x86-64-gnu-0.36.1)
                        ("rust-windows-x86-64-msvc" ,rust-windows-x86-64-msvc-0.36.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-sys-0.42.0
  (package
    (name "rust-windows-sys")
    (version "0.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19waf8aryvyq9pzk0gamgfwjycgzk4gnrazpfvv171cby0h1hgjs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-windows-aarch64-gnullvm" ,rust-windows-aarch64-gnullvm-0.42.0)
                        ("rust-windows-aarch64-msvc" ,rust-windows-aarch64-msvc-0.42.0)
                        ("rust-windows-i686-gnu" ,rust-windows-i686-gnu-0.42.0)
                        ("rust-windows-i686-msvc" ,rust-windows-i686-msvc-0.42.0)
                        ("rust-windows-x86-64-gnu" ,rust-windows-x86-64-gnu-0.42.0)
                        ("rust-windows-x86-64-gnullvm" ,rust-windows-x86-64-gnullvm-0.42.0)
                        ("rust-windows-x86-64-msvc" ,rust-windows-x86-64-msvc-0.42.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-aarch64-gnullvm-0.42.0
  (package
    (name "rust-windows-aarch64-gnullvm")
    (version "0.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_gnullvm" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17m1p753qk02r25afg31dxym4rpy7kpr0z8nwl5f1jzhyrqsmlj1"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/libwindows.a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-aarch64-msvc-0.36.1
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ixaxs2c37ll2smprzh0xq5p238zn8ylzb3lk1zddqmd77yw7f4v"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/windows.lib"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-aarch64-msvc-0.42.0
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1d6d9ny0yl5l9vvagydigvkfcphzk2aygchiccywijimb8pja3yx"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/windows.lib"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-i686-gnu-0.36.1
  (package
    (name "rust-windows-i686-gnu")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dm3svxfzamrv6kklyda9c3qylgwn5nwdps6p0kc9x6s077nq3hq"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/libwindows.a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-i686-gnu-0.42.0
  (package
    (name "rust-windows-i686-gnu")
    (version "0.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1rsxdjp50nk38zfd1dxj12i2qmhpvxsm6scdq8v1d10ncygy3spv"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/libwindows.a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-i686-msvc-0.36.1
  (package
    (name "rust-windows-i686-msvc")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "097h2a7wig04wbmpi3rz1akdy4s8gslj5szsx8g2v0dj91qr3rz2"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/windows.lib"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-i686-msvc-0.42.0
  (package
    (name "rust-windows-i686-msvc")
    (version "0.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ii2hrsdif2ms79dfiyfzm1n579jzj42ji3fpsxd57d3v9jjzhc4"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/windows.lib"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-x86-64-gnu-0.36.1
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qfrck3jnihymfrd01s8260d4snql8ks2p8yaabipi3nhwdigkad"))
        (modules '((guix build utils)))
        (snippet
         '(begin
            (for-each delete-file (find-files "." "\\.a$"))))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-x86-64-gnu-0.42.0
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vdh8k5a4m6pfkc5gladqznyqxgapkjm0qb8iwqvqb1nnlhinyxz"))
        (modules '((guix build utils)))
        (snippet
         '(begin
            (for-each delete-file (find-files "." "\\.a$"))))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-x86-64-gnullvm-0.42.0
  (package
    (name "rust-windows-x86-64-gnullvm")
    (version "0.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnullvm" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0a10rns9b07m9snlr97iqxq42zi9ai547gb5fqlv7vihpb92bm89"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/libwindows.a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-x86-64-msvc-0.36.1
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "103n3xijm5vr7qxr1dps202ckfnv7njjnnfqmchg8gl5ii5cl4f8"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/windows.lib"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-windows-x86-64-msvc-0.42.0
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xdnvhg8yj4fgjy0vkrahq5cbgfpcd7ak2bdv8s5lwjrazc0j07l"))
        (modules '((guix build utils)))
        (snippet '(delete-file "lib/windows.lib"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-winnow-0.5.35
  (package
    (name "rust-winnow")
    (version "0.5.35")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winnow" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0bbanirar6xpnh889fhfwfr4kklhyyqkniag2fh1v1kkkj5dfc8r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-memchr" ,rust-memchr-2.5.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define rust-winreg-0.10.1
  (package
    (name "rust-winreg")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winreg" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17c6h02z88ijjba02bnxi5k94q5cz490nf3njh9yypf8fbig9l40"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs (("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))

(define taplo-cli
  (package
    (name "taplo-cli")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "taplo-cli" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1q2kw75f0pacfgwdjrlskc5yqqq8iz97vkmdcmsggkdazh0yl7fd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-build-flags '("--features" "lsp")
        #:cargo-inputs (("rust-ansi-term" ,rust-ansi-term-0.12.1)
                        ("rust-anyhow" ,rust-anyhow-1.0.66)
                        ("rust-async-ctrlc" ,rust-async-ctrlc-1.2.0)
                        ("rust-atty" ,rust-atty-0.2.14)
                        ("rust-clap" ,rust-clap-3.2.23)
                        ("rust-codespan-reporting" ,rust-codespan-reporting-0.11.1)
                        ("rust-futures" ,rust-futures-0.3.25)
                        ("rust-glob" ,rust-glob-0.3.0)
                        ("rust-hex" ,rust-hex-0.4.3)
                        ("rust-itertools" ,rust-itertools-0.10.5)
                        ("rust-lsp-async-stub" ,rust-lsp-async-stub-0.6.1)
                        ("rust-once-cell" ,rust-once-cell-1.18.0)
                        ("rust-prettydiff" ,rust-prettydiff-0.6.1)
                        ("rust-regex" ,rust-regex-1.6.0)
                        ("rust-reqwest" ,rust-reqwest-0.11.12)
                        ("rust-schemars" ,rust-schemars-0.8.11)
                        ("rust-serde" ,rust-serde-1.0.147)
                        ("rust-serde-json" ,rust-serde-json-1.0.87)
                        ("rust-taplo" ,rust-taplo-0.13.0)
                        ("rust-taplo-common" ,rust-taplo-common-0.5.0)
                        ("rust-taplo-lsp" ,rust-taplo-lsp-0.7.0)
                        ("rust-time" ,rust-time-0.3.16)
                        ("rust-tokio" ,rust-tokio-1.24.2)
                        ("rust-toml" ,rust-toml-0.7.8)
                        ("rust-tracing" ,rust-tracing-0.1.37)
                        ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3.16)
                        ("rust-url" ,rust-url-2.3.1))))
    (inputs (list pkg-config openssl))
    (home-page "https://github.com/tamasfe/taplo")
    (synopsis "A TOML toolkit written in Rust")
    (description "A versatile, feature-rich TOML toolkit.")
    (license (list license:expat))))
