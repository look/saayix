;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages rust helix)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-check)
  #:use-module (gnu packages crates-compression)
  #:use-module (gnu packages crates-crypto)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-vcs)
  #:use-module (gnu packages crates-web)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages version-control)
  #:use-module ((gnu packages text-editors)
                #:select (helix)
                #:prefix guix:)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1))

(define rust-addr2line-0.22
  (package
    (name "rust-addr2line")
    (version "0.22.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "addr2line" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0y66f1sa27i9kvmlh76ynk60rxfrmkba9ja8x527h32wdb206ibf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gimli" ,rust-gimli-0.29))))
    (home-page
      "https://github.com/gimli-rs/addr2line")
    (synopsis
      "cross-platform symbolication library written in Rust, using `gimli`")
    (description
      "This package provides a cross-platform symbolication library written in Rust,\nusing `gimli`.")
    (license (list license:asl2.0 license:expat))))

(define rust-bitflags-2
  (package
    (name "rust-bitflags")
    (version "2.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitflags" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gp1bjw29d12ij9nx0v02nc78s9d04zmyrwzspn4blyncwmg9qqv"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/bitflags/bitflags")
    (synopsis
      "macro to generate structures which behave like bitflags.")
    (description
      "This package provides a macro to generate structures which behave like bitflags.")
    (license (list license:expat license:asl2.0))))

(define rust-cc-1
  (package
    (name "rust-cc")
    (version "1.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02rim94a0c7rdwyc5yz6wlzyd4cm22r4b6x3maylb4sx2rr3faf8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-shlex" ,rust-shlex-1))))
    (home-page "https://github.com/rust-lang/cc-rs")
    (synopsis
      "build-time dependency for Cargo build scripts to assist in invoking the native\nC compiler to compile native C code into a static archive to be linked into Rust\ncode.")
    (description
      "This package provides a build-time dependency for Cargo build scripts to assist\nin invoking the native C compiler to compile native C code into a static archive\nto be linked into Rust code.")
    (license (list license:expat license:asl2.0))))

(define rust-clru-0.6
  (package
    (name "rust-clru")
    (version "0.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clru" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ngyycxpxif84wpjjn0ixywylk95h5iv8fqycg2zsr3f0rpggl6b"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/marmeladema/clru-rs")
    (synopsis
      "An LRU cache implementation with constant time operations and weighted semantic")
    (description
      "This package provides An LRU cache implementation with constant time operations and weighted semantic.")
    (license license:expat)))

(define rust-fern-0.7
  (package
    (name "rust-fern")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fern" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0a9v59vcq2fgd6bwgbfl7q6b0zzgxn85y6g384z728wvf1gih5j3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/daboross/fern")
    (synopsis "Simple, efficient logging")
    (description
      "This package provides Simple, efficient logging.")
    (license license:expat)))

(define rust-gimli-0.29
  (package
    (name "rust-gimli")
    (version "0.29.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gimli" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zgzprnjaawmg6zyic4f2q2hc39kdhn116qnkqpgvsasgc3x9v20"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/gimli-rs/gimli")
    (synopsis
      "library for reading and writing the DWARF debugging format.")
    (description
      "This package provides a library for reading and writing the DWARF debugging\nformat.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-0.69
  (package
    (name "rust-gix")
    (version "0.69.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gimmnxcrnz0icw089pf93gg93059xgghdis8gahbx6wxkdfn3ld"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gix-actor" ,rust-gix-actor-0.33)
         ("rust-gix-attributes" ,rust-gix-attributes-0.23)
         ("rust-gix-command" ,rust-gix-command-0.4)
         ("rust-gix-commitgraph"
          ,rust-gix-commitgraph-0.25)
         ("rust-gix-config" ,rust-gix-config-0.42)
         ("rust-gix-date" ,rust-gix-date-0.9)
         ("rust-gix-diff" ,rust-gix-diff-0.49)
         ("rust-gix-dir" ,rust-gix-dir-0.11)
         ("rust-gix-discover" ,rust-gix-discover-0.37)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-filter" ,rust-gix-filter-0.16)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-gix-glob" ,rust-gix-glob-0.17)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-hashtable" ,rust-gix-hashtable-0.6)
         ("rust-gix-ignore" ,rust-gix-ignore-0.12)
         ("rust-gix-index" ,rust-gix-index-0.37)
         ("rust-gix-lock" ,rust-gix-lock-15)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-odb" ,rust-gix-odb-0.66)
         ("rust-gix-pack" ,rust-gix-pack-0.56)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-pathspec" ,rust-gix-pathspec-0.8)
         ("rust-gix-protocol" ,rust-gix-protocol-0.47)
         ("rust-gix-ref" ,rust-gix-ref-0.49)
         ("rust-gix-refspec" ,rust-gix-refspec-0.27)
         ("rust-gix-revision" ,rust-gix-revision-0.31)
         ("rust-gix-revwalk" ,rust-gix-revwalk-0.17)
         ("rust-gix-sec" ,rust-gix-sec-0.10)
         ("rust-gix-shallow" ,rust-gix-shallow-0.1)
         ("rust-gix-status" ,rust-gix-status-0.16)
         ("rust-gix-submodule" ,rust-gix-submodule-0.16)
         ("rust-gix-tempfile" ,rust-gix-tempfile-15)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-gix-traverse" ,rust-gix-traverse-0.43)
         ("rust-gix-url" ,rust-gix-url-0.28)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-gix-validate" ,rust-gix-validate-0.9)
         ("rust-gix-worktree" ,rust-gix-worktree-0.38)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "Interact with git repositories just like git would")
    (description
      "This package provides Interact with git repositories just like git would.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-actor-0.33
  (package
    (name "rust-gix-actor")
    (version "0.33.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-actor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cxgnw1rlmf4sj4j9bd87frhkxmcdnqa0wpv9nxzgkhlymql3cij"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-date" ,rust-gix-date-0.9)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-thiserror" ,rust-thiserror-2)
         ("rust-winnow" ,rust-winnow-0.6))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis "way to identify git actors")
    (description
      "This package provides a way to identify git actors.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-attributes-0.23
  (package
    (name "rust-gix-attributes")
    (version "0.23.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-attributes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1p6a6ai3pk8c7xn48vbw7gvjh7rc5m13cbcsd7zfvh4l462vzyfx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-glob" ,rust-gix-glob-0.17)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-quote" ,rust-gix-quote-0.4)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-kstring" ,rust-kstring-2)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2)
         ("rust-unicode-bom" ,rust-unicode-bom-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project dealing .gitattributes files")
    (description
      "This package provides a crate of the gitoxide project dealing .gitattributes\nfiles.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-command-0.4
  (package
    (name "rust-gix-command")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-command" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "140y16ykgsj6nw6ba16sig63wckivg3x43478rd3dy0pdsjw01cl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-shell-words" ,rust-shell-words-1))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project handling internal git command execution")
    (description
      "This package provides a crate of the gitoxide project handling internal git\ncommand execution.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-commitgraph-0.25
  (package
    (name "rust-gix-commitgraph")
    (version "0.25.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-commitgraph" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11cdlkbkv80imbdkiy8k09gb1c48k6qadpmxvavb53w6ly8nbnm8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-chunk" ,rust-gix-chunk-0.4)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-memmap2" ,rust-memmap2-0.9)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "Read-only access to the git commitgraph file format")
    (description
      "This package provides Read-only access to the git commitgraph file format.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-config-0.42
  (package
    (name "rust-gix-config")
    (version "0.42.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-config" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06bgc1isbk831763ii4f1yahf7r3id301ksri4acp68zr83b8jb6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-config-value"
          ,rust-gix-config-value-0.14)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-glob" ,rust-gix-glob-0.17)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-ref" ,rust-gix-ref-0.49)
         ("rust-gix-sec" ,rust-gix-sec-0.10)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2)
         ("rust-unicode-bom" ,rust-unicode-bom-2)
         ("rust-winnow" ,rust-winnow-0.6))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "git-config file parser and editor from the gitoxide project")
    (description
      "This package provides a git-config file parser and editor from the gitoxide\nproject.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-diff-0.49
  (package
    (name "rust-gix-diff")
    (version "0.49.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-diff" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xh4mac785zyvnyap5awpj2iw1kw6aqgz5hg19d21jnbxik2bsd8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-command" ,rust-gix-command-0.4)
         ("rust-gix-filter" ,rust-gix-filter-0.16)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-tempfile" ,rust-gix-tempfile-15)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-gix-traverse" ,rust-gix-traverse-0.43)
         ("rust-gix-worktree" ,rust-gix-worktree-0.38)
         ("rust-imara-diff" ,rust-imara-diff-0.1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "Calculate differences between various git objects")
    (description
      "This package provides Calculate differences between various git objects.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-dir-0.11
  (package
    (name "rust-gix-dir")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-dir" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14qjgsbb1dkyyha3l68z7n84j1c7jjycqrw3ia746d5xyjygz8pv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-discover" ,rust-gix-discover-0.37)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-gix-ignore" ,rust-gix-ignore-0.12)
         ("rust-gix-index" ,rust-gix-index-0.37)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-pathspec" ,rust-gix-pathspec-0.8)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-gix-worktree" ,rust-gix-worktree-0.38)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project dealing with directory walks")
    (description
      "This package provides a crate of the gitoxide project dealing with directory\nwalks.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-discover-0.37
  (package
    (name "rust-gix-discover")
    (version "0.37.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-discover" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vxnzwaz7g5dzn3dscy4qry3yb7r0748zldlxjdllsi69vx6vgw3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-dunce" ,rust-dunce-1)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-ref" ,rust-gix-ref-0.49)
         ("rust-gix-sec" ,rust-gix-sec-0.10)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "Discover git repositories and check if a directory is a git repository")
    (description
      "This package provides Discover git repositories and check if a directory is a git repository.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-features-0.39
  (package
    (name "rust-gix-features")
    (version "0.39.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-features" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07yqby9y0icx2l7kwbvxfg6z8b7gfznknwd4vd0a68p0y9rxd1bx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crc32fast" ,rust-crc32fast-1)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-prodash" ,rust-prodash-29)
         ("rust-sha1-smol" ,rust-sha1-smol-1)
         ("rust-thiserror" ,rust-thiserror-2)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate to integrate various capabilities using compile-time feature flags")
    (description
      "This package provides a crate to integrate various capabilities using\ncompile-time feature flags.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-filter-0.16
  (package
    (name "rust-gix-filter")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-filter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02krfk40gvz53wardlmzngkc27cf7zb5dzn742x41y37avpcs3ix"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-gix-attributes" ,rust-gix-attributes-0.23)
         ("rust-gix-command" ,rust-gix-command-0.4)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-packetline-blocking"
          ,rust-gix-packetline-blocking-0.18)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-quote" ,rust-gix-quote-0.4)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project implementing git filters")
    (description
      "This package provides a crate of the gitoxide project implementing git filters.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-fs-0.12
  (package
    (name "rust-gix-fs")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-fs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1f8xifs0wkq7lhy3c8091kq2lx15qkynjb6fwnbiyqjsa2n4yg9v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fastrand" ,rust-fastrand-2)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-utils" ,rust-gix-utils-0.1))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate providing file system specific utilities to `gitoxide`")
    (description
      "This package provides a crate providing file system specific utilities to\n`gitoxide`.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-glob-0.17
  (package
    (name "rust-gix-glob")
    (version "0.17.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-glob" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0d9lrxas6zjia91j3m4z8rnazz1s02j9kgw4fib82d8aximrmxma"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-bstr" ,rust-bstr-1)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-path" ,rust-gix-path-0.10))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project dealing with pattern matching")
    (description
      "This package provides a crate of the gitoxide project dealing with pattern\nmatching.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-hash-0.15
  (package
    (name "rust-gix-hash")
    (version "0.15.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-hash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kp4yjlkp8g4qg0r2zs0jmz19r076f2y91cjsikhxvclf70wqphb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-faster-hex" ,rust-faster-hex-0.9)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "Borrowed and owned git hash digests used to identify git objects")
    (description
      "This package provides Borrowed and owned git hash digests used to identify git objects.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-hashtable-0.6
  (package
    (name "rust-gix-hashtable")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-hashtable" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zhqgncv6jh3x7a7a2w3qbayghmiwv230mdw6gvqw1ricqjmpxhf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-hashbrown" ,rust-hashbrown-0.14)
         ("rust-parking-lot" ,rust-parking-lot-0.12))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate that provides hashtable based data structures optimized to utilize ObjectId keys")
    (description
      "This package provides a crate that provides hashtable based data structures\noptimized to utilize @code{ObjectId} keys.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-ignore-0.12
  (package
    (name "rust-gix-ignore")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-ignore" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12mv0lgq8aviy6fc4mdxr7r0ra0l1kb729wf8fkhmbx4s8jgpcdn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-glob" ,rust-gix-glob-0.17)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-unicode-bom" ,rust-unicode-bom-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project dealing .gitignore files")
    (description
      "This package provides a crate of the gitoxide project dealing .gitignore files.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-index-0.37
  (package
    (name "rust-gix-index")
    (version "0.37.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-index" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yi5g8vfk5vgb5la031s86a6j7i83f90sm51zz468ssm43yla1i7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-bstr" ,rust-bstr-1)
         ("rust-filetime" ,rust-filetime-0.2)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-gix-bitmap" ,rust-gix-bitmap-0.2)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-lock" ,rust-gix-lock-15)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-traverse" ,rust-gix-traverse-0.43)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-gix-validate" ,rust-gix-validate-0.9)
         ("rust-hashbrown" ,rust-hashbrown-0.14)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-memmap2" ,rust-memmap2-0.9)
         ("rust-rustix" ,rust-rustix-0.38)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "work-in-progress crate of the gitoxide project dedicated implementing the git index file")
    (description
      "This package provides a work-in-progress crate of the gitoxide project dedicated\nimplementing the git index file.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-lock-15
  (package
    (name "rust-gix-lock")
    (version "15.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-lock" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pn20dz20c2lzq8gi1bxiwhplnd9zg6iigfv71769qna9bgsq0ji"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gix-tempfile" ,rust-gix-tempfile-15)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis "git-style lock-file implementation")
    (description
      "This package provides a git-style lock-file implementation.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-object-0.46
  (package
    (name "rust-gix-object")
    (version "0.46.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-object" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1skp7mbqbcdqkv9n4nxk87ilzd4jxfs7k10864zh7vw3040mhbg4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-actor" ,rust-gix-actor-0.33)
         ("rust-gix-date" ,rust-gix-date-0.9)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-hashtable" ,rust-gix-hashtable-0.6)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-gix-validate" ,rust-gix-validate-0.9)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2)
         ("rust-winnow" ,rust-winnow-0.6))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "Immutable and mutable git objects with decoding and encoding support")
    (description
      "This package provides Immutable and mutable git objects with decoding and encoding support.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-odb-0.66
  (package
    (name "rust-gix-odb")
    (version "0.66.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-odb" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1bnhjzg360vy6f303ps7halnnkx3x81dwy4l8q2f4bipng70wy6b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arc-swap" ,rust-arc-swap-1)
         ("rust-gix-date" ,rust-gix-date-0.9)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-hashtable" ,rust-gix-hashtable-0.6)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-pack" ,rust-gix-pack-0.56)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-quote" ,rust-gix-quote-0.4)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "Implements various git object databases")
    (description
      "This package implements various git object databases.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-pack-0.56
  (package
    (name "rust-gix-pack")
    (version "0.56.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-pack" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0v2n5k0q08zdiqwbmn1rgwd0fcm943lcibwpmgkwlady564r4n21"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clru" ,rust-clru-0.6)
         ("rust-gix-chunk" ,rust-gix-chunk-0.4)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-hashtable" ,rust-gix-hashtable-0.6)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-memmap2" ,rust-memmap2-0.9)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "Implements git packs and related data structures")
    (description
      "This package implements git packs and related data structures.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-packetline-0.18
  (package
    (name "rust-gix-packetline")
    (version "0.18.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-packetline" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lz4vn93hki2cjgayk87hybi1w51a9hr1yasfwpyvgnsnalfw6li"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-faster-hex" ,rust-faster-hex-0.9)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project implementing the pkt-line serialization format")
    (description
      "This package provides a crate of the gitoxide project implementing the pkt-line\nserialization format.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-packetline-blocking-0.18
  (package
    (name "rust-gix-packetline-blocking")
    (version "0.18.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-packetline-blocking" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0aidwkwq2664qldardyjycxgnn0m38ach7hwn4wda3y03g70946f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-faster-hex" ,rust-faster-hex-0.9)
         ("rust-gix-trace" ,rust-gix-trace-0.1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "duplicate of `gix-packetline` with the `blocking-io` feature pre-selected")
    (description
      "This package provides a duplicate of `gix-packetline` with the `blocking-io`\nfeature pre-selected.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-pathspec-0.8
  (package
    (name "rust-gix-pathspec")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-pathspec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07mqfl6232285yzsmym2vr7vndwh3ivx9p7xgv7nzsd4wkxjsisc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-bstr" ,rust-bstr-1)
         ("rust-gix-attributes" ,rust-gix-attributes-0.23)
         ("rust-gix-config-value"
          ,rust-gix-config-value-0.14)
         ("rust-gix-glob" ,rust-gix-glob-0.17)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project dealing magical pathspecs")
    (description
      "This package provides a crate of the gitoxide project dealing magical pathspecs.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-protocol-0.47
  (package
    (name "rust-gix-protocol")
    (version "0.47.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-protocol" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rxhmvm3c1lamghxr9gp3bnb0mcw069rai6cx5f07mzynvl44in8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-date" ,rust-gix-date-0.9)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-ref" ,rust-gix-ref-0.49)
         ("rust-gix-shallow" ,rust-gix-shallow-0.1)
         ("rust-gix-transport" ,rust-gix-transport-0.44)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-maybe-async" ,rust-maybe-async-0.2)
         ("rust-thiserror" ,rust-thiserror-2)
         ("rust-winnow" ,rust-winnow-0.6))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project for implementing git protocols")
    (description
      "This package provides a crate of the gitoxide project for implementing git\nprotocols.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-ref-0.49
  (package
    (name "rust-gix-ref")
    (version "0.49.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-ref" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ps8xzgkblsn3k5961ykyjkpm55hmxwi340lf4dhz7c3divn26x9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gix-actor" ,rust-gix-actor-0.33)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-lock" ,rust-gix-lock-15)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-tempfile" ,rust-gix-tempfile-15)
         ("rust-gix-utils" ,rust-gix-utils-0.1)
         ("rust-gix-validate" ,rust-gix-validate-0.9)
         ("rust-memmap2" ,rust-memmap2-0.9)
         ("rust-thiserror" ,rust-thiserror-2)
         ("rust-winnow" ,rust-winnow-0.6))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis "crate to handle git references")
    (description
      "This package provides a crate to handle git references.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-refspec-0.27
  (package
    (name "rust-gix-refspec")
    (version "0.27.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-refspec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ga1dh6500q88wb0ymd2s2iqraw1j6gjqdgb1bmwfs3qfjxmdh00"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-revision" ,rust-gix-revision-0.31)
         ("rust-gix-validate" ,rust-gix-validate-0.9)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project for parsing and representing refspecs")
    (description
      "This package provides a crate of the gitoxide project for parsing and\nrepresenting refspecs.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-revision-0.31
  (package
    (name "rust-gix-revision")
    (version "0.31.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-revision" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rzrr2i5vb8yc0cffprgkyawxrigsxfp11c4rv96hnj0fk2dvqb1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-commitgraph"
          ,rust-gix-commitgraph-0.25)
         ("rust-gix-date" ,rust-gix-date-0.9)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-revwalk" ,rust-gix-revwalk-0.17)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project dealing with finding names for revisions and parsing specifications")
    (description
      "This package provides a crate of the gitoxide project dealing with finding names\nfor revisions and parsing specifications.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-revwalk-0.17
  (package
    (name "rust-gix-revwalk")
    (version "0.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-revwalk" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "178hrfgigvr3a1m8mn1949ms75wb10s7rwyqczqghmpl6by2c02i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gix-commitgraph"
          ,rust-gix-commitgraph-0.25)
         ("rust-gix-date" ,rust-gix-date-0.9)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-hashtable" ,rust-gix-hashtable-0.6)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate providing utilities for walking the revision graph")
    (description
      "This package provides a crate providing utilities for walking the revision\ngraph.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-shallow-0.1
  (package
    (name "rust-gix-shallow")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-shallow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00ffhjy3lzqr6zqj0h3c635634383z0g0wgndz5r4x7888r6gll8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-lock" ,rust-gix-lock-15)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "Handle files specifying the shallow boundary")
    (description
      "This package provides Handle files specifying the shallow boundary.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-status-0.16
  (package
    (name "rust-gix-status")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-status" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pv4fprwb2iiz737p7kljzxwdzxkdjzsv1mg1rmvs23n4w77fr8n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-filetime" ,rust-filetime-0.2)
         ("rust-gix-diff" ,rust-gix-diff-0.49)
         ("rust-gix-dir" ,rust-gix-dir-0.11)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-filter" ,rust-gix-filter-0.16)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-index" ,rust-gix-index-0.37)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-pathspec" ,rust-gix-pathspec-0.8)
         ("rust-gix-worktree" ,rust-gix-worktree-0.38)
         ("rust-portable-atomic" ,rust-portable-atomic-1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project dealing with 'git status'-like functionality")
    (description
      "This package provides a crate of the gitoxide project dealing with git\nstatus'-like functionality.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-submodule-0.16
  (package
    (name "rust-gix-submodule")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-submodule" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p4zx1v997wjglya5xrfxdixf586scigbj43dqmbwvnb1y65yid2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-config" ,rust-gix-config-0.42)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-pathspec" ,rust-gix-pathspec-0.8)
         ("rust-gix-refspec" ,rust-gix-refspec-0.27)
         ("rust-gix-url" ,rust-gix-url-0.28)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project dealing git submodules")
    (description
      "This package provides a crate of the gitoxide project dealing git submodules.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-tempfile-15
  (package
    (name "rust-gix-tempfile")
    (version "15.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-tempfile" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10nvk82g7fhljg5y63dxpd8p7296wrfzxyssk957misc17pqdsrg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dashmap" ,rust-dashmap-6)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-tempfile" ,rust-tempfile-3))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "tempfile implementation with a global registry to assure cleanup")
    (description
      "This package provides a tempfile implementation with a global registry to assure\ncleanup.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-transport-0.44
  (package
    (name "rust-gix-transport")
    (version "0.44.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-transport" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1bybr8741kilfvmwg71p80xfadjvsy2mm39ilb7i71vsa0gdj16x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-command" ,rust-gix-command-0.4)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-packetline" ,rust-gix-packetline-0.18)
         ("rust-gix-quote" ,rust-gix-quote-0.4)
         ("rust-gix-sec" ,rust-gix-sec-0.10)
         ("rust-gix-url" ,rust-gix-url-0.28)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project dedicated to implementing the git transport layer")
    (description
      "This package provides a crate of the gitoxide project dedicated to implementing\nthe git transport layer.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-traverse-0.43
  (package
    (name "rust-gix-traverse")
    (version "0.43.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-traverse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12dcmf67m409ss0jjpgr9vjh048cs6hbplkiz69kxqhrhrj7vm3f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-gix-commitgraph"
          ,rust-gix-commitgraph-0.25)
         ("rust-gix-date" ,rust-gix-date-0.9)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-hashtable" ,rust-gix-hashtable-0.6)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-revwalk" ,rust-gix-revwalk-0.17)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thiserror" ,rust-thiserror-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis "crate of the gitoxide project")
    (description
      "This package provides a crate of the gitoxide project.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-url-0.28
  (package
    (name "rust-gix-url")
    (version "0.28.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-url" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ncj6k4lk3qb0i27ida7ngi9z06qpmrbva6v0da3zgd67drzp5nh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2)
         ("rust-thiserror" ,rust-thiserror-2)
         ("rust-url" ,rust-url-2))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project implementing parsing and serialization of gix-url")
    (description
      "This package provides a crate of the gitoxide project implementing parsing and\nserialization of gix-url.")
    (license (list license:expat license:asl2.0))))

(define rust-gix-worktree-0.38
  (package
    (name "rust-gix-worktree")
    (version "0.38.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gix-worktree" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04za9fgw10w0r0sfqnjd6sji3xcwiwgr9asy1ma25yl8a7hvnvbm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-1)
         ("rust-gix-attributes" ,rust-gix-attributes-0.23)
         ("rust-gix-features" ,rust-gix-features-0.39)
         ("rust-gix-fs" ,rust-gix-fs-0.12)
         ("rust-gix-glob" ,rust-gix-glob-0.17)
         ("rust-gix-hash" ,rust-gix-hash-0.15)
         ("rust-gix-ignore" ,rust-gix-ignore-0.12)
         ("rust-gix-index" ,rust-gix-index-0.37)
         ("rust-gix-object" ,rust-gix-object-0.46)
         ("rust-gix-path" ,rust-gix-path-0.10)
         ("rust-gix-validate" ,rust-gix-validate-0.9))))
    (home-page
      "https://github.com/GitoxideLabs/gitoxide")
    (synopsis
      "crate of the gitoxide project for shared worktree related types and utilities.")
    (description
      "This package provides a crate of the gitoxide project for shared worktree\nrelated types and utilities.")
    (license (list license:expat license:asl2.0))))

(define rust-libloading-0.8
  (package
    (name "rust-libloading")
    (version "0.8.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libloading" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0d2ccr88f8kv3x7va2ccjxalcjnhrci4j2kwxp7lfmbkpjs4wbzw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-windows-targets"
          ,rust-windows-targets-0.52))))
    (home-page
      "https://github.com/nagisa/rust_libloading/")
    (synopsis
      "Bindings around the platform's dynamic library loading primitives with greatly improved memory safety")
    (description
      "This package provides Bindings around the platform's dynamic library loading primitives with greatly\nimproved memory safety.")
    (license license:isc)))

(define rust-maybe-async-0.2
  (package
    (name "rust-maybe-async")
    (version "0.2.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "maybe-async" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04fvg2ywb2p9dzf7i35xqfibxc05k1pirv36jswxcqg3qw82ryaw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/fMeow/maybe-async-rs")
    (synopsis
      "procedure macro to unify SYNC and ASYNC implementation")
    (description
      "This package provides a procedure macro to unify SYNC and ASYNC implementation.")
    (license license:expat)))

(define rust-nucleo-0.5
  (package
    (name "rust-nucleo")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nucleo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1m1rq0cp02hk31z7jsn2inqcpy9a1j8gfvxcqm32c74jji6ayqjj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-nucleo-matcher" ,rust-nucleo-matcher-0.3)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-rayon" ,rust-rayon-1))))
    (home-page
      "https://github.com/helix-editor/nucleo")
    (synopsis
      "plug and play high performance fuzzy matcher")
    (description
      "This package provides plug and play high performance fuzzy matcher.")
    (license license:mpl2.0)))

(define rust-nucleo-matcher-0.3
  (package
    (name "rust-nucleo-matcher")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nucleo-matcher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11dc5kfin1n561qdcg0x9aflvw876a8vldmqjhs5l6ixfcwgacxz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1))))
    (home-page
      "https://github.com/helix-editor/nucleo")
    (synopsis
      "plug and play high performance fuzzy matcher")
    (description
      "This package provides plug and play high performance fuzzy matcher.")
    (license license:mpl2.0)))

(define rust-open-5
  (package
    (name "rust-open")
    (version "5.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "open" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15ggfx1p8rl7w4rr1n5qj1wxy1kk7757lsjpyc947a9fwri3aj72"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-is-wsl" ,rust-is-wsl-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pathdiff" ,rust-pathdiff-0.2))))
    (home-page "https://github.com/Byron/open-rs")
    (synopsis
      "Open a path or URL using the program configured on the system")
    (description
      "This package provides Open a path or URL using the program configured on the system.")
    (license license:expat)))

(define rust-redox-syscall-0.5
  (package
    (name "rust-redox-syscall")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1916m7abg9649gkif055pn5nsvqjhp70isy0v7gx1zgi01p8m41a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2))))
    (home-page
      "https://gitlab.redox-os.org/redox-os/syscall")
    (synopsis
      "Rust library to access raw Redox system calls")
    (description
      "This package provides a Rust library to access raw Redox system calls.")
    (license license:expat)))

(define rust-regex-automata-0.4
  (package
    (name "rust-regex-automata")
    (version "0.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-automata" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02092l8zfh3vkmk47yjc8d631zhhcd49ck2zr133prvd3z38v7l0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-regex-syntax" ,rust-regex-syntax-0.8))))
    (home-page
      "https://github.com/rust-lang/regex/tree/master/regex-automata")
    (synopsis
      "Automata construction and matching using regular expressions")
    (description
      "This package provides Automata construction and matching using regular expressions.")
    (license (list license:expat license:asl2.0))))

(define rust-rustix-0.38
  (package
    (name "rust-rustix")
    (version "0.38.43")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustix" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xjfhdnmqsbwnfmm77vyh7ldhqx0g9waqm4982404d7jdgp93257"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-2)
         ("rust-errno" ,rust-errno-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-linux-raw-sys" ,rust-linux-raw-sys-0.4)
         ("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page
      "https://github.com/bytecodealliance/rustix")
    (synopsis
      "Safe Rust bindings to POSIX/Unix/Linux/Winsock2-like syscalls")
    (description
      "This package provides Safe Rust bindings to POSIX/Unix/Linux/Winsock2-like syscalls.")
    (license
      (list license:asl2.0
            license:expat))))

(define rust-serde-1
  (package
    (name "rust-serde")
    (version "1.0.217")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w2ck1p1ajmrv1cf51qf7igjn2nc51r0izzc00fzmmhkvxjl5z02"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://serde.rs")
    (synopsis
      "generic serialization/deserialization framework")
    (description
      "This package provides a generic serialization/deserialization framework.")
    (license (list license:expat license:asl2.0))))

(define rust-serde-derive-1
  (package
    (name "rust-serde-derive")
    (version "1.0.217")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "180r3rj5gi5s1m23q66cr5wlfgc5jrs6n1mdmql2njnhk37zg6ss"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://serde.rs")
    (synopsis
      "Macros 1.1 implementation of #[derive(Serialize, Deserialize)]")
    (description
      "This package provides Macros 1.1 implementation of #[derive(Serialize, Deserialize)].")
    (license (list license:expat license:asl2.0))))

(define rust-serde-json-1
  (package
    (name "rust-serde-json")
    (version "1.0.135")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_json" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1n9hcsbxpr2lg5kvlksbcy6bkvqv9rn3hy59600i21kli2i7n39b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-itoa" ,rust-itoa-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-ryu" ,rust-ryu-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/serde-rs/json")
    (synopsis "JSON serialization file format")
    (description
      "This package provides a JSON serialization file format.")
    (license (list license:expat license:asl2.0))))

(define rust-signal-hook-registry-1
  (package
    (name "rust-signal-hook-registry")
    (version "1.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook-registry" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cb5akgq8ajnd5spyn587srvs4n26ryq0p78nswffwhv46sf1sd9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/vorner/signal-hook")
    (synopsis "Backend crate for signal-hook")
    (description
      "This package provides Backend crate for signal-hook.")
    (license (list license:asl2.0 license:expat))))

(define rust-smawk-0.3
  (package
    (name "rust-smawk")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smawk" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0344z1la39incggwn6nl45k8cbw2x10mr5j0qz85cdz9np0qihxp"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/mgeisler/smawk")
    (synopsis
      "Functions for finding row-minima in a totally monotone matrix")
    (description
      "This package provides functions for finding row-minima in a totally monotone\nmatrix.")
    (license license:expat)))

(define rust-tempfile-3
  (package
    (name "rust-tempfile")
    (version "3.15.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tempfile" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "016pmkbwn3shas44gcwq1kc9lajalb90qafhiip5fvv8h6f5b2ls"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-fastrand" ,rust-fastrand-2)
         ("rust-getrandom" ,rust-getrandom-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-rustix" ,rust-rustix-0.38)
         ("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page
      "https://stebalien.com/projects/tempfile-rs/")
    (synopsis
      "library for managing temporary files and directories.")
    (description
      "This package provides a library for managing temporary files and directories.")
    (license (list license:expat license:asl2.0))))

(define rust-thiserror-2
  (package
    (name "rust-thiserror")
    (version "2.0.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1z0649rpa8c2smzx129bz4qvxmdihj30r2km6vfpcv9yny2g4lnl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror-impl" ,rust-thiserror-impl-2))))
    (home-page
      "https://github.com/dtolnay/thiserror")
    (synopsis "derive(Error)")
    (description
      "This package provides derive(Error).")
    (license (list license:expat license:asl2.0))))

(define rust-thiserror-impl-2
  (package
    (name "rust-thiserror-impl")
    (version "2.0.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hkkn7p2y4cxbffcrprybkj0qy1rl1r6waxmxqvr764axaxc3br6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page
      "https://github.com/dtolnay/thiserror")
    (synopsis
      "Implementation detail of the `thiserror` crate")
    (description
      "This package provides Implementation detail of the `thiserror` crate.")
    (license (list license:expat license:asl2.0))))

(define rust-tinyvec-1
  (package
    (name "rust-tinyvec")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinyvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0f5rf6a2wzyv6w4jmfga9iw7rp9fp5gf4d604xgjsf3d9wgqhpj4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinyvec-macros" ,rust-tinyvec-macros-0.1))))
    (home-page "https://github.com/Lokathor/tinyvec")
    (synopsis
      "`tinyvec` provides 100% safe vec-like data structures")
    (description
      "This package provides `tinyvec` provides 100% safe vec-like data structures.")
    (license
      (list license:zlib license:asl2.0 license:expat))))

(define rust-tokio-1
  (package
    (name "rust-tokio")
    (version "1.43.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17pdm49ihlhfw3rpxix3kdh2ppl1yv7nwp1kxazi5r1xz97zlq9x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-mio" ,rust-mio-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-signal-hook-registry"
          ,rust-signal-hook-registry-1)
         ("rust-socket2" ,rust-socket2-0.5)
         ("rust-tokio-macros" ,rust-tokio-macros-2)
         ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "https://tokio.rs")
    (synopsis
      "An event-driven, non-blocking I/O platform for writing asynchronous I/O\nbacked applications.")
    (description
      "This package provides An event-driven, non-blocking I/O platform for writing asynchronous I/O backed\napplications.")
    (license license:expat)))

(define rust-tokio-macros-2
  (package
    (name "rust-tokio-macros")
    (version "2.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1f6az2xbvqp7am417b78d1za8axbvjvxnmkakz9vr8s52czx81kf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-2))))
    (home-page "https://tokio.rs")
    (synopsis "Tokio's proc macros.")
    (description
      "This package provides Tokio's proc macros.")
    (license license:expat)))

(define rust-tree-sitter-0.22
  (package
    (name "rust-tree-sitter")
    (version "0.22.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tree-sitter" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jkda5n43m7cxmx2h7l20zxc74nf9v1wpm66gvgxrm5drscw8z6z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-regex" ,rust-regex-1))))
    (home-page
      "https://tree-sitter.github.io/tree-sitter")
    (synopsis
      "Rust bindings to the Tree-sitter parsing library")
    (description
      "This package provides Rust bindings to the Tree-sitter parsing library.")
    (license license:expat)))

(define rust-unicode-general-category-1
  (package
    (name "rust-unicode-general-category")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-general-category" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ls6j9faq62kqnvzcxjdjgjfcqcif8jz3pvag83nfja3261zxb94"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/yeslogic/unicode-general-category")
    (synopsis
      "Fast lookup of the Unicode General Category property for char")
    (description
      "This package provides Fast lookup of the Unicode General Category property for char.")
    (license license:asl2.0)))

(define rust-unicode-width-0.1.12
  (package
    (inherit rust-unicode-width-0.2)
    (name "rust-unicode-width")
    (version "0.1.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-width" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mk6mybsmi5py8hf8zy9vbgs4rw4gkdqdq3gzywd9kwf2prybxb8"))))
    (arguments
      `(#:cargo-inputs
        (("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
         ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
         ("rust-rustc-std-workspace-std" ,rust-rustc-std-workspace-std-1))))))

(define helix
  (package/inherit guix:helix
    (name "helix")
    (version "25.01.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/helix-editor/helix")
               (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1i9g1xk4vs11ccsrvdjz33yprcsim9ag045vifm7rx03hzcxfry0"))))
    (arguments
      (substitute-keyword-arguments (package-arguments guix:helix)
        ((#:cargo-inputs cargo-inputs)
         (list rust-ahash-0.8
               rust-anyhow-1
               rust-arc-swap-1
               rust-bitflags-2
               rust-cassowary-0.3
               rust-cc-1
               rust-chardetng-0.1
               rust-chrono-0.4
               rust-clipboard-win-5
               rust-content-inspector-0.2
               rust-crossterm-0.28
               rust-dunce-1
               rust-encoding-rs-0.8
               rust-etcetera-0.8
               rust-fern-0.7
               rust-futures-executor-0.3
               rust-futures-util-0.3
               rust-gix-0.69
               rust-globset-0.4
               rust-grep-regex-0.1
               rust-grep-searcher-0.1
               rust-hashbrown-0.14
               rust-ignore-0.4
               rust-imara-diff-0.1
               rust-indoc-2
               rust-libc-0.2
               rust-libloading-0.8
               rust-log-0.4
               rust-nucleo-0.5
               rust-once-cell-1
               rust-open-5
               rust-parking-lot-0.12
               rust-pulldown-cmark-0.12
               rust-quickcheck-1
               rust-regex-1
               rust-regex-automata-0.4
               rust-regex-cursor-0.1
               rust-ropey-1
               rust-rustix-0.38
               rust-same-file-1
               rust-serde-1
               rust-serde-json-1
               rust-serde-repr-0.1
               rust-signal-hook-0.3
               rust-signal-hook-tokio-0.3
               rust-slotmap-1
               rust-smallvec-1
               rust-smartstring-1
               rust-tempfile-3
               rust-termini-1
               rust-textwrap-0.16
               rust-thiserror-2
               rust-threadpool-1
               rust-tokio-1
               rust-tokio-stream-0.1
               rust-toml-0.8
               rust-tree-sitter-0.22
               rust-unicode-general-category-1
               rust-unicode-segmentation-1
               rust-unicode-width-0.1.12
               rust-url-2
               rust-which-7
               rust-windows-sys-0.59))))))

helix
