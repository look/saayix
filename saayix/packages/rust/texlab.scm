(define-module (saayix packages rust texlab)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define %texlab-version "5.16.0")

(define rust-adler-1.0.2
  (package
    (name "rust-adler")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "adler" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zim79cvzd5yrkzl3nyfx0avijwgk9fqv3yrscdy1cc79ih02qpj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-aho-corasick-1.1.1
  (package
    (name "rust-aho-corasick")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aho-corasick" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1aqqalh66jygy54fbnpglzrb9dwlrvn6zl1nhncdvynl8w376pga"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2.7.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anes-0.1.6
  (package
    (name "rust-anes")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16bj1ww1xkwzbckk32j2pnbn5vk6wgsl3q4p3j9551xbcarwnijb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstream-0.6.11
  (package
    (name "rust-anstream")
    (version "0.6.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstream" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19dndamalavhjwp4i74k8hdijcixb7gsfa6ycwyc1r8xn6y1wbkf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-utf8parse" ,rust-utf8parse-0.2.1)
         ("rust-colorchoice" ,rust-colorchoice-1.0.0)
         ("rust-anstyle-wincon"
          ,rust-anstyle-wincon-3.0.1)
         ("rust-anstyle-query" ,rust-anstyle-query-1.0.0)
         ("rust-anstyle-parse" ,rust-anstyle-parse-0.2.1)
         ("rust-anstyle" ,rust-anstyle-1.0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstyle-1.0.3
  (package
    (name "rust-anstyle")
    (version "1.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ihfi7r8m3dkysxm5zrm7hchiakvx2v6p8vgxgjq6amvbfhg0jxq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstyle-parse-0.2.1
  (package
    (name "rust-anstyle-parse")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cy38fbdlnmwyy6q8dn8dcfrpycwnpjkljsjqn3kmc40b7zp924k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-utf8parse" ,rust-utf8parse-0.2.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstyle-query-1.0.0
  (package
    (name "rust-anstyle-query")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-query" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0js9bgpqz21g0p2nm350cba1d0zfyixsma9lhyycic5sw55iv8aw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anstyle-wincon-3.0.1
  (package
    (name "rust-anstyle-wincon")
    (version "3.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anstyle-wincon" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0a066gr4p7bha8qwnxyrpbrqzjdvk8l7pdg7isljimpls889ssgh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48.0)
         ("rust-anstyle" ,rust-anstyle-1.0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anyhow-1.0.82
  (package
    (name "rust-anyhow")
    (version "1.0.82")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anyhow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06952ih07mhfnim7r1mmwkj1k0ag66d7z9psw2dnlvvfydx86f7m"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-autocfg-1.1.0
  (package
    (name "rust-autocfg")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "autocfg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ylp3cb47ylzabimazvbz9ms6ap784zhb6syaz6c1jqpmcmq0s6l"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-base-db-5.16.0
  (package
    (name "rust-base-db")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-url" ,rust-url-2.5.0)
         ("rust-titlecase" ,rust-titlecase-3.0.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-shellexpand" ,rust-shellexpand-3.1.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-regex" ,rust-regex-1.10.4)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2.3.1)
         ("rust-parser" ,rust-parser-5.16.0)
         ("rust-once-cell" ,rust-once-cell-1.19.0)
         ("rust-notify" ,rust-notify-6.1.1)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-line-index" ,rust-line-index-5.16.0)
         ("rust-itertools" ,rust-itertools-0.12.1)
         ("rust-distro" ,rust-distro-5.16.0)
         ("rust-dirs" ,rust-dirs-5.0.1)
         ("rust-bibtex-utils" ,rust-bibtex-utils-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-beef-0.5.2
  (package
    (name "rust-beef")
    (version "0.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "beef" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c95lbnhld96iwwbyh5kzykbpysq0fnjfhwxa1mhap5qxgrl30is"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bibfmt-5.16.0
  (package
    (name "rust-bibfmt")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-parser" ,rust-parser-5.16.0)
         ("rust-line-index" ,rust-line-index-5.16.0)
         ("rust-expect-test" ,rust-expect-test-1.5.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bibtex-utils-5.16.0
  (package
    (name "rust-bibtex-utils")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-normalization"
          ,rust-unicode-normalization-0.1.23)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-parser" ,rust-parser-5.16.0)
         ("rust-itertools" ,rust-itertools-0.12.1)
         ("rust-human-name" ,rust-human-name-2.0.3)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-chrono" ,rust-chrono-0.4.31))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bitflags-1.3.2
  (package
    (name "rust-bitflags")
    (version "1.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitflags" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12ki6w8gn1ldq7yz9y680llwk5gmrhrzszaa17g1sbrw2r2qvwxy"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bitflags-2.4.0
  (package
    (name "rust-bitflags")
    (version "2.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitflags" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dc6xa7flfl59makmhixjcrslwlvdxxwrgxbr8p7bkvz53k2ls5l"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bstr-1.9.1
  (package
    (name "rust-bstr")
    (version "1.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bstr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01ipr5rncw3kf4dyc1p2g00njn1df2b0xpviwhb8830iv77wbvq5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1.0.199)
         ("rust-regex-automata"
          ,rust-regex-automata-0.4.6)
         ("rust-memchr" ,rust-memchr-2.7.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bumpalo-3.14.0
  (package
    (name "rust-bumpalo")
    (version "3.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bumpalo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1v4arnv9kwk54v5d0qqpv4vyw2sgr660nk0w3apzixi1cm3yfc3z"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cast-0.3.0
  (package
    (name "rust-cast")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cast" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1dbyngbyz2qkk0jn2sxil8vrz3rnpcj142y184p9l4nbl9radcip"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-castaway-0.2.2
  (package
    (name "rust-castaway")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "castaway" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k1z4v61vq7la56js1azkr0k9b415vif2kaxiqk3d1gw6mbfs5wa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustversion" ,rust-rustversion-1.0.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cfg-if-1.0.0
  (package
    (name "rust-cfg-if")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cfg-if" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1za0vb97n4brpzpv8lsbnzmq5r8f2b0cpqqr0sy8h5bn751xxwds"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-chrono-0.4.31
  (package
    (name "rust-chrono")
    (version "0.4.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chrono" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0f6vg67pipm8cziad2yms6a639pssnvysk1m05dd9crymmdnhb3z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-traits" ,rust-num-traits-0.2.16))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ciborium-0.2.1
  (package
    (name "rust-ciborium")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ciborium" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09p9gr3jxys51v0fzwsmxym2p7pcz9mhng2xib74lnlfqzv93zgg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1.0.199)
         ("rust-ciborium-ll" ,rust-ciborium-ll-0.2.1)
         ("rust-ciborium-io" ,rust-ciborium-io-0.2.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ciborium-io-0.2.1
  (package
    (name "rust-ciborium-io")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ciborium-io" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mi6ci27lpz3azksxrvgzl9jc4a3dfr20pjx7y2nkcrjalbikyfd"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ciborium-ll-0.2.1
  (package
    (name "rust-ciborium-ll")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ciborium-ll" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0az2vabamfk75m74ylgf6nzqgqgma5yf25bc1ripfg09ri7a5yny"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-half" ,rust-half-1.8.2)
         ("rust-ciborium-io" ,rust-ciborium-io-0.2.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-citeproc-5.16.0
  (package
    (name "rust-citeproc")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-url" ,rust-url-2.5.0)
         ("rust-unicode-normalization"
          ,rust-unicode-normalization-0.1.23)
         ("rust-titlecase" ,rust-titlecase-3.0.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-parser" ,rust-parser-5.16.0)
         ("rust-itertools" ,rust-itertools-0.12.1)
         ("rust-isocountry" ,rust-isocountry-0.3.2)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-bibtex-utils" ,rust-bibtex-utils-5.16.0)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-4.5.4
  (package
    (name "rust-clap")
    (version "4.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1828wm9qws5gh2xnimnvmp2vria6d6hsxnqmhnm84dwjcxm0dg4h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap-derive" ,rust-clap-derive-4.5.4)
         ("rust-clap-builder" ,rust-clap-builder-4.5.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-builder-4.5.2
  (package
    (name "rust-clap-builder")
    (version "4.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_builder" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1d7p4hph4fyhaphkf0v5zv0kq4lz25a9jq2f901yrq3afqp9w4mf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-strsim" ,rust-strsim-0.11.0)
         ("rust-clap-lex" ,rust-clap-lex-0.7.0)
         ("rust-anstyle" ,rust-anstyle-1.0.3)
         ("rust-anstream" ,rust-anstream-0.6.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-derive-4.5.4
  (package
    (name "rust-clap-derive")
    (version "4.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0r2gs2p10pb435w52xzsgz2mmx5qd3qfkmk29y4mbz9ph11k30aj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-2.0.48)
         ("rust-quote" ,rust-quote-1.0.35)
         ("rust-proc-macro2" ,rust-proc-macro2-1.0.76)
         ("rust-heck" ,rust-heck-0.5.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-lex-0.7.0
  (package
    (name "rust-clap-lex")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_lex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kh1sckgq71kay2rrr149pl9gbsrvyccsq6xm5xpnq0cxnyqzk4q"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-colorchoice-1.0.0
  (package
    (name "rust-colorchoice")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "colorchoice" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ix7w85kwvyybwi2jdkl3yva2r2bvdcc3ka2grjfzfgrapqimgxc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-commands-5.16.0
  (package
    (name "rust-commands")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-url" ,rust-url-2.5.0)
         ("rust-thiserror" ,rust-thiserror-1.0.59)
         ("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-libc" ,rust-libc-0.2.153)
         ("rust-itertools" ,rust-itertools-0.12.1)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-distro" ,rust-distro-5.16.0)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5.12)
         ("rust-bstr" ,rust-bstr-1.9.1)
         ("rust-base-db" ,rust-base-db-5.16.0)
         ("rust-anyhow" ,rust-anyhow-1.0.82))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-compact-str-0.7.1
  (package
    (name "rust-compact-str")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "compact_str" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gvvfc2c6pg1rwr2w36ra4674w3lzwg97vq2v6k791w30169qszq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-static-assertions"
          ,rust-static-assertions-1.1.0)
         ("rust-serde" ,rust-serde-1.0.199)
         ("rust-ryu" ,rust-ryu-1.0.15)
         ("rust-itoa" ,rust-itoa-1.0.9)
         ("rust-cfg-if" ,rust-cfg-if-1.0.0)
         ("rust-castaway" ,rust-castaway-0.2.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-completion-5.16.0
  (package
    (name "rust-completion")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-url" ,rust-url-2.5.0)
         ("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-rayon" ,rust-rayon-1.10.0)
         ("rust-line-index" ,rust-line-index-5.16.0)
         ("rust-fuzzy-matcher" ,rust-fuzzy-matcher-0.3.7)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-distro" ,rust-distro-5.16.0)
         ("rust-criterion" ,rust-criterion-0.5.1)
         ("rust-completion-data"
          ,rust-completion-data-5.16.0)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-completion-data-5.16.0
  (package
    (name "rust-completion-data")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde-json" ,rust-serde-json-1.0.116)
         ("rust-serde" ,rust-serde-1.0.199)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-once-cell" ,rust-once-cell-1.19.0)
         ("rust-itertools" ,rust-itertools-0.12.1)
         ("rust-flate2" ,rust-flate2-1.0.29))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-countme-3.0.1
  (package
    (name "rust-countme")
    (version "3.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "countme" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dn62hhvgmwyxslh14r4nlbvz8h50cp5mnn1qhqsw63vs7yva13p"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crc32fast-1.3.2
  (package
    (name "rust-crc32fast")
    (version "1.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crc32fast" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03c8f29yx293yf43xar946xbls1g60c207m9drf8ilqhr25vsh5m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-criterion-0.5.1
  (package
    (name "rust-criterion")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "criterion" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bv9ipygam3z8kk6k771gh9zi0j0lb9ir0xi1pc075ljg80jvcgj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-walkdir" ,rust-walkdir-2.4.0)
         ("rust-tinytemplate" ,rust-tinytemplate-1.2.1)
         ("rust-serde-json" ,rust-serde-json-1.0.116)
         ("rust-serde-derive" ,rust-serde-derive-1.0.199)
         ("rust-serde" ,rust-serde-1.0.199)
         ("rust-regex" ,rust-regex-1.10.4)
         ("rust-rayon" ,rust-rayon-1.10.0)
         ("rust-plotters" ,rust-plotters-0.3.5)
         ("rust-oorandom" ,rust-oorandom-11.1.3)
         ("rust-once-cell" ,rust-once-cell-1.19.0)
         ("rust-num-traits" ,rust-num-traits-0.2.16)
         ("rust-itertools" ,rust-itertools-0.10.5)
         ("rust-is-terminal" ,rust-is-terminal-0.4.9)
         ("rust-criterion-plot"
          ,rust-criterion-plot-0.5.0)
         ("rust-clap" ,rust-clap-4.5.4)
         ("rust-ciborium" ,rust-ciborium-0.2.1)
         ("rust-cast" ,rust-cast-0.3.0)
         ("rust-anes" ,rust-anes-0.1.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-criterion-plot-0.5.0
  (package
    (name "rust-criterion-plot")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "criterion-plot" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c866xkjqqhzg4cjvg01f8w6xc1j3j7s58rdksl52skq89iq4l3b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-itertools" ,rust-itertools-0.10.5)
         ("rust-cast" ,rust-cast-0.3.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-channel-0.5.12
  (package
    (name "rust-crossbeam-channel")
    (version "0.5.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-channel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "159gp30clv2ci3r473659ii04pjznspb3g9gwkhj2lavkhmb0gdb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8.19))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-deque-0.8.3
  (package
    (name "rust-crossbeam-deque")
    (version "0.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-deque" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vqczbcild7nczh5z116w8w46z991kpjyw7qxkf24c14apwdcvyf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8.19)
         ("rust-crossbeam-epoch"
          ,rust-crossbeam-epoch-0.9.15)
         ("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-epoch-0.9.15
  (package
    (name "rust-crossbeam-epoch")
    (version "0.9.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-epoch" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ixwc3cq816wb8rlh3ix4jnybqbyyq4l61nwlx0mfm3ck0s148df"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-scopeguard" ,rust-scopeguard-1.2.0)
         ("rust-memoffset" ,rust-memoffset-0.9.0)
         ("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8.19)
         ("rust-cfg-if" ,rust-cfg-if-1.0.0)
         ("rust-autocfg" ,rust-autocfg-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-utils-0.8.19
  (package
    (name "rust-crossbeam-utils")
    (version "0.8.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-utils" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0iakrb1b8fjqrag7wphl94d10irhbh2fw1g444xslsywqyn3p3i4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-definition-5.16.0
  (package
    (name "rust-definition")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-itertools" ,rust-itertools-0.12.1)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-diagnostics-5.16.0
  (package
    (name "rust-diagnostics")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-url" ,rust-url-2.5.0)
         ("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-regex" ,rust-regex-1.10.4)
         ("rust-once-cell" ,rust-once-cell-1.19.0)
         ("rust-multimap" ,rust-multimap-0.10.0)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-line-index" ,rust-line-index-5.16.0)
         ("rust-itertools" ,rust-itertools-0.12.1)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-encoding-rs-io"
          ,rust-encoding-rs-io-0.1.7)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8.34)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dirs-5.0.1
  (package
    (name "rust-dirs")
    (version "5.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dirs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0992xk5vx75b2x91nw9ssb51mpl8x73j9rxmpi96cryn0ffmmi24"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dirs-sys" ,rust-dirs-sys-0.4.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dirs-sys-0.4.1
  (package
    (name "rust-dirs-sys")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dirs-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "071jy0pvaad9lsa6mzawxrh7cmr7hsmsdxwzm7jzldfkrfjha3sj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48.0)
         ("rust-redox-users" ,rust-redox-users-0.4.3)
         ("rust-option-ext" ,rust-option-ext-0.2.0)
         ("rust-libc" ,rust-libc-0.2.153))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dissimilar-1.0.7
  (package
    (name "rust-dissimilar")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dissimilar" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cn6i035s4hsvv078lg3alsfwjy0k2y7zy5hnsr1cvpf1v4bvqw6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-distro-5.16.0
  (package
    (name "rust-distro")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-anyhow" ,rust-anyhow-1.0.82))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-either-1.9.0
  (package
    (name "rust-either")
    (version "1.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "either" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01qy3anr7jal5lpc20791vxrw0nl6vksb5j7x56q2fycgcyy8sm2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-encoding-rs-0.8.34
  (package
    (name "rust-encoding-rs")
    (version "0.8.34")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "encoding_rs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nagpi1rjqdpvakymwmnlxzq908ncg868lml5b70n08bm82fjpdl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-encoding-rs-io-0.1.7
  (package
    (name "rust-encoding-rs-io")
    (version "0.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "encoding_rs_io" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10ra4l688cdadd8h1lsbahld1zbywnnqv68366mbhamn3xjwbhqw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-encoding-rs" ,rust-encoding-rs-0.8.34))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-errno-0.3.8
  (package
    (name "rust-errno")
    (version "0.3.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "errno" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ia28ylfsp36i27g1qih875cyyy4by2grf80ki8vhgh6vinf8n52"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.52.0)
         ("rust-libc" ,rust-libc-0.2.153))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-expect-test-1.5.0
  (package
    (name "rust-expect-test")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "expect-test" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1q55nrkgzg345905aqbsdrwlq4sk0gjn4z5bdph1an1kc6jy02wy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1.19.0)
         ("rust-dissimilar" ,rust-dissimilar-1.0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fastrand-2.0.1
  (package
    (name "rust-fastrand")
    (version "2.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fastrand" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19flpv5zbzpf0rk4x77z4zf25in0brg8l7m304d3yrf47qvwxjr5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fern-0.6.2
  (package
    (name "rust-fern")
    (version "0.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fern" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vpinainw32498p0zydmxc24yd3r6479pmhdfb429mfbji3c3w6r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4.21))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-file-id-0.2.1
  (package
    (name "rust-file-id")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "file-id" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jdg9xq830hghzrqkbnx8nda58a7z6mh8b6vlg5mj87v4l2ji135"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-filetime-0.2.22
  (package
    (name "rust-filetime")
    (version "0.2.22")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "filetime" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1w1a4zb4ciqjl1chvp9dplbacq07jv97pkdn0pzackbk7vfrw0nl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48.0)
         ("rust-redox-syscall" ,rust-redox-syscall-0.3.5)
         ("rust-libc" ,rust-libc-0.2.153)
         ("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-flate2-1.0.29
  (package
    (name "rust-flate2")
    (version "1.0.29")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "flate2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rw5bzdklf1611zr06jd1idf8092ypc30qdf2ws7lnv370kj4mj5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-miniz-oxide" ,rust-miniz-oxide-0.7.1)
         ("rust-crc32fast" ,rust-crc32fast-1.3.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fnv-1.0.7
  (package
    (name "rust-fnv")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fnv" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hc2mcqha06aibcaza94vbi81j6pr9a1bbxrxjfhc91zin8yr7iz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-folding-5.16.0
  (package
    (name "rust-folding")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-form-urlencoded-1.2.1
  (package
    (name "rust-form-urlencoded")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "form_urlencoded" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0milh8x7nl4f450s3ddhg57a3flcv6yq8hlkyk6fyr3mcb128dp1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-percent-encoding"
          ,rust-percent-encoding-2.3.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fsevent-sys-4.1.0
  (package
    (name "rust-fsevent-sys")
    (version "4.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fsevent-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1liz67v8b0gcs8r31vxkvm2jzgl9p14i78yfqx81c8sdv817mvkn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2.153))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fuzzy-matcher-0.3.7
  (package
    (name "rust-fuzzy-matcher")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fuzzy-matcher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "153csv8rsk2vxagb68kpmiknvdd3bzqj03x805khckck28rllqal"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thread-local" ,rust-thread-local-1.1.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-getrandom-0.2.10
  (package
    (name "rust-getrandom")
    (version "0.2.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "getrandom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09zlimhhskzf7cmgcszix05wyz2i6fcpvh711cv1klsxl6r3chdy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasi"
          ,rust-wasi-0.11.0+wasi-snapshot-preview1)
         ("rust-libc" ,rust-libc-0.2.153)
         ("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-half-1.8.2
  (package
    (name "rust-half")
    (version "1.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "half" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mqbmx2m9qd4lslkb42fzgldsklhv9c4bxsc8j82r80d8m24mfza"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hashbrown-0.14.3
  (package
    (name "rust-hashbrown")
    (version "0.14.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashbrown" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "012nywlg0lj9kwanh69my5x67vjlfmzfi9a0rq4qvis2j8fil3r9"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-heck-0.5.0
  (package
    (name "rust-heck")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "heck" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1sjmpsdl8czyh9ywl3qcsfsq9a307dg4ni2vnlwgnzzqhc4y0113"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hermit-abi-0.3.3
  (package
    (name "rust-hermit-abi")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hermit-abi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1dyc8qsjh876n74a3rcz8h43s27nj1sypdhsn2ms61bd3b47wzyp"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-highlights-5.16.0
  (package
    (name "rust-highlights")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hover-5.16.0
  (package
    (name "rust-hover")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-completion-data"
          ,rust-completion-data-5.16.0)
         ("rust-citeproc" ,rust-citeproc-5.16.0)
         ("rust-bibtex-utils" ,rust-bibtex-utils-5.16.0)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-human-name-2.0.3
  (package
    (name "rust-human-name")
    (version "2.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "human_name" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fcr2kcbagfvmz9yq8wlgp8vp0dlrfqdww2h3pmlj6f33fhx6h3x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unidecode" ,rust-unidecode-0.3.0)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1.10.1)
         ("rust-unicode-normalization"
          ,rust-unicode-normalization-0.1.23)
         ("rust-unicode-case-mapping"
          ,rust-unicode-case-mapping-0.4.0)
         ("rust-smallvec" ,rust-smallvec-1.11.1)
         ("rust-serde-json" ,rust-serde-json-1.0.116)
         ("rust-serde" ,rust-serde-1.0.199)
         ("rust-phf-codegen" ,rust-phf-codegen-0.11.2)
         ("rust-phf" ,rust-phf-0.11.2)
         ("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8.19)
         ("rust-compact-str" ,rust-compact-str-0.7.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-idna-0.5.0
  (package
    (name "rust-idna")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "idna" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xhjrcjqq0l5bpzvdgylvpkgk94panxgsirzhjnnqfdgc4a9nkb3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-normalization"
          ,rust-unicode-normalization-0.1.23)
         ("rust-unicode-bidi" ,rust-unicode-bidi-0.3.13))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-inlay-hints-5.16.0
  (package
    (name "rust-inlay-hints")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-inotify-0.9.6
  (package
    (name "rust-inotify")
    (version "0.9.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inotify" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zxb04c4qccp8wnr3v04l503qpxzxzzzph61amlqbsslq4z9s1pq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2.153)
         ("rust-inotify-sys" ,rust-inotify-sys-0.1.5)
         ("rust-bitflags" ,rust-bitflags-1.3.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-inotify-sys-0.1.5
  (package
    (name "rust-inotify-sys")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inotify-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1syhjgvkram88my04kv03s0zwa66mdwa5v7ddja3pzwvx2sh4p70"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2.153))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ipc-5.16.0
  (package
    (name "rust-ipc")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-uds-windows" ,rust-uds-windows-1.1.0)
         ("rust-serde-json" ,rust-serde-json-1.0.116)
         ("rust-serde" ,rust-serde-1.0.199)
         ("rust-parser" ,rust-parser-5.16.0)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-is-terminal-0.4.9
  (package
    (name "rust-is-terminal")
    (version "0.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "is-terminal" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12xgvc7nsrp3pn8hcxajfhbli2l5wnh3679y2fmky88nhj4qj26b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48.0)
         ("rust-rustix" ,rust-rustix-0.38.31)
         ("rust-hermit-abi" ,rust-hermit-abi-0.3.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-isocountry-0.3.2
  (package
    (name "rust-isocountry")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "isocountry" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "011y6sb6rs2i85g2jvifx5s54clw7nprinzzhfx08jgvy15xr88y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror" ,rust-thiserror-1.0.59)
         ("rust-serde" ,rust-serde-1.0.199))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-itertools-0.10.5
  (package
    (name "rust-itertools")
    (version "0.10.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itertools" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ww45h7nxx5kj6z2y6chlskxd1igvs4j507anr6dzg99x1h25zdh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-either" ,rust-either-1.9.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-itertools-0.12.1
  (package
    (name "rust-itertools")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itertools" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0s95jbb3ndj1lvfxyq5wanc0fm0r6hg6q4ngb92qlfdxvci10ads"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-either" ,rust-either-1.9.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-itoa-1.0.9
  (package
    (name "rust-itoa")
    (version "1.0.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itoa" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0f6cpb4yqzhkrhhg6kqsw3wnmmhdnnffi6r2xzy248gzi2v0l5dg"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-js-sys-0.3.64
  (package
    (name "rust-js-sys")
    (version "0.3.64")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "js-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nlkiwpm8dyqcf1xyc6qmrankcgdd3fpzc0qyfq2sw3z97z9bwf5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2.87))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kqueue-1.0.8
  (package
    (name "rust-kqueue")
    (version "1.0.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kqueue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "033x2knkbv8d3jy6i9r32jcgsq6zm3g97zh5la43amkv3g5g2ivl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2.153)
         ("rust-kqueue-sys" ,rust-kqueue-sys-1.0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kqueue-sys-1.0.4
  (package
    (name "rust-kqueue-sys")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kqueue-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12w3wi90y4kwis4k9g6fp0kqjdmc6l00j16g8mgbhac7vbzjb5pd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2.153)
         ("rust-bitflags" ,rust-bitflags-1.3.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lazy-static-1.4.0
  (package
    (name "rust-lazy-static")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lazy_static" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0in6ikhw8mgl33wjv6q6xfrb5b9jr16q8ygjy803fay4zcisvaz2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-libc-0.2.153
  (package
    (name "rust-libc")
    (version "0.2.153")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gg7m1ils5dms5miq9fyllrcp0jxnbpgkx71chd2i0lafa8qy6cw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-line-index-5.16.0
  (package
    (name "rust-line-index")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-text-size" ,rust-text-size-1.1.1)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-links-5.16.0
  (package
    (name "rust-links")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-linux-raw-sys-0.4.13
  (package
    (name "rust-linux-raw-sys")
    (version "0.4.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "linux-raw-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "172k2c6422gsc914ig8rh99mb9yc7siw6ikc3d9xw1k7vx0s3k81"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lock-api-0.4.10
  (package
    (name "rust-lock-api")
    (version "0.4.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lock_api" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05nd9nzxqidg24d1k8y5vlc8lz9gscpskrikycib46qbl8brgk61"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-scopeguard" ,rust-scopeguard-1.2.0)
         ("rust-autocfg" ,rust-autocfg-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-log-0.4.21
  (package
    (name "rust-log")
    (version "0.4.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "log" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "074hldq1q8rlzq2s2qa8f25hj4s3gpw71w64vdwzjd01a4g8rvch"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-logos-0.14.0
  (package
    (name "rust-logos")
    (version "0.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "logos" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1s227bgfl528vfw11dxngxz75df5cws0dq9kqgh7mnm0i3mp268n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-logos-derive" ,rust-logos-derive-0.14.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-logos-codegen-0.14.0
  (package
    (name "rust-logos-codegen")
    (version "0.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "logos-codegen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0amixv1zfk8fnp793w5p25dxhgby8lylgxi197giy4z5kpfvlccf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-2.0.48)
         ("rust-regex-syntax" ,rust-regex-syntax-0.8.2)
         ("rust-quote" ,rust-quote-1.0.35)
         ("rust-proc-macro2" ,rust-proc-macro2-1.0.76)
         ("rust-lazy-static" ,rust-lazy-static-1.4.0)
         ("rust-fnv" ,rust-fnv-1.0.7)
         ("rust-beef" ,rust-beef-0.5.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-logos-derive-0.14.0
  (package
    (name "rust-logos-derive")
    (version "0.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "logos-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02nyh2nfsk881xdwh31nbsxy4zz0sxcfxj87a5cvvmb8xfrnjahw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-logos-codegen" ,rust-logos-codegen-0.14.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lsp-server-0.7.6
  (package
    (name "rust-lsp-server")
    (version "0.7.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lsp-server" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15bhdhkinhhw5fifrpmiiqdd4hwblac40jv0n7hxidbdiyvnb3r4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde-json" ,rust-serde-json-1.0.116)
         ("rust-serde" ,rust-serde-1.0.199)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lsp-types-0.95.1
  (package
    (name "rust-lsp-types")
    (version "0.95.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lsp-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ra36fd4yr7lf5igfrdvwjx9g87z3a99mrjgzk9nq04viqxd6d4f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-url" ,rust-url-2.5.0)
         ("rust-serde-repr" ,rust-serde-repr-0.1.18)
         ("rust-serde-json" ,rust-serde-json-1.0.116)
         ("rust-serde" ,rust-serde-1.0.199)
         ("rust-bitflags" ,rust-bitflags-1.3.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-memchr-2.7.1
  (package
    (name "rust-memchr")
    (version "2.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memchr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jf1kicqa4vs9lyzj4v4y1p90q0dh87hvhsdd5xvhnp527sw8gaj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-memoffset-0.9.0
  (package
    (name "rust-memoffset")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memoffset" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0v20ihhdzkfw1jx00a7zjpk2dcp5qjq6lz302nyqamd9c4f4nqss"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-minimal-lexical-0.2.1
  (package
    (name "rust-minimal-lexical")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "minimal-lexical" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16ppc5g84aijpri4jzv14rvcnslvlpphbszc7zzp6vfkddf4qdb8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-miniz-oxide-0.7.1
  (package
    (name "rust-miniz-oxide")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "miniz_oxide" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ivl3rbbdm53bzscrd01g60l46lz5krl270487d8lhjvwl5hx0g7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-adler" ,rust-adler-1.0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mio-0.8.11
  (package
    (name "rust-mio")
    (version "0.8.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "034byyl0ardml5yliy1hmvx8arkmn9rv479pid794sm07ia519m4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48.0)
         ("rust-wasi"
          ,rust-wasi-0.11.0+wasi-snapshot-preview1)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-libc" ,rust-libc-0.2.153))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-multimap-0.10.0
  (package
    (name "rust-multimap")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "multimap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00vs2frqdhrr8iqx4y3fbq73ax5l12837fvbjrpi729d85alrz6y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1.0.199))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-nom-7.1.3
  (package
    (name "rust-nom")
    (version "7.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jha9901wxam390jcf5pfa0qqfrgh8li787jx2ip0yk5b8y9hwyj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-minimal-lexical"
          ,rust-minimal-lexical-0.2.1)
         ("rust-memchr" ,rust-memchr-2.7.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-notify-6.1.1
  (package
    (name "rust-notify")
    (version "6.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "notify" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bad98r0ilkhhq2jg3zs11zcqasgbvxia8224wpasm74n65vs1b2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48.0)
         ("rust-walkdir" ,rust-walkdir-2.4.0)
         ("rust-mio" ,rust-mio-0.8.11)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-libc" ,rust-libc-0.2.153)
         ("rust-kqueue" ,rust-kqueue-1.0.8)
         ("rust-inotify" ,rust-inotify-0.9.6)
         ("rust-fsevent-sys" ,rust-fsevent-sys-4.1.0)
         ("rust-filetime" ,rust-filetime-0.2.22)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5.12)
         ("rust-bitflags" ,rust-bitflags-2.4.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-notify-debouncer-full-0.3.1
  (package
    (name "rust-notify-debouncer-full")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "notify-debouncer-full" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0m31ad5wv0lhrncn6qqk4zmryf0fl9h1j9kzrx89p2rlkjsxmxa9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-walkdir" ,rust-walkdir-2.4.0)
         ("rust-parking-lot" ,rust-parking-lot-0.12.1)
         ("rust-notify" ,rust-notify-6.1.1)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-file-id" ,rust-file-id-0.2.1)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-num-traits-0.2.16
  (package
    (name "rust-num-traits")
    (version "0.2.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-traits" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hp6x4gayrib34y14gpcfx60hbqsmh7i8whjrbzy5rrvfayhl2zk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1.1.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-num-cpus-1.16.0
  (package
    (name "rust-num-cpus")
    (version "1.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_cpus" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hra6ihpnh06dvfvz9ipscys0xfqa9ca9hzp384d5m02ssvgqqa1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2.153)
         ("rust-hermit-abi" ,rust-hermit-abi-0.3.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-once-cell-1.19.0
  (package
    (name "rust-once-cell")
    (version "1.19.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "once_cell" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14kvw7px5z96dk4dwdm1r9cqhhy2cyj1l5n5b29mynbb8yr15nrz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-oorandom-11.1.3
  (package
    (name "rust-oorandom")
    (version "11.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "oorandom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xdm4vd89aiwnrk1xjwzklnchjqvib4klcihlc2bsd4x50mbrc8a"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-option-ext-0.2.0
  (package
    (name "rust-option-ext")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "option-ext" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zbf7cx8ib99frnlanpyikm1bx8qn8x602sw1n7bg6p9x94lyx04"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parking-lot-0.12.1
  (package
    (name "rust-parking-lot")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13r2xk7mnxfc5g0g6dkdxqdqad99j7s7z8zhzz4npw5r0g0v4hip"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-parking-lot-core"
          ,rust-parking-lot-core-0.9.8)
         ("rust-lock-api" ,rust-lock-api-0.4.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parking-lot-core-0.9.8
  (package
    (name "rust-parking-lot-core")
    (version "0.9.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ixlak319bpzldq20yvyfqk0y1vi736zxbw101jvzjp7by30rw4k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-targets"
          ,rust-windows-targets-0.48.5)
         ("rust-smallvec" ,rust-smallvec-1.11.1)
         ("rust-redox-syscall" ,rust-redox-syscall-0.3.5)
         ("rust-libc" ,rust-libc-0.2.153)
         ("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parser-5.16.0
  (package
    (name "rust-parser")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-versions" ,rust-versions-6.2.0)
         ("rust-tempfile" ,rust-tempfile-3.10.1)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-regex" ,rust-regex-1.10.4)
         ("rust-pathdiff" ,rust-pathdiff-0.2.1)
         ("rust-once-cell" ,rust-once-cell-1.19.0)
         ("rust-logos" ,rust-logos-0.14.0)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-expect-test" ,rust-expect-test-1.5.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pathdiff-0.2.1
  (package
    (name "rust-pathdiff")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pathdiff" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pa4dcmb7lwir4himg1mnl97a05b2z0svczg62l8940pbim12dc8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-percent-encoding-2.3.1
  (package
    (name "rust-percent-encoding")
    (version "2.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "percent-encoding" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gi8wgx0dcy8rnv1kywdv98lwcx67hz0a0zwpib5v2i08r88y573"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-0.11.2
  (package
    (name "rust-phf")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1p03rsw66l7naqhpgr1a34r9yzi1gv9jh16g3fsk6wrwyfwdiqmd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-shared" ,rust-phf-shared-0.11.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-codegen-0.11.2
  (package
    (name "rust-phf-codegen")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_codegen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nia6h4qfwaypvfch3pnq1nd2qj64dif4a6kai3b7rjrsf49dlz8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-shared" ,rust-phf-shared-0.11.2)
         ("rust-phf-generator" ,rust-phf-generator-0.11.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-generator-0.11.2
  (package
    (name "rust-phf-generator")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_generator" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c14pjyxbcpwkdgw109f7581cc5fa3fnkzdq1ikvx7mdq9jcrr28"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand" ,rust-rand-0.8.5)
         ("rust-phf-shared" ,rust-phf-shared-0.11.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-shared-0.11.2
  (package
    (name "rust-phf-shared")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0azphb0a330ypqx3qvyffal5saqnks0xvl8rj73jlk3qxxgbkz4h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-siphasher" ,rust-siphasher-0.3.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-plotters-0.3.5
  (package
    (name "rust-plotters")
    (version "0.3.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "plotters" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0igxq58bx96gz58pqls6g3h80plf17rfl3b6bi6xvjnp02x29hnj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-web-sys" ,rust-web-sys-0.3.64)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2.87)
         ("rust-plotters-svg" ,rust-plotters-svg-0.3.5)
         ("rust-plotters-backend"
          ,rust-plotters-backend-0.3.5)
         ("rust-num-traits" ,rust-num-traits-0.2.16))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-plotters-backend-0.3.5
  (package
    (name "rust-plotters-backend")
    (version "0.3.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "plotters-backend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02cn98gsj2i1bwrfsymifmyas1wn2gibdm9mk8w82x9s9n5n4xly"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-plotters-svg-0.3.5
  (package
    (name "rust-plotters-svg")
    (version "0.3.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "plotters-svg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1axbw82frs5di4drbyzihr5j35wpy2a75hp3f49p186cjfcd7xiq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-plotters-backend"
          ,rust-plotters-backend-0.3.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-proc-macro2-1.0.76
  (package
    (name "rust-proc-macro2")
    (version "1.0.76")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "136cp0fgl6rg5ljm3b1xpc0bn0lyvagzzmxvbxgk5hxml36mdz4m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-ident" ,rust-unicode-ident-1.0.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-quote-1.0.35
  (package
    (name "rust-quote")
    (version "1.0.35")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quote" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vv8r2ncaz4pqdr78x7f138ka595sp2ncr1sa2plm4zxbsmwj7i9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1.0.76))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-0.8.5
  (package
    (name "rust-rand")
    (version "0.8.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "013l6931nn7gkc23jz5mm3qdhf93jjf0fg64nz2lp4i51qd8vbrl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand-core" ,rust-rand-core-0.6.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-core-0.6.4
  (package
    (name "rust-rand-core")
    (version "0.6.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0b4j2v4cb5krak1pv6kakv4sz6xcwbrmy2zckc32hsigbrwy82zc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rayon-1.10.0
  (package
    (name "rust-rayon")
    (version "1.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rayon" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ylgnzwgllajalr4v00y4kj22klq2jbwllm70aha232iah0sc65l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rayon-core" ,rust-rayon-core-1.12.1)
         ("rust-either" ,rust-either-1.9.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rayon-core-1.12.1
  (package
    (name "rust-rayon-core")
    (version "1.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rayon-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qpwim68ai5h0j7axa8ai8z0payaawv3id0lrgkqmapx7lx8fr8l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8.19)
         ("rust-crossbeam-deque"
          ,rust-crossbeam-deque-0.8.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-redox-syscall-0.2.16
  (package
    (name "rust-redox-syscall")
    (version "0.2.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16jicm96kjyzm802cxdd1k9jmcph0db1a4lhslcnhjsvhp0mhnpv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.3.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-redox-syscall-0.3.5
  (package
    (name "rust-redox-syscall")
    (version "0.3.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0acgiy2lc1m2vr8cr33l5s7k9wzby8dybyab1a9p753hcbr68xjn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.3.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-redox-users-0.4.3
  (package
    (name "rust-redox-users")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_users" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0asw3s4iy69knafkhvlbchy230qawc297vddjdwjs5nglwvxhcxh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror" ,rust-thiserror-1.0.59)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2.16)
         ("rust-getrandom" ,rust-getrandom-0.2.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-references-5.16.0
  (package
    (name "rust-references")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-regex-1.10.4
  (package
    (name "rust-regex")
    (version "1.10.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0k5sb0h2mkwf51ab0gvv3x38jp1q7wgxf63abfbhi0wwvvgxn5y1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-regex-syntax" ,rust-regex-syntax-0.8.2)
         ("rust-regex-automata"
          ,rust-regex-automata-0.4.6)
         ("rust-memchr" ,rust-memchr-2.7.1)
         ("rust-aho-corasick" ,rust-aho-corasick-1.1.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-regex-automata-0.4.6
  (package
    (name "rust-regex-automata")
    (version "0.4.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-automata" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1spaq7y4im7s56d1gxa2hi4hzf6dwswb1bv8xyavzya7k25kpf46"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-regex-syntax" ,rust-regex-syntax-0.8.2)
         ("rust-memchr" ,rust-memchr-2.7.1)
         ("rust-aho-corasick" ,rust-aho-corasick-1.1.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-regex-syntax-0.8.2
  (package
    (name "rust-regex-syntax")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-syntax" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17rd2s8xbiyf6lb4aj2nfi44zqlj98g2ays8zzj2vfs743k79360"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rename-5.16.0
  (package
    (name "rust-rename")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rowan-0.15.15
  (package
    (name "rust-rowan")
    (version "0.15.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rowan" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0j9b340gsyf2h7v1q9xb4mqyqp4qbyzlbk1r9zn2mzyclyl8z99j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-text-size" ,rust-text-size-1.1.1)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-memoffset" ,rust-memoffset-0.9.0)
         ("rust-hashbrown" ,rust-hashbrown-0.14.3)
         ("rust-countme" ,rust-countme-3.0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustc-hash-1.1.0
  (package
    (name "rust-rustc-hash")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc-hash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qkc5khrmv5pqi5l5ca9p5nl5hs742cagrndhbrlk3dhlrx3zm08"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustix-0.38.31
  (package
    (name "rust-rustix")
    (version "0.38.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustix" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jg9yj3i6qnzk1y82hng7rb1bwhslfbh57507dxcs9mgcakf38vf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.52.0)
         ("rust-linux-raw-sys" ,rust-linux-raw-sys-0.4.13)
         ("rust-libc" ,rust-libc-0.2.153)
         ("rust-errno" ,rust-errno-0.3.8)
         ("rust-bitflags" ,rust-bitflags-2.4.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustversion-1.0.15
  (package
    (name "rust-rustversion")
    (version "1.0.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustversion" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0iyy66mldq0z7h6n2zm6fw2bndw04pifhv5s7xda8xzj668nzbw0"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ryu-1.0.15
  (package
    (name "rust-ryu")
    (version "1.0.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ryu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hfphpn1xnpzxwj8qg916ga1lyc33lc03lnf1gb3wwpglj6wrm0s"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-same-file-1.0.6
  (package
    (name "rust-same-file")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "same-file" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00h5j1w87dmhnvbv9l8bic3y7xxsnjmssvifw2ayvgx9mb1ivz4k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-util" ,rust-winapi-util-0.1.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-scopeguard-1.2.0
  (package
    (name "rust-scopeguard")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "scopeguard" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jcz9sd47zlsgcnm1hdw0664krxwb5gczlif4qngj2aif8vky54l"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-1.0.199
  (package
    (name "rust-serde")
    (version "1.0.199")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0slnp99f9in676pvkl3d15zii66v83xp2rwrjk6pfv03vxv6x7qc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde-derive" ,rust-serde-derive-1.0.199))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-derive-1.0.199
  (package
    (name "rust-serde-derive")
    (version "1.0.199")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1z5psgjcf14i14wsg6p88xxqh53pdzi4mlm65kj43qa1cmx2bg8i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-2.0.48)
         ("rust-quote" ,rust-quote-1.0.35)
         ("rust-proc-macro2" ,rust-proc-macro2-1.0.76))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-json-1.0.116
  (package
    (name "rust-serde-json")
    (version "1.0.116")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_json" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04r81f5myl41zrsyghnbmbl39c4n3azldb9zxfafnzyi4rqxn5ry"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1.0.199)
         ("rust-ryu" ,rust-ryu-1.0.15)
         ("rust-itoa" ,rust-itoa-1.0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-regex-1.1.0
  (package
    (name "rust-serde-regex")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_regex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pxsnxb8c198szghk1hvzvhva36w2q5zs70hqkmdf5d89qd6y4x8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1.0.199)
         ("rust-regex" ,rust-regex-1.10.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-repr-0.1.18
  (package
    (name "rust-serde-repr")
    (version "0.1.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_repr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nyzz7ph9nwmjrx6f7s37m9y7y5gc10f4vjxnqkgfgcxbsa6nbhb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-2.0.48)
         ("rust-quote" ,rust-quote-1.0.35)
         ("rust-proc-macro2" ,rust-proc-macro2-1.0.76))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-shellexpand-3.1.0
  (package
    (name "rust-shellexpand")
    (version "3.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "shellexpand" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jz1i14ziz8gbyj71212s7dqrw6q96f25i48zkmy66fcjhxzl0ys"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dirs" ,rust-dirs-5.0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-siphasher-0.3.11
  (package
    (name "rust-siphasher")
    (version "0.3.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "siphasher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03axamhmwsrmh0psdw3gf7c0zc4fyl5yjxfifz9qfka6yhkqid9q"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-smallvec-1.11.1
  (package
    (name "rust-smallvec")
    (version "1.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smallvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nmx8aw3v4jglqdcjv4hhn10d6g52c4bhjlzwf952885is04lawl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-static-assertions-1.1.0
  (package
    (name "rust-static-assertions")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "static_assertions" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gsl6xmw10gvn3zs1rv99laj5ig7ylffnh71f9l34js4nr4r7sx2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strsim-0.11.0
  (package
    (name "rust-strsim")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strsim" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00gsdp2x1gkkxsbjxgrjyil2hsbdg49bwv8q2y1f406dwk4p7q2y"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-symbols-5.16.0
  (package
    (name "rust-symbols")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-url" ,rust-url-2.5.0)
         ("rust-titlecase" ,rust-titlecase-3.0.0)
         ("rust-test-utils" ,rust-test-utils-5.16.0)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-regex" ,rust-regex-1.10.4)
         ("rust-line-index" ,rust-line-index-5.16.0)
         ("rust-itertools" ,rust-itertools-0.12.1)
         ("rust-expect-test" ,rust-expect-test-1.5.0)
         ("rust-distro" ,rust-distro-5.16.0)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-syn-2.0.48
  (package
    (name "rust-syn")
    (version "2.0.48")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gqgfygmrxmp8q32lia9p294kdd501ybn6kn2h4gqza0irik2d8g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-ident" ,rust-unicode-ident-1.0.12)
         ("rust-quote" ,rust-quote-1.0.35)
         ("rust-proc-macro2" ,rust-proc-macro2-1.0.76))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-syntax-5.16.0
  (package
    (name "rust-syntax")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-itertools" ,rust-itertools-0.12.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tempfile-3.10.1
  (package
    (name "rust-tempfile")
    (version "3.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tempfile" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wdzz35ri168jn9al4s1g2rnsrr5ci91khgarc2rvpb3nappzdw5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.52.0)
         ("rust-rustix" ,rust-rustix-0.38.31)
         ("rust-fastrand" ,rust-fastrand-2.0.1)
         ("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-test-utils-5.16.0
  (package
    (name "rust-test-utils")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-url" ,rust-url-2.5.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-line-index" ,rust-line-index-5.16.0)
         ("rust-distro" ,rust-distro-5.16.0)
         ("rust-base-db" ,rust-base-db-5.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-text-size-1.1.1
  (package
    (name "rust-text-size")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "text-size" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cwjbkl7w3xc8mnkhg1nwij6p5y2qkcfldgss8ddnawvhf3s32pi"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-thiserror-1.0.59
  (package
    (name "rust-thiserror")
    (version "1.0.59")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ai39ga7d50jny5zzqv1zzgmc81mfb65asmfqfgz4ygzig86l4ph"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror-impl"
          ,rust-thiserror-impl-1.0.59))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-thiserror-impl-1.0.59
  (package
    (name "rust-thiserror-impl")
    (version "1.0.59")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rlb2jvpxx5xkpk52yh6gixlw0d5dx5343k8yddlr2smblxl3kfi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-2.0.48)
         ("rust-quote" ,rust-quote-1.0.35)
         ("rust-proc-macro2" ,rust-proc-macro2-1.0.76))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-thread-local-1.1.7
  (package
    (name "rust-thread-local")
    (version "1.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thread_local" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lp19jdgvp5m4l60cgxdnl00yw1hlqy8gcywg9bddwng9h36zp9z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1.19.0)
         ("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-threadpool-1.8.1
  (package
    (name "rust-threadpool")
    (version "1.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "threadpool" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1amgfyzvynbm8pacniivzq9r0fh3chhs7kijic81j76l6c5ycl6h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-cpus" ,rust-num-cpus-1.16.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tinytemplate-1.2.1
  (package
    (name "rust-tinytemplate")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinytemplate" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1g5n77cqkdh9hy75zdb01adxn45mkh9y40wdr7l68xpz35gnnkdy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde-json" ,rust-serde-json-1.0.116)
         ("rust-serde" ,rust-serde-1.0.199))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tinyvec-1.6.0
  (package
    (name "rust-tinyvec")
    (version "1.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinyvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0l6bl2h62a5m44jdnpn7lmj14rd44via8180i7121fvm73mmrk47"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinyvec-macros"
          ,rust-tinyvec-macros-0.1.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tinyvec-macros-0.1.1
  (package
    (name "rust-tinyvec-macros")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinyvec_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "081gag86208sc3y6sdkshgw3vysm5d34p431dzw0bshz66ncng0z"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-titlecase-3.0.0
  (package
    (name "rust-titlecase")
    (version "3.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "titlecase" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05650b7k1pj0yddd150bdnk91jqnsq50cyxg462yqm7fnv3j6cmv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-regex" ,rust-regex-1.10.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-uds-windows-1.1.0
  (package
    (name "rust-uds-windows")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uds_windows" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fb4y65pw0rsp0gyfyinjazlzxz1f6zv7j4zmb20l5pxwv1ypnl9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3.9)
         ("rust-tempfile" ,rust-tempfile-3.10.1)
         ("rust-memoffset" ,rust-memoffset-0.9.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-bidi-0.3.13
  (package
    (name "rust-unicode-bidi")
    (version "0.3.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-bidi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0q0l7rdkiq54pan7a4ama39dgynaf1mnjj1nddrq1w1zayjqp24j"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-case-mapping-0.4.0
  (package
    (name "rust-unicode-case-mapping")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-case-mapping" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17rrnk7vbk995qdcm0jdj0hhslrvx59s7bzx41i5pg7zc8f3jsli"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-ident-1.0.12
  (package
    (name "rust-unicode-ident")
    (version "1.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-ident" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jzf1znfpb2gx8nr8mvmyqs1crnv79l57nxnbiszc7xf7ynbjm1k"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-normalization-0.1.23
  (package
    (name "rust-unicode-normalization")
    (version "0.1.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-normalization" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1x81a50h2zxigj74b9bqjsirxxbyhmis54kg600xj213vf31cvd5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinyvec" ,rust-tinyvec-1.6.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-segmentation-1.10.1
  (package
    (name "rust-unicode-segmentation")
    (version "1.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-segmentation" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dky2hm5k51xy11hc3nk85p533rvghd462b6i0c532b7hl4j9mhx"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unidecode-0.3.0
  (package
    (name "rust-unidecode")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unidecode" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1p0sm8j9223kw3iincv60s746s88k09xcaqf8nkx3w83isfv2as0"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-url-2.5.0
  (package
    (name "rust-url")
    (version "2.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "url" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cs65961miawncdg2z20171w0vqrmraswv2ihdpd8lxp7cp31rii"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1.0.199)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2.3.1)
         ("rust-idna" ,rust-idna-0.5.0)
         ("rust-form-urlencoded"
          ,rust-form-urlencoded-1.2.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-utf8parse-0.2.1
  (package
    (name "rust-utf8parse")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "utf8parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02ip1a0az0qmc2786vxk2nqwsgcwf17d3a38fkf0q7hrmwh9c6vi"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-versions-6.2.0
  (package
    (name "rust-versions")
    (version "6.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "versions" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nxcqiqh0b89394144kkl5gcyvg4kl5yf8300x468yqnilgr7a1q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-nom" ,rust-nom-7.1.3)
         ("rust-itertools" ,rust-itertools-0.12.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-walkdir-2.4.0
  (package
    (name "rust-walkdir")
    (version "2.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "walkdir" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vjl9fmfc4v8k9ald23qrpcbyb8dl1ynyq8d516cm537r1yqa7fp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-util" ,rust-winapi-util-0.1.6)
         ("rust-same-file" ,rust-same-file-1.0.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasi-0.11.0+wasi-snapshot-preview1
  (package
    (name "rust-wasi")
    (version "0.11.0+wasi-snapshot-preview1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08z4hxwkpdpalxjps1ai9y7ihin26y9f476i53dv98v45gkqg3cw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-0.2.87
  (package
    (name "rust-wasm-bindgen")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hm3k42gcnrps2jh339h186scx1radqy1w7v1zwb333dncmaf1kp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-macro"
          ,rust-wasm-bindgen-macro-0.2.87)
         ("rust-cfg-if" ,rust-cfg-if-1.0.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-backend-0.2.87
  (package
    (name "rust-wasm-bindgen-backend")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-backend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gcsh3bjxhw3cirmin45107pcsnn0ymhkxg6bxg65s8hqp9vdwjy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-shared"
          ,rust-wasm-bindgen-shared-0.2.87)
         ("rust-syn" ,rust-syn-2.0.48)
         ("rust-quote" ,rust-quote-1.0.35)
         ("rust-proc-macro2" ,rust-proc-macro2-1.0.76)
         ("rust-once-cell" ,rust-once-cell-1.19.0)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-bumpalo" ,rust-bumpalo-3.14.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-macro-0.2.87
  (package
    (name "rust-wasm-bindgen-macro")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07cg0b6zkcxa1yg1n10h62paid59s9zr8yss214bv8w2b7jrbr6y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-macro-support"
          ,rust-wasm-bindgen-macro-support-0.2.87)
         ("rust-quote" ,rust-quote-1.0.35))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-macro-support-0.2.87
  (package
    (name "rust-wasm-bindgen-macro-support")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro-support" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yqc46pr6mlgb9bsnfdnd50qvsqnrz8g5243fnaz0rb7lhc1ns2l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-shared"
          ,rust-wasm-bindgen-shared-0.2.87)
         ("rust-wasm-bindgen-backend"
          ,rust-wasm-bindgen-backend-0.2.87)
         ("rust-syn" ,rust-syn-2.0.48)
         ("rust-quote" ,rust-quote-1.0.35)
         ("rust-proc-macro2" ,rust-proc-macro2-1.0.76))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-shared-0.2.87
  (package
    (name "rust-wasm-bindgen-shared")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18bmjwvfyhvlq49nzw6mgiyx4ys350vps4cmx5gvzckh91dd0sna"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-web-sys-0.3.64
  (package
    (name "rust-web-sys")
    (version "0.3.64")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "web-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16r4fww3l99kxhb66hka3kxkmhhgzhnqkzdf0ay6l2i2ikpwp1cv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2.87)
         ("rust-js-sys" ,rust-js-sys-0.3.64))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winapi-0.3.9
  (package
    (name "rust-winapi")
    (version "0.3.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06gl025x418lchw1wxj64ycr7gha83m44cjr5sarhynd9xkrm0sw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-x86-64-pc-windows-gnu"
          ,rust-winapi-x86-64-pc-windows-gnu-0.4.0)
         ("rust-winapi-i686-pc-windows-gnu"
          ,rust-winapi-i686-pc-windows-gnu-0.4.0))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winapi-i686-pc-windows-gnu-0.4.0
  (package
    (name "rust-winapi-i686-pc-windows-gnu")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi-i686-pc-windows-gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1dmpa6mvcvzz16zg6d5vrfy4bxgg541wxrcip7cnshi06v38ffxc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winapi-util-0.1.6
  (package
    (name "rust-winapi-util")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15i5lm39wd44004i9d5qspry2cynkrpvwzghr6s2c3dsk28nz7pj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winapi-x86-64-pc-windows-gnu-0.4.0
  (package
    (name "rust-winapi-x86-64-pc-windows-gnu")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri
               "winapi-x86_64-pc-windows-gnu"
               version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gqq64czqb64kskjryj8isp62m2sgvx25yyj3kpc2myh85w24bki"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-sys-0.48.0
  (package
    (name "rust-windows-sys")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1aan23v5gs7gya1lc46hqn9mdh8yph3fhxmhxlw36pn6pqc28zb7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-targets"
          ,rust-windows-targets-0.48.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-sys-0.52.0
  (package
    (name "rust-windows-sys")
    (version "0.52.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gd3v4ji88490zgb6b5mq5zgbvwv7zx1ibn8v3x83rwcdbryaar8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-targets"
          ,rust-windows-targets-0.52.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-targets-0.48.5
  (package
    (name "rust-windows-targets")
    (version "0.48.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-targets" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "034ljxqshifs1lan89xwpcy1hp0lhdh4b5n0d2z4fwjx2piacbws"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-x86-64-msvc"
          ,rust-windows-x86-64-msvc-0.48.5)
         ("rust-windows-x86-64-gnullvm"
          ,rust-windows-x86-64-gnullvm-0.48.5)
         ("rust-windows-x86-64-gnu"
          ,rust-windows-x86-64-gnu-0.48.5)
         ("rust-windows-i686-msvc"
          ,rust-windows-i686-msvc-0.48.5)
         ("rust-windows-i686-gnu"
          ,rust-windows-i686-gnu-0.48.5)
         ("rust-windows-aarch64-msvc"
          ,rust-windows-aarch64-msvc-0.48.5)
         ("rust-windows-aarch64-gnullvm"
          ,rust-windows-aarch64-gnullvm-0.48.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-targets-0.52.4
  (package
    (name "rust-windows-targets")
    (version "0.52.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-targets" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06sdd7fin3dj9cmlg6n1dw0n1l10jhn9b8ckz1cqf0drb9z7plvx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-x86-64-msvc"
          ,rust-windows-x86-64-msvc-0.52.4)
         ("rust-windows-x86-64-gnullvm"
          ,rust-windows-x86-64-gnullvm-0.52.4)
         ("rust-windows-x86-64-gnu"
          ,rust-windows-x86-64-gnu-0.52.4)
         ("rust-windows-i686-msvc"
          ,rust-windows-i686-msvc-0.52.4)
         ("rust-windows-i686-gnu"
          ,rust-windows-i686-gnu-0.52.4)
         ("rust-windows-aarch64-msvc"
          ,rust-windows-aarch64-msvc-0.52.4)
         ("rust-windows-aarch64-gnullvm"
          ,rust-windows-aarch64-gnullvm-0.52.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-gnullvm-0.48.5
  (package
    (name "rust-windows-aarch64-gnullvm")
    (version "0.48.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1n05v7qblg1ci3i567inc7xrkmywczxrs1z3lj3rkkxw18py6f1b"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-gnullvm-0.52.4
  (package
    (name "rust-windows-aarch64-gnullvm")
    (version "0.52.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jfam5qfngg8v1syxklnvy8la94b5igm7klkrk8z5ik5qgs6rx5w"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-msvc-0.48.5
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.48.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1g5l4ry968p73g6bg6jgyvy9lb8fyhcs54067yzxpcpkf44k2dfw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-msvc-0.52.4
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.52.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xdn6db0rk8idn7dxsyflixq2dbj9x60kzdzal5rkxmwsffjb7ys"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-gnu-0.48.5
  (package
    (name "rust-windows-i686-gnu")
    (version "0.48.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gklnglwd9ilqx7ac3cn8hbhkraqisd0n83jxzf9837nvvkiand7"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-gnu-0.52.4
  (package
    (name "rust-windows-i686-gnu")
    (version "0.52.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1lq1g35sbj55ms86by4c080jcqrlfjy9bw5r4mgrkq4riwkdhx5l"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-msvc-0.48.5
  (package
    (name "rust-windows-i686-msvc")
    (version "0.48.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01m4rik437dl9rdf0ndnm2syh10hizvq0dajdkv2fjqcywrw4mcg"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-msvc-0.52.4
  (package
    (name "rust-windows-i686-msvc")
    (version "0.52.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00lfzw88dkf3fdcf2hpfhp74i9pwbp7rwnj1nhy79vavksifj58m"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnu-0.48.5
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.48.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13kiqqcvz2vnyxzydjh73hwgigsdr2z1xpzx313kxll34nyhmm2k"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnu-0.52.4
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.52.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00qs6x33bf9lai2q68faxl56cszbv7mf7zqlslmc1778j0ahkvjy"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnullvm-0.48.5
  (package
    (name "rust-windows-x86-64-gnullvm")
    (version "0.48.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k24810wfbgz8k48c2yknqjmiigmql6kk3knmddkv8k8g1v54yqb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnullvm-0.52.4
  (package
    (name "rust-windows-x86-64-gnullvm")
    (version "0.52.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xr13xxakp14hs4v4hg2ynjcv7wrzr3hg7zk5agglj8v8pr7kjkp"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-msvc-0.48.5
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.48.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0f4mdp895kkjh9zv8dxvn4pc10xr7839lf5pa9l0193i2pkgr57d"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-msvc-0.52.4
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.52.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1n0yc7xiv9iki1j3xl8nxlwwkr7dzsnwwvycvgxxv81d5bjm5drj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define texlab
  (package
    (name "texlab")
    (version "5.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/latex-lsp/texlab")
               (commit (string-append "v" %texlab-version))))
        (modules '((guix build utils)))
        (snippet
          '(begin
             (delete-file-recursively "Cargo.lock")))
        (sha256
          (base32 "10jl53xjs73vcy0yib497gr6a3p4r6qmk05g5avgqxm9dh4l1vq6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #f
        #:install-source? #f
        #:phases
        ,#~(modify-phases %standard-phases
             (replace 'install
                      (lambda* (#:key outputs #:allow-other-keys)
                               (let* ((out    (assoc-ref outputs "out"))
                                      (bin    (string-append out "/bin"))
                                      (texlab (string-append bin "/texlab"))
                                      (share  (string-append out "/lib")))
                                 (install-file "target/release/texlab" bin)))))
        #:cargo-inputs
        (("rust-threadpool" ,rust-threadpool-1.8.1)
         ("rust-tempfile" ,rust-tempfile-3.10.1)
         ("rust-syntax" ,rust-syntax-5.16.0)
         ("rust-symbols" ,rust-symbols-5.16.0)
         ("rust-serde-repr" ,rust-serde-repr-0.1.18)
         ("rust-serde-regex" ,rust-serde-regex-1.1.0)
         ("rust-serde-json" ,rust-serde-json-1.0.116)
         ("rust-serde" ,rust-serde-1.0.199)
         ("rust-rustc-hash" ,rust-rustc-hash-1.1.0)
         ("rust-rowan" ,rust-rowan-0.15.15)
         ("rust-rename" ,rust-rename-5.16.0)
         ("rust-regex" ,rust-regex-1.10.4)
         ("rust-references" ,rust-references-5.16.0)
         ("rust-parser" ,rust-parser-5.16.0)
         ("rust-parking-lot" ,rust-parking-lot-0.12.1)
         ("rust-notify-debouncer-full"
          ,rust-notify-debouncer-full-0.3.1)
         ("rust-notify" ,rust-notify-6.1.1)
         ("rust-lsp-types" ,rust-lsp-types-0.95.1)
         ("rust-lsp-server" ,rust-lsp-server-0.7.6)
         ("rust-log" ,rust-log-0.4.21)
         ("rust-links" ,rust-links-5.16.0)
         ("rust-line-index" ,rust-line-index-5.16.0)
         ("rust-ipc" ,rust-ipc-5.16.0)
         ("rust-inlay-hints" ,rust-inlay-hints-5.16.0)
         ("rust-hover" ,rust-hover-5.16.0)
         ("rust-highlights" ,rust-highlights-5.16.0)
         ("rust-folding" ,rust-folding-5.16.0)
         ("rust-fern" ,rust-fern-0.6.2)
         ("rust-distro" ,rust-distro-5.16.0)
         ("rust-diagnostics" ,rust-diagnostics-5.16.0)
         ("rust-definition" ,rust-definition-5.16.0)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5.12)
         ("rust-criterion" ,rust-criterion-0.5.1)
         ("rust-completion-data"
          ,rust-completion-data-5.16.0)
         ("rust-completion" ,rust-completion-5.16.0)
         ("rust-commands" ,rust-commands-5.16.0)
         ("rust-clap" ,rust-clap-4.5.4)
         ("rust-citeproc" ,rust-citeproc-5.16.0)
         ("rust-bibfmt" ,rust-bibfmt-5.16.0)
         ("rust-base-db" ,rust-base-db-5.16.0)
         ("rust-anyhow" ,rust-anyhow-1.0.82))))
    (home-page "https://github.com/latex-lsp/texlab")
    (synopsis "An implementation of the Language Server Protocol for LaTeX")
    (description "A cross-platform implementation of the Language Server Protocol providing rich cross-editing support for the LaTeX typesetting system. The server may be used with any editor that implements the Language Server Protocol.")
    (license license:gpl3)))
