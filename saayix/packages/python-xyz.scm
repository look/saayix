;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages python-xyz)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages check)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages jupyter)
  #:use-module (gnu packages machine-learning)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-graphics)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages time)
  #:use-module (gnu packages tree-sitter)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (saayix utils))

(define-public pybind11
  (hidden-package
    (package
      (name "pybind11")
      (version "2.13.6")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/pybind/pybind11")
                      (commit (string-append "v" version))))
                (sha256
                 (base32
                  "1dbnki0pnky39kr04afd9ks597bzjc530zbk33jjss53nfvdvlj8"))
                (file-name (git-file-name name version))))
      (build-system cmake-build-system)
      (native-inputs
       `(("python" ,python-wrapper)
         ;; The following dependencies are used for tests.
         ("python-pytest" ,python-pytest)
         ("catch" ,catch2-1)
         ("eigen" ,eigen)))
      (arguments
       `(#:configure-flags
         (list (string-append "-DCATCH_INCLUDE_DIR="
                              (assoc-ref %build-inputs "catch")
                              "/include/catch"))

         #:phases (modify-phases %standard-phases
                    (add-after 'install 'install-python
                      (lambda* (#:key outputs #:allow-other-keys)
                        (let ((out (assoc-ref outputs "out")))
                          (with-directory-excursion "../source"
                            (setenv "PYBIND11_USE_CMAKE" "yes")
                            (invoke "python" "setup.py" "install"
                                    "--single-version-externally-managed"
                                    "--root=/"
                                    (string-append "--prefix=" out)))))))

         #:test-target "check"))
      (home-page "https://github.com/pybind/pybind11/")
      (synopsis "Seamless operability between C++11 and Python")
      (description
       "@code{pybind11} is a lightweight header-only library that exposes C++
  types in Python and vice versa, mainly to create Python bindings of existing
  C++ code.  Its goals and syntax are similar to the @code{Boost.Python}
  library: to minimize boilerplate code in traditional extension modules by
  inferring type information using compile-time introspection.")
      (license license:bsd-3))))

(define-public python-cloup
  (hidden-package
    (package
      (name "python-cloup")
      (version "3.0.5")
      (source
       (origin
         (method url-fetch)
         (uri (pypi-uri "cloup" version))
         (sha256
          (base32
           "1ia30jcj3g05pvfxr519y78y53ddxnrv8gqgjc231qdpgcf2cay9"))))
      (build-system python-build-system)
      (arguments
       '(#:phases (modify-phases %standard-phases
                    ;; test phase fails with "error: invalid command pytest"
                    (delete 'check))))
      (propagated-inputs
       `(("python-click" ,python-click)))
      (native-inputs
       `(("python-setuptools" ,python-setuptools)
         ("python-setuptools-scm" ,python-setuptools-scm)
         ("python-pytest" ,python-pytest)))
      (home-page "https://github.com/janLuke/cloup")
      (synopsis "Extension library for python-click")
      (description
       "Cloup — originally from \"Click + option groups\" — enriches Click with
  several features that make it more expressive and configurable: option groups,
  constraints, subcommand aliases, subcommands sections and a themeable
  HelpFormatter.")
      (license license:bsd-3))))

(define-public python-glcontext
  (let (;; Upstream is known for abusing mutable tag, hence pinpoint the
        ;; relevant commit.
        (revision "1")
        (commit "1573e57e5423b313599155c7e4bc9c5253065f40"))
    (hidden-package
      (package
        (name "python-glcontext")
        (version (git-version "2.5.0" revision commit))
        (source (origin
                  (method git-fetch)
                  (uri (git-reference
                        (url "https://github.com/moderngl/glcontext")
                        (commit commit)))
                  (file-name (git-file-name name version))
                  (sha256
                   (base32
                    "0j1a6fa6iq0ridgf3ixspfg3bhpcv0rw84zbccv90386l8s8ipwm"))))
        (build-system pyproject-build-system)
        (arguments
         (list #:phases #~(modify-phases %standard-phases
                            (add-before 'build 'fix-lib-paths
                              (lambda* (#:key inputs outputs #:allow-other-keys)
                                (let ((mesa (assoc-ref inputs "mesa"))
                                      (libx11 (assoc-ref inputs "libx11")))
                                  (substitute* '("glcontext/x11.cpp"
                                                 "glcontext/egl.cpp")
                                    (("\"libGL.so\"")
                                     (string-append "\"" mesa "/lib/libGL.so\""))
                                    (("\"libEGL.so\"")
                                     (string-append "\"" mesa "/lib/libEGL.so\""))
                                    (("\"libX11.so\"")
                                     (string-append "\"" libx11 "/lib/libX11.so\"")))
                                  (substitute* '("glcontext/__init__.py")
                                    (("find_library\\('GL'\\)")
                                     (string-append "'" mesa "/lib/libGL.so'"))
                                    (("find_library\\('EGL'\\)")
                                     (string-append "'" mesa "/lib/libEGL.so'"))
                                    (("find_library\\(\"X11\"\\)")
                                     (string-append "'" libx11 "/lib/libX11.so'"))))))
                            (replace 'check
                              (lambda* (#:key inputs outputs tests?
                                        #:allow-other-keys)
                                (when tests?
                                  (system "Xvfb :1 &")
                                  (setenv "DISPLAY" ":1")
                                  (add-installed-pythonpath inputs outputs)
                                  (invoke "pytest" "tests")))))))
        (inputs
          (list libx11
                mesa))
        (native-inputs
          (list xorg-server-for-tests
                python-pytest
                python-psutil
                python-wheel
                python-setuptools))
        (home-page "https://github.com/moderngl/glcontext")
        (synopsis "Portable OpenGL Context for ModernGL")
        (description "Python-glcontext is a library providing an OpenGL
  implementation for ModernGL on multiple platforms.")
        (license license:expat)))))

(define-public python-isosurfaces
  (hidden-package
    (package
      (name "python-isosurfaces")
      (version "0.1.1")
      (source
       (origin
        (method url-fetch)
        (uri (pypi-uri "isosurfaces" version))
        (sha256
         (base32 "0k9qzrvzkvnrswrn3z55ix37xh4y1vm0d45ljxh3ga28f7nb9ahq"))))
      (build-system python-build-system)
      (arguments
       '(#:phases
         (modify-phases %standard-phases
           (delete 'check))))
      (native-inputs (list python-numpy))
      (home-page "https://github.com/jared-hughes/isosurfaces")
      (synopsis "Construct isolines/isosurfaces of a 2D/3D scalar field defined by
  a function.")
      (description "Construct isolines/isosurfaces of a 2D/3D scalar field
  defined by a function, i.e. curves over which f(x,y)=0 or surfaces over which
  f(x,y,z)=0.")
      (license (list license:expat license:bsd-3)))))


(define-public python-mapbox-earcut
  (hidden-package
    (package
      (name "python-mapbox-earcut")
      (version "1.0.2")
      (source
       (origin
         (method url-fetch)
         (uri (pypi-uri "mapbox_earcut" version))
         (sha256
          (base32
           "0lkxxs898hwzchhnilxikb7alxqcqfyi35mz3h530gy2pil09yl3"))))
      (build-system python-build-system)
      (propagated-inputs (list python-numpy))
      (native-inputs
       (list python-pytest
             python-wheel
             python-setuptools-scm
             cmake
             pybind11))
      (home-page "https://github.com/skogler/mapbox_earcut_python")
      (synopsis "Python bindings for the Mapbox Earcut C++ polygon triangulation
  library")
      (description
       "The Mapbox Earcut library is a header-only C++ library which provides a
  fast and robust triangulation of 2D polygons.  This package provides Python
  bindings for this library.")
      (license license:isc))))

(define-public python-markdown-it-py-for-dooit
  (hidden-package
    (package
      (name "python-markdown-it-py")
      (version "2.2.0")
      (source
        (origin
          (method url-fetch)
          (uri (pypi-uri "markdown-it-py" version))
          (sha256
            (base32 "189mx85mmjq6rzf16ja8wqyck1nnrpmvyb23cwf7gg484r0mx6kw"))))
      (build-system pyproject-build-system)
      (arguments (list #:tests? #f))      ;pypi source does not contains tests
      (propagated-inputs (list python-mdurl
                               python-flit
                               python-typing-extensions))
      (home-page "https://github.com/executablebooks/markdown-it-py")
      (synopsis "Python port of markdown-it")
      (description "This is a Python port of @code{markdown-it}, and some of its associated plugins.  The driving design philosophy of the port has been to change as little of the fundamental code structure (file names, function name, etc) as possible.")
      (license license:expat))))

(define-public python-moderngl
  (hidden-package
    (package
      (name "python-moderngl")
      (version "5.10.0")
      (source
        (origin
          (method url-fetch)
          (uri (pypi-uri "moderngl" version))
          (sha256
            (base32 "04b59zp52kk9yhnrb8alk5svl5v6j7a7w8wzq38xhg6y9lv8v70i"))))
      (build-system python-build-system)
      (propagated-inputs (list python-glcontext))
      (arguments (list #:tests? #f))
      (home-page "https://github.com/moderngl/moderngl")
      (synopsis "Python wrapper for OpenGL")
      (description "ModernGL is a python wrapper over OpenGL 3.3+ core that
  simplifies the creation of simple graphics applications like scientific
  simulations, games or user interfaces.  Usually, acquiring in-depth knowledge
  of OpenGL requires a steep learning curve.  In contrast, ModernGL is easy to
  learn and use, moreover it is capable of rendering with high performance and
  quality, with less code written.  The majority of the moderngl code base is
  also written in C++ for high performance.")
      (license license:expat))))

(define-public python-moderngl-window
  (hidden-package
    (package
     (name "python-moderngl-window")
     (version "2.4.6")
     (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/moderngl/moderngl-window")
               (commit version)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "1hjqkg6wkq8bzffyhl1nh1hw3prvd486v1rb2id0dg9zfm4s0g6d"))))
     (build-system pyproject-build-system)
     (arguments
       '(#:phases
         (modify-phases %standard-phases
           (add-before 'build 'fix-numpy
             (lambda _
               (substitute* '("pyproject.toml")
                 (("numpy>=1.16,<2") "numpy>=1.16"))))
           (add-before 'check 'fix-test
             (lambda _
               (substitute* '("tests/test_windowconfig.py")
                 (("terrain_Vs.glsl") "terrain_vs.glsl"))))
           (delete 'check))))
     (propagated-inputs
       (list python-pillow
             python-pyrr
             python-numpy-2
             python-moderngl
             python-pyglet))
     (native-inputs
       (list python-pytest
             xorg-server-for-tests
             python-wheel
             python-setuptools))
     (home-page "https://github.com/moderngl/moderngl-window")
     (synopsis "Cross-platform utility library for ModernGL")
     (description "This ModernGL utility library simplifies window creation and
  resource loading.  You can create a window for ModernGL using pyglet, pygame,
  PySide2, GLFW, SDL2, PyQt5 or tkinter.  Events are unified into a single event
  system.  Resource loading includes loading of 2D textures/texture arrays,
  shaders and objects/scenes.")
     (license license:expat))))

(define-public python-numpy-2
  (hidden-package
    (package
      (name "python-numpy")
      (version "2.1.2")
      (source
       (origin
         (method url-fetch)
         (uri (string-append
               "https://github.com/numpy/numpy/releases/download/v"
               version "/numpy-" version ".tar.gz"))
         (sha256
          (base32
           "0v0666vmx013s1kal52b86rj7pj08ssywhxqk5665yhph842llqk"))))
      (build-system pyproject-build-system)
      (arguments
       (list
        #:tests? #f
        #:modules '((guix build utils)
                    (guix build pyproject-build-system)
                    (ice-9 format))
        #:phases
        #~(modify-phases %standard-phases
            (add-before 'build 'parallelize-build
              (lambda _
                (setenv "NPY_NUM_BUILD_JOBS"
                        (number->string (parallel-job-count)))))
            (add-before 'build 'configure-blas
              (lambda* (#:key inputs #:allow-other-keys)
                (call-with-output-file "site.cfg"
                  (lambda (port)
                    (format port
                            "\
  [openblas]
  libraries = openblas
  library_dirs = ~a/lib
  include_dirs = ~:*~a/include~%"
                            (dirname (dirname
                                      (search-input-file
                                       inputs "include/openblas_config.h")))))))))))
            ; (add-before 'build 'fix-executable-paths
            ;   (lambda* (#:key inputs #:allow-other-keys)
            ;     ;; Make /gnu/store/...-bash-.../bin/sh the default shell,
            ;     ;; instead of /bin/sh.
            ;     (substitute* "numpy/distutils/exec_command.py"
            ;       (("'/bin/sh'")
            ;        (format #f "~s" (search-input-file inputs "bin/bash"))))
            ;     ;; Don't try to call '/bin/true' specifically.
            ;     (substitute* "numpy/core/tests/test_cpu_features.py"
            ;       (("/bin/true") (search-input-file inputs "bin/true"))))))))
      (native-inputs
       (list python-cython-3
             python-hypothesis
             python-pytest
             python-pytest-xdist
             python-typing-extensions
             python-meson-python
             ninja
             glibc
             gfortran))
      (inputs (list bash openblas))
      (home-page "https://numpy.org")
      (synopsis "Fundamental package for scientific computing with Python")
      (description "NumPy is the fundamental package for scientific computing
  with Python.  It contains among other things: a powerful N-dimensional array
  object, sophisticated (broadcasting) functions, tools for integrating C/C++
  and Fortran code, useful linear algebra, Fourier transform, and random number
  capabilities.")
      (properties
       '((upstream-name . "numpy")))
      (license license:bsd-3))))

(define-public python-pillow
  (hidden-package
    (package
      (name "python-pillow")
      (version "11.0.0")
      (source
        (origin
          (method url-fetch)
          (uri (pypi-uri "pillow" version))
          (sha256
            (base32 "0fbpcwgiac19ap0h1qa1imsqhq6vxv8kg67zkgm3y05c4jpwpfkj"))))
      (build-system python-build-system)
      (native-inputs (list python-pytest))
      (inputs (list freetype
                    lcms
                    libjpeg-turbo
                    libtiff
                    libwebp
                    openjpeg
                    zlib))
      (propagated-inputs (list python-olefile))
      (arguments
       `(#:tests? #f))
      (home-page "https://python-pillow.org")
      (synopsis "Fork of the Python Imaging Library")
      (description
       "The Python Imaging Library adds image processing capabilities to your
  Python interpreter.  This library provides extensive file format support, an
  efficient internal representation, and fairly powerful image processing
  capabilities.  The core image library is designed for fast access to data
  stored in a few basic pixel formats.  It should provide a solid foundation for
  a general image processing tool.")
      (properties `((cpe-name . "pillow")))
      (license (license:x11-style
                "http://www.pythonware.com/products/pil/license.htm"
                "The PIL Software License")))))

(define-public python-pygments-for-dooit
  (hidden-package
    (package
      (name "python-pygments")
      (version "2.13.0")
      (source
        (origin
          (method url-fetch)
          (uri (pypi-uri "Pygments" version))
          (sha256
            (base32 "1ha0pqk3f27zlb2h4gmlb3w8lz9zmvjnnfprpnwy562zx6551a2n"))))
      (build-system python-build-system)
      (arguments (list #:tests? #f))
      (home-page "https://pygments.org/")
      (synopsis "Syntax highlighting")
      (description "Pygments is a syntax highlighting package written in Python.")
      (license license:bsd-2))))

(define-public python-pyrr
  (hidden-package
    (package
      (name "python-pyrr")
      (version "0.10.3")
      (source
        (origin
          (method url-fetch)
          (uri (pypi-uri "pyrr" version))
          (sha256
            (base32 "07byxkq8dcc8n6fc2q7g3vq3mxzzj0hqzm8hlq3gfwbf68h7n3rw"))))
      (build-system python-build-system)
      (propagated-inputs
        (list
          python-multipledispatch
          python-numpy))
      (arguments
        (list #:tests? #f))
      (home-page "https://github.com/adamlwgriffiths/Pyrr")
      (synopsis "Mathematical functions for 3D graphics using NumPy")
      (description
       "This Python package provides a collection of object-oriented and
  procedural interfaces for working with matrices, quaternions, vectors and
  plane/line/ray objects for 3D graphics.")
      (license license:bsd-2))))

(define-public python-rich-for-dooit
  (hidden-package
    (package
      (name "python-rich")
      (version "13.3.3")
      (source
        (origin
          (method url-fetch)
          (uri (pypi-uri "rich" version))
          (sha256
            (base32 "059a1n2nc2lfi01i7jf16rgib3brxgcdsjppbyf3law4kl54116w"))))
      (build-system pyproject-build-system)
      (arguments (list #:tests? #f))
      (propagated-inputs (list python-attrs
                               python-colorama
                               python-commonmark
                               python-pygments-for-dooit
                               python-poetry-core
                               python-mdurl
                               python-markdown-it-py-for-dooit
                               python-typing-extensions))
      (home-page "https://github.com/willmcgugan/rich")
      (synopsis "Render rich text and more to the terminal")
      (description "This is a Python package for rendering rich text, tables, progress bars, syntax highlighting, markdown and more to the terminal.")
      (license license:expat))))

(define-public python-scipy
  (hidden-package
    (package
      (name "python-scipy")
      (version "1.14.1")
      (source
       (origin
         (method url-fetch)
         (uri (pypi-uri "scipy" version))
         (sha256
          (base32 "05w4q2kw8xa2p50yha3200hhhqcah8psg6j5k5b6l0i6wy25a9ss"))))
      (build-system pyproject-build-system)
      (arguments
       (list
        #:phases
        #~(modify-phases %standard-phases
            (replace 'check
              (lambda* (#:key tests? #:allow-other-keys)
                (when tests?
                  ;; Step out of the source directory to avoid interference.
                  (with-directory-excursion "/tmp"
                    (invoke "python" "-c"
                            (string-append
                             "import scipy; scipy.test('fast', parallel="
                             (number->string (parallel-job-count))
                             ", verbose=2)"))))))
            (add-after 'check 'install-doc
              (lambda* (#:key outputs #:allow-other-keys)
                ;; FIXME: Documentation cannot be built because it requires
                ;; a newer version of pydata-sphinx-theme, which currently
                ;; cannot build without internet access:
                ;; <https://github.com/pydata/pydata-sphinx-theme/issues/628>.
                ;; Keep the phase for easy testing.
                (let ((sphinx-build (false-if-exception
                                     (search-input-file input "bin/sphinx-build"))))
                  (if sphinx-build
                      (let* ((doc (assoc-ref outputs "doc"))
                             (data (string-append doc "/share"))
                             (docdir (string-append
                                      data "/doc/"
                                      #$(package-name this-package) "-"
                                      #$(package-version this-package)))
                             (html (string-append docdir "/html")))
                        (with-directory-excursion "doc"
                          ;; Build doc.
                          (invoke "make" "html"
                                  ;; Building the documentation takes a very long time.
                                  ;; Parallelize it.
                                  (string-append "SPHINXOPTS=-j"
                                                 (number->string (parallel-job-count))))
                          ;; Install doc.
                          (mkdir-p html)
                          (copy-recursively "build/html" html)))
                      (format #t "sphinx-build not found, skipping~%"))))))))
      (propagated-inputs
       (append
         (if (supported-package? python-jupytext)  ; Depends on pandoc.
             (list python-jupytext)
             '())
         (list python-matplotlib
               python-mpmath
               python-mypy
               python-numpy-2
               python-numpydoc
               python-pydata-sphinx-theme
               python-pydevtool
               python-pythran
               python-rich-click
               python-sphinx
               python-threadpoolctl
               python-typing-extensions)))
      (inputs (list openblas pybind11))
      (native-inputs
       (list gfortran
             ;; XXX: Adding gfortran shadows GCC headers, causing a compilation
             ;; failure.  Somehow also providing GCC works around it ...
             gcc
             meson-python
             pkg-config
             python-click
             python-cython-3
             python-doit
             python-hypothesis
             python-pooch
             python-pycodestyle
             python-pydevtool
             python-pytest
             python-pytest-cov
             python-pytest-timeout
             python-pytest-xdist))
      (home-page "https://scipy.org/")
      (synopsis "The Scipy library provides efficient numerical routines")
      (description "The SciPy library is one of the core packages that make up
  the SciPy stack.  It provides many user-friendly and efficient numerical
  routines such as routines for numerical integration and optimization.")
      (license license:bsd-3))))

(define-public python-screeninfo
  (hidden-package
    (package
      (name "python-screeninfo")
      (version "0.7")
      (source
       (origin
         (method url-fetch)
         (uri (pypi-uri "screeninfo" version))
         (sha256
          (base32
           "007m7sqv30qak56885dxbijksrg2hd121hfpvg2llm734wspra8j"))))
      (build-system python-build-system)
      (native-inputs (list python-pytest))
      (home-page "https://github.com/rr-/screeninfo")
      (synopsis "Obtain information about physical screens etc.")
      (description "Animation engine for explanatory math videos.")
      (license (list license:expat license:bsd-3)))))

(define-public python-textual-for-dooit
  (hidden-package
    (package
      (name "python-textual")
      (version "0.47.1")
      (source
        (origin
          (method url-fetch)
          (uri (pypi-uri "textual" version))
          (sha256
            (base32 "035qni42sfm538gi52qj1dd8n1pbkhqlqirzd4phkcabi0by70jb"))))
      (build-system pyproject-build-system)
      (arguments (list #:tests? #f))
      (propagated-inputs (list python-rich-for-dooit
                           python-typing-extensions
                           python-poetry-core
                           python-linkify-it-py
                           python-pytest
                           python-pygments-for-dooit
                           python-markdown-it-py-for-dooit
                           python-mdit-py-plugins))
      (home-page "https://github.com/Textualize/textual")
      (synopsis "Build text user interfaces in Python")
      (description "Textual is a @acronym{TUI, Text User Interface} framework
  for Python inspired by modern web development.")
      (license license:expat))))

(define-public python-tzlocal-for-dooit
  (hidden-package
    (package
      (name "python-tzlocal")
      (version "2.1")
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://github.com/regebro/tzlocal")
                 (commit version)))
          (file-name (git-file-name name version))
          (sha256
            (base32 "0awlj3453wq36m9ibmkimv53bnfm8v8vfhsd1fkh5sjf5vqaqwnz"))))
      (build-system pyproject-build-system)
      (arguments (list #:tests? #f))
      (propagated-inputs (list python-check-manifest
                           python-pytest
                           python-pytz
                           python-tzdata
                           python-pytest-cov
                           python-pytest-mock))
      (home-page "https://github.com/regebro/tzlocal")
      (synopsis "Local timezone information for Python")
      (description "Tzlocal returns a tzinfo object with the local timezone information. This module attempts to fix a glaring hole in pytz, that there is no way to get the local timezone information, unless you know the zoneinfo name, and under several distributions that's hard or impossible to figure out.")
      (license license:expat))))

(define rust-nohash-hasher-0.2
  (package
    (name "rust-nohash-hasher")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nohash-hasher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lf4p6k01w4wm7zn4grnihzj8s7zd5qczjmzng7wviwxawih5x9b"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page
      "https://github.com/paritytech/nohash-hasher")
    (synopsis
      "An implementation of `std::hash::Hasher` which does not hash at all")
    (description
      "This package provides An implementation of `std::hash::Hasher` which does not hash at all.")
    (license (list license:asl2.0 license:expat))))

(define-public python-rtoml
  (hidden-package
    (package
      (name "python-rtoml")
      (version "0.11")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/samuelcolvin/rtoml")
                      (commit (string-append "v" version))))
                (sha256
                 (base32
                  "1c988bl2j98xcdlybzn40n4h4dhbnid3p2j9qnkz1wqvqixd2rz3"))
                (file-name (git-file-name name version))))
      (build-system cargo-build-system)
      (arguments
        (list
          #:imported-modules `(,@%cargo-build-system-modules
                               ,@%pyproject-build-system-modules)
          #:modules '((guix build cargo-build-system)
                      ((guix build pyproject-build-system) #:prefix py:)
                      (guix build utils))
          #:phases
          #~(modify-phases %standard-phases
              (add-after 'build 'build-python-module
                (assoc-ref py:%standard-phases 'build))
              (add-after 'build-python-module 'install-python-module
                (assoc-ref py:%standard-phases 'install)))
          #:cargo-inputs
          `(("rust-ahash" ,rust-ahash-0.8)
            ("rust-nohash-hasher" ,rust-nohash-hasher-0.2)
            ("rust-pyo3" ,rust-pyo3-0.21)
            ("rust-serde" ,rust-serde-1)
            ("rust-toml" ,rust-toml-0.5))))
      (native-inputs (list python-wrapper
                           maturin))
      (home-page "")
      (synopsis "zero dependency, toml parser")
      (description
        "This package provides zero dependency, toml parser.")
      (license license:expat))))

(define-public python-pydantic-extra-types
  (hidden-package
    (package
      (name "python-pydantic-extra-types")
      (version "2.9.0")
      (source
       (origin
         (method url-fetch)
         (uri (pypi-uri "pydantic_extra_types" version))
         (sha256
          (base32 "0fxs05kb048jpcfgxqpdpz7vh9zkj79xqs7kd6xl71qq6qbc0qg0"))))
      (build-system pyproject-build-system)
      (arguments
        (list
           #:tests? #f
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'fix-pyproject
                 (lambda _
                   (substitute* "pyproject.toml"
                     (("  'Framework :: Pydantic :: 2.*") "")))))))
      (propagated-inputs
        (list (disable-tests python-pydantic-2)
              maturin
              python-hatchling))
      (home-page "")
      (synopsis "Extra Pydantic types.")
      (description "Extra Pydantic types.")
      (license #f))))

(define-public python-pptx
  (hidden-package
    (package
      (name "python-pptx")
      (version "1.0.2")
      (source
       (origin
         (method url-fetch)
         (uri (pypi-uri "python_pptx" version))
         (sha256
          (base32 "15bhrkcrs7ydj1932c4233ix4jl769vqic00dxmxgw7hxbq8m6j7"))))
      (build-system pyproject-build-system)
      (arguments (list #:tests? #f))
      (native-inputs (list python-setuptools python-wheel))
      (propagated-inputs (list python-lxml python-pillow
                               python-typing-extensions python-xlsxwriter))
      (home-page "")
      (synopsis "Create, read, and update PowerPoint 2007+ (.pptx) files.")
      (description
       "Create, read, and update @code{PowerPoint} 2007+ (.pptx) files.")
      (license license:expat))))

(define-public python-qtpy
  (hidden-package
    (package
      (name "python-qtpy")
      (version "2.4.1")
      (source
       (origin
         (method url-fetch)
         (uri (pypi-uri "QtPy" version))
         (sha256
            (base32
             "11r9cfpq6h2x75xprwcjfapscmnsgz0gymnw3cva2l4ma7ymz8d5"))))
      (build-system python-build-system)
      (propagated-inputs (list python-packaging))
                               ; python-pyside-6
                               ; python-pyqt-6))
      (inputs (list python-pyside-2
                    python-pyqt))
      (arguments
       `(;; Not all supported bindings are packaged. Especially PyQt4.
         #:tests? #f))
      (home-page "https://github.com/spyder-ide/qtpy")
      (synopsis
       "Qt bindings (PyQt5, PyQt4 and PySide) and additional custom QWidgets")
      (description
       "Provides an abstraction layer on top of the various Qt bindings
  (PyQt5, PyQt4 and PySide) and additional custom QWidgets.")
      (license license:expat))))

(define-public python-lxml
  (hidden-package
    (package
      (name "python-lxml")
      (version "4.9.2")
      (source
       (origin
         (method url-fetch)
         (uri (pypi-uri "lxml" version))
         (sha256
           (base32 "0rsvhd03cv7fczd04xqf1idlnkvjy0hixx2p6a5k6w5cnypcym94"))))
      (build-system python-build-system)
      (arguments
       `(#:phases (modify-phases %standard-phases
                    (replace 'check
                      (lambda* (#:key tests? #:allow-other-keys)
                        (when tests?
                          (invoke "make" "test")))))))
      (inputs
       (list libxml2 libxslt))
      (home-page "https://lxml.de/")
      (synopsis "Python XML processing library")
      (description
       "The lxml XML toolkit is a Pythonic binding for the C libraries
  libxml2 and libxslt.")
      (license license:bsd-3))))
