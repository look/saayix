;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages text-editors)
  #:use-module (guix packages)
  #:export (helix))

(define helix
  (package
    (inherit (@@ (saayix packages rust helix) helix))))
