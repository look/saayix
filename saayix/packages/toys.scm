(define-module (saayix packages toys)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xdisorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system meson)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:export (wshowkeys))

(define wshowkeys
  (package
    (name "wshowkeys")
    (version "1.1")
    (source
      (origin
        (method git-fetch)
        (uri
          (git-reference
            (url "https://codeberg.org/look/wshowkeys")
            (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0alqlsscnh8dkyrlnv0j4dzfa50ph6jng3jhpm3qrq6asxw83b14"))))
    (build-system meson-build-system)
    (arguments
      (list #:phases
            #~(modify-phases %standard-phases
                (delete 'check))))
    (native-inputs (list pkg-config))
    (inputs
      (list cairo
            libinput
            pango
            eudev
            wayland
            wayland-protocols
            libxkbcommon))
    (home-page "https://git.sr.ht/~sircmpwn/wshowkeys")
    (synopsis "Displays keys being pressed on a Wayland session")
    (description "Displays keypresses on screen on supported Wayland compositors.")
    (license license:gpl3)))
