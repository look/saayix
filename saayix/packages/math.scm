;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages math)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages jupyter)
  #:use-module (gnu packages python)
  #:use-module (gnu packages check)
  #:use-module ((gnu packages qt) #:hide (python-qtpy))
  #:use-module (gnu packages video)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages image-processing)
  #:use-module ((gnu packages xml) #:hide (python-lxml))
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages rust-apps)
  #:use-module ((gnu packages python-xyz)
                #:hide (python-cloup
                        python-pillow))
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (saayix packages python-xyz)
  #:use-module (saayix utils)
  #:export (kalker
            manim
            manimgl
            manim-slides))

(define kalker
  (package
    (inherit (@@ (saayix packages rust kalker) kalker))))

(define manim-slides
  (package
    (name "manim-slides")
    (version "5.4.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "manim_slides" version))
       (sha256
        (base32 "0h0ncxg6396mjhvc01yikrn9xa3r5glslbqdhqic62hcn53mja87"))))
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f))
    (native-inputs (list python-setuptools))
    (propagated-inputs (list ffmpeg
                             opencv
                             python-av
                             python-beautifulsoup4
                             python-click
                             python-click-default-group
                             python-hatch-fancy-pypi-readme
                             python-jinja2
                             python-lxml
                             python-numpy-2
                             python-pillow
                             python-pptx
                             python-pydantic-2
                             python-pydantic-extra-types
                             python-qtpy
                             python-requests
                             python-rich-for-dooit
                             python-rtoml
                             python-tqdm))
    (home-page "https://github.com/jeertmans/manim-slides")
    (synopsis "Tool for live presentations using manim")
    (description "Tool for live presentations using either Manim (community
edition) or ManimGL. Manim Slides will automatically detect the one you are
using!")
    (license license:expat)))

(define manim
  (package
    (name "manim")
    (version "0.19.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ManimCommunity/manim")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0fwjd67r4p0bp3yvj5gp049f2r0pqwfpqalqsl0g9c8adky2j23r"))))
    (build-system pyproject-build-system)
    (arguments
      (list
        #:tests? #f
        #:phases
        #~(modify-phases %standard-phases
            (add-before 'build 'fix-ffmpeg-path
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (substitute* '("manim/constants.py")
                  (("FFMPEG_BIN: str = \"ffmpeg")
                   (string-append "FFMPEG_BIN: str = \""
                                  (search-input-file inputs "/bin/ffmpeg")))))))))
    (native-inputs
      (list python-setuptools
            python-wheel))
    (propagated-inputs
      (list python-click
            python-cloup
            python-av
            python-decorator
            python-importlib-metadata
            python-isosurfaces
            ; python-jupyterlab-server
            python-manimpango
            python-mapbox-earcut
            python-moderngl
            python-moderngl-window
            python-networkx
            python-notebook
            python-numpy-2
            python-pillow
            python-pycairo
            python-pydub
            python-pygments
            python-rich
            python-scipy
            python-screeninfo
            python-skia-pathops
            python-srt
            python-svgelements
            python-tqdm
            python-typing-extensions
            python-watchdog
            python-poetry-core))
    (home-page "https://www.manim.community/")
    (synopsis
     "Python animation engine for explanatory math videos")
    (description
     "Manim is a Python library for creating mathematical animations.  The
animations are written as Python code which is based on predefined objects.
You can make animations with maths formulas (LaTeX-based), simple shapes, 3D
objects, function graphs and more.")
    (license license:expat)))

(define manimgl
  (package
    (name "manimgl")
    (version "1.7.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/3b1b/manim")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "194ikdlxfzg5sbdgmgcyrmnb24si3wc0b0lj4wwh6k9gqii432md"))))
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f
                     #:phases
                     #~(modify-phases %standard-phases
                         (delete 'sanity-check))))
    (native-inputs (list python-setuptools python-wheel))
    (propagated-inputs
      (list python-colour
            python-ipython
            python-isosurfaces
            python-fonttools
            python-manimpango
            python-mapbox-earcut
            python-matplotlib
            python-moderngl
            python-moderngl-window
            python-numpy
            python-pillow
            python-pydub
            python-pygments
            python-pyopengl
            python-pyperclip
            python-pyrr
            python-pyyaml
            python-rich
            python-scipy
            python-screeninfo
            python-skia-pathops
            python-svgelements
            python-sympy
            python-tqdm
            python-typing-extensions
            python-validators))
    (home-page "https://github.com/3b1b/manim")
    (synopsis "Animation engine for explanatory math videos")
    (description "Manim is an engine for precise programmatic animations,
designed for creating explanatory math videos.")
    (license license:expat)))
