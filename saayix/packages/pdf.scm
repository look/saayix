(define-module (saayix packages pdf)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages libreoffice)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages texlive)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:export (sioyek/xwayland
            zaread))

(define sioyek/xwayland
  (package/inherit sioyek
    (name "sioyek-xwayland")
    (source #f)
    (build-system trivial-build-system)
    (inputs (list sioyek bash-minimal))
    (arguments
      `(#:modules ((guix build utils))
        #:builder
        ,#~(begin
             (use-modules (guix build utils))
             (let* ((out (assoc-ref %outputs "out"))
                    (in (assoc-ref %build-inputs "sioyek"))
                    (sioyek-bin (string-append out "/bin/sioyek"))
                    (sh (string-append #$bash-minimal "/bin/sh")))
               (copy-recursively in out)
               (chmod sioyek-bin #o755)
               (wrap-program sioyek-bin #:sh sh `("QT_QPA_PLATFORM" prefix ("xcb")))))))))

(define zaread
  (package
    (name "zaread")
    (version "1.1")
    (home-page "https://github.com/paoloap/zaread")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/paoloap/zaread.git")
                     (commit "c2d45e1bbaeb0a637b1747cc89b483f2c6d11cb3")))
              (sha256
                (base32 "1zw0la1w4271c87b5ps31c3z9kbkvcqwn9pxgvmxzlw007mdp1c3"))))
    (build-system copy-build-system)
    (inputs (list zathura zathura-pdf-poppler libreoffice))
    (arguments
      '(#:install-plan '(("zaread" "bin/"))))
    (synopsis "A (very) lightweight ebook and Office document reader")
    (description "zaread is a simple posix shell script that uses zathura pdf/epub viewer to act as a lightweight document/ebook readonly reader.")
    (license license:gpl3)))
