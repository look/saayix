(define-module (saayix packages binaries)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bootstrap)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gnuzilla)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages nss)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages video)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xorg)
  #:use-module (guix build utils)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:export (croc-bin
            jdtls-bin
            maven-bin
            zen-browser-bin))

(define zen-browser-bin
  (package
    (name "zen-browser-bin")
    (version "1.7.6b")
    (source
      (origin
        (method url-fetch)
        (uri (string-append
                "https://github.com/zen-browser/desktop/releases/download/"
                version
                "/zen.linux-x86_64.tar.xz"))
        (sha256
          (base32 "0vk4sas4gsplxdrqi15wpbs58hs5fq0nm0nk1ybij2rb7a66ay12"))))
    (build-system copy-build-system)
    (arguments
      (list #:install-plan
            #~'(("." "lib/zen"))
            #:phases
            #~(modify-phases %standard-phases
                (add-after 'install 'patch-elf
                  (lambda* (#:key inputs #:allow-other-keys)
                    (let ((ld.so (string-append #$(this-package-input "glibc")
                                                #$(glibc-dynamic-linker)))
                          (rpath (string-join
                                   (cons*
                                     (string-append #$output "/lib/zen")
                                     (string-append #$(this-package-input "gtk+") "/share")
                                     (map
                                       (lambda (input)
                                         (string-append (cdr input) "/lib"))
                                       inputs))
                                   ":")))
                      ;; Got this proc from hako's Rosenthal, thanks
                      (define (patch-elf file)
                        (format #t "Patching ~a ..." file)
                        (unless (string-contains file ".so")
                          (invoke "patchelf" "--set-interpreter" ld.so file))
                        (invoke "patchelf" "--set-rpath" rpath file)
                        (display " done\n"))
                      (for-each
                        (lambda (binary)
                          (patch-elf binary))
                        (append
                          (map
                            (lambda (binary)
                              (string-append #$output "/lib/zen/" binary))
                            '("glxtest" "updater" "vaapitest" "zen" "zen-bin" "pingsender"))
                          (find-files (string-append #$output "/lib/zen") ".*\\.so.*"))))))
                (add-after 'patch-elf 'install-bin
                  (lambda _
                    (let* ((zen (string-append #$output "/lib/zen/zen"))
                           (bin-zen (string-append #$output "/bin/zen")))
                      (mkdir (string-append #$output "/bin"))
                      (symlink zen bin-zen))))
                (add-after 'install-bin 'install-desktop
                  (lambda _
                    (let* ((share-applications (string-append #$output "/share/applications"))
                           (desktop (string-append share-applications "/zen.desktop")))
                      (mkdir-p share-applications)
                      (make-desktop-entry-file desktop
                        #:name "Zen Browser"
                        #:icon "zen"
                        #:type "Application"
                        #:comment #$(package-synopsis this-package)
                        #:exec (string-append #$output "/bin/zen %u")
                        #:keywords '("Internet" "WWW" "Browser" "Web" "Explorer")
                        #:categories '("Network" "Browser")
                        ; #:actions '("new-window" "new-private-window" "profilemanager")
                        #:mime-type '("text/html"
                                      "text/xml"
                                      "application/xhtml+xml"
                                      "x-scheme-handler/http"
                                      "x-scheme-handler/https"
                                      "application/x-xpinstall"
                                      "application/pdf"
                                      "application/json")
                        #:startup-w-m-class "zen-alpha")))))))
    (native-inputs (list patchelf))
    (inputs (list alsa-lib
                  at-spi2-core
                  cairo
                  dbus
                  dbus
                  eudev
                  fontconfig
                  freetype
                  gcc-toolchain
                  gdk-pixbuf
                  glib
                  glib
                  glibc
                  gtk+
                  libnotify
                  libva
                  libx11
                  libxcb
                  libxcomposite
                  libxcursor
                  libxdamage
                  libxext
                  libxfixes
                  libxi
                  libxrandr
                  libxrender
                  nspr
                  nss
                  pango
                  pulseaudio))
    (home-page "https://zen-browser.app/")
    (synopsis "Experience tranquillity while browsing the web without people
tracking you!")
    (description "Beautifully designed, privacy-focused, and packed with features.
We care about your experience, not your data.")
    (license (list license:mpl2.0))))

(define maven-bin
  (package
    (name "maven-bin")
    (version "3.9.9")
    (source
      (origin
        (method url-fetch)
        (uri
          (string-append "https://dlcdn.apache.org/maven/maven-3/"
                         version
                         "/binaries/apache-maven-"
                         version
                         "-bin.tar.gz"))
        (sha256
          (base32 "0rnpi2jyir6r8ql6bwai5d8ipshhs6rk1wzmh9iksw619xkxz73s"))))
    (build-system copy-build-system)
    (arguments
      (list #:install-plan
            #~'(("." ""))))
    (home-page "https://maven.apache.org/")
    (synopsis "Build system")
    (description "Apache Maven is a software project management and comprehension
tool.  Based on the concept of a project object model: builds, dependency
management, documentation creation, site publication, and distribution
publication are all controlled from the `pom.xml' declarative file.  Maven
can be extended by plugins to utilise a number of other development tools for
reporting or the build process.")
    (license license:asl2.0)))

(define jdtls-bin
  (package
    (name "jdtls-bin")
    (version "1.39.0")
    (source
      (origin
        (method url-fetch/tarbomb)
        (uri (string-append
               "https://web.archive.org/web/20240923111419/" ; Download from web archive
               "https://download.eclipse.org/jdtls/milestones/"
               version
               "/jdt-language-server-"
               version
               "-"
               "202408291433"
               ".tar.gz"))
        (sha256
          (base32 "0aa7sdxljzcbkxbn69lz8p38k65hx9k44izdy9rygrkli7qdhiph"))))
    (build-system copy-build-system)
    (arguments
      (list #:install-plan
            #~'(("." ""))))
    ; (propagated-inputs (list python openjdk))
    (synopsis "Java language server")
    (description "The Eclipse JDT Language Server is a Java language specific implementation of
the Language Server Protocol and can be used with any editor that supports the
protocol, to offer good support for the Java Language.")
    (home-page "https://github.com/eclipse/eclipse.jdt.ls")
    (license license:expat)))

(define croc-bin
  (package
    (name "croc-bin")
    (version "10.2.1")
    (source
      (origin
        (method url-fetch/tarbomb)
        (uri (string-append
               "https://github.com/schollz/croc/releases/download/v"
               version
               "/croc_v"
               version
               "_Linux-64bit.tar.gz"))
        (sha256
          (base32 "15f1jsxv1mg6p7zqzpjqd0ak4akwr5z8875zfbdcbmc1algqzhgb"))))
    (build-system copy-build-system)
    (arguments
      (list #:install-plan
            #~'(("croc" "bin/croc"))))
    (synopsis "Easily and securely send things from one computer to another")
    (description "croc is a tool that allows any two computers to simply and securely transfer files and folders.")
    (home-page "https://github.com/schollz/croc")
    (license license:expat)))

(define cinny-desktop-bin
  (package
    (name "cinny-desktop-bin")
    (version "3.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (string-append
              "https://github.com/cinnyapp/cinny-desktop/releases/download/"
              "v" version "/Cinny_desktop-x86_64.deb"))
        (sha256
          (base32 "17sh98h03lxkb1nh6xywbxrz7lbgxnr4bsfkfiszfy4gapq5zfsn"))))
    (build-system copy-build-system)
    (arguments
      (list #:install-plan
            #~'(("usr/bin" "bin" #:include ("cinny")))
            #:phases
            #~(modify-phases %standard-phases
                (add-after 'unpack 'unpack-deb
                  (lambda _
                    (invoke "ar" "-x" "Cinny_desktop-x86_64.deb")
                    (invoke "tar" "-xf" "data.tar.gz")))
                (add-after 'install 'patch-elf
                  (lambda _
                    (let ((ld.so (string-append #$(this-package-input "glibc")
                                                #$(glibc-dynamic-linker)))
                          (rpath (string-join
                                   (list
                                     (string-append #$(this-package-input "cairo") "/lib")
                                     (string-append #$(this-package-input "gcc-toolchain") "/lib")
                                     (string-append #$(this-package-input "gdk-pixbuf") "/lib")
                                     (string-append #$(this-package-input "glib") "/lib")
                                     (string-append #$(this-package-input "dbus") "/lib")
                                     (string-append #$(this-package-input "glib-networking") "/lib")
                                     (string-append #$(this-package-input "gtk+") "/lib")
                                     (string-append #$(this-package-input "libsoup-minimal") "/lib")
                                     (string-append #$(this-package-input "libappindicator") "/lib")
                                     (string-append #$(this-package-input "openssl") "/lib")
                                     (string-append #$(this-package-input "pango") "/lib")
                                     (string-append #$(this-package-input "webkitgtk") "/lib")
                                     (string-append #$(this-package-input "webkitgtk-with-libsoup2") "/lib"))
                                  ":"))
                          (cinny (string-append #$output "/bin/cinny")))
                      (invoke "patchelf" cinny "--set-interpreter" ld.so)
                      (invoke "patchelf" cinny "--set-rpath" rpath))))
                (add-after 'patch-elf 'install-extra
                  (lambda* (#:key outputs #:allow-other-keys)
                           (let* ((out (assoc-ref outputs "out"))
                                  (share (string-append out "/share"))
                                  (applications (string-append share "/applications"))
                                  (source-share "usr/share")
                                  (source-applications (string-append source-share "/applications"))
                                  (source-cinny-desktop (string-append source-applications "/cinny.desktop")))
                             (install-file source-cinny-desktop applications)))))))
    (native-inputs (list patchelf))
    (inputs (list cairo
                  gcc-toolchain
                  gdk-pixbuf
                  glib
                  dbus
                  glib-networking
                  libappindicator
                  glibc
                  gtk+
                  libsoup-minimal-2
                  openssl-1.1
                  pango
                  webkitgtk
                  webkitgtk-with-libsoup2))
    (home-page "https://github.com/cinnyapp/cinny-desktop")
    (synopsis "Yet another matrix client for desktop")
    (description "Cinny is a matrix client focusing primarily on simple, elegant and secure interface. The desktop app is made with Tauri.")
    (license (list license:agpl3))))
