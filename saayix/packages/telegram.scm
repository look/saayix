(define-module (saayix packages telegram)
               #:use-module (guix build-system trivial)
               #:use-module (gnu packages telegram)
               #:use-module (guix packages))

(define-public telegram-desktop/xwayland
  (package
    (inherit telegram-desktop)
    (name "telegram-desktop-xwayland")
    (source #f)
    (build-system trivial-build-system)
    (inputs (list telegram-desktop))
    (arguments
      `(#:modules ((guix build utils))
        #:builder
          (begin
            (use-modules (guix build utils))
            (let* ((out (assoc-ref %outputs "out"))
                   (in (assoc-ref %build-inputs "telegram-desktop"))
                   (telegram-desktop-bin (string-append out "/bin/telegram-desktop")))
              (copy-recursively in out)
              (chmod telegram-desktop-bin #o755)
              (wrap-program telegram-desktop-bin
                            `("QT_QPA_PLATFORM" prefix ("xcb")))))))))
