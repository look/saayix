(define-module (saayix packages minecraft)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages java)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (saayix packages)
  #:export (prismlauncher
            glfw-wayland-minecraft))

;; Adapted from https://gitlab.com/guix-gaming-channels/games/-/blob/86fde8472dae038a59024252fdf4a1a88370ff84/games/packages/minecraft.scm
(define prismlauncher
  (package
    (name "prismlauncher")
    (version "9.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/PrismLauncher/PrismLauncher")
               (recursive? #t)
               (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32 "16ra1cb8rc00cd2si3k7qmw8db3g0pf6n15aar60dh7kp9ig8jwb"))))
    (build-system cmake-build-system)
    (arguments
      `(#:phases
        (modify-phases %standard-phases
          (add-after 'install 'patch-paths
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (let* ((out            (assoc-ref outputs "out"))
                     (bin            (string-append out "/bin/prismlauncher"))
                     (xrandr         (assoc-ref inputs "xrandr"))
                     (qtwayland      (assoc-ref inputs "qtwayland")))
                (wrap-program bin
                  `("PATH" ":" prefix (,(string-append xrandr "/bin")))
                  `("QT_PLUGIN_PATH" ":" prefix (,(string-append
                                                   qtwayland "/lib/qt5/plugins")))
                  `("LD_LIBRARY_PATH" ":" prefix
                    (,@(map (lambda (dep)
                              (string-append (assoc-ref inputs dep)
                                             "/lib"))
                            '("libx11" "libxext" "libxcursor"
                              "libxrandr" "libxxf86vm" "pulseaudio" "mesa"
                              "wayland" "libxkbcommon")))))
                #t))))))
    (native-inputs
      (list extra-cmake-modules))
    (inputs
      (list bash-minimal ; for wrap-program
            zlib
            qtbase
            qt5compat
            qtwayland
            qtnetworkauth
            xrandr
            wayland
            libx11
            libxcursor
            libxext
            libxkbcommon
            libxrandr
            libxxf86vm
            pulseaudio
            `(,openjdk17 "jdk")
            mesa))
    (home-page "https://prismlauncher.org/")
    (synopsis "Free, open source launcher for Minecraft")
    (description "Allows you to have multiple, separate instances of Minecraft
(each with their own mods, texture packs, saves, etc), and helps you manage them
and their associated options with a simple interface.")
    (license
      (list license:gpl3          ; PolyMC, launcher
            license:expat         ; MinGW runtime, lionshead, tomlc99
            license:lgpl3         ; Qt 5/6
            license:lgpl3+        ; libnbt++
            license:lgpl2.1+      ; rainbow (KGuiAddons)
            license:isc           ; Hoedown
            license:silofl1.1     ; Material Design Icons
            license:lgpl2.1       ; Quazip
            license:public-domain ; xz-minidec, murmur2, xz-embedded
            license:bsd-3         ; ColumnResizer, O2 (Katabasis fork),
            license:asl2.0))))        ; classparser, systeminfo

;; Adapted from https://github.com/Admicos/minecraft-wayland/issues/18
(define glfw-wayland-minecraft
  (package
    (name "glfw-wayland-minecraft")
    (version "3.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
              "https://github.com/glfw/glfw/archive/"
              "dc557ecf38a42b0b93898a7aef69f6dc48bf0e57" ;; commit
              ".tar.gz"))
       (sha256
         (base32 "0hymis39h032hsffdhcrm2x2c2ac63kn4rw8g4m1gmhwmvvxnz11"))
       (patches
        (search-patches
          ;; https://github.com/BoyOrigin/glfw-wayland
          "0001-Key-Modifiers-Fix.patch"
          "0002-Fix-duplicate-pointer-scroll-events.patch"
          "0003-Implement-glfwSetCursorPosWayland.patch"
          "0004-Fix-Window-size-on-unset-fullscreen.patch"
          "0005-Avoid-error-on-startup.patch"))))
    (build-system cmake-build-system)
    (arguments
      '(#:tests? #f ;; no test target
        #:configure-flags '("-DBUILD_SHARED_LIBS=1"
                            "-DBUILD_SHARED_LIBS=1"
                            "-DGLFW_BUILD_EXAMPLES=0"
                            "-DGLFW_BUILD_TESTS=0"
                            "-DGLFW_BUILD_DOCS=0"
                            "-DGLFW_BUILD_X11=0"
                            "-DGLFW_USE_LIBDECOR=ON"))) ;; libdecoration
    (native-inputs
      (list pkg-config
            doxygen
            unzip
            mesa
            extra-cmake-modules
            wayland-protocols
            libxrandr
            libxi
            libxinerama
            libxcursor
            libdecor))
    (inputs
      (list wayland
            libxkbcommon))
    (propagated-inputs
      (list libx11
            libxxf86vm))
    (home-page "https://www.glfw.org")
    (synopsis "OpenGL application development library")
    (description "GLFW is a library for OpenGL, OpenGL ES and Vulkan development for
desktop computers.  It provides a simple API for creating windows, contexts and
surfaces, receiving input and events.")
    (license license:zlib)))
