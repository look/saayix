(define-module (saayix packages hyprland)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages xdisorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:export (hyprpaper
            hyprlock))

(define hyprlock
  (package
    (name "hyprlock")
    (version "0.6.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
              (url "https://github.com/hyprwm/hyprlock")
              (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
         (base32 "12y7b505lfdp1m6wjgf030ffr7nj864rzhzzch221xvschv672ry"))))
    (build-system cmake-build-system)
    (arguments
      `(#:tests? #f)) ;no test
    (native-inputs (list gcc-14 pkg-config))
    (inputs
      (list hyprgraphics
            hyprlang
            hyprutils
            hyprwayland-scanner
            libglvnd
            libjpeg-turbo
            libwebp
            libxkbcommon
            linux-pam
            libdrm
            mesa
            pango
            sdbus-c++
            wayland
            wayland-protocols))
    (home-page "https://hyprland.org/")
    (synopsis "Hyprland's screen locking utility")
    (description "Hyprland's simple, yet multi-threaded and GPU-accelerated
screen locking utility.")
    (license license:bsd-3)))

(define hyprpaper
  (package
    (name "hyprpaper")
    (version "0.7.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/hyprwm/hyprpaper")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "151r6s04yy3digl3g6gs49xx41yv4xldmbnqr87gp5nz705hjsd6"))))
    (build-system cmake-build-system)
    (arguments
      `(#:tests? #f)) ; No tests
    (native-inputs (list gcc-14 pkg-config))
    (inputs
      (list hyprgraphics
            hyprlang
            hyprutils
            hyprwayland-scanner
            libglvnd
            pango
            wayland
            wayland-protocols))
    (home-page "https://hyprland.org/")
    (synopsis "Wallpaper utility for Hyprland")
    (description "Hyprpaper is a blazing fast wallpaper utility for Hyprland
with the ability to dynamically change wallpapers through sockets.  It will work
on all wlroots-based compositors, though.")
    (license license:bsd-3)))
