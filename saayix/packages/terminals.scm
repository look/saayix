;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages terminals)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages image)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system zig)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:export (ghostty))

(define ghostty
  (package
    (name "ghostty")
    (version "1.1.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ghostty-org/ghostty")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1vysqjv4n6bk84v38d86fhj0k6ckdd677nsfbm3kybg5fz3ax60x"))))
    (build-system zig-build-system)
    (arguments
      (list #:tests? #f
            #:install-source? #f
            #:zig-release-type "fast"
            #:zig-build-flags
            #~(list "-Dcpu=baseline"
                    "--prefix"
                    "."
                    "--system"
                    (string-append (getenv "TMPDIR") "/source/zig-cache")
                    "--search-prefix"
                    #$(this-package-input "libadwaita")
                    "--search-prefix"
                    (string-append (getenv "TMPDIR") "/source/bzip2")
                    "-fno-sys=oniguruma")
            #:modules
           '((guix build zig-build-system)
             (guix build utils)
             (ice-9 match))
            #:phases
            #~(modify-phases %standard-phases
                (replace 'unpack-dependencies
                  (lambda _
                    (mkdir-p "bzip2/lib")
                    (symlink
                      (string-append #$(this-package-input "bzip2")
                                     "/lib/libbz2.so")
                      "bzip2/lib/libbzip2.so")))
                (add-after 'unpack 'unpack-zig
                  (lambda _
                    (for-each
                      (match-lambda
                        ((dst src)
                         (let* ((dest (string-append "zig-cache/" dst)))
                           (mkdir-p dest)
                           (cond
                             ((string-contains src ".tar.gz")
                              (invoke "tar" "-xf" src "-C" dest "--strip-components=1"))
                             ((string-contains src ".tar.zst")
                              (invoke "tar" "-xf" src "-C" dest "--strip-components=1"))
                             (else (copy-recursively src dest))))))
                      `(("12200df4ebeaed45de26cb2c9f3b6f3746d8013b604e035dae658f86f586c8c91d2f"
                         #$(origin
                             (method git-fetch)
                             (uri (git-reference
                                    (url "https://github.com/rockorager/libvaxis")
                                    (commit "6d729a2dc3b934818dffe06d2ba3ce02841ed74b")))
                             (file-name "vaxis")
                             (sha256
                               (base32 "157lcj4xc1xavy0k04wmbklc3j28mmnyy3fpah2h9qdjy3sznmvw"))))
                        ("1220dd654ef941fc76fd96f9ec6adadf83f69b9887a0d3f4ee5ac0a1a3e11be35cf5"
                         #$(origin
                             (method git-fetch)
                             (uri (git-reference
                                    (url "https://github.com/zigimg/zigimg")
                                    (commit "3a667bdb3d7f0955a5a51c8468eac83210c1439e")))
                             (file-name "zigimg")
                             (sha256
                               (base32 "1ajcy7gawy3i98gpj07m9m6lqd089jl1kgskj4i0wypjgmhggdx0"))))
                        ("12201f0d542e7541cf492a001d4d0d0155c92f58212fbcb0d224e95edeba06b5416a"
                         #$(origin
                             (method git-fetch)
                             (uri (git-reference
                                    (url "https://github.com/vancluever/z2d")
                                    (commit "4638bb02a9dc41cc2fb811f092811f6a951c752a")))
                             (file-name "z2d")
                             (sha256
                               (base32 "10w264fcqkvqcr2cnhks7vypadvzh2cxp0dm189a893paagrg5b2"))))
                        ("1220edc3b8d8bedbb50555947987e5e8e2f93871ca3c8e8d4cc8f1377c15b5dd35e8"
                         #$(origin
                             (method git-fetch)
                             (uri (git-reference
                                    (url "https://github.com/natecraddock/zf")
                                    (commit "ed99ca18b02dda052e20ba467e90b623c04690dd")))
                             (file-name "zf")
                             (sha256
                               (base32 "1ig2bb0xib31ab9w86svcc52idknx65qvcc8n5cndq2r8an0v95p"))))
                        ("1220c72c1697dd9008461ead702997a15d8a1c5810247f02e7983b9f74c6c6e4c087"
                         #$(origin
                             (method git-fetch)
                             (uri (git-reference
                                    (url "https://github.com/rockorager/libvaxis")
                                    (commit "dc0a228a5544988d4a920cfb40be9cd28db41423")))
                             (file-name "vaxis")
                             (sha256
                               (base32 "1cnc82d5rm1iy1nzr6b75w4ffx70r5qi17m9dsjm5xy0xa67hqs1"))))
                        ("12207e0851c12acdeee0991e893e0132fc87bb763969a585dc16ecca33e88334c566"
                         #$(origin
                             (method git-fetch)
                             (uri (git-reference
                                    (url "https://github.com/KDE/plasma-wayland-protocols")
                                    (commit "db525e8f9da548cffa2ac77618dd0fbe7f511b86")))
                             (file-name "plasma_wayland_protocols")
                             (sha256
                               (base32 "0ghf43gmdzy35dkc358pdkm5f1lfl509nz12g1k7r64zwggnyr49"))))
                        ("12207ff340169c7d40c570b4b6a97db614fe47e0d83b5801a932dcd44917424c8806"
                         #$(origin
                             (method git-fetch)
                             (uri (git-reference
                                    (url "https://github.com/make-github-pseudonymous-again/pixels")
                                    (commit "d843c2714d32e15b48b8d7eeb480295af537f877")))
                             (file-name "pixels")
                             (sha256
                               (base32 "0hb0sjkn9dfk90y14ak66x7j0wh47l749aw077rggn5798xhcxli"))))
                        ("1220ebf88622c4d502dc59e71347e4d28c47e033f11b59aff774ae5787565c40999c"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mitchellh/libxev/archive/31eed4e337fed7b0149319e5cdbb62b848c24fbd.tar.gz")
                             (sha256
                               (base32 "17rhp763yb4dffggxjisyb9kvpn4isil864va5y8dd7jsk8gswsl"))))
                        ("12206ed982e709e565d536ce930701a8c07edfd2cfdce428683f3f2a601d37696a62"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mitchellh/mach-glfw/archive/37c2995f31abcf7e8378fba68ddcf4a3faa02de0.tar.gz")
                             (sha256
                               (base32 "1nghcw6hj2dnwvl7sfhpxz0kwjk4nvqmq5cfb63z1wqjcnywh58y"))))
                        ("1220736fa4ba211162c7a0e46cc8fe04d95921927688bff64ab5da7420d098a7272d"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mitchellh/glfw/archive/b552c6ec47326b94015feddb36058ea567b87159.tar.gz")
                             (sha256
                               (base32 "0da2qj9jlsnwcarc768s08zyx2fnlkvmrvhwaxm23dr6wh05bq11"))))
                        ("12202adbfecdad671d585c9a5bfcbd5cdf821726779430047742ce1bf94ad67d19cb"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mitchellh/xcode-frameworks/archive/69801c154c39d7ae6129ea1ba8fe1afe00585fc8.tar.gz")
                             (sha256
                               (base32 "0fnrswx0ahc59cz37qqm9z11021w5h7y3zpxkc4vbrqbrbcwizwq"))))
                        ("122004bfd4c519dadfb8e6281a42fc34fd1aa15aea654ea8a492839046f9894fa2cf"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mitchellh/vulkan-headers/archive/04c8a0389d5a0236a96312988017cd4ce27d8041.tar.gz")
                             (sha256
                               (base32 "0mgqhrvavvrm3nm4yihyi4slr125mma9z16f72j5n730wx3fpv1b"))))
                        ("1220b3164434d2ec9db146a40bf3a30f490590d68fa8529776a3138074f0da2c11ca"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mitchellh/wayland-headers/archive/5f991515a29f994d87b908115a2ab0b899474bd1.tar.gz")
                             (sha256
                               (base32 "00jx7w2vz1bdddpx239hkg76jqjjwmflsm8px5nlcam7k0nsan5q"))))
                        ("122089c326186c84aa2fd034b16abc38f3ebf4862d9ae106dc1847ac44f557b36465"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mitchellh/x11-headers/archive/2ffbd62d82ff73ec929dd8de802bc95effa0ef88.tar.gz")
                             (sha256
                               (base32 "1rrvyidabdrkvax5s0mz0i122gc0z5fvl12w6wcf7z6qcip7c58j"))))
                        ("122055beff332830a391e9895c044d33b15ea21063779557024b46169fb1984c6e40"
                         #$(origin
                             (method url-fetch)
                             (uri "https://codeberg.org/atman/zg/archive/v0.13.2.tar.gz")
                             (sha256
                               (base32 "1mnc261y9dc2z69pbv65dx62zr3bnpykkmb2c64x5ayqnr7n27yv"))))
                        ("1220e17e64ef0ef561b3e4b9f3a96a2494285f2ec31c097721bf8c8677ec4415c634"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mitchellh/zig-objc/archive/9b8ba849b0f58fe207ecd6ab7c147af55b17556e.tar.gz")
                             (sha256
                               (base32 "1m0z955wwyiwg5rnlsw0f3a2pllpxxgzygf8msrppnwk3mpciq8z"))))
                        ("12205a66d423259567764fa0fc60c82be35365c21aeb76c5a7dc99698401f4f6fefc"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mitchellh/zig-js/archive/d0b8b0a57c52fbc89f9d9fecba75ca29da7dd7d1.tar.gz")
                             (sha256
                               (base32 "0gaqqfb125pj62c05z2cpzih0gcm3482cfln50d41xf2aq4mw8vz"))))
                        ("12207831bce7d4abce57b5a98e8f3635811cfefd160bca022eb91fe905d36a02cf25"
                         #$(origin
                             (method url-fetch)
                             (uri "https://deps.files.ghostty.org/ziglyph-b89d43d1e3fb01b6074bc1f7fc980324b04d26a5.tar.gz")
                             (sha256
                               (base32 "1ngkyc81gqqfkgccxx4hj4w4kb3xk0ng7z73bwihbwbdw7rvvivj"))))
                        ("12209ca054cb1919fa276e328967f10b253f7537c4136eb48f3332b0f7cf661cad38"
                         #$(origin
                             (method url-fetch)
                             (uri "https://deps.files.ghostty.org/zig-wayland-fbfe3b4ac0b472a27b1f1a67405436c58cbee12d.tar.gz")
                             (sha256
                               (base32 "1mwm6lqz8n9x10pk1fyz594d3fhgkx92p4xb49c69z4avar35l26"))))
                        ("12202cdac858abc52413a6c6711d5026d2d3c8e13f95ca2c327eade0736298bb021f"
                         #$(origin
                             (method url-fetch)
                             (uri "https://deps.files.ghostty.org/wayland-9cb3d7aa9dc995ffafdbdef7ab86a949d0fb0e7d.tar.gz")
                             (sha256
                               (base32 "03f574n5w0y6glr7lf8xjd71844qh8kxxb1s3zjpfxj3ivb92hga"))))
                        ("12201a57c6ce0001aa034fa80fba3e1cd2253c560a45748f4f4dd21ff23b491cddef"
                         #$(origin
                             (method url-fetch)
                             (uri "https://deps.files.ghostty.org/wayland-protocols-258d8f88f2c8c25a830c6316f87d23ce1a0f12d9.tar.gz")
                             (sha256
                               (base32 "1y1h0pmql53x6ixbsycgkzxlxsxqs9fkps754c7ycx8vx3fwmvaw"))))
                        ("12203d2647e5daf36a9c85b969e03f422540786ce9ea624eb4c26d204fe1f46218f3"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/mbadolato/iTerm2-Color-Schemes/archive/db227d159adc265818f2e898da0f70ef8d7b580e.tar.gz")
                             (sha256
                               (base32 "1ifg8xyklvk91wb0nix3wkkmwsv02av123l0bw7xkg79i99zn9r3"))))
                        ("1220bc6b9daceaf7c8c60f3c3998058045ba0c5c5f48ae255ff97776d9cd8bfc6402"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/ocornut/imgui/archive/e391fe2e66eb1c96b1624ae8444dc64c23146ef4.tar.gz")
                             (sha256
                               (base32 "0q3qxycyl0z64mxf5j24c0g0yhif3mi7qf183rwan4fg0hgd0px0"))))
                        ("1220b81f6ecfb3fd222f76cf9106fecfa6554ab07ec7fdc4124b9bb063ae2adf969d"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/freetype/freetype/archive/refs/tags/VER-2-13-2.tar.gz")
                             (sha256
                               (base32 "035r5bypzapa1x7za7lpvpkz58fxynz4anqzbk8705hmspsh2wj2"))))
                        ("1220aa013f0c83da3fb64ea6d327f9173fa008d10e28bc9349eac3463457723b1c66"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/glennrp/libpng/archive/refs/tags/v1.6.43.tar.gz")
                             (sha256
                               (base32 "0fm0y7543w2gx5sz3zg9i46x1am51c77a554r0zqwpphdjs9bk7y"))))
                        ("1220fed0c74e1019b3ee29edae2051788b080cd96e90d56836eea857b0b966742efb"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/madler/zlib/archive/refs/tags/v1.3.1.tar.gz")
                             (sha256
                               (base32 "0p6h2i9ajdp46lckdpibfqy4vz5nh5r22bqq96mp41k0ydiqis0p"))))
                        ("12201149afb3326c56c05bb0a577f54f76ac20deece63aa2f5cd6ff31a4fa4fcb3b7"
                         #$(origin
                             (method url-fetch)
                             (uri "https://deps.files.ghostty.org/fontconfig-2.14.2.tar.gz")
                             (sha256
                               (base32 "0mcarq6v9k7k9a8is23vq9as0niv0hbagwdabknaq6472n9dv8iv"))))
                        ("122032442d95c3b428ae8e526017fad881e7dc78eab4d558e9a58a80bfbd65a64f7d"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/GNOME/libxml2/archive/refs/tags/v2.11.5.tar.gz")
                             (sha256
                               (base32 "05b2kbccbkb5pkizwx2s170lcqvaj7iqjr5injsl5sry5sg0aa3c"))))
                        ("1220b8588f106c996af10249bfa092c6fb2f35fbacb1505ef477a0b04a7dd1063122"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/harfbuzz/harfbuzz/archive/refs/tags/8.4.0.tar.gz")
                             (sha256
                               (base32 "0mk574w6i1zb4454iawvvhsns0hkh80px36fs559819vh64s074z"))))
                        ("12205c83b8311a24b1d5ae6d21640df04f4b0726e314337c043cde1432758cbe165b"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/google/highway/archive/refs/tags/1.1.0.tar.gz")
                             (sha256
                               (base32 "1jgpcqc3s6z8qjy7ghy4089p9wp3fd188w7ck05yg25m752qnjim"))))
                        ("1220c15e72eadd0d9085a8af134904d9a0f5dfcbed5f606ad60edc60ebeccd9706bb"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/kkos/oniguruma/archive/refs/tags/v6.9.9.tar.gz")
                             (sha256
                               (base32 "187jk4fxdkzc0wrcx4kdy4v6p1snwmv8r97i1d68yi3q5qha26h0"))))
                        ("1220446be831adcca918167647c06c7b825849fa3fba5f22da394667974537a9c77e"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/getsentry/sentry-native/archive/refs/tags/0.7.8.tar.gz")
                             (sha256
                               (base32 "1pqqqcin8nw398rvn187dfqlab4vikdssiry14qqs6nnr1y4kiia"))))
                        ("12207fd37bb8251919c112dcdd8f616a491857b34a451f7e4486490077206dc2a1ea"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/getsentry/breakpad/archive/b99f444ba5f6b98cac261cbb391d8766b34a5918.tar.gz")
                             (sha256
                               (base32 "1nbadlml3r982bz1wyp17w33hngzkb07f47nrrk0g68s7na9ijkc"))))
                        ("1220d4d18426ca72fc2b7e56ce47273149815501d0d2395c2a98c726b31ba931e641"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/nemtrif/utfcpp/archive/refs/tags/v4.0.5.tar.gz")
                             (sha256
                               (base32 "1ksrdf7dy4csazhddi64xahks8jzf4r8phgkjg9hfxp722iniipz"))))
                        ("122037b39d577ec2db3fd7b2130e7b69ef6cc1807d68607a7c232c958315d381b5cd"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/google/wuffs/archive/refs/tags/v0.4.0-alpha.9.tar.gz")
                             (sha256
                               (base32 "04qwpr8c4xjla4skwb1fpvkjc0c611qhbhz9xp3c9rlnpq5d4k4y"))))
                        ("12201278a1a05c0ce0b6eb6026c65cd3e9247aa041b1c260324bf29cee559dd23ba1"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/KhronosGroup/glslang/archive/refs/tags/14.2.0.tar.gz")
                             (sha256
                               (base32 "1dcpm70fhxk07vk37f5l0hb9gxfv6pjgbqskk8dfbcwwa2xyv8hl"))))
                        ("1220fb3b5586e8be67bc3feb34cbe749cf42a60d628d2953632c2f8141302748c8da"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/KhronosGroup/SPIRV-Cross/archive/476f384eb7d9e48613c45179e502a15ab95b6b49.tar.gz")
                             (sha256
                               (base32 "1qspcsx56v0mddarb6f05i748wsl2ln3d8863ydsczsyqk7nyaxm"))))
                        ("12208d70ee791d7ef7e16e1c3c9c1127b57f1ed066a24f87d57fc9f730c5dc394b9d"
                         #$(origin
                             (method url-fetch)
                             (uri "https://github.com/ianprime0509/zig-gobject/releases/download/v0.2.2/bindings-gnome47.tar.zst")
                             (sha256
                               (base32 "1hmmc5dyhnv5gm2ilz5g1cpakdhc8hw7dxdc7k89qvgzvf87nksi")))))))))))
    (native-inputs
      (list `(,glib "bin")
            ncurses
            pandoc
            pkg-config
            tar))
            ; zstd))
    (inputs
      (list bzip2
            expat
            fontconfig
            freetype
            glslang
            harfbuzz
            libadwaita
            libglvnd
            libpng
            libx11
            libxcursor
            libxi
            libxrandr
            zlib))
    (native-search-paths
      ;; FIXME: This should only be located in 'ncurses'.  Nonetheless it is
      ;; provided for usability reasons.  See <https://bugs.gnu.org/22138>.
      (list (search-path-specification
              (variable "TERMINFO_DIRS")
              (files '("share/terminfo")))))
    (synopsis "Fast, native, feature-rich terminal emulator pushing modern features")
    (description "Ghostty is a terminal emulator that differentiates itself by
being fast, feature-rich, and native. While there are many excellent terminal
emulators available, they all force you to choose between speed, features, or
native UIs. Ghostty provides all three.")
    (home-page "https://ghostty.org")
    (license license:expat)))
