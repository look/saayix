;;; Copyright © 2024 Murilo <murilo@disroot.org>
;;;
;;; This file is NOT part of GNU Guix.

(define-module (saayix packages file-managers)
  #:use-module (guix packages)
  #:export (yazi))

(define yazi
  (package
    (inherit (@@ (saayix packages rust yazi) yazi))))
