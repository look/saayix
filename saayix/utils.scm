(define-module (saayix utils)
  #:use-module (gnu packages python-build)
  #:use-module (gnu system setuid)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix transformations)
  #:use-module (guix utils)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 textual-ports)
  #:export (fix-pyproject-package
            disable-tests))

(define (fix-pyproject-package p)
  (package
    (inherit p)
    (native-inputs
      (modify-inputs (package-native-inputs p)
        (prepend python-setuptools)
        (prepend python-wheel)))))

(define (disable-tests p)
  (package
    (inherit p)
    (arguments
      (substitute-keyword-arguments (package-arguments p)
        ((#:phases phases)
         #~(modify-phases #$phases
             (delete 'check)))))))
